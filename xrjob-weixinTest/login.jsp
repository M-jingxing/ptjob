<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">

	<meta charset="utf-8">
	<meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<title>企业后台管理登录</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	*{
		margin:0;
		padding:0;
	}
	html{
		height:100%;

	}
	body{
		background-color:#fff;
		font-size:14px;
		color:#222;
		width:100%;
		height:100%;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}

	/*主体内容*/
	:-moz-placeholder{
		color:#999; /*mozilla ff 4-18*/
	}
	::-moz-placeholder{
		color:#999; /*mozilla ff 19+*/
	}
	:-ms-input-placeholder{
		color:#999; /*ie 10+*/
	}
	::-webkit-input-placeholder{
		color:#999; /*webkit browsers*/
	}
	input[type=text],select,input[type=password]{
		border:none;
		outline:none;
		background-color:#fff;
		color:#999;
		appearance: none;
		-moz-appearance:none;
		-webkit-appearance: none;
		padding-left:1px;
		text-align:left;
		height:26px;
		width:100%;
	}
	select::-ms-expand{
		display:none;
	}
	.logo{
		text-align:center;
		padding-top:70px;
		padding-bottom:34px;
	}
	.logo img{
		width:80px;
		height:80px;
	}
	.loginContent{
		margin-left:50px;
		margin-right:50px;
	}
	.userName,.psd{
		border-bottom:1px solid #999;
		padding-left:28px;
	}
	.userName{
		background:url(img/user-yellow.png) no-repeat;
		background-size:24px 24px;
		padding-bottom:5px;
		margin-bottom:18px;
	}
	.psd{
		background:url(img/password-yellow.png) no-repeat;
		background-size:24px 24px;
		padding-bottom:5px;
		margin-bottom:28px;
	}
	.forgetPsd a{
		border-bottom:1px solid #999;
		color:#999;
		font-size:12px;
		padding-bottom:2px;
	}
	button{
		width:100%;
		padding-top:10px;
		padding-bottom:10px;
		border:none;
	}
	.btn1{
		background-color:#ff9137;
		color:#fff;
		border-radius: 6px;
		outline:none;
		margin-bottom:16px;
		font-size:15px;
	}
	.btn2{
		background-color:#fff;
		color:#ff9137;
		outline:none;
		font-size:15px;
	}
	.contact{
		text-align:center;
		color:#999;
		font-size:12px;
	}
	.error{
		font-size:12px;
		color:#ff9037;
	}
	
	/*导航栏*/
	nav a:focus,nav a:hover{
		color:#fff;
	}
	.backTo{
		float:left;
		color:#fff;
		font-size:16px;
		vertical-align:middle;
		line-height:35px;
	}
	a.home{
		float:right;
		color:#fff;
		font-size:16px;
		line-height:35px;
	}
	nav{
		width:100%;
		background-color:#ff9137;
		color:#fff;
		font-size:18px;
		text-align:center;
		padding:0 10px;
		height:35px;
		line-height:35px;
	}
	nav .txt{
		text-align:center;
		font-size:16px;
		padding-right:37px;
	}
	nav img{
		width:20px;
		height:20px;
		vertical-align:middle;
	}
	
</style>
</head>
<body>
	<c:if test="${apploginflag==11 }">
		<nav>
			<a href="http://xiaoranjob.com/xrjob-weixin/xrjobPages/login.jsp" class="backTo"><span class="glyphicon glyphicon-chevron-left"></span>
				返回</a><span class="txt">登录</span>
			<a href="http://xiaoranjob.com/xrjob-weixin/index.jsp" class="home"><span class="glyphicon glyphicon-home"></span></a>
		</nav>
	</c:if>
	<div class="content">
		<!--导航栏-->
		
		<!--主体内容-->
		<div class="logo">
			<img src="img/logo_business.png">
		</div>
		<div class="loginContent">
			<form id="frmLogin" autocomplete="off" method="post" action="user/login.do">
				<div class="userName">
					<input type="text" id="loginN" name="loginName" placeholder="请输入用户名">
				</div>
				<div class="psd">
					<input type="password" id="pwd" name="password" placeholder="请输入密码">
				</div>
				<div>
					<button class="btn1" type="button" onclick="login()">登录</button>
				</div>
			</form>
			<div>
				<button class="btn2" onclick="window.location='backstagePages/reg.jsp'" type="button">注册</button>
			</div>
		</div>
	</div>
	<div class="contact">
		<p>忘记账号、忘记密码等账号相关问题请联系我们</p>
		<p>联系电话：13777374547</p>
	</div>
<input type="hidden" value="${sessionScope.companyId}"
					id="companyId">
<script src="js/jquery-2.2.3.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script>
	//	密码验证：在6位数以上
	$(function(){
		$("#frmLogin").validate({
			rules:{
				userName:{
					required:true
				},
				psd:{
					required:true,
					minlength:6
				}
			},
			messages:{
				userName:{
					required:"请输入账号"
				},
				psd:{
					required:"请输入密码",
					minlength:"请输入6位以上密码"
				}
			}
		});

		/*让最下方的联系我们这个div距离下底边26px*/
		var wContact=$('body').height()-$('.content').outerHeight();
		var w=wContact-80;
		$('.contact').css('margin-top',w);

	});
</script>
<script type="text/javascript">
	function login() {
		var loginName = $("#loginN").val();
		var password = $("#pwd").val();

		$.post("user/login.do", {
			loginName : loginName,
			password : password
		}, function(data) {
			
			if (data.res) {
                location = "backstagePages/menu.jsp";
			} else {
				
			     if(data.status==2){
			     	alert(data.msg);
			     }else if (data.status == 0) {
					if(data.returnValue != null
							&& data.returnValue != ""){	
					
						if(data.upPic){
						 window.location.href = "backstagePages/regSuccess.jsp";
						}else{
							if(confirm("您的企业信息不完善，点击确定继续完善资料")){
								window.location.href="company/toPerfectCompany.do";
								}
						}					
					}else	if (confirm("您还未添加任何企业信息，是否要继续添加企业信息？")) {
							location = "backstagePages/addCompany.jsp";
						}
				} else if (data.status == 3) {
					if(data.returnValue != null 
							&& data.returnValue != ""){
						if (confirm(data.msg)){
							window.location.href = "company/showCompany.do";
						}
					}else if (confirm("您还未添加任何企业信息，是否要继续添加企业信息？")) {
							location = "backstagePages/addCompany.jsp";
						}
					
				} else {
					alert(data.msg);
				}

			}
		}, 'JSON');
	}
</script>
</body>	
</html>