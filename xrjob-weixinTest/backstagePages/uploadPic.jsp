<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
	<meta charset="utf-8">
	<meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<title>营业执照上传</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	*{
		margin:0;
		padding:0;
	}
	html{
		height:100%;
	}
	body{
		background-color:#f5f5f5;
		width:100%;
		height:100%;
	}
	/*导航栏*/
	nav{
		width:100%;
		height:44px;
		background-color:#ff9137;
		color:#fff;
		line-height:44px;
		font-size:18px;
		text-align:center;
		padding-left:10px;
		padding-right:30px;
	}
	nav a{
		float:left;
	}
	nav img{
		width:20px;
		height:20px;
		vertical-align:middle;
	}
	/*主体内容*/
	form{
		height:80%;
	}
	table{
		width:100%;
		height:100%;
		table-layout:fixed;
	}
	td{
		text-align:center;
		vertical-align: middle;
	}
	tr:first-child{
		height:70%;
	}
	.btn1{
		background:#fff url(img/add.png) no-repeat 50% 50%;
		background-size:60px 60px;
		width:200px;
		height:200px;
		border:0;
		color:#ccc;
		padding-top:100px;
	}
	.store{
		padding:10px;
	}
	.store .btn{
		width:100%;
		padding-top:6px;
		padding-bottom:6px;
		border:0;
		background-color:#ff9137;
		border-radius: 4px;
		-moz-border-radius: 4px;
		-webkit-border-radius: 4px;
		color:#fff;
		margin-top:10px;
		margin-bottom:20px;
	}

</style>
</head>
<body>
 <form action="fileForm" style="display:none;" enctype="multipart/form-data" id="fileForm">
		<input type="file" id="file" name="imgFile" onchange="uploadImg()">
	</form>
	<!--导航栏-->
	<nav>
		<a href="javascript:history.go(-1)"><img src="img/back.png"></a>
		营业执照上传
	</nav>
	<!--主体内容-->
	<form>
		<table>
			<tr>
				<td>
					<button class="btn1" id="uploadBtn" type="button">
						<span id="msg">点击上传营业执照</span>
					</button>
				</td>
			</tr>
			<tr>
				<td>
					<div class="store">
						<button class="btn" type="button" onclick="updatePic()">保存</button>
					</div>
				</td>
			</tr>
		</table>
		<input type="hidden" id="companyLicense" name="companyLicense" >
	</form>

<script src="js/jquery-2.2.3.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#uploadBtn").on("click", function(){
				$("#file").click();
			});
		});	
		
		function uploadImg(){

			 var formData = new FormData($("#fileForm")[0]);
			if($("#file").val() == "") return false;
             
			$.ajax({  
	            url : "user/uploadPic.do",  
	            type : 'POST',  
	            data : formData,  
	            dataType:'json',
	            processData : false,  
	            contentType : false,  
	            success : function(data) {  
	                if(data.res){
	                	$("#uploadBtn").css({
	                		"background-image":"url(../xrjob-img/"+data.msg+")",
	                		"background-size":"200px 200px"
	                	});
	               
	                	$("#companyLicense").val(data.msg);
	                	$("#msg").remove();
	                	
	                }else{
	                	parent.$.messager.alert('提示', data.msg, 'error');
	                }
	            },  
	            error : function(responseStr) {  
	            	parent.$.messager.alert('提示', "网络异常", 'error');
	            }  
	        }); 
		}	

	function updatePic(){
		var companyLicense=$("#companyLicense").val();
		if(companyLicense==null||companyLicense==''){
		   alert("请上传图片");
		   return;
		}
		$.post('company/addCompanyLic.do',{companyLicense:companyLicense},function(data){
			if(data.res){
				alert(data.msg);
				location="backstagePages/regSuccess.jsp";
			}else{
				
			}
			
			
		},'json');
	}	
		

</script>
</body>	
</html>