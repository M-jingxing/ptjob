<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
	<meta charset="utf-8">	 
	<meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<title>资料审核</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	*{
		margin:0;
		padding:0;
	}
	html{
		height:100%;
	}
	body{
		background-color:#fff;
		color:#666;
		width:100%;
		height:100%;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	/*导航栏*/
	nav{
		width:100%;
		height:44px;
		background-color:#ff9137;
		color:#fff;
		line-height:44px;
		font-size:18px;
		text-align:center;
		padding-left:10px;
		padding-right:30px;
	}
	nav a{
		float:left;
	}
	nav img{
		width:20px;
		height:20px;
		vertical-align:middle;
	}
	/*主体内容*/
	table{
		width:100%;
		height:80%;
	}
	td{
		text-align:center;
		vertical-align: middle;
	}
	tr:first-child{
		width:100%;
		height:50%;
	}
	tr:first-child td{
		vertical-align: bottom;
	}
	img{
		width:45%;
		height:auto;
	}
	.txt{
		font-size:15px;
		text-align:center;
		line-height:30px;
		letter-spacing:2px;
	}

</style>
</head>
<body>
	<!--导航栏-->
	<nav>
		<a href="login.jsp"><img src="img/back.png"></a>
		注册成功
	</nav>
	<!--主体内容-->
	<table>
		<tr>
			<td>
				<img src="img/Data-audit.png">
			</td>
		</tr>
		<tr>
			<td>
				<div class="txt">
					恭喜您，已成功注册该企业<br>
					如果问题请联系客服<br>
					联系电话:13777374547
				</div>
			</td>
		</tr>
	</table>




<script src="js/jquery-2.2.3.min.js"></script>
<script src="js/jquery.validate.js"></script>

</body>	
</html>