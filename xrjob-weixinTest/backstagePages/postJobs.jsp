<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
	<meta charset="utf-8">
	<meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<title>发布岗位</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	*{
		margin:0;
		padding:0;
	}
	body{
		background-color:#f5f5f5;
		font-size:14px;
		font-weight:400;
		color:#222;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	/*导航栏*/
	nav{
		width:100%;
		height:44px;
		background-color:#ff9137;
		color:#fff;
		line-height:44px;
		font-size:18px;
		text-align:center;
		padding-left:10px;
		padding-right:30px;
	}
	nav a{
		float:left;
	}
	nav img{
		width:20px;
		height:20px;
		vertical-align:middle;
	}
	/*主体内容*/
    .hint{
        color:#ff9137;
        padding-left:16px;
        margin-bottom:0;
        line-height:25px;
    }
	:-moz-placeholder{
		color:#ccc; /*mozilla ff 4-18*/
	}
	::-moz-placeholder{
		color:#ccc; /*mozilla ff 19+*/
	}
	:-ms-input-placeholder{
		color:#ccc; /*ie 10+*/
	}
	::-webkit-input-placeholder{
		color:#ccc; /*webkit browsers*/
	}
	input[type=text],select,input[type=password]{
		border:none;
		outline:none;
		background-color:#fff;
		appearance: none;
		-moz-appearance:none;
		-webkit-appearance: none;
		text-align:left;
		height:26px;
		display:inline;
	}
	select::-ms-expand{
		display:none;
	}
    /*input{
        color:#222;
        padding-left:3px;
    }*/
    select{
        color:#ccc;
    }
	.content{
		margin:0 10px 10px 10px;
		background-color:#fff;
	}
	.rowContent{
		background-color:#fff;
		padding:15px 6px;
		border-bottom:1px solid #ccc;
	}
	.rowContent:last-child{
		border:0;
	}
	label{
		font-weight:400;
		margin-bottom:0;
		color:#666;
	}
    .star{
        color:#f00;
    }
	.vLine{
		color:#ccc;
		padding-left:7px;
		padding-right:7px;
	}
	.star1{
		visibility:hidden;
	}
    .rowContent .arrowRight{
        width:8px;
        height:16px;
        float:right;
        vertical-align:middle;
        margin-top:4px;
    }
    .rowContent .locationImg{
        width:8px;
        height:16px;
        margin-bottom:4px;
    }
	textarea{
		border:0;
		outline:none;
		resize:none;
		vertical-align:top;
		background-color:#fff;
	}
	.rowContent p{
		display:inline-block;
		vertical-align:top;
		margin-bottom:0;
		color:#999;
		word-break:break-all;
	}
	.store{
		padding:10px;
	}
	.store .btn{
		width:100%;
		padding-top:6px;
		padding-bottom:6px;
		border:0;
		background-color:#ff9137;
		border-radius: 4px;
		-moz-border-radius: 4px;
		-webkit-border-radius: 4px;
		color:#fff;
		margin-top:10px;
		margin-bottom:20px;
	}
    #jAddress{
        padding-left:79px;
		padding-top:15px;
    }
    .locationLabel{
        float:left;
    }
    .locationDiv{
        float:left;
    }
    .locationDiv .selectHalf2{
        padding-left:10px;
    }
	.select1{
		padding-right:3px;
	}
	.select2{
		padding-left:3px;
		padding-right:3px;

	}
	.select3{
		padding-left:3px;

	}

</style>
</head>
<body>
	<!--导航栏-->
	<nav>
		<a href="backstagePages/menu.jsp"><img src="img/back.png"></a>
		发布岗位
	</nav>
	<!--主体内容-->
    <p class="hint">带<span class="star">*</span>为必填项</p>
	<form id="form">
		<div class="content">
			<div class="rowContent">
				<label>岗位名称</label><span class="star">*</span><span class="vLine">|</span><input type="text" id="jName" name="jobName" placeholder="请填写岗位名称">
			</div>
			<div class="rowContent">
				<label>招聘人数</label><span class="star">*</span><span class="vLine">|</span><input type="text" id="jNumber" name="jobNumber" placeholder="请填写招聘人数">
			</div>
			<div class="rowContent">
				<label>工作年限</label><span class="star1">*</span><span class="vLine vLine1">|</span><select class="select" name="jobWorkYear">
                <option value="0">不限</option>
                <option value="1">1年以下</option>
                <option value="2">1-2年</option>
                <option value="3">3-5年</option>
                <option value="4">5-10年</option>
                <option value="5">10年以上</option>
                </select>
                <img class="arrowRight" src="img/arrow-right.png">
			</div>
            <div class="rowContent">
                <label>学历要求</label><span class="star">*</span><span class="vLine">|</span><select class="select" id="jDegree" name="jobDegree">
                <option value="0">不限</option>
                <option value="1">高中以下</option>
                <option value="2">高中</option>
                <option value="3">中专/技校</option>
                <option value="4">大专</option>
                <option value="5">本科</option>
                <option value="6">硕士以上</option>
                </select>
                <img class="arrowRight" src="img/arrow-right.png">
            </div>
            <div class="rowContent">
                <label>薪资水平</label><span class="star">*</span><span class="vLine">|</span><select class="select" id="jSalary" name="jobSalaryId">
                <option value="0">薪资面议</option>
                <option value="1">1500元以下/月</option>
                <option value="2">1500-2500元/月</option>
                <option value="3">2500-3500元/月</option>
                <option value="4">3500-5000元/月</option>
                <option value="5">5000-7000元/月</option>
                <option value="6">7000-10000元/月</option>
                <option value="7">10000-15000元/月</option>
                <option value="8">15000元以上/月</option>
                </select>
                <img class="arrowRight" src="img/arrow-right.png">
            </div>
            <div class="rowContent">
                <label>工作类型</label><span class="star">*</span><span class="vLine">|</span><select class="select" name="jobType">
                <option value="1">全职</option>
                <option value="2">兼职</option>
                <option value="3">应届生</option>
            </select>
                <img class="arrowRight" src="img/arrow-right.png">
            </div>
            <div class="rowContent">
                <label>工作行业</label><span class="star">*</span><span class="vLine">|</span><select class="select1" name="topId" id="topId" onchange="changeTop()">
				<option value="">全部</option>
	    				<c:forEach items="${tops}" var="top">
	    					<option value="${top.typeId}">
	    						${top.typeName}
	    					</option>
	    			 </c:forEach>
			</select><img class="locationImg" src="img/arrow-right.png"><select class="select2" name="secondId" id="secondId" onchange="changeSecond()">
			<option value="">全部</option>
			</select><img class="locationImg" src="img/arrow-right.png"><select class="select3" name="jobIndustry" id="jobIndustry">
				 <option value="">全部</option>
			</select>
                <img class="arrowRight" src="img/arrow-right.png">
            </div>
            <div class="rowContent">
                <div class="locationLabel">
                <label>工作地点</label><span class="star">*</span><span class="vLine">|</span></div><div class="locationDiv"><select class="selectHalf1" name="cityId" id="cityId" onchange="changeCity()">
                    <option value="">请选择城市</option>
                <c:forEach items="${cityList}" var="city">
	    					<option value="${city.cityId}">
	    						${city.cityName}
	    					</option>
	    		</c:forEach>
                </select>
                <img class="locationImg" src="img/arrow-right.png"><select class="selectHalf2" name="jobAreaId" id="jobAreaId">
                       <option value="">请选择区域</option>
            </select>
            </div>
                <img class="arrowRight" src="img/arrow-right.png">
                <textarea rows="1" id="jAddress" name="jobAddress" onkeyup="MaxMe(this)" placeholder="请填写详细地址"></textarea>
            </div>
            <div class="rowContent">
                <label>岗位描述</label><span class="star">*</span><span class="vLine">|</span><textarea rows="3"  id="jDescription" name="jobDescribe" maxlength="150" placeholder="请填写岗位描述"></textarea>
            </div>
            <div class="rowContent">
                <label>岗位要求</label><span class="star">*</span><span class="vLine">|</span><textarea rows="3"   id="jRequire" name="jobRequire" maxlength="150" placeholder="请填写岗位要求" ></textarea>
            </div>
        </div>
		<div class="store">
			<button class="btn" type="button" id="submit_btn">确认发布</button>
		</div>
	</form>



<script src="js/jquery-2.2.3.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script>
	$(function(){
		/*p和textarea的内容随页面宽度改变而改变*/
		var w=$('.rowContent').width()-95;
		$('p').width(w);
		$('input,textarea,.select,.locationDiv').width(w);
        $('.selectHalf1').width(w/2-19);
        $('.selectHalf2').width(w/2-4);
		$('.select1').width(w/3-8);
		$('.select2').width(w/3-8);
		$('.select3').width(w/3-8);
        $('select').focus(function(){
            $(this).css('color','#222');
        });
	})


</script>
<script type="text/javascript">

	function changeCity(){
		var cityId = $("#cityId").val();
		var area = $("#jobAreaId");
		area.html("<option value=\"\">请选择</option>");
		if(cityId == ""){
			return;
		}
		
		$.post("address/getAreaList.do", {cityId:cityId}, function(data){
			if(data == null || data == ""){
				return;
			}
			
			for(var o in data){
				area.append("<option value=\"" + data[o].areaId + "\">" + data[o].areaName + "</option>");
			}
		}, 'json');
	}
	
	function changeTop(){
		var topId = $("#topId").val();
		var second = $("#secondId");
		var third = $("#jobIndustry");
		  if(topId==""){
	    	   second.html("<option value=\"\">全部</option>");
	   		   third.html("<option value=\"\">全部</option>");
			}else{
				second.html("");
				third.html("");
			}
		if(topId == ""){
			return;
		}
		
		$.post("type/changeTopType.do", {topId:topId}, function(data){
			if(data == null || data == ""){
				return;
			}
			second.append("<option value=\"" + data[0].typeId + "\"selected=selected>" + data[0].typeName + "</option>");
			second.val(data[0].typeId);
			for(var o in data){
				if(o!=0){
				second.append("<option value=\"" + data[o].typeId + "\">" + data[o].typeName + "</option>");
				}
			}
			second.trigger("onchange");
		}, 'json');
	}
	
	function changeSecond(){
		var secondId = $("#secondId").val();
		var third = $("#jobIndustry");
		if(secondId==""){
			third.html("<option value=\"\">全部</option>");
		}else{
			third.html("");
		}
		if(secondId == ""){
			return;
		}
		
		$.post("type/changeSecondType.do", {secondId:secondId}, function(data){
			if(data == null || data == ""){
				return;
			}
			
			third.append("<option value=\"" + data[0].typeId + "\"selected=selected>" + data[0].typeName + "</option>");
			for(var o in data){
				if(o!=0){
				third.append("<option value=\"" + data[o].typeId + "\">" + data[o].typeName + "</option>");
				}
			}
		}, 'json');
	}
	
	function checkForm(){
	   if($("#jName").val()==""){
	     	alert( '岗位名称不能为空！');
			return false;
	   }
	   if($("#jNumber").val()==""){
	        alert( '招聘人数不能为空');
			
			return false;
	   }
	   if(/[^0-9]/.test($("#jNumber").val()) && $("#jNumber").val() != "若干"){
			alert( '招聘人数只能为数字或者若干！');
		
			return false;
		}
	  if($("#jDegree").val()==""){
	       alert('学历要求不能为空');
			return false;
	  }
	  if ($("#jSalary").val()=="") {
		  alert('薪资要求不能为空');
			return false;
	 }
	  if($("#cityId").val()==""||$("#jobAreaId").val()==""||$("#jAddress").val()==""){
	        alert('工作地点不能为空');
			return false;
	  }
	  if($("#topId").val()==""||$("#secondId").val()==""||$("#jobIndustry").val()==""){
	      alert( '工作行业不能为空');
			return false;
	  }
	  if($("#jDescription").val()==""){
	      alert( '岗位描述不能为空');
			return false;
	  } 
	  if($("#jRequire").val()==""){
	      alert( '岗位要求不能为空');
			return false;
	  } 
	  
	   
	   return true;
	}
	
	$(document).ready(function(){
   $("#submit_btn").on("click", function(){
          if(!checkForm())
				return;
		$.post("job/addJob.do",$("#form").serialize()
		, function(data) {
			if (data.res) {
			    alert(data.msg)
				location = "backstagePages/menu.jsp";
			} else {
				alert(data.msg);
			}
		}, 'JSON');
		});
    });
	
	</script>
</body>	
</html>