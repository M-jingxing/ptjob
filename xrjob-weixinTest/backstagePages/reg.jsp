<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
	<meta charset="utf-8" />
	<meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<title>企业后台管理注册</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	body{
		background-color:#fff;
		font-size:14px;
		color:#222;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	/*导航栏*/
	nav{
		width:100%;
		height:44px;
		background-color:#ff9137;
		color:#fff;
		line-height:44px;
		font-size:18px;
		text-align:center;
		padding-left:10px;
		padding-right:30px;
	}
	nav a{
		float:left;
	}
	nav img{
		width:20px;
		height:20px;
		vertical-align:middle;
	}
	:-moz-placeholder{
		color:#999; /*mozilla ff 4-18*/
	}
	::-moz-placeholder{
		color:#999; /*mozilla ff 19+*/
	}
	:-ms-input-placeholder{
		color:#999; /*ie 10+*/
	}
	::-webkit-input-placeholder{
		color:#999; /*webkit browsers*/
	}
	input[type=text],select,input[type=password],input[type=tel]{
		border:none;
		outline:none;
		background-color:#fff;
		color:#999;
		appearance: none;
		-moz-appearance:none;
		-webkit-appearance: none;
		padding-left:1px;
		text-align:left;
		height:26px;
		width:100%;
	}
	select::-ms-expand{
		display:none;
	}
	.logo{
		text-align:center;
		padding-top:24px;
		padding-bottom:22px;
	}
	.logo img{
		width:80px;
		height:80px;
	}
	.loginContent{
		margin-left:50px;
		margin-right:50px;
	}
	.userName,.psd,.mobile{
		border-bottom:1px solid #999;
		padding-left:28px;
	}
	.userName{
		background:url(img/user-yellow.png) no-repeat;
		background-size:24px 24px;
		padding-bottom:5px;
		margin-bottom:25px;
	}
	.psd{
		background:url(img/password-yellow.png) no-repeat;
		background-size:24px 24px;
		padding-bottom:5px;
		margin-bottom:25px;
	}
	.mobile{
		background:url(img/mobilephone.png) no-repeat;
		background-size:24px 24px;
		padding-bottom:5px;
		margin-bottom:25px;
	}
	button{
		width:100%;
		padding-top:10px;
		padding-bottom:10px;
		border:none;
	}
	.vCode{
		border-bottom:1px solid #999;
		margin-bottom:35px;
	}
	#checkCode{
		width:45%;
		text-align:left;
		padding:0;
	}
	.btn3{
		background-color:#fff;
		border:none;
		color:#ff9037;
		outline:none;
		width:50%;
		padding-left:0;
		padding-right:0;
		padding-bottom:2px;
		text-align:right;
	}
	.vLine{
		color:#ccc;
		font-size:18px;
	}
	.btn1{
		background-color:#ff9137;
		color:#fff;
		border-radius: 6px;
		outline:none;
		margin-bottom:16px;
		font-size:15px;
	}
	.btn2{
		background-color:#fff;
		color:#ff9137;
		outline:none;
		font-size:15px;
		margin-bottom:30px;
	}
	.error{
		font-size:12px;
		color:#ff9037;
	}
</style>
</head>
<body>
	<!--导航栏-->
	<nav>
		<a href="login.jsp"><img src="img/back.png"></a>
		注册
	</nav>
	<!--主体内容-->
	<div class="logo">
		<img src="img/logo_business.png">
	</div>
	<div class="loginContent">
		<form id="frmInc" autocomplete="off"  method="post" action="user/register.do">
			<div class="userName">
				<input type="text" name="loginName" id="loginName" placeholder="请输入6位以上用户名">
			</div>
			<div class="psd">
					<input type="password" name="password" id="password" placeholder="请输入6位以上密码">
			</div>
			<div class="psd">
				<input type="password" name="surePassword"  id="surePassword" placeholder="请确认密码">
			</div>
			<div class="mobile">
				<input type="tel" name="userMobile" id="userMobile" placeholder="请输入手机号">
			</div>
			<div>
				<button class="btn1" type="button" onclick="reg()">注册</button>
			</div>
		</form>
		<div>
			<a href="login.jsp"><button class="btn2">已有账号,立即登录</button></a>
		</div>
	</div>
<script src="js/jquery-2.2.3.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script>
	/*表单验证
	 * 用户名不能为空
	 * 密码不能为空，在6位数以上
	 * 确认密码不能为空，和密码equal
	 * 手机号不能为空，正确的手机号
	 * 验证码不能为空
	 * */
	$(function(){
		$("#frmInc").validate({
			rules:{
				loginName:{
					required:true,
					minlength:6
				},
				password:{
					required:true,
					minlength:6
				},
				surePassword:{
					required:true,
					equalTo:'#password'
				},
				userMobile:{
					required:true,
					mobile:true
				},
				checkCode:{
					required:true
				}
			},
			messages:{
				loginName:{
					required:'请输入账号',
			        minlength:'请输入6位以上用户名'
				},
				password:{
					required:'请输入密码',
					minlength:'请输入6位以上密码'
				},
				surePassword:{
					required:'请输入确认密码',
					equalTo:'请与密码保持一致'
				},
				userMobile:{
					required:'请输入手机号',
					mobile:'请输入正确的手机号'
				},
				checkCode:{
					required:'请输入验证码'
				}
			},
			/*修改错误信息的提示位置*/
			errorPlacement: function(error, element) {
				if ( element.is(":radio") )
					error.appendTo ( element.parent() );
				else if ( element.is(":checkbox") )
					error.appendTo ( element.parent() );
				else if ( element.is("input[name=vCode]") )
					error.appendTo ( element.parent() );
				else
					error.insertAfter(element);
				$('<br/>').appendTo(element.parent());
				error.appendTo(element.parent());
			}
		});
	});

function checkForm(){
	    var loginName=$("#loginName").val();
	    var password=$("#password").val();
	    var surePassword=$("#surePassword").val();
	    var userMobile=$("#userMobile").val()
	    
	    if(loginName.length<6){
	    	alert("请输入6位以上用户名");
	    	return false;
	    }
	    if(password.length<6){
	    	alert("请输入6位以上密码");
	    	return false;
	    }
	    if(password!=surePassword){
	    	alert("密码与确认密码不一致");
	    	return false;
	    }
	    if(userMobile==''){
	    	alert("手机号码不能为空");
	    	return false;
	    }

	    return true;
}
	
	
function reg(){
    var loginName=$("#loginName").val();
    var password=$("#password").val();
    var surePassword=$("#surePassword").val();
    var userMobile=$("#userMobile").val()
    
    if(checkForm()){
  $.post("user/register.do",{
					loginName:loginName,
					password:password,
					surePassword:surePassword,
					userMobile:userMobile,
				},function(data) {
					if(data.res){
						location="backstagePages/addCompany.jsp";
					}
					else{
                             alert(data.msg);
                             
					}
				
				}, 'JSON');		
    }
}

</script>
</body>	
</html>