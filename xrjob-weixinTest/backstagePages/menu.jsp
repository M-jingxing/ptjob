<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
	<meta charset="utf-8">
	<meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<title>企业后台管理主页</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	html{
		height:100%;
	}
	body{
		background-color:#fff;
		font-size:14px;
		color:#222;
		height:100%;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
		color:#222;
	}
	a{
		color:#222;
	}
	/*导航栏*/
	nav{
		width:100%;
		height:44px;
		background-color:#ff9137;
		color:#fff;
		line-height:44px;
		font-size:18px;
		text-align:center;
		padding-left:65px;
		padding-right:10px;
		position:relative;
	}
	button{
		background-color:#ff9137;
		width:55px;
		height:44px;
		outline:none;
		border:0;
		padding:0;
		float:right;
		color:#fff;
	}
	#loaded{
		display:none;
		position:relative;
		background:url(img/angle.png) no-repeat 50% 30px;
		background-size:12px 10px;
	}
	.loadedTxt{
		position:absolute;
		left:0;
		top:-5px;
	}
	#exit{
		display:none;
		width:55px;
		height:30px;
		position:absolute;
		right:10px;
		top:44px;
		background-color:#fff;
		color:#ff9137;
		border-radius: 4px;
		-moz-border-radius: 4px;
		-webkit-border-radius: 4px;
	}
	#exit span{
		position:absolute;
		left:0;
		right:0;
		top:-8px;
	}
	/*主体内容*/
	.banner{
		width:100%;
		height:140px;
		margin-bottom:15px;
	}
	table{
		width:100%;
	}
	td{
		text-align:center;
		vertical-align:middle;
		width:50%;
	}
	.menu{
		width:100%;
		margin-bottom:12px;
	}
	.menu a{
		width:100%;
	}
	.menu a img{
		max-width:80px;
		height:auto;
		margin-bottom:5px;	/*对图与字之间距离的设置*/
	}
	.menu a span{
		padding-top:10px;	/*对图与字之间的距离设置没有什么作用，是为了消除br对a的影响，使图与字之间的空间都能点，如果没有这项设置，会有一部分空间被br占领，鼠标点击不能跳转*/
	}
	.panel{
            border:0;
            margin-top:16px;
            margin-bottom:0;
            position:relative;
            padding-top:25px;
        }
        img.resumeImg{
            position:absolute;
            left:10px;
            top:-5px;
            width:80px;
            height:30px;
            z-index:77;
        }
        .line{
            margin:0 0 10px 0;
            border:0;
            border-bottom:1px solid #ff9137;
        }
	
</style>
</head>
<body>
	<!--导航栏-->
	<nav>
		企业管理后台
		<button id="load" onclick="location='login.jsp'">登录</button>
		<button id="loaded"><span class="loadedTxt">已登录</span></button>
		<button id="exit" onclick="exit()"><span>退出</span></button>
	</nav>
	<!--主体内容-->
	<div id="content">
		<img class="banner" src="img/banner_business.png">
			<table>
				<tr>
					<td><div class="menu">
						<a href="company/showCompany.do">
							<img src="img/Business-management.png"><br>
							<span>企业管理</span>
						</a>
					</div></td>
					<td><div class="menu">
						<a href="job/toAddJob.do">
							<img src="img/Post-job.png"><br>
							<span>发布岗位</span>
						</a>
					</div></td>
				</tr>
				<tr>
					<td>
						<div class="menu">
							<a href="job/queryJobPage.do">
								<img src="img/Post-management.png"><br>
								<span>岗位管理</span>
							</a>
						</div>
					</td>
					<td>
						<div class="menu">
							<a href="delivery/queryDelivery.do">
								<img src="img/Delivery-management.png"><br>
								<span>投递管理</span>
							</a>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="menu">
							<a href="resume/toTalentPool.do">
								<img src="img/Talent-pool.png"><br>
								<span>人才库</span>
							</a>
						</div>
					</td>
					<td>
						<div class="menu">
							<a href="company/toMessageAlert.do">
								<img src="img/Reminder-settings.png"><br>
								<span>提醒设置</span>
							</a>
						</div>
					</td>
				</tr>
			</table>
		</div>
		  <input type="hidden" id="userId" value="${sessionScope.userId }">
<script src="js/jquery-2.2.3.min.js"></script>
<script>
$(document).ready(function(){ 
	if (${isRead==null}) {
    	location="user/toMemu.do";
	}
})


	$(function(){
		var h=$('body').outerHeight()-210;
		$('table').height(h);
	/* 	 $('#load').click(function(){
			$(this).hide();
			$('#loaded').show();
		}); */
		/* $('#exit').click(function(){
		$(this).hide();
		$('#loaded').hide();
		$('#load').show();
	    }); */ 
		$('#loaded').click(function(){
			$('#exit').toggle();
		});
		$("#content").click(function(){
			$("#exit").hide();
		});	
		var userId=$("#userId").val();
		if(userId==null||userId==""){
			$('#load').show();
			$('#loaded').hide();
			$("#exit").hide();
		}else{
			$('#load').hide();
			$('#loaded').show();
		}

	})
	
	function  exit(){
		var userId=$("#userId").val();
		$.post("user/logout.do",{userId:userId},function(data){
			if(data.res){
				location.href='login.jsp';
// 				locatinon.href="http://xiaoranjob.com/xrjob-weixingzh/baseController/companyUserFromApp.do";
			}
		},"json")
		
	}
	
var h=$('body').outerHeight()-210;
$('table').height(h);
var $img=$('.menu a img');
if(h<310){
	$img.width('30%');
}else{
	$img.width('40%');
}


</script>
</body>	
</html>