<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
    <title>企业信息编辑</title>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta name="format-detection" content="telephone=no">
    <meta charset="utf-8" />
    <meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport">
    <!--CSS-->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">

<style>
	*{
		margin:0;
		padding:0;
	}
	body{
		background-color:#f5f5f5;
		font-size:14px;
		font-weight:400;
		color:#222;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	/*导航栏*/
	nav{
		width:100%;
		height:44px;
		background-color:#ff9137;
		color:#fff;
		line-height:44px;
		font-size:18px;
		text-align:center;
		padding-left:10px;
		padding-right:30px;
	}
	nav a{
		float:left;
	}
	nav img{
		width:20px;
		height:20px;
		vertical-align:middle;
	}
	/*主体内容*/
	input[type=text],select,input[type=password]{
		border:none;
		outline:none;
		background-color:#fff;
		appearance: none;
		-moz-appearance:none;
		-webkit-appearance: none;
		text-align:left;
		height:26px;
		display:inline;
		color:#222;
	}
	input{
		padding-left:3px;
	}
	select{
		color:#222;
	}
	select::-ms-expand{
		display:none;
	}
	.content{
		margin:10px;
		background-color:#fff;
	}
	.rowContent{
		background-color:#fff;
		padding:15px 6px;
		border-bottom:1px solid #ccc;
	}
	.rowContent:last-child{
		border:0;
	}
	label{
		font-weight:400;
		margin-bottom:0;
		color:#666;
	}
	label.contact{
		letter-spacing:7px;
	}
	.vLine{
		color:#999;
		padding-left:7px;
		padding-right:7px;
	}
	.vLine1{
		color:#999;
		padding-left:0px;
		padding-right:7px;
	}
	.rowContent .arrowRight{
		width:8px;
		height:16px;
		float:right;
		vertical-align:middle;
		margin-top:4px;
	}
	textarea{
		border:0;
		outline:none;
		resize:none;
		vertical-align:top;
		background-color:#fff;
		padding-left:4px;
	}
	.rowContent p{
		display:inline-block;
		vertical-align:top;
		margin-bottom:0;
		color:#222;
		word-break:break-all;
	}
	.store{
		padding:10px;
	}
	.store .btn{
		width:100%;
		padding-top:6px;
		padding-bottom:6px;
		border:0;
		background-color:#ff9137;
		border-radius: 4px;
		-moz-border-radius: 4px;
		-webkit-border-radius: 4px;
		color:#fff;
		margin-top:10px;
		margin-bottom:20px;
	}
	#uploadBtn{
		width:50px;
		border:1px solid #ff9137;
		background-color:#fff;
		outline:none;
	}
	#uploadBtn:focus{
		outline:none;
	}
	input[type=text]:disabled,select:disabled,input[type=password]:disabled,textarea:disabled{
		color:#999;
	}

</style>
</head>
<body>
	<!--导航栏-->
	<nav>
		<a href="backstagePages/menu.jsp"><img src="img/back.png"></a>
		企业管理
	</nav>

<form action="fileForm" style="display:none;" enctype="multipart/form-data" id="fileForm">
		<input type="file" id="file" name="imgFile" onchange="uploadImg()">
	</form>
<form class="form-horizontal" role="form" action="company/saveCompany.do" method="post" id="form">
   <div class="content">
    <div class="rowContent">
        <label>企业名称</label><span class="vLine">|</span><input type="text"  id="cName" name="companyName" 
                   value="${company.companyName}" disabled>
         <input type="hidden" id="cNameH" name="companyName" value="${company.companyName}"/>
    </div>
    <div class="rowContent">
        <label class="contact">联系人</label><span class="vLine1">|</span><input type="text"  id="cUser"  disabled name="companyContractPerson" 
                   value="${company.companyContractPerson}" request>
    </div>
    <div class="rowContent">
        <label>联系电话</label><span class="vLine">|</span><input type="text"  id="cTel" disabled name="companyContractMobile"  
                   value="${company.companyContractMobile}" pattern="(^(\d{3,4}-)?\d{7,8})$|(13[0-9]{9})" request>
    </div>
    <div class="rowContent">
        <label>企业地址</label><span class="vLine">|</span><input type="text" id="cLoc" name="companyAddress" value="${company.companyAddress }" disabled="disabled">
    </div>
    <div class="rowContent">
        <label>企业性质</label><span class="vLine">|</span><select  name="companyNature" disabled id="cNature">
                    <option  value="1" <c:if test="${company.companyNature == 1}">selected="selected"</c:if>>国有企业</option>
					<option  value="2" <c:if test="${company.companyNature == 2}">selected="selected"</c:if>>外资企业</option>
					<option  value="3" <c:if test="${company.companyNature == 3}">selected="selected"</c:if>>合资企业</option>
					<option  value="4" <c:if test="${company.companyNature == 4}">selected="selected"</c:if>>私营企业</option>
					<option  value="5" <c:if test="${company.companyNature == 5}">selected="selected"</c:if>>民营企业</option>
					<option  value="6" <c:if test="${company.companyNature == 6}">selected="selected"</c:if>>股份制企业</option>
					<option  value="7" <c:if test="${company.companyNature == 7}">selected="selected"</c:if>>集体企业</option>
					<option  value="8" <c:if test="${company.companyNature == 8}">selected="selected"</c:if>>社会团体</option>
					<option  value="9" <c:if test="${company.companyNature == 9}">selected="selected"</c:if>>分支机构</option>
	    </select>
	   <!--  <img class="arrowRight" src="img/arrow-right.png"> -->
    </div>
    <div class="rowContent">
        <label>企业规模</label><span class="vLine">|</span><select  name="companyScale" disabled id="cScale">
                 <option value="1" <c:if test="${company.companyScale == 1}">selected="selected"</c:if>>15人以下</option>
				 <option value="2" <c:if test="${company.companyScale == 2}">selected="selected"</c:if>>15-50人</option>
			     <option value="3" <c:if test="${company.companyScale == 3}">selected="selected"</c:if>>50-200人</option>
				 <option value="4" <c:if test="${company.companyScale == 4}">selected="selected"</c:if>>200-500人</option>
				 <option value="5" <c:if test="${company.companyScale == 5}">selected="selected"</c:if>>500-2000人</option>
				 <option value="6" <c:if test="${company.companyScale == 6}">selected="selected"</c:if>>2000人以上</option>
		   </select>
		   <!-- <img class="arrowRight" src="img/arrow-right.png"> -->
    </div>
    <div class="rowContent">
        <label>企业描述</label><span class="vLine">|</span><textarea  id="cDes" name="companyDescribe" rows="5" request disabled>${company.companyDescribe}</textarea>
    </div>
    <div class="rowContent">
        <label>营业执照</label><span class="vLine">|</span><img style="width:150px;height:150px;" src="../xrjob-img/${company.companyLicense }" id="licenseImg">
        <input style="padding-top:8px;padding-bottom:6px;" type="button" value="上传" id="uploadBtn" >
            <input type="hidden" id="companyLicense" name="companyLicense" value="${company.companyLicense }">
    </div>
   </div>
  <div class="store">
			<button class="btn" type="button" id="edit_btn">编辑</button>
		</div>
      <input type="hidden" value="${userStatus}" id="userStatus"> 
</form>
</body>
<script src="js/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="js/jquery.validate.js"></script>
<!-- <script type="text/javascript">
 /*点击开关，通知提醒的显示与隐藏*/
    function changeDisplay() {
        if ($('#btnShow').attr('value')== '添加附件') {
            $('#IdShow').show();
            $('#btnShow').attr('value', '取消添加附件');
        }
        else {
            $('#IdShow').hide();
            $('#btnShow').attr('value', '添加附件');
        }
    }

    /*select的改变，相应的input内容也改变*/
    function sel_div(t){   /*如果有'请选择'，且'请选择'不显示内容，则i=0改为i=1.在.display='none'之后再判断if(t.value!='null'){document.getElementById(t.value).style.display='block';}*/
        for(var i=0;i<t.length;i++) {
            document.getElementById(t.options[i].value).style.display='none';
            document.getElementById(t.value).style.display='block';
        }
        /*p中提示内容的显示与隐藏*/
        if(t.value!='d_3'){
            document.getElementById('p_1').style.display='block';
            document.getElementById('p_2').style.display='none';
        }else{
            document.getElementById('p_2').style.display='block';
            document.getElementById('p_1').style.display='none';
        }
    }
    /*手机开关处表单验证*/
    $(function(){
        $('#cManagerForm').validate({
            rules:{
                tel:{
                    tel:true
                },
                email: {
                    email:true
                }


            }
        });
    });



</script> -->
<script type="text/javascript">

$(function(){
	/* p和textarea的内容随页面宽度改变而改变*/
	var w=$('.rowContent').width()-90;
	$('p,input[type=text],textarea,select').width(w); 
	

})


function checkForm(){

	  if($("#cName").val()==null||$("#cName").val()==''){
		   alert("请输入企业名称");
	       return false;
	   }
	   if($("#cUser").val()==null||$("#cUser").val()==''){
		   alert("请输入企业联系人");
	       return false;
	   }
	   if($("#cTel").val()==null||$("#cTel").val()==''){
		   alert("请输入企业联系电话");
	       return false;
	   }
	   if (!(/^1[3458][0-9]\d{4,8}$|^((\+86)|(86))?(13)\d{9}$|(^(\d{3,4}-)?\d{7,8})$|^400[016789]\d{6}$/.test($("#cTel").val()))) {
	       alert("请输入正确的联系方式")
	      return false;
		}
	   
	   if($("#cLoc").val()==null||$("#cLoc").val()==''){
		   alert("请输入企业地址");
	       return false;
	   }
	   if($("#cNature").val()==null||$("#cNature").val()==''){
		   alert("请输入企业性质");
	       return false;
	   }
	   if($("#cScale").val()==null||$("#cScale").val()==''){
		   alert("请输入企业规模");
	       return false;
	   }
	   if($("#companyLicense").val()==null||$("#companyLicense").val()==''){
		   alert("请上传营业执照");
	       return false;
	   }
	   
	   
	return true;
}
  var flag=0;
  $(document).ready(function(){
       $("#uploadBtn").hide(); 
    $("#edit_btn").on("click",function(){
    if(flag==0){
       
      if($("#userStatus").val()==3){
        $("#cUser").removeAttr("disabled");
        $("#cTel").removeAttr("disabled");
        $("#cLoc").removeAttr("disabled");
        $("#cDes").removeAttr("disabled");
        $("#cName").removeAttr("disabled");
        $("#cNature").removeAttr("disabled");
        $("#cScale").removeAttr("disabled");
        $("#uploadBtn").show();
        $("#cNameH").attr("name","");
       $("#uploadBtn").val("上传"); 
        $("#edit_btn").html("保存");
      }else{
       $("#cUser").removeAttr("disabled");
       $("#cTel").removeAttr("disabled");
       $("#cLoc").removeAttr("disabled");
       $("#cDes").removeAttr("disabled");
       $("#edit_btn").html("保存");
       }
       flag=1;
       }else{
    	   if(checkForm()){
        $.post("company/saveCompany.do",$("#form").serialize(),function(data) {
					if(data.res){
					       if($("#userStatus").val()==3){
                             alert(data.msg); 
                             location="user/changeStatus.do"    
                             }else{
					         alert(data.msg);
						     location="backstagePages/menu.jsp";
						     }
					}
					else{
                             alert(data.msg);  
					}
				}, 'JSON');
    	   }
       }
    })
  });

$(document).ready(function(){
$("#uploadBtn").on("click", function(){
			$("#file").click();
		});
	});	
	
	function uploadImg(){
		var formData = new FormData($("#fileForm")[0]);
		if($("#file").val() == "") return false;

		$.ajax({  
            url : "user/uploadPic.do",  
            type : 'POST',  
            data : formData,  
            dataType:'json',
            processData : false,  
            contentType : false,  
            success : function(data) {  
                if(data.res){
                	$("#licenseImg").attr("src","../xrjob-img/"+data.msg);
                	$("#companyLicense").val(data.msg);
                	
                }
                else{
                	parent.$.messager.alert('提示', data.msg, 'error');
                }
            },  
            error : function(responseStr) {  
            	parent.$.messager.alert('提示', "网络异常", 'error');
            }  
    
        });
	}	
</script>

</html>