<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
	<meta charset="utf-8">
	<meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<title>企业注册</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	*{
		margin:0;
		padding:0;
	}
	body{
		background-color:#f5f5f5;
		font-size:14px;
		font-weight:400;
		color:#222;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	/*导航栏*/
	nav{
		width:100%;
		height:44px;
		background-color:#ff9137;
		color:#fff;
		line-height:44px;
		font-size:18px;
		text-align:center;
		padding-left:10px;
		padding-right:30px;
	}
	nav a{
		float:left;
	}
	nav img{
		width:20px;
		height:20px;
		vertical-align:middle;
	}
	/*主体内容*/
	:-moz-placeholder{
		color:#ccc; /*mozilla ff 4-18*/
	}
	::-moz-placeholder{
		color:#ccc; /*mozilla ff 19+*/
	}
	:-ms-input-placeholder{
		color:#ccc; /*ie 10+*/
	}
	::-webkit-input-placeholder{
		color:#ccc; /*webkit browsers*/
	}
	input[type=text],select,input[type=password]{
		border:none;
		outline:none;
		background-color:#fff;
		appearance: none;
		-moz-appearance:none;
		-webkit-appearance: none;
		text-align:left;
		height:26px;
		display:inline;
	}
	input{
		padding-left:3px;
	}
	select{
		color:#ccc;
	}
	select::-ms-expand{
		display:none;
	}
	.content{
		margin:10px;
		background-color:#fff;
	}
	.rowContent{
		background-color:#fff;
		padding:15px 6px;
		border-bottom:1px solid #ccc;
	}
	.rowContent:last-child{
		border:0;
	}
	label{
		font-weight:400;
		margin-bottom:0;
		color:#666;
	}
	label.contact{
		letter-spacing:7px;
	}
	.vLine{
		color:#999;
		padding-left:7px;
		padding-right:7px;
	}
	.vLine1{
		color:#999;
		padding-left:0px;
		padding-right:7px;
	}
	.rowContent .arrowRight{
		width:8px;
		height:16px;
		float:right;
		vertical-align:middle;
		margin-top:4px;
	}
	textarea{
		border:0;
		outline:none;
		resize:none;
		vertical-align:top;
		background-color:#fff;
		padding-left:4px;
	}
	.rowContent p{
		display:inline-block;
		vertical-align:top;
		margin-bottom:0;
		color:#999;
		word-break:break-all;
	}
	.store{
		padding:10px;
	}
	.store .btn{
		width:100%;
		padding-top:6px;
		padding-bottom:6px;
		border:0;
		background-color:#ff9137;
		border-radius: 4px;
		-moz-border-radius: 4px;
		-webkit-border-radius: 4px;
		color:#fff;
		margin-top:10px;
		margin-bottom:20px;
	}

</style>
</head>
<body>
	<!--导航栏-->
	<nav>
		<a href="javascript:history.go(-1)"><img src="img/back.png"></a>
		企业注册
	</nav>
	<!--主体内容-->
	<form role="form" action="company/addCompany.do" method="post" id="form">
		<div class="content">
			<div class="rowContent">
				<label>企业名称</label><span class="vLine">|</span><input type="text" id="cName"
                 name="companyName" placeholder="请填写企业名称" value="${company.companyName }">
			</div>
			<div class="rowContent">
				<label class="contact">联系人</label><span class="vLine1">|</span><input type="text" id="cUser" name="companyContractPerson"
				placeholder="请填写联系人" value="${company.companyContractPerson }">
			</div>
			<div class="rowContent">
				<label>联系电话</label><span class="vLine">|</span><input type="text" id="cTel" name="companyContractMobile" placeholder="请填写联系人电话" value="${company.companyContractMobile }">
			</div>
			<div class="rowContent">
				<label>企业地址</label><span class="vLine">|</span><input type="text" id="cLoc" name="companyAddress"placeholder="请填写企业地址" value="${company.companyAddress }">
			</div>
			<div class="rowContent">
				<label>企业性质</label><span class="vLine">|</span><select class="select"  name="companyNature" id="cNature">
				<option value="">请选择企业性质</option>
				     <option  value="1" <c:if test="${company.companyNature == 1}">selected="selected"</c:if>>国有企业</option>
					<option  value="2" <c:if test="${company.companyNature == 2}">selected="selected"</c:if>>外资企业</option>
					<option  value="3" <c:if test="${company.companyNature == 3}">selected="selected"</c:if>>合资企业</option>
					<option  value="4" <c:if test="${company.companyNature == 4}">selected="selected"</c:if>>私营企业</option>
					<option  value="5" <c:if test="${company.companyNature == 5}">selected="selected"</c:if>>民营企业</option>
					<option  value="6" <c:if test="${company.companyNature == 6}">selected="selected"</c:if>>股份制企业</option>
					<option  value="7" <c:if test="${company.companyNature == 7}">selected="selected"</c:if>>集体企业</option>
					<option  value="8" <c:if test="${company.companyNature == 8}">selected="selected"</c:if>>社会团体</option>
					<option  value="9" <c:if test="${company.companyNature == 9}">selected="selected"</c:if>>分支机构</option>
			</select>
				<img class="arrowRight" src="img/arrow-right.png">
			</div>
			<div class="rowContent">
				<label>企业规模</label><span class="vLine">|</span><select class="select" name="companyScale" id="cScale">
				<option value="">请选择企业规模</option>
				   <option value="1" <c:if test="${company.companyScale == 1}">selected="selected"</c:if>>15人以下</option>
				 <option value="2" <c:if test="${company.companyScale == 2}">selected="selected"</c:if>>15-50人</option>
			     <option value="3" <c:if test="${company.companyScale == 3}">selected="selected"</c:if>>50-200人</option>
				 <option value="4" <c:if test="${company.companyScale == 4}">selected="selected"</c:if>>200-500人</option>
				 <option value="5" <c:if test="${company.companyScale == 5}">selected="selected"</c:if>>500-2000人</option>
				 <option value="6" <c:if test="${company.companyScale == 6}">selected="selected"</c:if>>2000人以上</option>
			</select>
				<img class="arrowRight" src="img/arrow-right.png">
			</div>
			<div class="rowContent">
				<label>企业描述</label><span class="vLine">|</span><textarea rows="8" id="cDescription" name="companyDescribe" placeholder="请填写企业描述">${company.companyDescribe}</textarea>
			</div>
		</div>
		<div class="store">
			<button class="btn" type="button"  onclick="addCompany()">保存</button>
		</div>
	</form>

<script src="js/jquery-2.2.3.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script>
	$(function(){
		/*p和textarea的内容随页面宽度改变而改变*/
		var w=$('.rowContent').width()-90;
		$('p,input,textarea,select').width(w);
		$('select').focus(function(){
			$(this).css('color','#222');
		});

	})

function checkForm(){
   if($("#cName").val()==null||$("#cName").val()==''){
	   alert("请输入企业名称");
       return false;
   }
   if($("#cUser").val()==null||$("#cUser").val()==''){
	   alert("请输入企业联系人");
       return false;
   }
   if($("#cTel").val()==null||$("#cTel").val()==''){
	   alert("请输入企业联系电话");
       return false;
   }
   if (!(/^1[3458][0-9]\d{4,8}$|^((\+86)|(86))?(13)\d{9}$|(^(\d{3,4}-)?\d{7,8})$|^400[016789]\d{6}$/.test($("#cTel").val()))) {
       alert("请输入正确的联系方式")
      return false;
	}
   
   if($("#cLoc").val()==null||$("#cLoc").val()==''){
	   alert("请输入企业地址");
       return false;
   }
   if($("#cNature").val()==null||$("#cNature").val()==''){
	   alert("请输入企业性质");
       return false;
   }
   if($("#cScale").val()==null||$("#cScale").val()==''){
	   alert("请输入企业规模");
       return false;
   }

	
	return true;
}

  function addCompany(){  
	  if(checkForm()){
  $.post("company/addCompany.do",$("#form").serialize(),function(data) {
					if(data.res){
						alert("保存成功，点击确定继续添加营业执照");
						location="backstagePages/uploadPic.jsp";		
					}
					else{
                             alert(data.msg);     
					}
				}, 'JSON');
	  }
				
}
</script>
</body>	
</html>