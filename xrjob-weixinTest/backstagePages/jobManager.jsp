<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
	<meta charset="utf-8">
	<meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<title>岗位管理</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	*{
		margin:0;
		padding:0;
	}
	body{
		font-size:14px;
		font-weight:400;
		color:#222;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	/*导航栏*/
	nav{
		width:100%;
		height:44px;
		background-color:#ff9137;
		color:#fff;
		line-height:44px;
		font-size:18px;
		text-align:center;
		padding-left:10px;
		padding-right:30px;
	}
	nav a{
		float:left;
	}
	nav img{
		width:20px;
		height:20px;
		vertical-align:middle;
	}
	/*主体内容*/
	.content{
		padding-left:10px;
		padding-right:10px;
	}
	table{
		table-layout:fixed;
		width:100%;
	}
	th,td{
		text-align:center;
		vertical-align:middle;
		padding-top:15px;
		padding-bottom:15px;
	}
	td:first-child a{
		color:#ff9137;
	}
	th:first-child,td:first-child{
		text-align:left;
	}
	th:nth-child(2),td:nth-child(2){
		max-width:120px;
		text-align:left;
	}
	th:nth-child(2){
		padding-left:4px;
	}
	th:last-child,td:last-child{
		width:135px;
	}
	td:last-child{
		text-align:right;
	}
	tr{
		border-bottom:1px solid #ccc;
	}
	tr:last-child{
		border:0;
	}
	th{
		font-weight:700;
		font-size:14px;
	}
	td{
		color:#666;
		font-size:13px;
	}
	td span:last-child a{
		color:#ff9137;
		padding-left:5px;
		letter-spacing: -1px;
	}
	button{
		background-color:#fff;
		outline:none;
		border:0;
		padding-left:5px;
		padding-right:5px;
		letter-spacing: -1px;
	}
	button:focus{
		outline:none;
	}
	

</style>
</head>
<body>
	<!--导航栏-->
	<nav>
		<a href="backstagePages/menu.jsp"><img src="img/back.png"></a>
		岗位管理
	</nav>
	<!--主体内容-->
	<div class="content">
		<table>
			<tr>
				<th>岗位名称</th>
				<th>发布时间</th>
				<th>操作</th>
			</tr>
			<!--循环start-->
			 <c:if test="${jobList.size()!=0}">
       <c:forEach varStatus="status" items="${jobList}" var="jobs">
   
			<tr>
				<td><a class="jobPost" href="job/showJob.do?jobId=${jobs.jobId }">${jobs.jobName }</a></td>
				<td><fmt:formatDate value="${jobs.createTime}" pattern="yyyy-MM-dd"/></td>
				<td><button onclick="reflash('${jobs.jobId}')">刷新</button><c:if test="${jobs.jobStatus==1 }"><button onclick="pause('${jobs.jobId}')">暂停</button></c:if><c:if test="${jobs.jobStatus==2 }"><button onclick="cancelPause('${jobs.jobId}')">发布</button></c:if><button onclick="DoEmpty('${jobs.jobId}')">删除</button><span><a href="job/editJob.do?jobId=${jobs.jobId}">编辑</a></span></td>
			</tr>
			
			</c:forEach>
    </c:if>
	<!--循环end-->
		</table>
	</div>




<script src="js/jquery-2.2.3.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script>
function DoEmpty(params)
{
if (confirm("您确定要删除该条岗位吗？删除后该岗位的所有内容都将被删除并且无法恢复！")){
  $.post("job/deleteJob.do",{jobId:params},function(data){
    if (data.res) {
			    alert(data.msg)
				location.reload();
			} else {
				alert(data.msg);
			}
		}, 'JSON'  ); 
  }
}

function reflash(jobId){
	if (confirm("您确定要刷新该条岗位吗？")){
		  $.post("job/reflashJob.do",{jobId:jobId},function(data){
		    if (data.res) {
					    alert(data.msg)
					    location.reload();
					} else {
						alert(data.msg);
					}
				}, 'JSON'  ); 
		  }
}

function pause(jobId){
	if (confirm("您确定要暂停该条岗位吗？")){
		  $.post("job/pauseJob.do",{jobId:jobId},function(data){
		    if (data.res) {
					    alert(data.msg)
					    location.reload();
					} else {
						alert(data.msg);
					}
				}, 'JSON'  ); 
		  }
	
}
function cancelPause(jobId){
	if (confirm("您确定要发布该条岗位吗？")){
		  $.post("job/cancelPauseJob.do",{jobId:jobId},function(data){
		    if (data.res) {
					    alert(data.msg)
					    location.reload();
					} else {
						alert(data.msg);
					}
				}, 'JSON'  ); 
		  }
	
}
</script>
</body>	
</html>