<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
	<meta charset="utf-8">
	<meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<title>人才库搜索结果</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	*{
		margin:0;
		padding:0;
	}
	body{
		font-size:14px;
		font-weight:400;
		color:#222;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
		color:#222;
	}
	/*导航栏*/
	nav{
		width:100%;
		height:44px;
		background-color:#ff9137;
		color:#fff;
		line-height:44px;
		font-size:18px;
		text-align:center;
		padding-left:10px;
		padding-right:30px;
	}
	nav a{
		float:left;
	}
	nav img{
		width:20px;
		height:20px;
		vertical-align:middle;
	}
	/*主体内容*/
	/*搜索结果*/
	.jobContent{
		float:left;
		background-color:#fff;
		width:100%;
		height:100px;
		padding:15px 10px;
	}
	.jobList{
		float:left;
		white-space:nowrap;
		overflow:hidden;
		text-overflow:ellipsis;
	}
	.name{
		font-size:15px;
	}
	ul{
		margin-bottom:0px;
	}
	.job{
		color:#666;
		padding-bottom:5px;
	}
	.time{
		color:#999;
		overflow:visible;
		padding-bottom:5px;
	}
	.rightArrow{
		float:right;
		line-height:70px;
		padding-left:10px;
		text-align:right;
		color:#ff9137;
		font-size:15px;
	}
	hr{
		border:1px solid #f5f5f5;
		margin:0;
	}
.linethrough{
		text-align:center;
		margin-bottom:0;
		padding-top:5px;
		padding-bottom:5px;
		color:#f00;
	}
</style>
</head>
<body>
	<!--导航栏-->
	<nav>
		<a href="javascript:" onclick="self.location=document.referrer;"><img src="img/back.png"></a>
		搜索结果
	</nav>
	<!--主体内容-->
	<!--循环start-->
	 <c:if test="${resumeList.size()!=0 }">
        
         <c:forEach  items="${resumeList}" var="resumeList" varStatus="status">
	<a href="resume/viewResume.do?resumeId=${resumeList.resumeId}">
	<div class="jobContent">
		
			<ul class="jobList">
				<li class="name">
					${resumeList.resumeUserName }
				</li>
				<li class="job">
					${resumeList.typeName }
				</li>
				<li class="time">
					<span><c:if test="${resumeList.sex==1 }">男</c:if><c:if test="${resumeList.sex==0 }">女</c:if>&nbsp;&nbsp;|&nbsp;&nbsp;<c:if 
					test="${resumeList.workYear==1 }">一年以下</c:if> <c:if test="${resumeList.workYear==2 }">1-2年</c:if><c:if 
					test="${resumeList.workYear==3 }">3-5年</c:if><c:if
					 test="${resumeList.workYear==4 }">5-10年</c:if><c:if
					  test="${resumeList.workYear==5 }">10年以上</c:if>&nbsp;&nbsp;|&nbsp;&nbsp;<c:if test="${resumeList.highDegree==1 }">高中以下</c:if><c:if
					   test="${resumeList.highDegree==2 }">高中</c:if><c:if 
					   test="${resumeList.highDegree==3 }">中专/技校</c:if><c:if 
					   test="${resumeList.highDegree==4 }">大专</c:if><c:if 
					   test="${resumeList.highDegree==5 }">本科</c:if><c:if 
					   test="${resumeList.highDegree==6 }">硕士以上</c:if></span>
				</li>
			</ul>
			<div class="rightArrow">
				<span class="salary"><c:if test="${resumeList.salaryId==0 }">薪资面议</c:if><c:if 
				test="${resumeList.salaryId==1 }">1500元以下/月</c:if><c:if 
				test="${resumeList.salaryId==2 }">1500-2500元/月</c:if><c:if
                test="${resumeList.salaryId==3 }">2500-3500元/月</c:if><c:if 
                test="${resumeList.salaryId==4 }">3500-5000元/月</c:if><c:if
                 test="${resumeList.salaryId==5 }">5000-7000元/月</c:if><c:if
                  test="${resumeList.salaryId==6 }">7000-10000元/月</c:if><c:if 
                  test="${resumeList.salaryId==7 }">10000-15000元/月</c:if><c:if
                   test="${resumeList.salaryId==8 }">15000元以上/月</c:if></span>
			</div>
	</div>
	</a>
	<hr>
 	  </c:forEach> 
   </c:if>
    <c:if test="${resumeList.size()==0 }">
    没有查询到记录，请更改搜索条件
    </c:if>
	<!--循环end-->
<p class="linethrough">以下为最新发布的简历</p>
    <c:forEach  items="${newResumeList}" var="resumeList" varStatus="status">
	<div class="jobContent">
		<a href="resume/viewResume.do?resumeId=${resumeList.resumeId}">
			<ul class="jobList">
				<li class="name">
					${resumeList.resumeUserName }
				</li>
				<li class="job">
					${resumeList.typeName }
				</li>
				<li class="time">
					<span><c:if test="${resumeList.sex==1 }">男</c:if><c:if test="${resumeList.sex==0 }">女</c:if>&nbsp;&nbsp;|&nbsp;&nbsp;<c:if 
					test="${resumeList.workYear==1 }">一年以下</c:if> <c:if test="${resumeList.workYear==2 }">1-2年</c:if><c:if 
					test="${resumeList.workYear==3 }">3-5年</c:if><c:if
					 test="${resumeList.workYear==4 }">5-10年</c:if><c:if
					  test="${resumeList.workYear==5 }">10年以上</c:if>&nbsp;&nbsp;|&nbsp;&nbsp;<c:if test="${resumeList.highDegree==1 }">高中以下</c:if><c:if
					   test="${resumeList.highDegree==2 }">高中</c:if><c:if 
					   test="${resumeList.highDegree==3 }">中专/技校</c:if><c:if 
					   test="${resumeList.highDegree==4 }">大专</c:if><c:if 
					   test="${resumeList.highDegree==5 }">本科</c:if><c:if 
					   test="${resumeList.highDegree==6 }">硕士以上</c:if></span>
				</li>
			</ul>
			<div class="rightArrow">
				<span class="salary"><c:if test="${resumeList.salaryId==0 }">薪资面议</c:if><c:if 
				test="${resumeList.salaryId==1 }">1500元以下/月</c:if><c:if 
				test="${resumeList.salaryId==2 }">1500-2500元/月</c:if><c:if
                test="${resumeList.salaryId==3 }">2500-3500元/月</c:if><c:if 
                test="${resumeList.salaryId==4 }">3500-5000元/月</c:if><c:if
                 test="${resumeList.salaryId==5 }">5000-7000元/月</c:if><c:if
                  test="${resumeList.salaryId==6 }">7000-10000元/月</c:if><c:if 
                  test="${resumeList.salaryId==7 }">10000-15000元/月</c:if><c:if
                   test="${resumeList.salaryId==8 }">15000元以上/月</c:if></span>
			</div>
		</a>
	
	</div>
	<hr>
 	  </c:forEach> 

<script src="js/jquery-2.2.3.min.js"></script>
<script src="js/jquery.validate.js"></script>
</body>	
</html>