<%@page import="com.job.model.UserResumeWork"%>
<%@page import="com.job.model.UserResume"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
    <title>个人简历</title>
	<base href="<%=basePath%>">
    <meta charset="utf-8" />
    <meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport">
    <meta name="format-detection" content="telephone=no">
    <!--CSS-->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <style>
        a:link,a:visited,a:hover,a:active{
            text-decoration:none;
        }
        html{
            overflow-x:hidden;
        }
        body{
            overflow-x:hidden;
            font-size:14px;
            font-weight:400;
            color:#666;
            background-color:#f5f5f5;
        }
        /*导航栏*/
        nav{
            width:100%;
            height:44px;
            background-color:#ff9137;
            color:#fff;
            line-height:44px;
            font-size:18px;
            text-align:center;
            padding-left:10px;
            padding-right:10px;
        }
        nav a{
            float:left;
        }
        nav img{
            width:20px;
            height:20px;
            vertical-align:middle;
        }
        /*主要内容*/
        header{
            padding-top:20px;
            background-color:#ff9137;
            padding-bottom:15px;
        }
        .row{
            margin:0;
        }
        .row>div{
            padding-right:0;
        }
        .row p{
            margin-bottom:5px;
        }
        .viewResumeInfo span{
            margin:0;
            font-weight:400;
        }
        .panel{
            border:0;
            margin-top:16px;
            margin-bottom:0;
            position:relative;
            padding-top:25px;
        }
        img.resumeImg{
            position:absolute;
            left:10px;
            top:-5px;
            width:80px;
            height:30px;
            z-index:77;
        }
        .line{
            margin:0 0 10px 0;
            border:0;
            border-bottom:1px solid #ff9137;
        }
        .panel-heading{
            position:absolute;
            left:20px;
            top:0;
            z-index:88;
            color:#fff;
            padding:2px 0;
        }
        .panel-body1{
            padding:0 10px;
            letter-spacing:-1px;
        }
        .panel-body1 a{
            color:#666;
        }
        .row .name{
            font-size:14px;
        }
        .panel-body{
            padding:10px 10px 0 10px;
            border-bottom:1px solid #ccc;
        }
        .panel-body:first-child{
        	padding-top:0;
        }
        .panel-body:last-child{
        	border:0;
        }
        .panel-body p{
            margin-bottom:5px;
        }
        .col-xs-6{
            padding-left:0;
        }
        .col-xs-11{
            padding-left:0;
        }
        .panel-body:last-child{
            padding-bottom:10px;
        }

        .panel-body p span{
            line-height:20px;
        }
        .selfEvaluate{
            text-indent:24px;
        }
        textarea{
        	background-color:#fff;
        	resize:none;
        	border:0;
        	vertical-align:top;
        	font-size:14px;
        	padding:0;
        	outline:none;
        	color:#666;
        }
        textarea:focus{
        	outline:none;
        	color:#666;
        }
        textarea:disabled{
        	color:#666;
        	font-weight:400;
        }
        nav a.right{
        	float:right;
        	color:#fff;
        }
        nav a.right .glyphicon-home{
        	font-size:18px;
        	line-height:44px;
        }

    </style>
</head>
<body>
    <!--导航栏-->
    <nav>
        <a href="javascript:history.go(-1)"><img src="img/back.png"></a>
        个人简历<a href="backstagePages/menu.jsp" class="right"><span class="glyphicon glyphicon-home"></span></a>
    </nav>
    <div class="viewResumeInfo">
        <div class="panel">
            <img class="resumeImg" src="img/resume.png">
            <div class="panel-heading">
                <h3 class="panel-title">个人信息</h3>
            </div>
            <hr class="line">
            <div class="panel-body1">
                <div class="row">
                    <div class="col-xs-11 col-sm-11 name">${resume.resumeUserName}</div>
                </div>
                <div class="row">
                    <div class="col-xs-11 col-sm-11"><c:if test="${resume.sex == 1}">男</c:if><c:if 
                    test="${resume.sex == 0}">女</c:if>&nbsp;&nbsp;|&nbsp;&nbsp;${resume.originPlace}</div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-xs-6">
                        <p>出生年月：<span><fmt:formatDate value="${resume.birthDate}" pattern="yyyy-MM-dd"/></span></p>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <p>最高学历：<span> <c:if test="${resume.highDegree == 1}">高中以下</c:if><c:if
                         test="${resume.highDegree == 2}">高中</c:if><c:if
                          test="${resume.highDegree == 3}">中专/技校</c:if><c:if
                           test="${resume.highDegree == 4}">大专</c:if><c:if 
                           test="${resume.highDegree == 5}">本科</c:if><c:if 
                           test="${resume.highDegree == 6}">硕士以上</c:if></span></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-xs-6">
                        <p>期望岗位：<span>${resume.typeName}</span></p>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <p>期望薪资：<span> <c:if test="${resume.salaryId == 1}">1500元及以下/月</c:if><c:if 
                        test="${resume.salaryId == 2}">1500-2500元/月</c:if><c:if
                         test="${resume.salaryId == 3}">2500-3500元/月</c:if><c:if
                          test="${resume.salaryId == 4}">3500-5000元/月</c:if><c:if 
                          test="${resume.salaryId == 5}">5000-7000元/月</c:if><c:if
                           test="${resume.salaryId == 6}">7000-10000元/月</c:if><c:if
                            test="${resume.salaryId == 7}">10000-15000元/月</c:if><c:if
                             test="${resume.salaryId == 8}">15000元以上/月</c:if><c:if
                              test="${resume.salaryId == 0}">薪资面议</c:if></span></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-xs-6">
                        <p>联系电话：<span style="white-space:pre"></span><a href="tel://${resume.userMobile }">${resume.userMobile }</a></p>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <p>工作年限：<span><c:if test="${resume.workYear == 1}">1年以下</c:if><c:if
                         test="${resume.workYear == 2}">1-2年</c:if><c:if 
                         test="${resume.workYear == 3}">3-5年</c:if><c:if 
                         test="${resume.workYear == 4}">5-10年</c:if><c:if 
                         test="${resume.workYear == 5}">10年以上</c:if></span></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-xs-6">
                        <p>求职状态：<span><c:if test="${resume.jobStatus == 1}">离职在找工作</c:if><c:if 
                        test="${resume.jobStatus == 2}">在职在找工作</c:if><c:if 
                        test="${resume.jobStatus == 3}">暂时不想找工作</c:if></span></p>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <p>工作类型：<span><c:if test="${resume.jobType == 1}">全职</c:if><c:if
                         test="${resume.jobType == 2}">兼职</c:if><c:if 
                         test="${resume.jobType == 3}">应届生</c:if></span></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-11 col-xs-11">
                        <p>邮箱：<span  style="word-break:break-all;">${resume.userEmail}</span></p>
                    </div>
                </div>
            </div>
        </div>
        <c:if test="${userWorkList.size()!=0 }">
        <div class="panel">
            <img class="resumeImg" src="img/resume.png">
            <div class="panel-heading">
                <h3 class="panel-title">工作经历</h3>
            </div>
            <hr class="line">	
         <c:forEach items="${userWorkList}" var="item">
            <div class="jobExp panel-body">
                <div class="row">
                    <div>
                        <p>公司名称：<span>${item.companyName}</span></p>
                    </div>
                </div>
                <div class="row">
                    <p>工作时间：<span>${item.startDate}至${item.endDate}</span></p>
                </div>
                <div class="row">
                    <p>工作岗位：<span>${item.userJob}</span></p>
                </div>
                <div class="row">
                    <p><span>岗位描述：</span><textarea class="postDescription" readonly="readonly" rows="3">${item.workDescribe}</textarea> </p>
                </div>
            </div>
			</c:forEach>
        </div>
        </c:if>
        <c:if test="${userEduList.size()!=0 }">
        <div class="panel">
            <img class="resumeImg" src="img/resume.png">
            <div class="panel-heading">
                <h3 class="panel-title">教育经历</h3>
            </div>
            <hr class="line">
			
          <c:forEach items="${userEduList}" var="item">
           <div class="panel-body">
            <p>学校名称：<span>${item.schoolName}</span></p>
            <p>学习时间：<span>${item.startDate}至${item.endDate}</span></p>
            <p>专业：<span>${item.professionalName}</span></p>
            <p>学历：<span><c:if test="${item.educationGrade == 1}">高中以下</c:if><c:if 
            test="${item.educationGrade == 2}">高中</c:if><c:if
             test="${item.educationGrade == 3}">中专/技校</c:if><c:if
              test="${item.educationGrade == 4}">大专</c:if><c:if
               test="${item.educationGrade == 5}">本科</c:if><c:if 
               test="${item.educationGrade == 6}">硕士以上</c:if></span></p>
        </div>
          </c:forEach>
        </div>
         </c:if>
        <c:if test="${resume.userDescribe!=null && resume.userDescribe!=''}">
        <div class="panel">
            <img class="resumeImg" src="img/resume.png">
            <div class="panel-heading">
                <h3 class="panel-title">自我评价</h3>
            </div>
            <hr class="line">
            <div class="panel-body">
                <p class="selfEvaluate"><span><textarea class="self" readonly="readonly" rows="7">${resume.userDescribe}</textarea></span></p>
            </div>
        </div>
        </c:if>
    </div>

<script src="js/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script>
	$(function(){
		var w=$('.row>p').width()-70;
		$('textarea').width(w);
	});
</script>
</body>
</html>