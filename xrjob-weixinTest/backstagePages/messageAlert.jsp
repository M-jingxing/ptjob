<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
	<meta charset="utf-8">
	<meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<title>设置提醒</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	*{
		margin:0;
		padding:0;
	}
	body{
		font-size:14px;
		font-weight:400;
		color:#222;
		background-color:#f5f5f5;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
		color:#222;
	}
	/*导航栏*/
	nav{
		width:100%;
		height:44px;
		background-color:#ff9137;
		color:#fff;
		line-height:44px;
		font-size:18px;
		text-align:center;
		padding-left:10px;
		padding-right:10px;
	}
	nav a{
		float:left;
	}
	nav img{
		width:20px;
		height:20px;
		vertical-align:middle;
	}
	/*主体内容*/
	input[type=text],select,input[type=password],input[type=email]{
		border:none;
		outline:none;
		background-color:#f5f5f5;
		appearance: none;
		-moz-appearance:none;
		-webkit-appearance: none;
		height:26px;
		display:block;
		border-bottom:1px solid #ff9137;
		margin-left:40px;
		visibility:hidden;
		text-align:left;
		color:#666;
		padding:0;
		font-size:14px;
	}
	input[type="radio"]{
		-webkit-appearance:none;
		-moz-appearance:none;
		outline:none;
		border:0;
		background:url(img/checkbox-no.png) no-repeat 50% 50%;
		background-size:20px 20px;
		width:20px;
		height:40px;
		margin-left:10px;
		vertical-align:middle;
		margin-right:10px;
		margin-top:0;
	}
	input[type="radio"]:focus,input[type="radio"]:hover{
		background:url(img/checkbox-no.png) no-repeat 50% 50%;
		background-size:20px 20px;
		outline:none;
	}
	input[type="radio"]:checked{
		background:url(img/checkbox.png) no-repeat 50% 50%;
		background-size:20px 20px;
		outline:none;
	}
	input[type="radio"][disabled]:checked{
		background:url(img/checkbox-no.png) no-repeat 50% 50%;
		background-size:20px 20px;
		outline:none;
	}
	.hint{
		padding:20px 0 20px 30px;
		border-bottom:1px solid #ccc;
	}
	.hint:last-child{
		border:0;
	}
	.emailHint{
		font-size:18px;
		vertical-align:middle;
		line-height:40px;
	}
	.microHint{
		padding-left:40px;
		color:#999;
		visibility:hidden;
	}
	.store{
		padding:10px;
		margin-top:60px;
	}
	.store .btn1{
		width:100%;
		padding-top:6px;
		padding-bottom:6px;
		border:0;
		background-color:#ff9137;
		border-radius: 4px;
		-moz-border-radius: 4px;
		-webkit-border-radius: 4px;
		color:#fff;
		letter-spacing:40px;
		font-size:15px;
		padding-left:40px;
	}
	.store .btn1:focus{
		outline:none;
		border:0;
		box-shadow: none;
	}
	.error{
		font-size:12px;
		color:#ff9037;
		font-weight:400;
		margin-left:40px;
		padding-top:5px;
	}

</style>
</head>
<body>
	<!--导航栏-->
	<nav>
		<a href="backstagePages/menu.jsp"><img src="img/back.png"></a>
		设置提醒
	</nav>
	<!--主体内容-->
	<form id="frmSetting" >
		<div class="hint">
			<div>
				<input type="radio" name="messageAlert" value="1"  <c:if test="${company.messageAlert==1 }">checked="checked"</c:if>><span class="emailHint">邮件提醒</span>
			</div>
			<input type="email" name="messageEmail" placeholder="请输入邮箱号" value="${company.messageEmail }">
		</div>
		<div class="hint">
			<div>
				<input type="radio" name="messageAlert" value="2" <c:if test="${company.messageAlert==2 }">checked="checked"</c:if>><span class="emailHint">短信提醒</span>
			</div>
			<input type="text" name="messageMobile" placeholder="请输入手机号" value="${company.messageMobile }">
		</div>
		<div class="hint">
			<div>
				<input type="radio" name="messageAlert" value="3" <c:if test="${company.messageAlert==3 }">checked="checked"</c:if>><span class="emailHint">微信提醒</span>
			</div>
			<p class="microHint">关注微信公众号，开启微信提醒功能</p>
		</div>
		<div class="store">
			<button class="btn1" type="button" onclick="saveMsg()">保存</button>
		</div>
	</form>

<script src="js/jquery-2.2.3.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script>
	$(function(){
	 	/*邮箱、手机号验证*/
	 	$("#frmSetting").validate({
			rules:{
				messageEmail:{
					messageEmail:true
				},
				messageMobile:{
					messageMobile:true
				}
			},
			messages:{
				messageEmail:{
					messageEmail:"请输入正确的邮箱号"
				},
				messageMobile:{
					messageMobile:"请输入正确的手机号"
				}
			}
		}); 
 
		/*点击不同单选框出现不同选项*/
		$('input[type=radio]').click(function(){
			$('input[type=radio]').parent().next().css('visibility','hidden').next().hide();
			$('input[type=radio]:checked').parent().next().css('visibility','visible').attr('required');
		});
	});	
	
	function saveMsg(){
		
		$.post('company/messageAlert.do',$("#frmSetting").serialize(),function(data){
			if(data.res){
				alert(data.msg);
				location="backstagePages/menu.jsp";
			}
			
		},'json')
			
		
		
		
	}
	
	
</script>
</body>	
</html>