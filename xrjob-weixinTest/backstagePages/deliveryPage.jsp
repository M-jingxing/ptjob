<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
    <title>投递管理</title>
	<base href="<%=basePath%>">
    <meta charset="utf-8" />
    <meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport">
    <meta name="format-detection" content="telephone=no">
    <!--CSS-->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <style>
        *{
            padding:0;
            margin:0;
        }
        body{
            font-size:14px;
            color:#666;
        }
        th,td{
            text-align:center;
        }
        ul{
            margin:0;
            padding:0;
        }
        li{
            list-style-type:none;
        }
        a:hover{
            text-decoration: none;
            color:#666;
        }
        a:link,a:visited,a:hover,a:active{
            color:#666;
        }
        /*导航栏*/
        nav{
            width:100%;
            height:44px;
            background-color:#ff9137;
            color:#fff;
            line-height:44px;
            font-size:18px;
            text-align:center;
            padding-left:10px;
            padding-right:30px;
        }
        nav a{
            float:left;
        }
        nav img{
            width:20px;
            height:20px;
            vertical-align:middle;
        }
        /*主要内容*/
        .content{
            padding-left:10px;
            padding-right:10px;
        }
        table{
            table-layout: fixed;
            width:100%;
        }
        th a{
            color:#000;
        }
        .notLook{
            color:#ff9137;
        }
        .lookResume{
            color:#666;
        }
        .lookResume:hover{
            text-decoration:none;
        }
        .lookResume:visited{
            color:#666;
        }
        .delete{
            color:#666;
            background-color:#fff;
            border:0;
            padding-left:5px;
            padding-right:5px;
        }
        .delete:focus{
            outline:none;
        }
        .dLabel{
            background-color:#fff;
            border:0;
            padding-left:0;
            padding-bottom: 0;
            font-weight:700;
            color:#666;
        }
        .dLabel:focus{
            outline:none;
            background-color:#fff;
            border:0;
            box-shadow:none;
        }
        .table>tbody>tr>td,.table>tfoot>tr>td,.table>thead>tr>td{
            padding-right:0;
            vertical-align:middle;
            font-size:13px;
            padding-left:0;
            letter-spacing:-1px;
        }
         .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>th{
            padding-left:0;
            padding-right:0;
            vertical-align:middle;
            font-size:14px;
            font-weight:700;
            color:#666;
            letter-spacing:-1px;
        }
        .table>tbody>tr>th:nth-child(1), .table>thead>tr>th:nth-child(3){
        	padding-left:10px;
        }
         th:last-child,td:last-child{
             width:100px;
         }
        td:last-child{
            text-align:right;
        }
        td:last-child a{
            padding-right:5px;
        }
        .btn{
            padding:0;
        }
        .btn:focus{
            outline:none;
            touch-action:none;
        }


    </style>
</head>
<body>
<!--导航栏-->
<nav>
    <a href="backstagePages/menu.jsp"><img src="img/back.png"></a>
    投递管理
</nav>
<div class="content">
    <table class="table">
        <thead>
            <tr>
                <th>
                    <div class="dropdown">
                        <a role="button" data-toggle="dropdown" class="dLabel btn" data-target="#"
                           href="javascript:;">
                            岗位名称 <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                            <li><a href="delivery/queryDelivery.do">全部</a></li>
                   <c:forEach varStatus="status" items="${jobNameList}" var="jobNameList" >
                    <li><a href="delivery/queryDelivery.do?jobId=${jobNameList.jobId}">${jobNameList.jobName}</a></li>
                    </c:forEach>

                        </ul>
                    </div>
                </th>
                <th>投递时间</th>
                <th>
                    <div class="dropdown">
                        <a role="button" data-toggle="dropdown" class="dLabel btn" data-target="#"
                           href="javascript:;">
                            投递状态 <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                                   <li><a href="delivery/queryDelivery.do">全部</a></li>
                    <li><a href="delivery/queryDelivery.do?jobId=${jobId}&status=1">未查看</a></li>
                    <li><a href="delivery/queryDelivery.do?jobId=${jobId}&status=2">已查看</a></li>
                        </ul>
                    </div>
                </th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
        <c:if test="${deliveryList.size()!=0}">
    <c:forEach varStatus="status" items="${deliveryList}" var="deliveryList" >
    <tr>
        <td>${deliveryList.jobName}</td>
        <td>
            <ul>
                <li><fmt:formatDate value="${deliveryList.createTime}" pattern="yyyy-MM-dd"/></li>
                
            </ul>
        </td>
          <c:if test="${deliveryList.deliveryStatus==1}">
        <td class="notLook">未查看</td></c:if>
         <c:if test="${deliveryList.deliveryStatus==2}">
        <td class="looked">已查看</td></c:if>
        <td>
        	<a class="lookResume" href="resume/viewResume.do?deliveryId=${deliveryList.deliveryId}&resumeId=${deliveryList.resumeId}&jobId=${deliveryList.jobId}">查看简历</a>
        	&nbsp;&nbsp;
        	<a class="delete" href="javascript:del('${deliveryList.deliveryId}');">删除</a>
        </td>
    </tr>
   </c:forEach>
    </c:if>
           
        </tbody>
    </table>
</div>

<script src="js/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script>
function del(params){
   
if (confirm("您确定要删除该条记录吗？删除后此该记录的所有内容都将被删除并且无法恢复！")){

   $.post("delivery/deleteDelivery.do",{deliveryId:params},function(data){
      if(data.res){
        alert(data.msg);
        location.reload(); 
      }else{
        alert(data.msg);
      }
   },'Json');
   }
  
}

</script>
</body>
</html>