<%@page import="com.job.model.CompanyJob"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<title>查看岗位</title>
	<base href="<%=basePath%>">
	<meta charset="utf-8" />
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	p{
		margin:0;
		padding:0;
	}
	body{
		font-weight:400;
		font-size:14px;
		background-color:#fff;
		color:#666;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	/*导航栏*/
	nav{
		width:100%;
		height:44px;
		background-color:#ff9137;
		color:#fff;
		line-height:44px;
		font-size:18px;
		text-align:center;
		padding-left:10px;
		padding-right:30px;
	}
	nav a{
		float:left;
	}
	nav img{
		width:20px;
		height:20px;
		vertical-align:middle;
	}
	/*主要内容*/
	.info{
		padding:18px 20px 0 20px;
		background-color:#fff;
		border-bottom:10px solid #f5f5f5;
	}
	ul{
		padding-left:0;
		padding-top:5px;
		padding-bottom:2px;
		margin-bottom:0;
	}
	.post{
		font-size:16px;
		margin-bottom:0;
		font-weight:700;
	}
	.info .salary{
		color:#fe552c;
		font-size:16px;
		line-height:22px;
	}
	.edu li{
		padding-bottom:10px;
		line-height:25px;
	}
	.edu li span{
		width:50%;
		float:left;
		line-height:25px;
	}
	.edu li img{
		width:20px;
		height:20px;
	}
	/*岗位要求*/
	.postRequire,.postDescription{
		background-color:#fff;
		padding:10px 20px;
	}
	.postRequire p:first-child,.postDescription p:first-child{
		font-size:16px;
		margin-bottom:5px;
		font-weight:700;
	}
	.postRequire p:last-child,.postDescription p:last-child{
		line-height:24px;
		padding-bottom:10px;
	}


</style>
</head>
<body>
	<!--导航栏-->
	<nav>
		<a href="javascript:history.go(-1)"><img src="img/back.png"></a>
		岗位详情
	</nav>
	<!--主要内容-->
<div class="info">
	<p class="post"><span>${job.jobName }</span></p>
	<ul>
		<li>
			<span class="salary"><c:if test="${job.jobSalaryId==0 }">薪资面议</c:if><c:if
			 test="${job.jobSalaryId==1 }">1500元以下/月</c:if><c:if 
			 test="${job.jobSalaryId==2 }">1500-2500元/月</c:if><c:if
			 test="${job.jobSalaryId==3 }">2500-3500元/月</c:if><c:if 
			 test="${job.jobSalaryId==4 }">3500-5000元/月</c:if><c:if
			 test="${job.jobSalaryId==5 }">5000-7000元/月</c:if><c:if
			 test="${job.jobSalaryId==6 }">7000-10000元/月</c:if><c:if
			 test="${job.jobSalaryId==7 }">10000-15000元/月</c:if><c:if
			 test="${job.jobSalaryId==8 }">15000元以上/月</c:if></span></li>
		<li>
			<span class="jobKind">${job.companyName}</span>
		</li>
	</ul>
	<ul class="edu">
		<li>
			<span>
				<img src="img/experience.png">&nbsp;&nbsp;<c:if 
				test="${job.jobWorkYear==0}">不限 </c:if><c:if 
				test="${job.jobWorkYear==1 }">1年以下 </c:if><c:if 
				test="${job.jobWorkYear==2 }">1-2年 </c:if><c:if 
				test="${job.jobWorkYear==3 }">3-5年</c:if><c:if 
				test="${job.jobWorkYear==4 }">5-10年 </c:if><c:if 
				test="${job.jobWorkYear==5 }">10年以上 </c:if>
			</span>
			<span>
				<img src="img/qualifications.png">&nbsp;&nbsp;<c:if 
				test="${job.jobDegree==0 }">不限</c:if><c:if 
				test="${job.jobDegree==1 }">高中以下</c:if><c:if 
				test="${job.jobDegree==2 }">高中</c:if><c:if 
				test="${job.jobDegree==3 }">中专/技校</c:if><c:if 
				test="${job.jobDegree==4 }">大专</c:if><c:if
				 test="${job.jobDegree==5 }">本科</c:if><c:if 
				 test="${job.jobDegree==6 }">硕士以上 </c:if>
			</span>
		</li>
		<li>
			<span>
				<img src="img/jobAll.png">&nbsp;&nbsp;<c:if  
				test="${job.jobType==1 }">全职</c:if><c:if 
				test="${job.jobType==2 }">兼职</c:if><c:if 
				test="${job.jobType==3 }">应届生</c:if>
			</span>
			<span>
				<img src="img/people.png">&nbsp;&nbsp;${job.jobNumber }人
			</span>
		</li>
		<li>
			<img src="img/address.png">&nbsp;&nbsp;${job.jobAddress}
		</li>
	</ul>
</div>
<div class="postDescription">
	<p>岗位描述</p>
	<p>
		
	     <%
								 CompanyJob companyJob=(CompanyJob)request.getAttribute("job");
								  String msg = companyJob.getJobRequire();
                                  String str = msg.replaceAll("\r\n","<br>");
                                  out.print(str); 
		   %>
	</p>
</div>
<div class="postRequire">
	<p>
		岗位要求
	</p>
	<p>
		<%                    CompanyJob companyJob1=(CompanyJob)request.getAttribute("job");
								  String msg1 = companyJob1.getJobDescribe();
                                  String str1 = msg1.replaceAll("\r\n","<br>");
                                  out.print(str1) ;
                                  %>
	</p>
</div>
<script src="js/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script>

</script>
</body>	
</html>