<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
	<meta charset="utf-8">
	<meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<title>人才库</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	*{
		margin:0;
		padding:0;
	}
	body{
		font-size:14px;
		font-weight:400;
		color:#222;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
		color:#222;
	}
	/*导航栏*/
	nav{
		width:100%;
		height:44px;
		background-color:#ff9137;
		color:#fff;
		line-height:44px;
		font-size:18px;
		text-align:center;
		padding-left:10px;
		padding-right:30px;
	}
	nav a{
		float:left;
	}
	nav img{
		width:20px;
		height:20px;
		vertical-align:middle;
	}
	/*主体内容*/
	input[type=text],select,input[type=password]{
		border:none;
		outline:none;
		background-color:#fff;
		appearance: none;
		-moz-appearance:none;
		-webkit-appearance: none;
		text-align:left;
		height:26px;
		display:inline;
		color:#222;
		width:90%;
	}
    input{
        padding-left:3px;
    }
	select{
		color:#999;
	}
	select::-ms-expand {
		display: none;
	}
	.content{
		margin:0 10px 10px 10px;
		background-color:#fff;
	}
	.rowContent{
		background-color:#fff;
		padding:15px 6px;
		border-bottom:1px solid #ccc;
	}
	label{
		font-weight:400;
		margin-bottom:0;
		color:#666;
	}
    .rowContent .arrowRight{
        width:8px;
        height:16px;
        float:right;
        vertical-align:middle;
        margin-top:4px;
    }
    .rowContent .locationImg{
        width:8px;
        height:16px;
        margin-bottom:4px;
    }
	textarea{
		border:0;
		outline:none;
		resize:none;
		vertical-align:top;
		background-color:#fff;
        padding-left:4px;
		color:#222;
	}
	.rowContent p{
		display:inline-block;
		vertical-align:top;
		margin-bottom:0;
		word-break:break-all;
	}
	.store{
		padding:10px;
	}
	.store .btn{
		width:100%;
		padding-top:6px;
		padding-bottom:6px;
		border:0;
		background-color:#ff9137;
		border-radius: 4px;
		-moz-border-radius: 4px;
		-webkit-border-radius: 4px;
		color:#fff;
		margin-top:10px;
		margin-bottom:20px;
	}
	.select1{
		padding-right:10px;
		width:30%;
	}
	.select2{
		padding-left:10px;
		padding-right:10px;
		width:30%;

	}
	.select3{
		padding-left:10px;
		width:30%;

	}
	/*近期搜索记录*/
	.record{
		position:relative;
		border-top:10px solid #f5f5f5;
	}
	.recordTxt{
		padding:15px 10px 3px 10px;
		font-size:15px;
		border-bottom:1px solid #ccc;
	}
	.glyphicon-trash{
		color:#999;
	}
	.check{
		position:relative;
		height:80px;
		border-bottom:1px solid #ccc;
		margin-left:10px;
		margin-right:10px;
	}
	.check:last-child{
		border:0;
	}
	input[type="checkbox"]{
		-webkit-appearance:none;
		-moz-appearance:none;
		outline:none;
		border:0;
		background:url(img/checkbox-no.png) no-repeat 50% 10px;
		background-size:20px 20px;
		height:40px;
		width:20px;
		position:absolute;
		left:-5px;
		top:10px;
		margin-left:10px;
	}
	input[type="checkbox"]:focus,input[type="checkbox"]:hover{
		background:url(img/checkbox-no.png) no-repeat 50% 50%;
		background-size:20px 20px;
		outline:none;
	}
	input[type="checkbox"]:checked{
		background:url(img/checkbox.png) no-repeat 50% 50%;
		background-size:20px 20px;
		outline:none;
	}
	input[type="checkbox"][disabled]:checked{
		background:url(img/checkbox-no.png) no-repeat 50% 50%;
		background-size:20px 20px;
		outline:none;
	}
	.checkboxContent{
		position:absolute;
		left:35px;
		top:13px;
	}
	.salary{
		position:absolute;
		right:10px;
		top:0;
		color:#fe552c;
	}
	.checkboxContent p:last-child{
		color:#999;
	}




</style>
</head>
<body>
	<!--导航栏-->
	<nav>
		<a href="backstagePages/menu.jsp"><img src="img/back.png"></a>
		人才库
	</nav>
	<!--主体内容-->
	<form method="post" action="resume/queryResumePage.do">
		<div class="content">
			<div class="rowContent" >
				<select class="select1" name="topId" id="topId" onchange="changeTop()">
				 <option value="">全部</option>
	    				<c:forEach items="${tops}" var="top">
	    					<option value="${top.typeId}">
	    						${top.typeName}
	    					</option>
	    			 </c:forEach>
			</select ><img class="locationImg" src="img/arrow-right.png"><select class="select2" name="secondId" id="secondId" onchange="changeSecond()">
				<option value="">全部</option>
			</select ><img class="locationImg" src="img/arrow-right.png"><select class="select3" name="thirdId" id="jobIndustry">
				<option value="">全部</option>
			</select>
				<img class="arrowRight" src="img/arrow-right.png">
			</div>
			<div class="rowContent">
				<select class="select" name="salaryId">
				<option value="">请选择薪资水平</option>
				 <option value="0">薪资面议</option>
                <option value="1">1500元以下/月</option>
                <option value="2">1500-2500元/月</option>
                <option value="3">2500-3500元/月</option>
                <option value="4">3500-5000元/月</option>
                <option value="5">5000-7000元/月</option>
                <option value="6">7000-10000元/月</option>
                <option value="7">10000-15000元/月</option>
                <option value="8">15000元以上/月</option>
			</select>
				<img class="arrowRight" src="img/arrow-right.png">
			</div>
			<div class="rowContent">
				<select class="select" name="highDegree">
				<option value="">请选择学历</option>
			    <option value="">不限</option>
                <option value="1">高中以下</option>
                <option value="2">高中</option>
                <option value="3">中专/技校</option>
                <option value="4">大专</option>
                <option value="5">本科</option>
                <option value="6">硕士以上</option>
			</select>
				<img class="arrowRight" src="img/arrow-right.png">
			</div>
			<div class="rowContent">
				<select class="select" name="workYear">
                    <option value="">请选择工作年限</option>
                <option value="">不限</option>
                <option value="1">一年以下</option>
                <option value="2">1-2年</option>
                <option value="3">3-5年</option>
                <option value="4">5-10年</option>
                <option value="5">10年以上</option>
                </select>
                <img class="arrowRight" src="img/arrow-right.png">
			</div>
            <div class="rowContent">
                <select class="select" name="sex">
                <option value="">请选择性别</option>
                <option value="1">男</option>
                <option value="0">女</option>
            </select>
                <img class="arrowRight" src="img/arrow-right.png">
            </div>
        </div>
		<div class="store">
			<button class="btn" id="edit" type="submit">搜索</button>
		</div>
	</form>
	<div class="record">
		<div class="recordTxt">
			近期搜索记录
			<span class="glyphicon glyphicon-trash pull-right"></span>
		</div>
		<!--循环start-->
		<c:forEach items="${recordList }" varStatus="status" var="recordList" begin="0" end="5">
		<div class="check">
		<input type="checkbox" name="record" value="${recordList.recordId }">
			<a href="resume/queryResumePage.do?thirdId=${recordList.intentJob }&salaryId=${recordList.salaryId}&highDegree=${resumeList.highDegree }&workYear=${resumeList.workYear }&sex=${resumeList.sex}">			
				<div class="checkboxContent">
					<p><c:forEach items="${intentJob }" var="intentJob">
					    <c:if test="${intentJob.typeId == recordList.intentJob}">
					         ${intentJob.typeName }
					    </c:if>
					</c:forEach><span class="salary"><c:if test="${recordList.salaryId==null }">不限</c:if><c:if 
					test="${recordList.salaryId==0 }">薪资面议</c:if><c:if
					 test="${recordList.salaryId==1 }">1500元以下/月</c:if><c:if 
					 test="${recordList.salaryId==2 }">1500-2500元/月</c:if><c:if
					 test="${recordList.salaryId==3 }">2500-3500元/月</c:if><c:if 
					 test="${recordList.salaryId==4 }">3500-5000元/月</c:if><c:if 
					 test="${recordList.salaryId==5 }">5000-7000元/月</c:if><c:if 
					 test="${recordList.salaryId==6 }">7000-10000元/月</c:if><c:if 
					 test="${recordList.salaryId==7 }">10000-15000元/月</c:if><c:if 
					 test="${recordList.salaryId==8 }">15000元以上/月</c:if></span></p>
					<p><c:if test="${recordList.sex==null }">不限</c:if><c:if 
					test="${recordList.sex==1 }">男</c:if><c:if 
					test="${recordList.sex==0 }">女</c:if>&nbsp;|&nbsp;<c:if 
					test="${recordList.workYear==null }">不限</c:if><c:if 
					test="${recordList.workYear==1 }">一年以下</c:if><c:if
					 test="${recordList.workYear==2 }">1-2年</c:if><c:if 
					 test="${recordList.workYear==3 }">3-5年</c:if><c:if
					  test="${recordList.workYear==4 }">5-10年</c:if><c:if 
					  test="${recordList.workYear==5 }">10年以上</c:if>&nbsp;|&nbsp;<c:if
					   test="${recordList.highDegree==null }">不限</c:if><c:if
					   test="${recordList.highDegree==1 }">高中以下</c:if><c:if
					    test="${recordList.highDegree==2 }">高中</c:if><c:if 
					    test="${recordList.highDegree==3 }">中专/技校</c:if><c:if
					     test="${recordList.highDegree==4 }">大专</c:if><c:if 
					     test="${recordList.highDegree==5 }">本科</c:if><c:if 
					     test="${recordList.highDegree==6 }">硕士以上</c:if></p>
				</div>
			</a>
		</div>
		</c:forEach>
		<!--循环end-->
	</div>

<script src="js/jquery-2.2.3.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script>
	$(function(){
		$('select').focus(function(){
			$(this).css('color','#222');
		});
		var w=$('body').width()-50;
		$('.checkboxContent').width(w);
		$('.glyphicon-trash').click(function(){
			text = $("input:checkbox[name='record']:checked").map(function(index,elem) {
				return $(elem).val();
			}).get().join(',');
			
           $.post('resume/delRecord.do',{records:text},function(data){
        	   if(data.res){
        		   alert(data.msg);
        		   location.reload();   
        	   }
        	   
           },'json')
		})
	})
	



</script>
<script type="text/javascript">
function changeTop(){
	var topId = $("#topId").val();
	var second = $("#secondId");
	var third = $("#jobIndustry");
	   if(topId==""){
    	   second.html("<option value=\"\">全部</option>");
   		   third.html("<option value=\"\">全部</option>");
		}else{
			second.html("");
			third.html("");
		}
	
	if(topId == ""){
		return;
	}
	
	$.post("type/changeTopType.do", {topId:topId}, function(data){
	  
		if(data == null || data == ""){
			return;
		}
		
		second.append("<option value=\"" + data[0].typeId + "\"selected=selected>" + data[0].typeName + "</option>");
		second.val(data[0].typeId);
		for(var o in data){
			if(o!=0){
			second.append("<option value=\"" + data[o].typeId + "\">" + data[o].typeName + "</option>");
			}
		}
		second.trigger("onchange");
		
	}, 'json');	
	
	
	
}


function changeSecond(){

	var secondId = $("#secondId").val();
	var third = $("#jobIndustry");
	if(secondId==""){
		third.html("<option value=\"\">全部</option>");
	}else{
		third.html("");
	}
	if(secondId == ""){
		return;
	}
	
	$.post("type/changeSecondType.do", {secondId:secondId}, function(data){
		if(data == null || data == ""){
			return;
		}
		third.append("<option value=\"" + data[0].typeId + "\"selected=selected>" + data[0].typeName + "</option>");
		for(var o in data){
			if(o!=0){
			third.append("<option value=\"" + data[o].typeId + "\">" + data[o].typeName + "</option>");
			}
		}
	}, 'json');
	
}
</script>
</body>	
</html>