package com.job.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DataGrid implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2340613656268195659L;

	private int total;
	
	private List rows = new ArrayList();
	


	public DataGrid() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DataGrid(int total, List rows) {
		super();
		this.total = total;
		this.rows = rows;
	}

	public List getRows() {
		return rows;
	}

	public void setRows(List rows) {
		this.rows = rows;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

}
