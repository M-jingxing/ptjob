package com.job.model;

import java.util.List;

public class CityModel {

	/**
	 * 城市ID
	 */
	private Integer cityId;
	
	/**
	 * 城市名称
	 */
	private String cityName;
	
	/**
	 * 区域列表
	 */
	private List<AreaModel> list;

	public List<AreaModel> getList() {
		return list;
	}

	public void setList(List<AreaModel> list) {
		this.list = list;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
}
