package com.job.model;

public class JobInCompany {
	 private Integer jobId;
	 private Integer companyId;
	 private String jobName;
	 private String companyName;
	 private String companyContractPerson;
	 private String companyContractMobile;
	 private Integer jobSalaryId;
	 private String jobAddress;
	 private String companyAddress;
	 private String jobRequire;
	 private String jobDegree;
	 private String jobDescribe;
	 private String companyLicense;
	 private Double distance;
	
	
 	public Double getDistance() {
		return distance;
	}
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	public String getCompanyLicense() {
		return companyLicense;
	}
	public void setCompanyLicense(String companyLicense) {
		this.companyLicense = companyLicense;
	}
	public JobInCompany(Integer jobId, Integer companyId, String jobName,
			String companyName, Integer jobSalaryId, String jobAddress) {
		super();
		this.jobId = jobId;
		this.companyId = companyId;
		this.jobName = jobName;
		this.companyName = companyName;
		this.jobSalaryId = jobSalaryId;
		this.jobAddress = jobAddress;
	}
	public JobInCompany(Integer jobId, Integer companyId, String jobName,
			String companyName, String jobDescribe) {
		super();
		this.jobId = jobId;
		this.companyId = companyId;
		this.jobName = jobName;
		this.companyName = companyName;
		this.jobDescribe = jobDescribe;
	}
	public JobInCompany(Integer jobId, Integer companyId, String jobName,
			String companyName, String companyContractPerson,
			String companyContractMobile, Integer jobSalaryId, String jobAddress,
			String companyAddress, String jobRequire, String jobDegree,
			String jobDescribe) {
		super();
		this.jobId = jobId;
		this.companyId = companyId;
		this.jobName = jobName;
		this.companyName = companyName;
		this.companyContractPerson = companyContractPerson;
		this.companyContractMobile = companyContractMobile;
		this.jobSalaryId = jobSalaryId;
		this.jobAddress = jobAddress;
		this.companyAddress = companyAddress;
		this.jobRequire = jobRequire;
		this.jobDegree = jobDegree;
		this.jobDescribe = jobDescribe;
	}
	public String getCompanyContractPerson() {
		return companyContractPerson;
	}
	public void setCompanyContractPerson(String companyContractPerson) {
		this.companyContractPerson = companyContractPerson;
	}
	public String getCompanyContractMobile() {
		return companyContractMobile;
	}
	public void setCompanyContractMobile(String companyContractMobile) {
		this.companyContractMobile = companyContractMobile;
	}
	public Integer getJobSalaryId() {
		return jobSalaryId;
	}
	public void setJobSalaryId(Integer jobSalaryId) {
		this.jobSalaryId = jobSalaryId;
	}
	public String getJobAddress() {
		return jobAddress;
	}
	public void setJobAddress(String jobAddress) {
		this.jobAddress = jobAddress;
	}
	public String getCompanyAddress() {
		return companyAddress;
	}
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	public String getJobRequire() {
		return jobRequire;
	}
	public void setJobRequire(String jobRequire) {
		this.jobRequire = jobRequire;
	}
	public String getJobDegree() {
		return jobDegree;
	}
	public void setJobDegree(String jobDegree) {
		this.jobDegree = jobDegree;
	}
	public String getJobDescribe() {
		return jobDescribe;
	}
	public void setJobDescribe(String jobDescribe) {
		this.jobDescribe = jobDescribe;
	}
	public JobInCompany(String jobName, String companyName, Integer jobId,
			Integer companyId) {
		super();
		this.jobName = jobName;
		this.companyName = companyName;
		this.jobId = jobId;
		this.companyId = companyId;
	}
	public Integer getJobId() {
		return jobId;
	}
	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public JobInCompany() {
		super();
		// TODO Auto-generated constructor stub
	}
	public JobInCompany(String jobName, String companyName) {
		super();
		this.jobName = jobName;
		this.companyName = companyName;
	}
	 
}
