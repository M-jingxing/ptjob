package com.job.model;

import java.util.Date;

public class CompanyImformation {
    private Integer companyId;

    private String companyName;

    private String companyDescribe;

    private String companyAddress;

    private Double companyLongitude;

    private Double companyLatitude;

    private Integer companyScale;

    private Integer companyNature;

    private Date createTime;

    private Date modifyTime;

    private String companyContractPerson;

    private String companyContractMobile;
    
  
    private String companyLicense;
    
    private Integer messageAlert;
    private String messageEmail;
    private String messageMobile;
    private String messageWechat;
    
    
    
  

	public CompanyImformation(String companyName, String companyDescribe,
			String companyAddress, Integer companyScale, Integer companyNature,
			String companyContractPerson, String companyContractMobile,
			String companyLicense,
			Integer messageAlert, String messageEmail, String messageMobile,
			String messageWechat) {
		super();
		
		this.companyName = companyName;
		this.companyDescribe = companyDescribe;
		this.companyAddress = companyAddress;
		this.companyScale = companyScale;
		this.companyNature = companyNature;
		this.companyContractPerson = companyContractPerson;
		this.companyContractMobile = companyContractMobile;
		this.companyLicense = companyLicense;
		this.messageAlert = messageAlert;
		this.messageEmail = messageEmail;
		this.messageMobile = messageMobile;
		this.messageWechat = messageWechat;
	}

	public Integer getMessageAlert() {
		return messageAlert;
	}

	public void setMessageAlert(Integer messageAlert) {
		this.messageAlert = messageAlert;
	}

	public String getMessageEmail() {
		return messageEmail;
	}

	public void setMessageEmail(String messageEmail) {
		this.messageEmail = messageEmail;
	}

	public String getMessageMobile() {
		return messageMobile;
	}

	public void setMessageMobile(String messageMobile) {
		this.messageMobile = messageMobile;
	}

	public String getMessageWechat() {
		return messageWechat;
	}

	public void setMessageWechat(String messageWechat) {
		this.messageWechat = messageWechat;
	}

	public CompanyImformation() {
		super();
	}

	public CompanyImformation(String companyName, String companyDescribe,
			String companyAddress, Integer companyScale, Integer companyNature,
			String companyContractPerson, String companyContractMobile,
			String companyLicense) {
		super();
		this.companyName = companyName;
		this.companyDescribe = companyDescribe;
		this.companyAddress = companyAddress;
		this.companyScale = companyScale;
		this.companyNature = companyNature;
		this.companyContractPerson = companyContractPerson;
		this.companyContractMobile = companyContractMobile;
		this.companyLicense = companyLicense;
	}

	public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    public String getCompanyDescribe() {
        return companyDescribe;
    }

    public void setCompanyDescribe(String companyDescribe) {
        this.companyDescribe = companyDescribe == null ? null : companyDescribe.trim();
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress == null ? null : companyAddress.trim();
    }

    public Double getCompanyLongitude() {
        return companyLongitude;
    }

    public void setCompanyLongitude(Double companyLongitude) {
        this.companyLongitude = companyLongitude;
    }

    public Double getCompanyLatitude() {
        return companyLatitude;
    }

    public void setCompanyLatitude(Double companyLatitude) {
        this.companyLatitude = companyLatitude;
    }

    public Integer getCompanyScale() {
        return companyScale;
    }

    public void setCompanyScale(Integer companyScale) {
        this.companyScale = companyScale;
    }

    public Integer getCompanyNature() {
        return companyNature;
    }

    public void setCompanyNature(Integer companyNature) {
        this.companyNature = companyNature;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getCompanyContractPerson() {
        return companyContractPerson;
    }

    public void setCompanyContractPerson(String companyContractPerson) {
        this.companyContractPerson = companyContractPerson == null ? null : companyContractPerson.trim();
    }

    public String getCompanyContractMobile() {
        return companyContractMobile;
    }

    public void setCompanyContractMobile(String companyContractMobile) {
        this.companyContractMobile = companyContractMobile == null ? null : companyContractMobile.trim();
    }

	public String getCompanyLicense() {
		return companyLicense;
	}

	public void setCompanyLicense(String companyLicense) {
		this.companyLicense = companyLicense;
	}
}