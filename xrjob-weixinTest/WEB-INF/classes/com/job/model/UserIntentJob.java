package com.job.model;

public class UserIntentJob {

	private Integer intentId;

    private Integer resumeId;

    private Integer intentJobId;
    
    private String intentJobName;

    public Integer getIntentId() {
        return intentId;
    }

    public void setIntentId(Integer intentId) {
        this.intentId = intentId;
    }

    public Integer getResumeId() {
        return resumeId;
    }

    public void setResumeId(Integer resumeId) {
        this.resumeId = resumeId;
    }

    public Integer getIntentJobId() {
        return intentJobId;
    }

    public void setIntentJobId(Integer intentJobId) {
        this.intentJobId = intentJobId;
    }

	public String getIntentJobName() {
		return intentJobName;
	}

	public void setIntentJobName(String intentJobName) {
		this.intentJobName = intentJobName;
	}
}