package com.job.model;

/**
 * 返回结果model
 *
 */
public class Json {

	private String msg;//返回信息
	
	private boolean res;//返回结果
	
	private Integer status;
	
	private boolean upPic;
	
	private Integer returnValue;
	
	public Json(){
		res = false;
	}
  

   
	public Integer getReturnValue() {
		return returnValue;
	}



	public void setReturnValue(Integer returnValue) {
		this.returnValue = returnValue;
	}



	public boolean isUpPic() {
		return upPic;
	}

	public void setUpPic(boolean upPic) {
		this.upPic = upPic;
	}


	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public boolean isRes() {
		return res;
	}

	public void setRes(boolean res) {
		this.res = res;
	}
	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
