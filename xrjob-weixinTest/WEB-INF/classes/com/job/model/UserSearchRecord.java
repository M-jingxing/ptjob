package com.job.model;

import java.util.Date;

public class UserSearchRecord {
   private Integer recordId;
   private Integer userId;
   private String searchRecord;
   private Date createTime;
   private Integer intentJob;
   private Integer salaryId;
   private Integer sex;
   private Integer workYear;
   private Integer highDegree;
   private Integer recordType;
   
   
public UserSearchRecord() {
	super();
	// TODO Auto-generated constructor stub
}

public Integer getIntentJob() {
	return intentJob;
}

public void setIntentJob(Integer intentJob) {
	this.intentJob = intentJob;
}

public Integer getSalaryId() {
	return salaryId;
}

public void setSalaryId(Integer salaryId) {
	this.salaryId = salaryId;
}

public Integer getSex() {
	return sex;
}

public void setSex(Integer sex) {
	this.sex = sex;
}

public Integer getWorkYear() {
	return workYear;
}

public void setWorkYear(Integer workYear) {
	this.workYear = workYear;
}

public Integer getHighDegree() {
	return highDegree;
}

public void setHighDegree(Integer highDegree) {
	this.highDegree = highDegree;
}

public Integer getRecordType() {
	return recordType;
}

public void setRecordType(Integer recordType) {
	this.recordType = recordType;
}

public Integer getRecordId() {
	return recordId;
}
public void setRecordId(Integer recordId) {
	this.recordId = recordId;
}
public Integer getUserId() {
	return userId;
}
public void setUserId(Integer userId) {
	this.userId = userId;
}
public String getSearchRecord() {
	return searchRecord;
}
public void setSearchRecord(String searchRecord) {
	this.searchRecord = searchRecord;
}
public Date getCreateTime() {
	return createTime;
}
public void setCreateTime(Date createTime) {
	this.createTime = createTime;
}
   
   
   
   
}
