package com.job.model;

import java.util.Date;

public class JobComplain {
  private Integer complainId;
  private String complainContent;
  private Integer userId;
  private String userContract;
  private Integer objId;
  private Integer objType;
  private Date createTime;
  private Integer objMsg;
public Integer getComplainId() {
	return complainId;
}
public void setComplainId(Integer complainId) {
	this.complainId = complainId;
}
public String getComplainContent() {
	return complainContent;
}
public void setComplainContent(String complainContent) {
	this.complainContent = complainContent;
}
public Integer getUserId() {
	return userId;
}
public void setUserId(Integer userId) {
	this.userId = userId;
}
public String getUserContract() {
	return userContract;
}
public void setUserContract(String userContract) {
	this.userContract = userContract;
}
public Integer getObjId() {
	return objId;
}
public void setObjId(Integer objId) {
	this.objId = objId;
}
public Integer getObjType() {
	return objType;
}
public void setObjType(Integer objType) {
	this.objType = objType;
}
public Date getCreateTime() {
	return createTime;
}
public void setCreateTime(Date createTime) {
	this.createTime = createTime;
}
public Integer getObjMsg() {
	return objMsg;
}
public void setObjMsg(Integer objMsg) {
	this.objMsg = objMsg;
}
  
  
}
