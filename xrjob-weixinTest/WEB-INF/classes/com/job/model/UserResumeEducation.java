package com.job.model;

import java.util.Date;

public class UserResumeEducation {
    private Integer educationId;

    private Integer resumeId;

    private String startDate;

    private String endDate;

    private String schoolName;

    private String professionalName;

    private Integer educationGrade;

    private Date createTime;

    private Date modifyTime;

    private String professionalDescribe;
    
    public Integer getEducationId() {
        return educationId;
    }

    public void setEducationId(Integer educationId) {
        this.educationId = educationId;
    }

    public Integer getResumeId() {
        return resumeId;
    }

    public void setResumeId(Integer resumeId) {
        this.resumeId = resumeId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate == null ? null : startDate.trim();
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate == null ? null : endDate.trim();
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName == null ? null : schoolName.trim();
    }

    public String getProfessionalName() {
        return professionalName;
    }

    public void setProfessionalName(String professionalName) {
        this.professionalName = professionalName == null ? null : professionalName.trim();
    }

    public Integer getEducationGrade() {
        return educationGrade;
    }

    public void setEducationGrade(Integer educationGrade) {
        this.educationGrade = educationGrade;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

	public String getProfessionalDescribe() {
		return professionalDescribe;
	}

	public void setProfessionalDescribe(String professionalDescribe) {
		this.professionalDescribe = professionalDescribe;
	}
    
}