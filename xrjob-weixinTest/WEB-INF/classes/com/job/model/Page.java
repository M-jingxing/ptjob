package com.job.model;

public class Page {

	private Integer page;
	
	private Integer rows;
	
	private Integer startIndex;

	public Integer getStartIndex() {
		if(null == page)
			page = 1;
		if(null == rows)
			rows = 10;
		return (page - 1) * rows;
	}

	public void setStartIndex(Integer startIndex) {
		this.startIndex = startIndex;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}
}
