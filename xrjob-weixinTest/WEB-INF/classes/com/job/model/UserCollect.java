package com.job.model;

import java.util.Date;

public class UserCollect {
   private Integer collectId;
   private Integer userId;
   private Integer jobId;
   private Date createTime;
   private Integer collectType;
   private String jobName;
   private Integer jobSalaryId;
   private String companyName;
   private String companyAddress;
   private String jobAddress;
   
   
public String getJobName() {
	return jobName;
}
public void setJobName(String jobName) {
	this.jobName = jobName;
}
public Integer getJobSalaryId() {
	return jobSalaryId;
}
public void setJobSalaryId(Integer jobSalaryId) {
	this.jobSalaryId = jobSalaryId;
}
public String getCompanyName() {
	return companyName;
}
public void setCompanyName(String companyName) {
	this.companyName = companyName;
}
public String getCompanyAddress() {
	return companyAddress;
}
public void setCompanyAddress(String companyAddress) {
	this.companyAddress = companyAddress;
}
public String getJobAddress() {
	return jobAddress;
}
public void setJobAddress(String jobAddress) {
	this.jobAddress = jobAddress;
}
public Integer getCollectId() {
	return collectId;
}
public void setCollectId(Integer collectId) {
	this.collectId = collectId;
}
public Integer getUserId() {
	return userId;
}
public void setUserId(Integer userId) {
	this.userId = userId;
}
public Integer getJobId() {
	return jobId;
}
public void setJobId(Integer jobId) {
	this.jobId = jobId;
}
public Date getCreateTime() {
	return createTime;
}
public void setCreateTime(Date createTime) {
	this.createTime = createTime;
}
public Integer getCollectType() {
	return collectType;
}
public void setCollectType(Integer collectType) {
	this.collectType = collectType;
}
   
}
