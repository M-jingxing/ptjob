package com.job.model;

import java.util.Date;

public class User {
	private Integer userId;
	private String userMobile;
	private String userName;
	private String password;
	private Integer sex;
	private Date createTime;
	private Date modifyTime;
	private Integer userStatus;
	private Date lastLoginTime;
	private String intentJob;
	private String registerUniqueCode;
	private String lastLoginUniqueCode;
	private String userLoginToken;
	private String userPic;
	private String openId;
	
	
	
	 public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public Integer getUserId() {
	     return userId;
	 }

	 public void setUserId(Integer userId) {
	     this.userId = userId;
	 }
	 
	 public String getUserName() {
	     return userName;
	 }

	 public void setUserName(String userName) {
	     this.userName = userName== null ? null : userName.trim();
	 }
	 
	 public String getUserMobile() {
	     return userMobile;
	 }

	 public void setUserMobile(String userMobile) {
	     this.userMobile = userMobile;
	 }
	 
	 public String getPassword() {
	     return password;
	 }

	 public void setPassword(String password) {
	     this.password = password == null ? null : password.trim();
	 }
	 
	 public Integer getSex() {
	     return sex;
	 }

	 public void setSex(Integer sex) {
	     this.sex = sex;
	 }	
	 
	 public Date getCreateTime() {
	     return createTime;
	 }

	 public void setCreateTime(Date createTime) {
	     this.createTime = createTime;
	 }

	 public Date getModifyTime() {
	     return modifyTime;
	 }

	 public void setModifyTime(Date modifyTime) {
	     this.modifyTime = modifyTime;
	 }
	 
	 public Integer getUserStatus() {
	     return userStatus;
	 }

	 public void setUserStatus(Integer userStatus) {
	     this.userStatus = userStatus;
	 }
	 
	 public Date getLastLoginTime() {
	     return lastLoginTime;
	 }

	 public void setLastLoginTime(Date lastLoginTime) {
	     this.lastLoginTime = lastLoginTime;
	 }
	 
	 public String getIntentJob() {
	     return intentJob;
	 }

	 public void setIntentJob(String intentJob) {
	     this.intentJob = intentJob;
	 }
	 
	 public String getRegisterUniqueCode() {
	     return registerUniqueCode;
	 }

	 public void setRegisterUniqueCode(String registerUniqueCode) {
	     this.registerUniqueCode = registerUniqueCode;
	 }
	 
	 public String getLastLoginUniqueCode() {
	     return lastLoginUniqueCode;
	 }

	 public void setLgastLoginUniqueCode(String lastLoginUniqueCode) {
	     this.lastLoginUniqueCode = lastLoginUniqueCode;
	 }
	 
	 public String getUserLoginToken() {
	     return userLoginToken;
	 }

	 public void setUserLoginToken(String userLoginToken) {
	     this.userLoginToken= userLoginToken;
	 }
	 
	 public String getUserPic() {
	     return userPic;
	 }

	 public void setUserPic(String userPic) {
	     this.userPic= userPic;
	 }

}
