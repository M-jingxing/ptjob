package com.job.model;

import java.util.Date;
import java.util.List;

public class UserResume {
    private Integer resumeId;

    private Integer userId;

    private String resumeName;

    private Date createTime;

    private Date modifyTime;

    private String resumePic;

    private String resumeUserName;

    private Integer sex;

    private Integer highDegree;

    private Integer workYear;

    private Date birthDate;

    private Integer liveAreaId;

    private String userMobile;

    private String userEmail;

    private Integer jobStatus;

    private String intentJob;

    private Integer toJobDate;

    private Integer jobType;

    private Integer intentCityId;

    private Integer salaryId;

    private String userDescribe;

    private Integer resumeVisibility;
    
    private String typeName;
    
    private List<UserResumeEducation> educationList;
    
    private List<UserResumeWork> workList;
    
    private List<UserIntentJob> intentJobList;
    
  
	private String liveAreaName;
	
	
	private String intentCityName;
	
	
	private String originPlace;

	 
    public Integer getResumeId() {
        return resumeId;
    }

    public void setResumeId(Integer resumeId) {
        this.resumeId = resumeId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getResumeName() {
        return resumeName;
    }

    public void setResumeName(String resumeName) {
        this.resumeName = resumeName == null ? null : resumeName.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getResumePic() {
        return resumePic;
    }

    public void setResumePic(String resumePic) {
        this.resumePic = resumePic == null ? null : resumePic.trim();
    }

    public String getResumeUserName() {
        return resumeUserName;
    }

    public void setResumeUserName(String resumeUserName) {
        this.resumeUserName = resumeUserName == null ? null : resumeUserName.trim();
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getHighDegree() {
        return highDegree;
    }

    public void setHighDegree(Integer highDegree) {
        this.highDegree = highDegree;
    }

    public Integer getWorkYear() {
        return workYear;
    }

    public void setWorkYear(Integer workYear) {
        this.workYear = workYear;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getLiveAreaId() {
        return liveAreaId;
    }

    public void setLiveAreaId(Integer liveAreaId) {
        this.liveAreaId = liveAreaId;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile == null ? null : userMobile.trim();
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail == null ? null : userEmail.trim();
    }

    public Integer getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(Integer jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getIntentJob() {
        return intentJob;
    }

    public void setIntentJob(String intentJob) {
        this.intentJob = intentJob == null ? null : intentJob.trim();
    }

    public Integer getToJobDate() {
        return toJobDate;
    }

    public void setToJobDate(Integer toJobDate) {
        this.toJobDate = toJobDate;
    }

    public Integer getJobType() {
        return jobType;
    }

    public void setJobType(Integer jobType) {
        this.jobType = jobType;
    }

    public Integer getIntentCityId() {
        return intentCityId;
    }

    public void setIntentCityId(Integer intentCityId) {
        this.intentCityId = intentCityId;
    }

    public Integer getSalaryId() {
        return salaryId;
    }

    public void setSalaryId(Integer salaryId) {
        this.salaryId = salaryId;
    }

    public String getUserDescribe() {
        return userDescribe;
    }

    public void setUserDescribe(String userDescribe) {
        this.userDescribe = userDescribe == null ? null : userDescribe.trim();
    }

    public Integer getResumeVisibility() {
        return resumeVisibility;
    }

    public void setResumeVisibility(Integer resumeVisibility) {
        this.resumeVisibility = resumeVisibility;
    }

	public List<UserResumeEducation> getEducationList() {
		return educationList;
	}

	public void setEducationList(List<UserResumeEducation> educationList) {
		this.educationList = educationList;
	}

	public List<UserResumeWork> getWorkList() {
		return workList;
	}

	public void setWorkList(List<UserResumeWork> workList) {
		this.workList = workList;
	}

	public List<UserIntentJob> getIntentJobList() {
		return intentJobList;
	}

	public void setIntentJobList(List<UserIntentJob> intentJobList) {
		this.intentJobList = intentJobList;
	}

	public String getLiveAreaName() {
		return liveAreaName;
	}

	public void setLiveAreaName(String liveAreaName) {
		this.liveAreaName = liveAreaName;
	}

	public String getIntentCityName() {
		return intentCityName;
	}

	public void setIntentCityName(String intentCityName) {
		this.intentCityName = intentCityName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getOriginPlace() {
		return originPlace;
	}

	public void setOriginPlace(String originPlace) {
		this.originPlace = originPlace;
	}

	
	
}