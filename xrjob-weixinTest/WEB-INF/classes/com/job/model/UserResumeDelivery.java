package com.job.model;

import java.util.Date;

public class UserResumeDelivery {
    private Integer deliveryId;

    private Integer resumeId;

    private Integer jobId;

    private Date createTime;

    private Date browseTime;

    private Integer deliveryStatus;

    private Integer userId;
    
    private String jobName;

    private Integer jobSalaryId;
    
    private String companyName;
 
    
    
    public Integer getJobSalaryId() {
		return jobSalaryId;
	}

	public void setJobSalaryId(Integer jobSalaryId) {
		this.jobSalaryId = jobSalaryId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Integer getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(Integer deliveryId) {
        this.deliveryId = deliveryId;
    }

    public Integer getResumeId() {
        return resumeId;
    }

    public void setResumeId(Integer resumeId) {
        this.resumeId = resumeId;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getBrowseTime() {
        return browseTime;
    }

    public void setBrowseTime(Date browseTime) {
        this.browseTime = browseTime;
    }

    public Integer getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(Integer deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public UserResumeDelivery(Integer deliveryId, Integer resumeId,
			Integer jobId, Date createTime, Date browseTime,
			Integer deliveryStatus, Integer userId, String jobName) {
		super();
		this.deliveryId = deliveryId;
		this.resumeId = resumeId;
		this.jobId = jobId;
		this.createTime = createTime;
		this.browseTime = browseTime;
		this.deliveryStatus = deliveryStatus;
		this.userId = userId;
		this.jobName = jobName;
	}

	public UserResumeDelivery() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}