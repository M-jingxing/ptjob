package com.job.model;

import java.util.Date;

public class CompanyJob {
    private Integer jobId;

    private Integer companyId;

    private String jobName;

    private String jobNumber;

    private Integer jobDegree;

    private Integer jobAreaId;

    private String jobDescribe;

    private String jobRequire;

    private Integer jobWorkYear;

    private Integer jobSalaryId;

    private Date createTime;

    private String jobAddress;

    private Date modifyTime;

    private Integer jobStatus;

    private Integer jobType;

    private Integer jobIndustry;
    private Integer jobStick;
    private Integer jobHot;
    private Integer secondId;
    
    
    private Integer topId;
    
    
    private Integer cityId;

    private String companyName;
    private String companyContractPerson;
    private String companyContractMobile;
    private String companyLicense;
   
    
    public Integer getJobHot() {
		return jobHot;
	}

	public void setJobHot(Integer jobHot) {
		this.jobHot = jobHot;
	}

	public Integer getJobStick() {
		return jobStick;
	}

	public void setJobStick(Integer jobStick) {
		this.jobStick = jobStick;
	}

	public String getCompanyLicense() {
		return companyLicense;
	}

	public void setCompanyLicense(String companyLicense) {
		this.companyLicense = companyLicense;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyContractPerson() {
		return companyContractPerson;
	}

	public void setCompanyContractPerson(String companyContractPerson) {
		this.companyContractPerson = companyContractPerson;
	}

	public String getCompanyContractMobile() {
		return companyContractMobile;
	}

	public void setCompanyContractMobile(String companyContractMobile) {
		this.companyContractMobile = companyContractMobile;
	}

	public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName == null ? null : jobName.trim();
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber == null ? null : jobNumber.trim();
    }

    public Integer getJobDegree() {
        return jobDegree;
    }

    public void setJobDegree(Integer jobDegree) {
        this.jobDegree = jobDegree;
    }

    public Integer getJobAreaId() {
        return jobAreaId;
    }

    public void setJobAreaId(Integer jobAreaId) {
        this.jobAreaId = jobAreaId;
    }

    public String getJobDescribe() {
        return jobDescribe;
    }

    public void setJobDescribe(String jobDescribe) {
        this.jobDescribe = jobDescribe == null ? null : jobDescribe.trim();
    }

    public String getJobRequire() {
        return jobRequire;
    }

    public void setJobRequire(String jobRequire) {
        this.jobRequire = jobRequire == null ? null : jobRequire.trim();
    }

    public Integer getJobWorkYear() {
        return jobWorkYear;
    }

    public void setJobWorkYear(Integer jobWorkYear) {
        this.jobWorkYear = jobWorkYear;
    }

    public Integer getJobSalaryId() {
        return jobSalaryId;
    }

    public void setJobSalaryId(Integer jobSalaryId) {
        this.jobSalaryId = jobSalaryId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getJobAddress() {
        return jobAddress;
    }

    public void setJobAddress(String jobAddress) {
        this.jobAddress = jobAddress == null ? null : jobAddress.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Integer getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(Integer jobStatus) {
        this.jobStatus = jobStatus;
    }

    public Integer getJobType() {
        return jobType;
    }

    public void setJobType(Integer jobType) {
        this.jobType = jobType;
    }

    public Integer getJobIndustry() {
        return jobIndustry;
    }

    public void setJobIndustry(Integer jobIndustry) {
        this.jobIndustry = jobIndustry;
    }

	public Integer getSecondId() {
		return secondId;
	}

	public void setSecondId(Integer secondId) {
		this.secondId = secondId;
	}

	public Integer getTopId() {
		return topId;
	}

	public void setTopId(Integer topId) {
		this.topId = topId;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}
}