package com.job.model;

import java.util.Date;

public class JobFair {
	private Integer fairId;
	private String fairName;
	private String fairContent;
	private String fairTime;
	private String fairContractPerson;
	private String fairContractTel;
	private Integer cityId;
	private Integer areaId;
	private String fairAddress;
	private Date createTime;
	private Date modifyTime;

	public Integer getFairId() {
		return fairId;
	}

	public void setFairId(Integer fairId) {
		this.fairId = fairId;
	}

	public String getFairName() {
		return fairName;
	}

	public void setFairName(String fairName) {
		this.fairName = fairName;
	}

	public String getFairContent() {
		return fairContent;
	}

	public void setFairContent(String fairContent) {
		this.fairContent = fairContent;
	}

	public String getFairTime() {
		return fairTime;
	}

	public void setFairTime(String fairTime) {
		this.fairTime = fairTime;
	}

	public String getFairContractPerson() {
		return fairContractPerson;
	}

	public void setFairContractPerson(String fairContractPerson) {
		this.fairContractPerson = fairContractPerson;
	}

	public String getFairContractTel() {
		return fairContractTel;
	}

	public void setFairContractTel(String fairContractTel) {
		this.fairContractTel = fairContractTel;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public String getFairAddress() {
		return fairAddress;
	}

	public void setFairAddress(String fairAddress) {
		this.fairAddress = fairAddress;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public JobFair(Integer fairId, String fairName, String fairContent,
			String fairTime, String fairContractPerson, String fairContractTel,
			Integer cityId, Integer areaId, String fairAddress,
			Date createTime, Date modifyTime) {
		super();
		this.fairId = fairId;
		this.fairName = fairName;
		this.fairContent = fairContent;
		this.fairTime = fairTime;
		this.fairContractPerson = fairContractPerson;
		this.fairContractTel = fairContractTel;
		this.cityId = cityId;
		this.areaId = areaId;
		this.fairAddress = fairAddress;
		this.createTime = createTime;
		this.modifyTime = modifyTime;
	}

	public JobFair() {
		super();
		// TODO Auto-generated constructor stub
	}

}
