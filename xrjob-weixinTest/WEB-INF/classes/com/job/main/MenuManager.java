package com.job.main;

 
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;  
import org.slf4j.LoggerFactory;  

import com.job.util.WeixinUtil;
import com.job.weixinModel.Button;
import com.job.weixinModel.CommonButton;
import com.job.weixinModel.ComplexButton;
import com.job.weixinModel.Menu;
import com.job.weixinModel.Token;
  
/** 
 * 菜单管理器类 
 *  
 */  
public class MenuManager {  
    private static Logger log = LoggerFactory.getLogger(MenuManager.class);  

    // 第三方用户唯一凭证  
    private static String appId = "wxdf131e71d1194db5";  
    // 第三方用户唯一凭证密钥  
    private static String appSecret = "fa56295304a97bac49ee8b60aa290c14"; 
    
    public static void main(String[] args) {   
  
        // 调用接口获取access_token  
        Token at = WeixinUtil.getAccessToken(MenuManager.appId, MenuManager.appSecret);  
       
        System.out.println(at.getAccessToken());
     
        if (null != at) {  
            // 调用接口创建菜单  
            int result = WeixinUtil.createMenu(getMenu(), at.getAccessToken());  
  
            // 判断菜单创建结果  
            if (0 == result)  
                log.info("菜单创建成功！");  
            else  
                log.info("菜单创建失败，错误码：" + result);  
        }  
    }  
  
    /** 
     * 组装菜单数据 
     *  
     * @return 
     */  
    private static Menu getMenu() {  
  
  
    	CommonButton btn11 = new CommonButton();  
        btn11.setName("The Youth");  
        btn11.setType("view");  
        //btn11.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid="+appId+"&redirect_uri=http%3A%2F%2F120.25.0.63%2Fxrjob-weixinTest%2FbaseController%2FgetOpenId.do&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect");
        btn11.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid="+appId+"&redirect_uri=http%3A%2F%2F 	%2Fxrjob-weixinTest&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect");


        CommonButton btn12 = new CommonButton();  
        btn12.setName("企业入口");  
        btn12.setType("view");  
        //btn12.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid="+appId+"&redirect_uri=http%3A%2F%2F120.25.0.63%2Fxrjob-weixinTest%2FbaseController%2FcompanyUserGetOpenId.do&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect"); 
        btn12.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid="+appId+"&redirect_uri=http%3A%2F%2F120.25.0.63%2Fxrjob-weixinTest%2Flogin.jsp&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect"); 


        CommonButton btn13 = new CommonButton();
        btn13.setName("More");
        btn13.setType("view");
        btn13.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid="+appId+"&redirect_uri=http%3A%2F%2F120.25.0.63%2Fxrjob-weixinTest%2FbaseController%2FcompanyUserGetOpenId.do&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect"); 

        
  
        ComplexButton mainBtn1 = new ComplexButton();  
        mainBtn1.setName("The Youth");  
        mainBtn1.setSub_button(new CommonButton[] {btn11 });  
  
        ComplexButton   mainBtn2 = new ComplexButton();  
        mainBtn2.setName("企业入口");  
        mainBtn2.setSub_button(new CommonButton[] { btn12 });  
        
 
        /** 
         * 这是公众号xiaoqrobot目前的菜单结构，每个一级菜单都有二级菜单项<br> 
         *  
         * 在某个一级菜单下没有二级菜单的情况，menu该如何定义呢？<br> 
         * 比如，第三个一级菜单项不是“更多体验”，而直接是“幽默笑话”，那么menu应该这样定义：<br> 
         * menu.setButton(new Button[] { mainBtn1, mainBtn2, btn33 }); 
         */  
        Menu menu = new Menu();  
        menu.setButton(new Button[] { btn11, btn12,btn13});  
  
        return menu;  
    }  

}  
