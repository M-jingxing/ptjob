package com.job.interceptor;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class BaseInterceptor implements HandlerInterceptor {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		String url = request.getRequestURI();
		// ////////////以下页面不需要登录,chy add companyUserFromApp.do
		if (url.contains("user/UserActionsms.do")||url.contains("baseController/companyUserGetOpenId.do")||url.contains("user/login.do") || url.contains("user/toRegister.do")||url.contains("baseController/companyUserFromApp.do")
				|| url.contains("checkCode.do") | url.contains("register.do")) {
			logger.info("--请求地址--" + url + "不需要登录");
			return true;
		}
		if (url.contains("person/toPersonPage.do")
				|| url.contains("person/login.do")) {
			return true;
		}
		if (url.contains("distance/distanceJob.do")
				|| url.contains("distance/toJobSearch.do")
				|| url.contains("distance/jobFair.do")
				|| url.contains("distance/jobDetail.do")
				|| url.contains("distance/jobSearch.do")
				|| url.contains("distance/jobSearchHot")
				|| url.contains("distance/jobSearch2.do")
				|| url.contains("distance/toJobSearch.do")
				|| url.contains("distance/getLocation.do")
				|| url.contains("type/changeTopType.do")
				|| url.contains("type/changeSecondType.do")
				|| url.contains("type/queryTopTypeList.do")
				|| url.contains("person/UserActionsms.do")
				|| url.contains("person/UserActionCheckCode.do")
				|| url.contains("baseController/getOpenId.do")
				||url.contains("job/indexJobList.do")
				||url.contains("person/changePwd")
				||url.contains("job/jobDetail")
				||url.contains("distance/toJobSearchPage")
				||url.contains("distance/delKey.do")
				||url.contains("job/getNewList.do")
				||url.contains("job/practiceJob.do")
				||url.contains("job/companyOtherJob.do")
				||url.contains("job/companyInfo.do")) {
			return true;
		}
		// ////////////以下页面需要个人用户登录
		if (url.contains("person/saveResume.do")
				|| url.contains("person/toSaveResume.do")
				|| url.contains("delivery/deliveryJob.do")
				|| url.contains("person/uploadPic.do")
				|| url.contains("person/toChangeInfo.do")
				||url.contains("person/toChangeIntentJob")
				||url.contains("person/saveIntentJob")
				||url.contains("person/toAddResumeWork")
				||url.contains("person/addResumeWork")
				||url.contains("person/editResumeWork")
				||url.contains("person/toEditResumeWork")
				||url.contains("person/deleteResumeWork")
				||url.contains("person/toAddResumeEdu")
				||url.contains("person/toEditResumeEdu")
				||url.contains("person/addResumeEdu")
				||url.contains("person/deleteResumeEdu")
				||url.contains("person/editResumeEdu")
				||url.contains("person/toEditUserDescribe")
				||url.contains("person/quitUser")
				||url.contains("job/jobReport")
				||url.contains("job/jobCollect")
				||url.contains("job/deleteJobCollect")
				||url.contains("job/toJobCollect")
				||url.contains("delivery/userDelivery")
				||url.contains("job/deleteJobCollect"))
				 {
			HttpSession session = request.getSession();
			Integer personId = (Integer) session.getAttribute("personId");
			if (null == personId) {
				response.sendRedirect(request.getContextPath()
						+ "/xrjobPages/login.jsp");
				return false;
			} else {
				return true;
			}
		}
		// ////////////以下页面需要企业用户登录
		HttpSession session = request.getSession();
		Integer userId = (Integer) session.getAttribute("userId");
		// 未登录
		if (null == userId) {
			response.sendRedirect(request.getContextPath() + "/login.jsp");
			return false;
		}

		if (url.contains("company/companyManager.do")
				|| url.contains("company/addCompany.do")
				|| url.contains("company/saveCompany.do")
				|| url.contains("address/getAreaList.do")
				|| url.contains("user/logout.do")
				|| url.contains("user/changePwdPage.do")
				|| url.contains("user/changePwd.do")
				|| url.contains("company/uploadPic.do")
				||url.contains("company/addCompanyLic.do")
				||url.contains("company/toPerfectCompany.do")
				) {
			logger.info("--请求地址--" + url + "不需要通过审核");
			return true;
		}

		/*
		 * Integer userStatus = (Integer) session.getAttribute("userStatus");
		 * Integer companyId = (Integer) session.getAttribute("companyId");
		 * if(null == userStatus || userStatus.equals(0) ||
		 * userStatus.equals(3)){ responseNoSession(request, response,
		 * "您还没有审核，请耐心等待"); return false; } if(userStatus.equals(4)){
		 * responseNoSession(request, response, "您的账户已过期，请通知管理员重新审核"); return
		 * false; } if(null == companyId){ responseNoSession(request, response,
		 * "您还未添加企业信息，请先保存企业信息"); return false; }
		 */
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
	}

	public static void responseNoSession(HttpServletRequest request,
			HttpServletResponse response, String msg) {
		response.setContentType("application/json;charset=utf-8");
		try {
			PrintWriter out = response.getWriter();
			out.print(msg);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
