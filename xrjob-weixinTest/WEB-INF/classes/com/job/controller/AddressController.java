package com.job.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.job.model.AreaModel;
import com.job.service.AddressService;
/**
 * 地区管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("address")
public class AddressController {
	
	@Autowired
	private AddressService addressService;

	
	@RequestMapping("getAreaList.do")
	@ResponseBody
	public List<AreaModel> quertAreaList(Integer cityId){
		if(null == cityId) return null;
		return addressService.queryAreaByCity(cityId);
	}
}
