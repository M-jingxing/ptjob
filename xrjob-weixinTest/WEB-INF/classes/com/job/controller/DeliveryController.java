package com.job.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.job.form.DeliveryForm;
import com.job.model.CompanyImformation;
import com.job.model.CompanyJob;
import com.job.model.CompanyUser;
import com.job.model.DataGrid;
import com.job.model.Json;
import com.job.model.User;
import com.job.model.UserResume;
import com.job.model.UserResumeDelivery;
import com.job.service.CompanyService;
import com.job.service.DeliveryService;
import com.job.service.JobService;
import com.job.service.ResumeService;
import com.job.service.SendWeixinService;
import com.job.service.UserService;
import com.job.weixinModel.Constants;
import com.sun.xml.internal.messaging.saaj.packaging.mime.MessagingException;

/**
 * 投递管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("delivery")
public class DeliveryController extends BaseController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DeliveryService deliveryService;

	@Autowired
	private JobService jobService;

	@Autowired
	private ResumeService resumeService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private UserService userService;

	@Autowired
	private SendWeixinService sendWeixinService;

	/**
	 * 企业端投递管理页面
	 * 
	 * @return
	 */
	@RequestMapping("toDeliveryPage.do")
	public String toDeliveryPage(Model model, HttpSession session) {
		Integer companyId = getCompanyId(session);
		
		List<CompanyJob> jobList=jobService.queryJobByCompany(companyId);
		List<UserResumeDelivery> deliveryList=new ArrayList<>();
		for (int i = 0; i < jobList.size(); i++) {
			
			deliveryList.addAll(deliveryService.queryDeliveryListByJobId(jobList.get(i).getJobId()));
		}
		model.addAttribute("jobNameList",
				jobService.queryJobByCompany(companyId));
		model.addAttribute("deliveryList",deliveryList);
		return "backstagePages/deliveryPage";
	}

	/**
	 * 企业查询投递信息
	 * 
	 * @return
	 */
	@RequestMapping("queryDelivery.do")
	public String queryDelivery(DeliveryForm form, HttpSession session,
			Model model, HttpServletRequest request) {
		Integer jobId = null;
		Integer status = null;
		if (request.getParameter("jobId") != null
				&& request.getParameter("jobId") != "") {
			jobId = Integer.parseInt(request.getParameter("jobId"));
		}
		if (request.getParameter("status") != null
				&& request.getParameter("status") != "") {
			status = Integer.parseInt(request.getParameter("status"));

		}
		form.setJobId(jobId);
		form.setDeliveryStatus(status);
		form.setCompanyId(getCompanyId(session));
		model.addAttribute("jobId", jobId);
		model.addAttribute("jobNameList",
				jobService.queryJobByCompany(getCompanyId(session)));
		model.addAttribute("deliveryList",
				deliveryService.queryDeliveryList(form));

		return "backstagePages/deliveryPage";
	}

	/**
	 * 用户投递记录
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("userDelivery")
	public String userDelivery(Model model, HttpSession session) throws Exception {
		Integer personId = (Integer) session.getAttribute("personId");
	List<UserResumeDelivery> DList=	deliveryService.queryDeliveryByUserId(personId);
	List<UserResumeDelivery> dList=new  ArrayList<UserResumeDelivery>();
	for (int i = 0; i < DList.size(); i++) {
		Date newDate=new Date();
		Date oldDate=DList.get(i).getCreateTime();
		int days=daysBetween(oldDate, newDate);
		if(jobService.getJobByJobId(DList.get(i).getJobId())!=null){
			if(days<=60){  //只显示60日之内的记录
			dList.add(DList.get(i));
			}
		}
	}
	model.addAttribute("deliveryList",
				dList);
		return "xrjobPages/deliveryRecord";
	}

	/**
	 * 投递简历，并向公司发生提醒消息
	 * 
	 * @param jobId
	 * @param session
	 * @param request
	 * @return
	 */
	@RequestMapping("deliveryJob.do")
	@ResponseBody
	public Json deliveryJob(Integer jobId, HttpSession session,
			HttpServletRequest request) {
		jobId = Integer.parseInt(request.getParameter("jobId"));
		Json res = new Json();
		Integer personId = (Integer) session.getAttribute("personId");
		String userMobile = (String) session.getAttribute("userMobile");
		UserResume userResume = resumeService.selectResumeByUser(personId);
		UserResumeDelivery userResumeDelivery = new UserResumeDelivery();
		userResumeDelivery.setResumeId(userResume.getResumeId());
		userResumeDelivery.setJobId(jobId);
		userResumeDelivery.setUserId(personId);
		userResumeDelivery.setDeliveryStatus(1);
		userResumeDelivery.setCreateTime(new Date());
   
	   List<UserResumeDelivery> deliveries=deliveryService.queryListByUserIdAndjobId(jobId, personId);
	   Integer isSuc=0;
	   if(deliveries.size()==0){
		   isSuc = deliveryService.insertSelective(userResumeDelivery);
	   }else {
		   userResumeDelivery.setDeliveryId(deliveries.get(0).getDeliveryId());
		    isSuc=  deliveryService.updateSelective(userResumeDelivery);
	   }
		if (isSuc == 1) {
			CompanyJob companyJob = jobService.getJobByJobId(jobId);
			CompanyImformation companyImformation = companyService
					.queryCompanyById(companyJob.getCompanyId());
			Integer alert = companyImformation.getMessageAlert();
			if (alert != null) {
				/*
				 * System.out.println("companyId:"+companyImformation.getCompanyId
				 * ()+"alert:"+alert);
				 */
				if (alert == 2) {
					res = deliveryService.sendPhoneMessage(
							companyImformation.getMessageMobile(),
							companyJob.getJobName(), userMobile);
				} else if (alert == 1) {
					try {
						res = deliveryService.SendMail(
								companyImformation.getMessageEmail(),
								companyJob.getJobName(), userMobile);

					} catch (MessagingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (javax.mail.MessagingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (alert == 3) {
					
					
					String appId = Constants.appid;
					String appSecret = Constants.appsecret;
				    CompanyUser companyUser=userService.selectByCompanyId(companyImformation.getCompanyId());
					String openId = (String) companyUser.getOpenId();
				/*	System.out.println(openId);*/
					sendWeixinService.send_template_message(appId, appSecret,
							openId,companyJob.getJobName(),userResume);

				}

			}
			res.setMsg("投递成功");
			res.setRes(true);
		} else {
			res.setRes(false);
			res.setMsg("投递失败");
		}
		return res;
	}

	/**
	 * 删除投递记录
	 * 
	 * @param deliveryId
	 * @param request
	 * @return
	 */
	@RequestMapping("deleteDelivery.do")
	@ResponseBody
	public Json deleteDelivery(Integer deliveryId, HttpServletRequest request) {
		Json res = new Json();
		if (null == deliveryId) {
			res.setMsg("简历信息不存在");
			return res;
		}
		deliveryService.deleteByPrimaryKey(deliveryId);
		res.setRes(true);
		res.setMsg("删除成功！");
		return res;

	}
	
	/**  
     * 计算两个日期之间相差的天数  
     * @param smdate 较小的时间 
     * @param bdate  较大的时间 
     * @return 相差天数 
     * @throws ParseException  
     */    
    public static int daysBetween(Date smdate,Date bdate) throws ParseException    
    {    
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
        smdate=sdf.parse(sdf.format(smdate));  
        bdate=sdf.parse(sdf.format(bdate));  
        java.util.Calendar cal = java.util.Calendar.getInstance();    
        cal.setTime(smdate);    
        long time1 = cal.getTimeInMillis();                 
        cal.setTime(bdate);    
        long time2 = cal.getTimeInMillis();         
        long between_days=(time2-time1)/(1000*3600*24);  
            
       return Integer.parseInt(String.valueOf(between_days));           
    } 
    
}
