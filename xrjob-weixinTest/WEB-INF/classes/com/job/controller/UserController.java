package com.job.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

//import jdk.nashorn.internal.ir.RuntimeNode.Request;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.job.form.RegisterForm;
import com.job.model.CompanyImformation;
import com.job.model.CompanyUser;
import com.job.model.Json;
import com.job.model.User;
import com.job.service.CompanyService;
import com.job.service.UserService;
import com.job.util.MD5Util;
import com.mysql.jdbc.StringUtils;
import com.sun.org.apache.regexp.internal.RE;

/**
 * 企业用户管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("user")
public class UserController extends BaseController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private UserService userService;
	@Autowired
	private CompanyService companyService;
	
    @RequestMapping("toMemu.do")
    public String toMemu(Model model){
    	model.addAttribute("isRead", 1);
    	return "backstagePages/menu";
    }
	
	/**
	 * 注册
	 * @param loginName
	 * @param password
	 * @param surePassword
	 * @param checkCode
	 * @param session
	 * @return
	 */
	@RequestMapping("register.do")
	@ResponseBody
	public  Json register(String loginName,String password,String surePassword,String userMobile,Model model,HttpSession session){
		
		Json res = new Json();
		
		if(StringUtils.isNullOrEmpty(loginName) || StringUtils.isNullOrEmpty(password) || StringUtils.isNullOrEmpty(surePassword) || StringUtils.isNullOrEmpty(userMobile)){
			logger.error("用户注册，参数为空，loginName:" + loginName);
			res.setMsg("用户名或密码或手机号为空");
			return res;
		}
		
		if(!checkForm(loginName, password, surePassword, res, session)){
			logger.error("用户注册，表单错误，" + loginName);
			return res;
		}
		
		if(null != userService.selectUserByName(loginName)){
			logger.error("用户注册，用户已存在，loginName:" + loginName);
			res.setMsg("该用户名已经注册，请重新输入");
			return res;
		}
		
		Integer userId = userService.register(loginName, password,userMobile);
		if(null == userId){
			logger.error("用户注册，注册失败，loginName:" + loginName);
			res.setMsg("注册失败");
			return res;
		}
	  
		
		session.setAttribute("userId", userId);
		session.setAttribute("loginName", loginName);
		res.setRes(true);
		return res;
	}
	
	/**
	 * 登录
	 * @param loginName
	 * @param password
	 * @param session
	 * @return
	 */
	@RequestMapping("login.do")
	@ResponseBody
	public Json login(String loginName, String password, HttpSession session,HttpServletRequest request,Model model){
	/*	loginName= request.getParameter("loginName");
		password=request.getParameter("password");*/
		
		System.out.println("loginName:"+loginName);
		Json res = new Json();
		if(StringUtils.isNullOrEmpty(loginName) || StringUtils.isNullOrEmpty(password)){
			logger.error("用户登录，参数为空，loginName:" + loginName);
			res.setMsg("用户名或密码为空");
			return res;
		}
		
		CompanyUser user = userService.selectUserByName(loginName);
		if(null == user){
			logger.error("用户登录，用户不存在，loginName:" + loginName);
			res.setMsg("用户不存在");
			return res;
		}
		
		if(!user.getPassword().equals(MD5Util.getMD5Str(password))){
			logger.error("用户登录，密码错误，loginName:" + loginName);
			res.setMsg("用户名或密码错误");
			return res;
		}
		
		//冻结用户
		if(user.getUserStatus().equals(2)){
			logger.error("用户登录，用户被冻结，loginName:" + loginName);
			res.setMsg("用户已被冻结，请联系管理员");
			return res;
		}
		//用户审核中
		if(user.getUserStatus().equals(0)){
			logger.error("用户登录，用户还在审核中，loginName:" + loginName);
			res.setMsg("信息还在审核中，我们将在7个工作日内进行审核");
			res.setStatus(user.getUserStatus());
			if(user.getCompanyId()!=null){
				
				CompanyImformation companyImformation=companyService.queryCompanyById(user.getCompanyId());
				if(companyImformation.getCompanyLicense()==null||companyImformation.getCompanyLicense().equals("")){//未上传营业执照
					res.setUpPic(false);
				}else {
					res.setUpPic(true);
				}
			}
			res.setReturnValue(user.getCompanyId());
			session.setAttribute("userStatus", user.getUserStatus());
			session.setAttribute("userId", user.getUserId());
			session.setAttribute("loginName", loginName);
			session.setAttribute("companyId", user.getCompanyId());
			return res;
		}
		//用户审核未通过 
		if(user.getUserStatus().equals(3)){
			logger.error("用户登录，审核未通过，loginName:" + loginName);
			res.setMsg("您的资料未通过审核，是否修改资料重新审核？");
		    res.setStatus(user.getUserStatus());
		    res.setReturnValue(user.getCompanyId());
			session.setAttribute("userStatus", user.getUserStatus());
			session.setAttribute("userId", user.getUserId());
			session.setAttribute("loginName", loginName);
			session.setAttribute("companyId", user.getCompanyId());
			return res;
			
		}
		
		
		String openId = (String) session.getAttribute("openId");
		user.setOpenId(openId);
		user.setLastLoginTime(new Date());
		userService.changeUser(user);
		System.out.println("userID:"+user.getUserId());
		session.setAttribute("userStatus", user.getUserStatus());
		session.setAttribute("userId", user.getUserId());
		session.setAttribute("loginName", loginName);
		session.setAttribute("companyId", user.getCompanyId());
		res.setStatus(user.getUserStatus());
		res.setRes(true);
		return res;
	}
	
	/**
	 *  用户登出
	 * @param session
	 * @return
	 */
	@RequestMapping("logout.do")
	@ResponseBody
	public Json logout(HttpSession session,Integer userId){
		Json res = new Json();
		CompanyUser companyUser=userService.selectUserById(userId);
		companyUser.setOpenId("1");
		userService.changeUser(companyUser);
		session.removeAttribute("userId");
		session.removeAttribute("companyId");
		session.removeAttribute("userStatus");
		session.removeAttribute("loginName");
		res.setRes(true);
		return res;
	}
	
   /**
    * 更改状态
    */
	@RequestMapping("changeStatus.do")
	public String changStatus(HttpSession session){
		CompanyUser companyUser=userService.selectUserByName((String)session.getAttribute("loginName"));
		companyUser.setUserStatus(0);
		userService.changeUser(companyUser);
		return "backstagePages/regSuccess";
	}
	
	/**
	 * 修改密码页面
	 * @return
	 */
	@RequestMapping("changePwdPage.do")
	public String changePwdPage(){
		return "user/changePwd";
	}
	
	/**
	 * 修改密码
	 * @param session
	 * @param password
	 * @param newPwd
	 * @return
	 */
	@RequestMapping("changePwd.do")
	@ResponseBody
	public Json changePwd(HttpSession session, String password, String newPwd){
		Json res = new Json();
		if(StringUtils.isNullOrEmpty(password) || StringUtils.isNullOrEmpty(newPwd)){
			res.setMsg("参数有误");
			return res;
		}
		
		userService.changeUserPwd(newPwd, password, getUserId(session), res);
		//修改成功，则重新登录
		if(res.isRes()){
			session.removeAttribute("userId");
			session.removeAttribute("userStatus");
			session.removeAttribute("loginName");
			session.removeAttribute("companyId");
		}
		return res;
	}
	
	public boolean checkForm(String loginName, String password, String surePassword, Json res, HttpSession session){
		/*Pattern p = Pattern.compile("[^0-9a-zA-Z]");
		Matcher m = p.matcher(loginName);
		if(m.find()){
			res.setMsg("请输入正确的用户名");
			return false;
		}*/
		if(loginName.length() > 20){
			res.setMsg("请输入20位以下用户名");
			return false;
		}
		if(password.length() < 6){
			res.setMsg("请至少输入6位密码");
			return false;
		}
		
		if(!password.equals(surePassword)){
			res.setMsg("两次密码不一致");
			return false;
		}
		
	/*	Object code = session.getAttribute("checkCode");
		if(code == null || !code.toString().equalsIgnoreCase(checkCode)){
			res.setMsg("请输入正确的验证码");
			return false;
		}*/
		return true;
	}
	
	@RequestMapping("uploadPic")
	@ResponseBody
	public Json uploadPic(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		Json result = new Json();
		DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
		String ymd = dateFormat.format(new Date());
/*		String path = "/var/www/html/xrjob-company/images/company/" + ymd + "/";session.getServletContext().getRealPath("/images/company/" + ymd + "/");
*/		String path=session.getServletContext().getRealPath("/");
		path=path.substring(0, path.lastIndexOf("/"));
		path=path.substring(0, path.lastIndexOf("/"));
		System.out.println(path);
	    path=path+"/xrjob-img"+"/images/company/" + ymd + "/";
		File floder = new File(path);
		if (!floder.exists()) {
			logger.info("创建目录：" + path);
			floder.mkdirs();
		}
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;

		String fileName = "";
		Iterator<String> it = multipartRequest.getFileNames();
		
		while (it.hasNext()) {
			String oldName = it.next();
			CommonsMultipartFile ogiFile = (CommonsMultipartFile) multipartRequest.getFile(oldName);

			if(ogiFile.isEmpty())
				continue;
			
			if(ogiFile.getSize() > 5242880){
				logger.error("文件大小超过5M");
				result.setMsg("请上传小于5M的图片");
				return result;
			}
			
			String originalFilename = ogiFile.getOriginalFilename();

			String fileSuffix = originalFilename.substring(originalFilename.lastIndexOf("."));
			if(!fileSuffix.equalsIgnoreCase(".jpg") && !fileSuffix.equalsIgnoreCase(".png") && !fileSuffix.equalsIgnoreCase(".gif") && !fileSuffix.equalsIgnoreCase(".jpeg")){
				logger.error(String.format("文件后缀不是图片, %s", fileSuffix));
				result.setMsg("请选择以.png、.jpg、.gif或.jpeg结尾的图片");
				return result;
			}
			
			//生成新的文件
			fileName = UUID.randomUUID() + fileSuffix;

			File imageFile = new File(path, fileName);
			try {
				FileCopyUtils.copy(ogiFile.getBytes(), imageFile);
			} catch (IOException e) {
				logger.error("上传图片发生错误", e);
			}

			fileName = "images/company/" + ymd + "/" + fileName;
			logger.info("新生成图片文件名：" + fileName);
			break;
		}
		
        result.setRes(true);
        result.setMsg(fileName);
        System.out.println("fileName:"+fileName);
		return result;
	}
	
	private static String MD5(String sourceStr) {
		String result = "";
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(sourceStr.getBytes());
			byte b[] = md.digest();
			int i;
			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
			result = buf.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String encrypt(String content, String password) {
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
			int blockSize = cipher.getBlockSize();
			byte[] dataBytes = content.getBytes();
			int plaintextLength = dataBytes.length;
			if (plaintextLength % blockSize != 0) {
				plaintextLength = plaintextLength + (blockSize - (plaintextLength % blockSize));
			}
			byte[] plaintext = new byte[plaintextLength];
			System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);
			SecretKeySpec keyspec = new SecretKeySpec(password.getBytes(), "AES");
			IvParameterSpec ivspec = new IvParameterSpec(password.getBytes());
			cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
			byte[] encrypted = cipher.doFinal(plaintext);
			return parseByte2HexStr(encrypted);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String parseByte2HexStr(byte buf[]) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < buf.length; i++) {
			String hex = Integer.toHexString(buf[i] & 0xFF);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}
			sb.append(hex.toUpperCase());
		}
		return sb.toString();
	}
}
	

