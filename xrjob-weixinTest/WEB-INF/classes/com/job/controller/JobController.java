package com.job.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.job.form.JobForm;
import com.job.model.CompanyImformation;
import com.job.model.CompanyJob;

import com.job.model.JobComplain;
import com.job.model.Json;
import com.job.model.Page;
import com.job.model.User;
import com.job.model.UserCollect;
import com.job.model.UserResumeDelivery;
import com.job.model.Data;
import com.job.service.AddressService;
import com.job.service.CompanyService;
import com.job.service.DeliveryService;
import com.job.service.JobService;
import com.job.service.JobTypeService;
import com.sun.javafx.sg.prism.NGShape.Mode;
import com.sun.org.apache.regexp.internal.recompile;

/**
 * 岗位管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("job")
public class JobController extends BaseController {
	
	@Autowired
	private JobTypeService typeService;
	
	@Autowired
	private JobService jobService;
	
	@Autowired
	private AddressService addressService;

	@Autowired
	private DeliveryService deliveryService;
	
	@Autowired
	private CompanyService companyService;
	//萧然找工作客户端岗位操作start
	/**
	 * 首页最新与热门推荐工作列表
	 * @param model
	 * @return
	 */
	
	@RequestMapping("indexJobList")
	public String indexJobList(Model model){
		Page page =new Page();
		
		//置顶的热门工作列表
        List<CompanyJob> hotJobList =new ArrayList<CompanyJob>();
        List<CompanyJob> jobStickList=jobService.jobListByJobStick(2);
        List<Integer> nums=new ArrayList<Integer>();
        for (int i = 0; i < jobStickList.size(); i++) {
			nums.add(i);
		}  
      
       Collections.shuffle(nums);
       Iterator<Integer> ite = nums.iterator();    
       while(ite.hasNext()){              
           hotJobList.add(jobStickList.get(ite.next()));
       }    
     
       
		 model.addAttribute("hotJobList",hotJobList);
	     model.addAttribute("newJobList",jobService.queryJobAll(page));
	     model.addAttribute("isRead",1);
		return "index";
	}
	
	/**
	 * 下拉刷新ajax请求
	 * @param request
	 * @return
	 */
	@RequestMapping("getNewList")
	@ResponseBody
	public Data getNewList(HttpServletRequest request){
		
		Page page=new Page();
		Integer numPage=0;
		String strPage=request.getParameter("page");
			
		if(strPage==null||strPage.equals("")){
			numPage=2;
		}else {
			
			numPage=Integer.parseInt(strPage);
		}
/*  System.out.println(numPage);*/
		page.setPage(numPage);
		page.setRows(10);
		List<CompanyJob> companyJobs=jobService.queryJobAll(page);
		
		Data data=new Data();
		data.setList(companyJobs);
		data.setNumPage(numPage+1);
		
		return data;
	}
	
	
	
	/**
	 * 岗位详情
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("jobDetail")
	public String jobDetail(Model model,HttpServletRequest request,HttpSession session){
		String jobId=request.getParameter("jobId");
		Integer userId=(Integer) session.getAttribute("personId");
	    UserCollect userCollect=jobService.collectByJobIdAndUserId(Integer.parseInt(jobId), userId);  
	   if(userCollect!=null){
		   model.addAttribute("userCollectId",userCollect.getCollectId());
		   }else {
			   model.addAttribute("userCollectId",0);
		}
	    List<UserResumeDelivery> userDeliveriesList = deliveryService.queryListByUserIdAndjobId(Integer.parseInt(jobId), userId);
	    int betweenDay = -1;
	    Integer isDelivery;
	    /*System.out.println("size:"+userDeliveriesList.size());*/
	    if(userDeliveriesList.size()!=0){
	    Date deliveryDate=userDeliveriesList.get(0).getCreateTime();
	    Date newDate=new Date();
	 /*   System.out.println("deliveryDate:"+deliveryDate);
	    System.out.println("newDate:"+newDate);*/
	    try {
			 betweenDay=daysBetween(deliveryDate, newDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      if(betweenDay>7){
		       isDelivery=0;
		    }else {
			   isDelivery=1;
			}
	    }else {
	    	isDelivery=0;
		}
	   
	    CompanyJob companyJob=jobService.jobDetailByJobId(Integer.parseInt(jobId));
	    List<CompanyJob> jobList=jobService.queryJobByCompany(companyJob.getCompanyId());
	    
        model.addAttribute("url", request.getRequestURL()+"?"+request.getQueryString());
	    model.addAttribute("isDelivery",isDelivery);
	    model.addAttribute("jobDetail",jobService.jobDetailByJobId(Integer.parseInt(jobId)));	
	    model.addAttribute("jobCount",jobList.size());
		return "xrjobPages/jobDetail";
	}
	
	/**
	 * 举报岗位
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping("jobReport")
	@ResponseBody
	public Json jobReport(HttpServletRequest request,HttpSession session){
		Json res = new Json();
		String companyId=request.getParameter("companyId");
		String jobId=request.getParameter("jobId");
		String objMsg=request.getParameter("objMsg");
		String objType=request.getParameter("objType");
		String complainContent=request.getParameter("complainContent");
		String userMobile=(String) session.getAttribute("userMobile");
		Integer personId=(Integer) session.getAttribute("personId");
		JobComplain jobComplain=new JobComplain();
		
		if(objType!=null&&!objType.equals("")){
			jobComplain.setObjType(Integer.parseInt(objType));
			if(Integer.parseInt(objType)==1){
				jobComplain.setObjId(Integer.parseInt(companyId));
			}else {
				jobComplain.setObjId(Integer.parseInt(jobId));
			}			
		}
		jobComplain.setObjMsg(Integer.parseInt(objMsg));
		jobComplain.setComplainContent(complainContent);
		jobComplain.setUserContract(userMobile);
		jobComplain.setCreateTime(new Date());
		jobComplain.setUserId(personId);
		jobService.addJobComplain(jobComplain);
		res.setMsg("举报成功");
		res.setRes(true);
		return res;
	}
	/**
	 * 用户收藏岗位
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping("jobCollect")
	@ResponseBody
	public Json jobCollect(HttpServletRequest request,HttpSession session){
		Json res = new Json();
		String jobId=request.getParameter("jobId");	
		Integer personId=(Integer) session.getAttribute("personId");
		UserCollect userCollect=new UserCollect();
		userCollect.setJobId(Integer.parseInt(jobId));
		userCollect.setUserId(personId);
		userCollect.setCollectType(1);
		userCollect.setCreateTime(new Date());
	    Integer collectId=jobService.addUserCollect(userCollect);
	    res.setMsg(collectId.toString());
		res.setRes(true);
		return res;
	}
	/**
	 * 删除收藏记录
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping("deleteJobCollect")
	@ResponseBody
	public Json deleteJobCollect(HttpServletRequest request,HttpSession session){
		Json res = new Json();
		String collectId=request.getParameter("collectId");
		if(collectId!=null&&!collectId.equals("")){
			jobService.deleteUserCollect(Integer.parseInt(collectId));
			res.setRes(true);
		}
		
		return res;
	}
	/**
	 * 我的收藏界面
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("toJobCollect")
	public String toJobCollect(Model model,HttpSession session) throws Exception{
		Integer personId=(Integer) session.getAttribute("personId");
		List<UserCollect> DList=jobService.getCollectListByUserId(personId);	
		List<UserCollect> dList=new  ArrayList<UserCollect>();
		for (int i = 0; i < DList.size(); i++) {
			Date newDate=new Date();
			Date oldDate=DList.get(i).getCreateTime();
			int days=daysBetween(oldDate, newDate);
			if(jobService.getJobByJobId(DList.get(i).getJobId())!=null){
				if(days<=60){  //只显示60日之内的记录
				  dList.add(DList.get(i));
				}
			}
		}
		
		model.addAttribute("collectList",dList);
		return "xrjobPages/myStar";
	}
	/**
	 * 找应届生
	 * @param model
	 * @return
	 */
	@RequestMapping("practiceJob")
	public String practiceJob(Model model){
		model.addAttribute("jobs", jobService.jobListByJobType(3));
		return "xrjobPages/internship";
	}
	
	/**
	 * 查看公司的其他岗位
	 * @param model
	 * @param companyId
	 * @param jobId
	 * @return
	 */
	@RequestMapping("companyOtherJob")
	public String companyOtherJob(Model model,int companyId,int jobId){
		List<CompanyJob> jobs=jobService.queryJobByCompany(companyId);
		for (int i = 0; i < jobs.size(); i++) {
			if(jobs.get(i).getJobId()==jobId){
				jobs.remove(i);
			}
		}
		model.addAttribute("jobs",jobs);
		return "xrjobPages/otherJobList";
	}
	/**
	 * 查看企业详情
	 * @param model
	 * @param companyId
	 * @return
	 */
	@RequestMapping("companyInfo")
	public String companyInfo(Model model,int companyId){
		CompanyImformation companyImformation=companyService.queryCompanyById(companyId);
		model.addAttribute("company", companyImformation);
		return "xrjobPages/companyInfo";
	}
	//萧然找工作客户端岗位操作end
	
	
	//企业后台管理平台岗位操作 start
	
	/**
	 * 发布岗位界面
	 * @param model
	 * @return
	 */
	@RequestMapping("toJobPage.do")
	public String toJobPage(Model model){
		model.addAttribute("tops", typeService.queryTopTypeList());
		return "backstagePages/postJobs";
	}
	/**
	 * 显示岗位信息
	 */
	@RequestMapping("showJob.do")
	public String showJob(HttpServletRequest request,Model model,HttpSession session){
		Integer jobId=Integer.parseInt(request.getParameter("jobId"));
		CompanyJob job = jobService.jobDetail(jobId, getCompanyId(session));
		model.addAttribute("tops", typeService.queryTopTypeList());
		model.addAttribute("seconds", typeService.querySecondTypeByTop(job.getTopId()));
		model.addAttribute("thirds", typeService.queryThirdTypeBySecond(job.getSecondId()));
		model.addAttribute("cityList", addressService.queryAllCityList());
		model.addAttribute("areaList", addressService.queryAreaByCity(job.getCityId()));
		model.addAttribute("job", job);
		return "backstagePages/viewJob";
	}
	
	
	/**
	 * 查询岗位列表
	 * @param form
	 * @param session
	 * @return
	 */
	@RequestMapping("queryJobPage")
	public String queryJobPage(Model model, HttpSession session){
	
	 	List<CompanyJob> jobList= jobService.queryJobByCompany((Integer)session.getAttribute("companyId"));
	 	if(jobList.size()!=0){
	 		model.addAttribute("jobList",jobList);
	 	}
	 	
		return "backstagePages/jobManager";
	}
	
	/**
	 * 添加岗位页面
	 * @param model
	 * @return
	 */
	@RequestMapping("toAddJob.do")
	public String toAddJob(Model model){
		model.addAttribute("tops", typeService.queryTopTypeList());
		model.addAttribute("cityList", addressService.queryAllCityList());
		return "backstagePages/postJobs";
	}
	
	/**
	 * 添加岗位
	 * @param job
	 * @param session
	 * @return
	 */
	@RequestMapping("addJob.do")
	@ResponseBody
	public Json addJob(CompanyJob job, HttpSession session,Model model){
		job.setCompanyId(getCompanyId(session));
		job.setJobStatus(1);
		job.setJobStick(1);
		Json res = new Json();
		jobService.addJob(job);
		res.setRes(true);
		res.setMsg("添加成功");
		return res;
	}
	
	/**
	 * 编辑岗位页面
	 * @param jobId
	 * @param session
	 * @param model
	 * @return
	 */
	@RequestMapping("editJob.do")
	public String editJob(Integer jobId, HttpSession session, Model model,HttpServletRequest request){
		jobId= Integer.parseInt(request.getParameter("jobId"));
		CompanyJob job = jobService.jobDetail(jobId, getCompanyId(session));
		model.addAttribute("tops", typeService.queryTopTypeList());
		model.addAttribute("seconds", typeService.querySecondTypeByTop(job.getTopId()));
		model.addAttribute("thirds", typeService.queryThirdTypeBySecond(job.getSecondId()));
		model.addAttribute("cityList", addressService.queryAllCityList());
		model.addAttribute("areaList", addressService.queryAreaByCity(job.getCityId()));
		model.addAttribute("job", job);
		
		return "backstagePages/editJob";
	}
	
	/**
	 * 修改岗位
	 * @param job
	 * @param session
	 * @return
	 */
	@RequestMapping("updateJob.do")
	@ResponseBody
	public Json updateJob(CompanyJob job, HttpSession session,Model model){
		job.setCompanyId(getCompanyId(session));
		Json res = new Json();
		
		if(null == job.getJobId()){
			res.setMsg("参数错误");
			return res;
		}
		
		jobService.updateJob(job);
		res.setRes(true);
		res.setMsg("修改成功");
		return res;
	}
	
	/**
	 * 删除岗位
	 * @param jobId
	 * @param session
	 * @return
	 */
	@RequestMapping("deleteJob")
	@ResponseBody
	public Json deleteJob(Integer jobId, HttpSession session,Model model,HttpServletRequest request){
		Json res = new Json();
		jobId=Integer.parseInt(request.getParameter("jobId"));

		jobService.deleteJob(jobId, getCompanyId(session));
		//同时删除对该岗位的投递记录
		deliveryService.deleteByJobId(jobId);
		res.setRes(true);
		res.setMsg("删除成功");
	
		return res;
	}
	/**
	 * 暂停岗位
	 * @param jobId
	 * @return
	 */
	@RequestMapping("pauseJob")
	@ResponseBody
	public Json pauseJob(Integer jobId){
		Json res = new Json();
		CompanyJob companyJob=jobService.getJobByJobId(jobId);
		companyJob.setJobStatus(2);
		jobService.updateJob(companyJob);
		res.setRes(true);
		res.setMsg("暂停成功");
		return res;
	}
	/**
	 * 取消暂停
	 * @param jobId
	 * @return
	 */
	@RequestMapping("cancelPauseJob")
	@ResponseBody
	public Json cancelPauseJob(Integer jobId){
		Json res = new Json();
		CompanyJob companyJob=jobService.getJobByJobId(jobId);
		companyJob.setJobStatus(1);
		jobService.updateJob(companyJob);
		res.setRes(true);
		res.setMsg("发布成功");
		return res;
	}
	
	/**
	 * 刷新岗位，一天只能执行一次
	 * @param jobId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("reflashJob")
	@ResponseBody
	public Json  reflashJob(Integer jobId) throws Exception{
		Json res = new Json();
		CompanyJob companyJob=jobService.getJobByJobId(jobId);
		Date newDate=new Date();
		int day=daysBetween(companyJob.getCreateTime(), newDate);
		System.out.println(day);
		if(day>1){
			companyJob.setCreateTime(newDate);
			jobService.updateJob(companyJob);
			res.setRes(true);
			res.setMsg("刷新成功");
		}else {
			res.setRes(false);
			res.setMsg("对不起，发布或刷新24小时之后才能再次刷新");
		}
		
		return res;
	}
	
	
	
	//企业后台管理平台岗位操作 end
	
	/**  
     * 计算两个日期之间相差的天数  
     * @param smdate 较小的时间 
     * @param bdate  较大的时间 
     * @return 相差天数 
     * @throws ParseException  
     */    
    public static int daysBetween(Date smdate,Date bdate) throws ParseException    
    {    
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
        smdate=sdf.parse(sdf.format(smdate));  
        bdate=sdf.parse(sdf.format(bdate));  
        java.util.Calendar cal = java.util.Calendar.getInstance();    
        cal.setTime(smdate);    
        long time1 = cal.getTimeInMillis();                 
        cal.setTime(bdate);    
        long time2 = cal.getTimeInMillis();         
        long between_days=(time2-time1)/(1000*3600*24);  
            
       return Integer.parseInt(String.valueOf(between_days));           
    } 
    

}
