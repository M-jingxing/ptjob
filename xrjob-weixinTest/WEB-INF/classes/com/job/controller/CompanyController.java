package com.job.controller;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.job.form.CompanyForm;
import com.job.model.CompanyImformation;
import com.job.model.CompanyUser;
import com.job.model.Json;
import com.job.service.CompanyService;
import com.job.service.UserService;
/**
 * 企业管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("company")
public class CompanyController extends BaseController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private CompanyService companyService;

	@Autowired
	private UserService userService;

	@RequestMapping("companyManager")
	public String companyPage(Model model, HttpSession session) {
		Integer companyId = getCompanyId(session);
		// 该用户是否已经存在企业
		if (null != companyId) {
			model.addAttribute("company",
					companyService.queryCompanyById(companyId));
			return "backstagePages/menu.jsp";
		} else {
			return "backstagePages/addCompany.jsp";
		}
	}

	/**
	 * 添加企业
	 * 
	 * @param company
	 * @return
	 */
	@RequestMapping("addCompany")
	@ResponseBody
	public Json addCompany(CompanyImformation company, HttpSession session,
			Model model, HttpServletRequest request) {
		Json res = new Json();
		System.out.println(company.getCompanyName() + "-"
				+ company.getCompanyContractPerson() + "-"
				+ company.getCompanyContractMobile() + "-"
				+ company.getCompanyAddress());
		if (StringUtils.isBlank(company.getCompanyName())
				|| StringUtils.isBlank(company.getCompanyContractPerson())
				|| StringUtils.isBlank(company.getCompanyContractMobile())
				|| StringUtils.isBlank(company.getCompanyAddress())
				) {
			logger.error(String.format("保存企业信息，参数错误，companyId=%s",
					company.getCompanyId()));
			res.setMsg("请完善企业信息");
			model.addAttribute("res", res);
			return res;
		}

	/*	if (companyService.queryCompanyByName(company.getCompanyName()) != null) {
			res.setMsg("企业信息已存在，不能新增！");
			model.addAttribute("res", res);
			return res;
		}*/

		/* Integer userId= Integer.parseInt(request.getParameter("userId")); */
        CompanyUser companyUser=userService.selectUserById(getUserId(session));
        Integer companyId=null;
        if(companyUser.getCompanyId()==null){
        	 companyId = companyService.addCompany(company,
    				getUserId(session));
        }else {
        	company.setCompanyId(companyUser.getCompanyId());
			companyService.saveCompany(company);
		    companyId=companyUser.getCompanyId();
		}
		
		
		session.setAttribute("companyId", companyId);
		res.setRes(true);
		res.setMsg("保存成功");
		return res;
	}
   /**
    * 添加营业执照
    * @param companyLicense
    * @param request
    * @param session
    * @return
    */
	@RequestMapping("addCompanyLic")
	@ResponseBody
	public Json addCompanyLic(String companyLicense,HttpServletRequest request,HttpSession session){

		Json res = new Json();
		Integer companyId=(Integer) session.getAttribute("companyId");
		CompanyImformation companyImformation=companyService.queryCompanyById(companyId);
		companyImformation.setCompanyLicense(companyLicense);
		companyService.saveCompany(companyImformation);
		res.setRes(true);
		res.setMsg("保存成功");
		return res;
	}
	/**
	 * 完善企业信息界面
	 * @param session
	 * @param model
	 * @return
	 */
	@RequestMapping("toPerfectCompany")
	public String perfectCompany(HttpSession session, Model model){
		CompanyImformation companyImformation = companyService
				.queryCompanyById(getCompanyId(session));
		model.addAttribute("company", companyImformation);
		
		return "backstagePages/addCompany";
	}
	
	
	
	
	/**
	 * 更新企业信息
	 * 
	 * 
	 * @param company
	 * @return
	 */
	@RequestMapping("saveCompany")
	@ResponseBody
	public Json saveCompany(CompanyImformation company, HttpSession session,
			Model model) {
		Json res = new Json();
		System.out.println(company.getCompanyName() + "--"
				+ company.getCompanyContractPerson() + "--"
				+ company.getCompanyContractMobile() + "--"
				+ company.getCompanyAddress());
		if (StringUtils.isBlank(company.getCompanyName())
				|| StringUtils.isBlank(company.getCompanyContractPerson())
				|| StringUtils.isBlank(company.getCompanyContractMobile())
				|| StringUtils.isBlank(company.getCompanyAddress())
				|| StringUtils.isBlank(company.getCompanyLicense())) {
			logger.error(String.format("保存企业信息，参数错误，companyId=%s",
					company.getCompanyId()));
			res.setMsg("请完善企业信息");

			return res;
		}
        System.out.println(company.getCompanyContractPerson());
		/*CompanyImformation companyImformation = new CompanyImformation(
				company.getCompanyName(), company.getCompanyDescribe(),
				company.getCompanyAddress(), company.getCompanyScale(),
				company.getCompanyNature(), company.getCompanyContractPerson(),
				company.getCompanyContractMobile(), company.getCompanyLicense());*/
        company.setCompanyId(getCompanyId(session));
		companyService.saveCompany(company);
		res.setRes(true);
		res.setStatus((Integer) session.getAttribute("userStatus"));
		res.setMsg("保存成功");
		return res;
	}

	/**
	 * 显示企业信息
	 */
	@RequestMapping("showCompany")
	public String showCompany(HttpSession session, Model model) {

		CompanyImformation companyImformation = companyService
				.queryCompanyById(getCompanyId(session));
		model.addAttribute("company", companyImformation);

		return "backstagePages/companyContent";
	}
	/**
	 * 提醒方式界面
	 * @param session
	 * @param model
	 * @return
	 */
	@RequestMapping("toMessageAlert")
	public String toMessageAlert(HttpSession session,Model model){
		CompanyImformation companyImformation=companyService.queryCompanyById(getCompanyId(session));
		model.addAttribute("company",companyImformation);
		return "backstagePages/messageAlert";
	}
	/**
	 * 添加提醒方式
	 * @param company
	 * @param session
	 * @return
	 */
    @RequestMapping("messageAlert")
    @ResponseBody
    public Json messageAlert(CompanyForm company,HttpSession session){
    	Json res=new Json();
    	CompanyImformation companyImformation=companyService.queryCompanyById(getCompanyId(session));
    	companyImformation.setMessageAlert(company.getMessageAlert());
    	companyImformation.setMessageEmail(company.getMessageEmail());
    	companyImformation.setMessageMobile(company.getMessageMobile());
    	companyService.saveCompany(companyImformation);
    	res.setMsg("保存成功");
    	res.setRes(true);
    	return res;
    }
	
}
