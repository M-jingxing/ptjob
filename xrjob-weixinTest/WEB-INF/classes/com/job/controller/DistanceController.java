package com.job.controller;

import java.awt.JobAttributes;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.RespectBinding;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.job.form.DistanceForm;
import com.job.form.JobSearchForm;
import com.job.model.CompanyImformation;
import com.job.model.CompanyJob;
import com.job.model.JobInCompany;
import com.job.model.Json;
import com.job.model.Page;
import com.job.model.UserSearchRecord;
import com.job.service.CompanyService;
import com.job.service.DistanceService;
import com.job.service.JobFairService;
import com.job.service.JobService;
import com.job.service.JobTypeService;
import com.mchange.v2.sql.filter.SynchronizedFilterCallableStatement;
import com.sun.tracing.dtrace.Attributes;

/**
 * 职位搜索
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("distance")
public class DistanceController extends BaseController {

	@Autowired
	private DistanceService distanceService;

	@Autowired
	private JobFairService jobFairService;

	@Autowired
	private JobTypeService jobTypeService;

	@Autowired
	private JobService jobService;

	@Autowired
	private CompanyService companyService;

	/**
	 * 获取用户经纬度，并存入session
	 * 
	 * @param request
	 * @param form
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping("getLocation")
	public String getLocation(HttpServletRequest request, DistanceForm form, Model model, HttpSession session) {
		session.setAttribute("lng", form.getLng());
		session.setAttribute("lat", form.getLat());
		System.out.println("lng:" + form.getLng());
		System.out.println("lat:" + form.getLat());
		String isRead = "false";
		request.setAttribute("isRead", isRead);
		Page page = new Page();

		model.addAttribute("tops", jobTypeService.queryTopTypeList());
		model.addAttribute("newJobList", jobService.queryJobAll(page));

		return "xrjobPages/jobSearch";

	}

	/**
	 * 删除搜索记录
	 * 
	 * @param request
	 * @param key
	 * @param response
	 * @return
	 */
	@RequestMapping("delKey")
	@ResponseBody
	public Json delCookie(HttpServletRequest request, Integer recordId) {
		Json res = new Json();
		jobService.deleteSearchRecordById(recordId);
		res.setRes(true);
		return res;
	}

	/**
	 * 搜索框点击进入详细搜索界面
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("toJobSearchPage")
	public String toJobSearch(Model model, HttpServletRequest request, HttpSession session) {
		int personId = 0;
		if (session.getAttribute("personId") != null) {
			personId = (Integer) session.getAttribute("personId");

		}
		
		model.addAttribute("keyList", jobService.queryByUserIdAndRecordType(personId, 1));
		model.addAttribute("newJobList", jobService.jobListByJobHot(2));
        
		return "xrjobPages/search";
	}

	/**
	 * 职位搜索页面，获取用户定位
	 * 
	 * @param request
	 * @param form
	 * @param model
	 * @return
	 */
	@RequestMapping("toJobSearch.do")
	public String setLocation(HttpServletRequest request, Model model) {

		Page page = new Page();

		model.addAttribute("tops", jobTypeService.queryTopTypeList());
		model.addAttribute("newJobList", jobService.queryJobAll(page));

		return "xrjobPages/jobSearch";
	}

	/**
	 * 离你最近的岗位
	 * 
	 * @param form
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/distanceJob.do")
	public String jobInCompany(DistanceForm form, Model model, HttpServletRequest request) {
		model.addAttribute("jobs", distanceService.getDistanceJob(form.getLng(), form.getLat()));

		String isRead = "false";
		request.setAttribute("isRead", isRead);
		return "xrjobPages/nearHotJobs";

	}

	/**
	 * 招聘会
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/jobFair")
	public String jobFair(Model model) {
		model.addAttribute("jobFair", jobFairService.getJobFair());

		return "xrjobPages/jobfair";
	}

	/**
	 * 关键字搜索
	 * 
	 * @param request
	 * @param model
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = "/jobSearch")
	public String jobSearch(HttpServletRequest request, Model model, HttpServletResponse response, HttpSession session)
			throws UnsupportedEncodingException {

		/*
		 * request.setCharacterEncoding("UTF-8");
		 */
		String key = request.getParameter("key");
	    String isRecord=request.getParameter("isRecord");
			Page page = new Page();
			if (key.equals("")) {
				ArrayList list = new ArrayList();
				model.addAttribute("jobs", list);
				model.addAttribute("newJobList", jobService.queryJobAll(page));
			} else {
				
				if (session.getAttribute("personId") != null&&isRecord==null) {//插入用户搜索记录	
					int personId = (Integer) session.getAttribute("personId");
						UserSearchRecord userSearchRecord = new UserSearchRecord();
						userSearchRecord.setUserId(personId);
						userSearchRecord.setSearchRecord(key);
						userSearchRecord.setRecordType(1);
						jobService.insertSearchRecord(userSearchRecord);
						
						List<UserSearchRecord> records=jobService.querySearchRecordByUserId(personId);
						if(records.size()>6){//一个用户只保存6条搜索记录
							jobService.deleteSearchRecordById(records.get(records.size()-1).getRecordId());
						}
					}
				
				model.addAttribute("jobs", jobService.getJobListByKey(key));
				model.addAttribute("newJobList", jobService.queryJobAll(page));
			}

		

		return "xrjobPages/keySearch";

	}

	/**
	 * 热门搜索
	 * 
	 * @param request
	 * @param model
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = "/jobSearchHot")
	public String jobSearchHotKey(HttpServletRequest request, Model model) throws UnsupportedEncodingException {

		String key = request.getParameter("key");
		Page page = new Page();
		model.addAttribute("jobs", jobService.getJobListByKey(key));
		model.addAttribute("newJobList", jobService.queryJobAll(page));
		return "xrjobPages/keySearch";

	}

	/**
	 * 多条件职位搜索
	 * 
	 * @param request
	 * @param model
	 * @param form
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = "/jobSearch2")
	public String jobSearch2(HttpServletRequest request, Model model, JobSearchForm form)
			throws UnsupportedEncodingException {
		HttpSession session = request.getSession();
		
		Double lng = 0.0;
		Double lat = 0.0;
		if (session.getAttribute("lng") != null && session.getAttribute("lat") != null) {
			lng = (double) session.getAttribute("lng");
			lat = (double) session.getAttribute("lat");
		}
		Double range1 = 0.0;
		Double range2 = 1000000000.0;

		if (form.getDistance() != null) {
			Integer distance = form.getDistance();

			if (distance == 1) {
				range1 = 0.0;
				range2 = 1000.0;
			} else if (distance == 2) {
				range1 = 1000.0;
				range2 = 5000.0;
			} else if (distance == 3) {
				range1 = 5000.0;
				range2 = 7000.0;
			} else if (distance == 4) {
				range1 = 7000.0;
				range2 = 100000000.0;
			}

		}
		// 通过距离查找公司，并与公司对应的岗位信息组装成JobInCompany对象
		List<JobInCompany> jobComList = new ArrayList<JobInCompany>();
		List<CompanyImformation> companyList = distanceService.DistanceList(lng, lat, range1, range2);

		if (companyList.size() != 0) {

			for (int i = 0; i < companyList.size(); i++) {
				form.setCompanyId(companyList.get(i).getCompanyId());
				List<CompanyJob> jobList = jobService.queryJobSearch(form);
				if (jobList.size() != 0) {
					for (int j = 0; j < jobList.size(); j++) {
						JobInCompany jobInCompany = new JobInCompany(jobList.get(j).getJobId(),
								companyList.get(i).getCompanyId(), jobList.get(j).getJobName(),
								companyList.get(i).getCompanyName(), jobList.get(j).getJobSalaryId(),
								jobList.get(j).getJobAddress());
						jobInCompany.setCompanyLicense(companyList.get(i).getCompanyLicense());
						jobComList.add(jobInCompany);
					}
				}
			}
		}
		Page page = new Page();

		String isRead = "false";
		model.addAttribute("jobs", jobComList);
		model.addAttribute("isRead", isRead);
		model.addAttribute("tops", jobTypeService.queryTopTypeList());
		model.addAttribute("seconds", jobTypeService.querySecondTypeByTop(form.getTopId()));
		model.addAttribute("thirds", jobTypeService.queryThirdTypeBySecond(form.getSecondId()));
		model.addAttribute("form",form);
		model.addAttribute("newJobList", jobService.queryJobAll(page));
		return "xrjobPages/jobSearch";

	}

}
