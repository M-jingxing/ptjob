package com.job.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.job.form.ResumeForm;
import com.job.model.DataGrid;
import com.job.model.Json;
import com.job.model.UserResume;
import com.job.model.UserSearchRecord;
import com.job.service.AddressService;
import com.job.service.DeliveryService;
import com.job.service.JobService;
import com.job.service.JobTypeService;
import com.job.service.ResumeService;

/**
 * 企业简历管理
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("resume")
public class ResumeController extends BaseController {

	@Autowired
	private ResumeService resumeService;

	@Autowired
	private DeliveryService deliveryService;

	@Autowired
	private JobTypeService typeService;

	@Autowired
	private AddressService addressService;

	@Autowired
	private JobService jobService;

	/**
	 * 企业查看投递者简历
	 * 
	 * @param deliveryId
	 * @param resumeId
	 * @param jobId
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("viewResume.do")
	public String viewResume(Integer deliveryId, Integer resumeId, Integer jobId, Model model,
			HttpServletRequest request) {

		if (null != deliveryId && null != jobId) {
			deliveryService.browseResume(deliveryId, jobId);
		}
		model.addAttribute("userWorkList", resumeService.getWorkListByResumeId(resumeId));
		model.addAttribute("userEduList", resumeService.getEduListByResumeId(resumeId));
		model.addAttribute("resume", resumeService.selectResumeById(resumeId));
		model.addAttribute("tops", typeService.queryTopTypeList());
		model.addAttribute("cityList", addressService.queryAllCityList());

		return "backstagePages/viewResume";
	}

	/**
	 * 人才库界面
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("toTalentPool.do")
	public String toResumePage(Model model, ResumeForm form,HttpSession session) {
		model.addAttribute("tops", typeService.queryTopTypeList());
		model.addAttribute("cityList", addressService.queryAllCityList());
		model.addAttribute("recordList",jobService.queryByUserIdAndRecordType(getUserId(session), 2));
		model.addAttribute("intentJob", typeService.getThirdTypeList());
		return "backstagePages/talentPool";
	}

	/**
	 * 搜索人才简历列表
	 * 
	 * @param form
	 * @param session
	 * @param model
	 * @return
	 */
	@RequestMapping("queryResumePage.do")
	public String queryResumePage(ResumeForm form, HttpSession session, Model model) {
		if(form.getThirdId()!=null){
		// 插入搜索记录,若搜索行业相同则更新搜索记录
		List<UserSearchRecord> userSearchRecords = jobService.queryByUserIdAndIntentJob(getUserId(session),
				form.getThirdId());
		if (userSearchRecords.size() == 0) {
			UserSearchRecord userSearchRecord = new UserSearchRecord();
			userSearchRecord.setUserId(getUserId(session));
			userSearchRecord.setIntentJob(form.getThirdId());
			userSearchRecord.setHighDegree(form.getHighDegree());
			userSearchRecord.setSalaryId(form.getSalaryId());
			userSearchRecord.setSex(form.getSex());
			userSearchRecord.setWorkYear(form.getWorkYear());
			userSearchRecord.setRecordType(2);
			jobService.insertSearchRecord(userSearchRecord);
		} else {
			UserSearchRecord userSearchRecord = userSearchRecords.get(0);
			userSearchRecord.setIntentJob(form.getThirdId());
			userSearchRecord.setHighDegree(form.getHighDegree());
			userSearchRecord.setSalaryId(form.getSalaryId());
			userSearchRecord.setSex(form.getSex());
			userSearchRecord.setWorkYear(form.getWorkYear());
			userSearchRecord.setCreateTime(new Date());
			jobService.updateSearchRecord(userSearchRecord);
		}
		}
		form.setCompanyId(getCompanyId(session));
		/*model.addAttribute("tops", typeService.queryTopTypeList());*/
		model.addAttribute("intentJob", typeService.getThirdTypeList());
		model.addAttribute("resumeList", resumeService.queryResumePage(form));
		ResumeForm form2=new ResumeForm();
		model.addAttribute("newResumeList", resumeService.queryResumePage(form2));
		/* List<UserResume> resumeList=resumeService.queryResumePage(form); */
		return "backstagePages/talentPoolResult";
	}
	/**
	 * 删除历史记录
	 * @param records
	 * @return
	 */
	@RequestMapping("delRecord")
	@ResponseBody
	public Json delRecord(String records){
		Json res=new Json();
		String[] recordIds = records.split(",");
		for (int i = 0; i < recordIds.length; i++) {
            System.out.println(recordIds[i]);
            jobService.deleteSearchRecordById(Integer.parseInt(recordIds[i]));
        }
		res.setMsg("删除成功");
		res.setRes(true);
		return res;
	}
	
}
