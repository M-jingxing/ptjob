package com.job.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.SeekableByteChannel;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import jdk.nashorn.internal.ir.RuntimeNode.Request;

import jdk.nashorn.internal.scripts.JS;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.job.form.RegisterForm;
import com.job.model.CompanyImformation;
import com.job.model.User;
import com.job.model.Json;
import com.job.model.UserIntentJob;
import com.job.model.UserResume;
import com.job.model.UserResumeEducation;
import com.job.model.UserResumeWork;
import com.job.service.CompanyService;
import com.job.service.JobTypeService;
import com.job.service.PersonService;
import com.job.service.ResumeService;
import com.job.util.MD5Util;
import com.job.util.SensitivewordFilter;
import com.job.util.UploadUtil;
import com.job.weixinModel.Constants;
import com.mysql.jdbc.StringUtils;
import com.job.util.UploadUtil;;

/**
 * 个人用户管理
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("person")
public class PersonController {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private PersonService personService;
	@Autowired
	private ResumeService resumeService;
	@Autowired
	private JobTypeService typeService;

	/**
	 * 用户简历首页
	 * 
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping("toSaveResume.do")
	public String toSaveResume(Model model, HttpSession session) {
		Integer personId = (Integer) session.getAttribute("personId");
		UserResume userResumeByUser = resumeService.selectResumeByUser(personId);
		model.addAttribute("userResume", resumeService.selectResumeById(userResumeByUser.getResumeId()));
		model.addAttribute("userResumeWork", resumeService.getWorkListByResumeId(userResumeByUser.getResumeId()));
		model.addAttribute("userResumeEdu", resumeService.getEduListByResumeId(userResumeByUser.getResumeId()));
		return "xrjobPages/myResume";
	}

	/**
	 * 编辑用户基本信息首页
	 * 
	 * @param model
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping("toChangeInfo")
	public String toChangeInfo(Model model, HttpServletRequest request, HttpSession session) {
		Integer personId = (Integer) session.getAttribute("personId");
		UserResume userResumeByUser = resumeService.selectResumeByUser(personId);
		model.addAttribute("userResume", resumeService.selectResumeById(userResumeByUser.getResumeId()));

		return "xrjobPages/editInfo";
	}

	/**
	 * 编辑用户期望岗位界面
	 * 
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping("toChangeIntentJob")
	public String toChangeIntentJob(Model model, HttpSession session) {
		Integer personId = (Integer) session.getAttribute("personId");
		UserResume userResumeByUser = resumeService.selectResumeByUser(personId);
		UserIntentJob userIntentJob = typeService.getIntentJobByResumeId(userResumeByUser.getResumeId());
		Integer thirdId = 0;
		Integer secondId = 0;
		Integer topId = 0;
		if (userIntentJob != null) {
			thirdId = userIntentJob.getIntentJobId();
			secondId = typeService.getSecondType(thirdId);
			topId = typeService.getTopType(secondId);
		}

		model.addAttribute("thirdId", thirdId);
		model.addAttribute("secondId", secondId);
		model.addAttribute("topId", topId);
		model.addAttribute("userResume", resumeService.selectResumeById(userResumeByUser.getResumeId()));
		model.addAttribute("tops", typeService.queryTopTypeList());
		model.addAttribute("seconds", typeService.querySecondTypeByTop(topId));
		model.addAttribute("thirds", typeService.queryThirdTypeBySecond(secondId));
		return "xrjobPages/editJob";
	}

	/**
	 * 用户工作经验界面
	 * 
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping("toAddResumeWork")
	public String toAddResumeWork(Model model, HttpSession session) {
		Integer personId = (Integer) session.getAttribute("personId");
		UserResume userResumeByUser = resumeService.selectResumeByUser(personId);

		model.addAttribute("userResumeWork", resumeService.getWorkListByResumeId(userResumeByUser.getResumeId()));

		return "xrjobPages/editJobExp";
	}

	/**
	 * 编辑用户工作经验界面
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("toEditResumeWork")
	public String toEditResumeWork(Model model, HttpServletRequest request) {
		String workId = request.getParameter("workId");
		model.addAttribute("workDetail", resumeService.getWorkDetail(Integer.parseInt(workId)));
		return "xrjobPages/editJobExpDetail";
	}

	/**
	 * 用户教育经验界面
	 * 
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping("toAddResumeEdu")
	public String toAddResumeEdu(Model model, HttpSession session) {
		Integer personId = (Integer) session.getAttribute("personId");
		UserResume userResumeByUser = resumeService.selectResumeByUser(personId);
		model.addAttribute("userResumeEdu", resumeService.getEduListByResumeId(userResumeByUser.getResumeId()));
		return "xrjobPages/editEduExp";
	}

	/**
	 * 编辑用户教育经验界面
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("toEditResumeEdu")
	public String toEditResumeEdu(Model model, HttpServletRequest request) {
		String eduId = request.getParameter("eduId");
		model.addAttribute("eduDetail", resumeService.getEduDetail(Integer.parseInt(eduId)));
		return "xrjobPages/editEduExpDetail";
	}

	@RequestMapping("toEditUserDescribe")
	public String toEditUserDescribe(Model model, HttpSession session) {
		Integer personId = (Integer) session.getAttribute("personId");
		UserResume userResumeByUser = resumeService.selectResumeByUser(personId);
		model.addAttribute("resumeId", userResumeByUser.getResumeId());
		model.addAttribute("userDescribe", userResumeByUser.getUserDescribe());
		return "xrjobPages/editNote";
	}

	/**
	 * 更新用户基本信息
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("saveResume.do")
	@ResponseBody
	public Json saveresume(HttpServletRequest request, Model model, HttpSession session) throws Exception {

		Json res = new Json();
		String resumeId = request.getParameter("resumeId");
		String resumeUserName = request.getParameter("resumeUserName");
		String sex = request.getParameter("sex");
		String birthDate = request.getParameter("birthDate");
		String highDegree = request.getParameter("highDegree");
		String jobStatus = request.getParameter("jobStatus");
		String toJobDate = request.getParameter("toJobDate");
		String workYear = request.getParameter("workYear");
		String originPlace = request.getParameter("originPlace");
		String userMobile = request.getParameter("userMobile");
		String userEmail = request.getParameter("userEmail");
		String userDescribe = request.getParameter("userDescribe");

		String path = session.getServletContext().getRealPath("/") + "SensitiveWord.txt";
		Constants.sensitiveWord_path = path;
		SensitivewordFilter filter = new SensitivewordFilter();
		if (resumeUserName != null && !resumeUserName.equals("")) {
			resumeUserName = filter.replaceSensitiveWord(resumeUserName, 1, "*");
		}
		if (originPlace != null && !originPlace.equals("")) {
			originPlace = filter.replaceSensitiveWord(originPlace, 1, "*");
		}
		if (userDescribe != null && !userDescribe.equals("")) {
			userDescribe = filter.replaceSensitiveWord(userDescribe, 1, "*");
		}

		UserResume userResume = new UserResume();
		if (resumeId != null && !resumeId.equals("")) {
			userResume.setResumeId(Integer.parseInt(resumeId));
		}
		userResume.setResumeUserName(resumeUserName);
		if (sex != null && !sex.equals("")) {
			userResume.setSex(Integer.parseInt(sex));
		}
		SimpleDateFormat dFormater = new SimpleDateFormat("yyyy-MM-dd");
		Date d = null;
		if (birthDate != null && !birthDate.equals("")) {
			try {
				d = dFormater.parse(birthDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		userResume.setBirthDate(d);
		if (highDegree != null && !highDegree.equals("")) {
			userResume.setHighDegree(Integer.parseInt(highDegree));
		}
		if (jobStatus != null && !jobStatus.equals("")) {
			userResume.setJobStatus(Integer.parseInt(jobStatus));
		}
		if (toJobDate != null && !toJobDate.equals("")) {
			userResume.setToJobDate(Integer.parseInt(toJobDate));
		}
		if (workYear != null && !workYear.equals("")) {
			userResume.setWorkYear(Integer.parseInt(workYear));
		}
		userResume.setOriginPlace(originPlace);
		userResume.setUserMobile(userMobile);
		userResume.setUserEmail(userEmail);
		userResume.setUserDescribe(userDescribe);
		userResume.setModifyTime(new Date());
		resumeService.updateByPrimaryKeySelective(userResume);

		res.setMsg("保存成功");
		res.setRes(true);
		return res;
	}

	/**
	 * 更新用户期望岗位
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping("saveIntentJob")
	@ResponseBody
	public Json saveIntentJob(HttpServletRequest request, Model model, HttpSession session) {
		Json res = new Json();

		String resumeId = request.getParameter("resumeId");
		String strintentJob = request.getParameter("intentJob");
		String jobType = request.getParameter("jobType");
		String salaryId = request.getParameter("salaryId");

		if (strintentJob != null && !strintentJob.equals("")) {
			UserIntentJob userIntentJobOld = typeService.getIntentJobByResumeId(Integer.parseInt(resumeId));
			Integer intentJob = Integer.parseInt(strintentJob);
			UserIntentJob userIntentJob = new UserIntentJob();
			userIntentJob.setIntentJobId(intentJob);
			userIntentJob.setResumeId(Integer.parseInt(resumeId));
			if (userIntentJobOld == null) {
				resumeService.insertIntentJob(userIntentJob);
			} else {
				userIntentJob.setIntentId(userIntentJobOld.getIntentId());
				resumeService.updateIntentJob(userIntentJob);
			}
		}

		if (jobType != null && !jobType.equals("") || salaryId != null && !salaryId.equals("")) {
			UserResume userResume = new UserResume();
			userResume.setJobType(Integer.parseInt(jobType));
			userResume.setSalaryId(Integer.parseInt(salaryId));
			userResume.setResumeId(Integer.parseInt(resumeId));
			resumeService.updateByPrimaryKeySelective(userResume);

		}
		res.setMsg("保存成功");
		res.setRes(true);
		return res;
	}

	/**
	 * 添加用户工作经验
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("addResumeWork")
	@ResponseBody
	public Json addResumeWork(HttpServletRequest request, Model model, HttpSession session) throws Exception {
		Json res = new Json();
		Integer personId = (Integer) session.getAttribute("personId");
		UserResume userResumeByUser = resumeService.selectResumeByUser(personId);
		Integer resumeId = userResumeByUser.getResumeId();
		String companyName = request.getParameter("companyName");
		String userJob = request.getParameter("userJob");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String workDescribe = request.getParameter("workDescribe");
		// 替换敏感词汇
		String path = session.getServletContext().getRealPath("/") + "SensitiveWord.txt";
		Constants.sensitiveWord_path = path;
		SensitivewordFilter filter = new SensitivewordFilter();
		companyName = filter.replaceSensitiveWord(companyName, 1, "*");
		userJob = filter.replaceSensitiveWord(userJob, 1, "*");
		if (workDescribe != null && !workDescribe.equals("")) {
			workDescribe = filter.replaceSensitiveWord(workDescribe, 1, "*");
		}

		UserResumeWork userResumeWork = new UserResumeWork();
		userResumeWork.setResumeId(resumeId);
		userResumeWork.setCompanyName(companyName);
		userResumeWork.setUserJob(userJob);
		userResumeWork.setStartDate(startDate);
		userResumeWork.setEndDate(endDate);
		userResumeWork.setWorkDescribe(workDescribe);
		userResumeWork.setCreateTime(new Date());
		resumeService.insertResumeWork(userResumeWork);

		res.setMsg("保存成功");
		res.setRes(true);
		return res;
	}

	/**
	 * 删除用户工作经验
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("deleteResumeWork")
	@ResponseBody
	public Json deleteResumeWork(HttpServletRequest request) {
		Json res = new Json();
		String workId = request.getParameter("workId");
		resumeService.deleteResumeWork(Integer.parseInt(workId));
		res.setMsg("删除成功");
		res.setRes(true);
		return res;
	}

	/**
	 * 编辑用户工作经验
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("editResumeWork")
	@ResponseBody
	public Json editResumeWork(HttpServletRequest request, Model model, HttpSession session) throws Exception {
		Json res = new Json();
		Integer personId = (Integer) session.getAttribute("personId");
		UserResume userResumeByUser = resumeService.selectResumeByUser(personId);
		Integer resumeId = userResumeByUser.getResumeId();
		String workId = request.getParameter("workId");
		String companyName = request.getParameter("companyName");
		String userJob = request.getParameter("userJob");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String workDescribe = request.getParameter("workDescribe");
		// 替换敏感词汇
		String path = session.getServletContext().getRealPath("/") + "SensitiveWord.txt";
		Constants.sensitiveWord_path = path;
		SensitivewordFilter filter = new SensitivewordFilter();
		companyName = filter.replaceSensitiveWord(companyName, 1, "*");
		userJob = filter.replaceSensitiveWord(userJob, 1, "*");
		if (workDescribe != null && !workDescribe.equals("")) {
			workDescribe = filter.replaceSensitiveWord(workDescribe, 1, "*");
		}

		UserResumeWork userResumeWork = new UserResumeWork();
		userResumeWork.setWorkId(Integer.parseInt(workId));
		userResumeWork.setResumeId(resumeId);
		userResumeWork.setCompanyName(companyName);
		userResumeWork.setUserJob(userJob);
		userResumeWork.setStartDate(startDate);
		userResumeWork.setEndDate(endDate);
		userResumeWork.setWorkDescribe(workDescribe);
		userResumeWork.setCreateTime(new Date());
		resumeService.updateResumeWork(userResumeWork);

		res.setMsg("保存成功");
		res.setRes(true);
		return res;
	}

	/**
	 * 添加用户教育经验
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("addResumeEdu")
	@ResponseBody
	public Json addResumeEdu(HttpServletRequest request, Model model, HttpSession session) throws Exception {
		Json res = new Json();
		Integer personId = (Integer) session.getAttribute("personId");
		UserResume userResumeByUser = resumeService.selectResumeByUser(personId);
		Integer resumeId = userResumeByUser.getResumeId();
		String schoolName = request.getParameter("schoolName");
		String professionalName = request.getParameter("professionalName");
		String educationGrade = request.getParameter("educationGrade");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String professionalDescribe = request.getParameter("professionalDescribe");

		// 替换敏感词汇
		String path = session.getServletContext().getRealPath("/") + "SensitiveWord.txt";
		Constants.sensitiveWord_path = path;
		SensitivewordFilter filter = new SensitivewordFilter();
		schoolName = filter.replaceSensitiveWord(schoolName, 1, "*");
		professionalName = filter.replaceSensitiveWord(professionalName, 1, "*");
		if (professionalDescribe != null && !professionalDescribe.equals("")) {
			professionalDescribe = filter.replaceSensitiveWord(professionalDescribe, 1, "*");
		}
		UserResumeEducation userResumeEducation = new UserResumeEducation();
		userResumeEducation.setResumeId(resumeId);
		userResumeEducation.setSchoolName(schoolName);
		userResumeEducation.setProfessionalName(professionalName);
		userResumeEducation.setStartDate(startDate);
		userResumeEducation.setEndDate(endDate);
		userResumeEducation.setProfessionalDescribe(professionalDescribe);
		if (educationGrade != null && !educationGrade.equals("")) {
			userResumeEducation.setEducationGrade(Integer.parseInt(educationGrade));
		}
		resumeService.insertResumeEdu(userResumeEducation);

		res.setMsg("保存成功");
		res.setRes(true);
		return res;
	}

	/**
	 * 删除用户教育经验
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("deleteResumeEdu")
	@ResponseBody
	public Json deleteResumeEdu(HttpServletRequest request) {
		Json res = new Json();
		String eduId = request.getParameter("eduId");
		resumeService.deleteResumeEdu(Integer.parseInt(eduId));
		res.setMsg("删除成功");
		res.setRes(true);
		return res;
	}

	/**
	 * 编辑用户教育经验
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("editResumeEdu")
	@ResponseBody
	public Json editResumeEdu(HttpServletRequest request, Model model, HttpSession session) throws Exception {
		Json res = new Json();
		Integer personId = (Integer) session.getAttribute("personId");
		UserResume userResumeByUser = resumeService.selectResumeByUser(personId);
		Integer resumeId = userResumeByUser.getResumeId();
		String eduId = request.getParameter("eduId");
		String schoolName = request.getParameter("schoolName");
		String professionalName = request.getParameter("professionalName");
		String educationGrade = request.getParameter("educationGrade");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String professionalDescribe = request.getParameter("professionalDescribe");

		// 替换敏感词汇
		String path = session.getServletContext().getRealPath("/") + "SensitiveWord.txt";
		Constants.sensitiveWord_path = path;
		SensitivewordFilter filter = new SensitivewordFilter();
		schoolName = filter.replaceSensitiveWord(schoolName, 1, "*");
		professionalName = filter.replaceSensitiveWord(professionalName, 1, "*");
		if (professionalDescribe != null && !professionalDescribe.equals("")) {
			professionalDescribe = filter.replaceSensitiveWord(professionalDescribe, 1, "*");
		}
		UserResumeEducation userResumeEducation = new UserResumeEducation();
		userResumeEducation.setEducationId(Integer.parseInt(eduId));
		userResumeEducation.setResumeId(resumeId);
		userResumeEducation.setSchoolName(schoolName);
		userResumeEducation.setProfessionalName(professionalName);
		userResumeEducation.setStartDate(startDate);
		userResumeEducation.setEndDate(endDate);
		userResumeEducation.setProfessionalDescribe(professionalDescribe);
		if (educationGrade != null && !educationGrade.equals("")) {
			userResumeEducation.setEducationGrade(Integer.parseInt(educationGrade));
		}
		resumeService.updateResumeEdu(userResumeEducation);

		res.setMsg("保存成功");
		res.setRes(true);
		return res;
	}

	/**
	 * 上传用户头像
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	/*@RequestMapping("uploadPic")
	@ResponseBody
	public Json uploadPic(HttpServletRequest request, HttpServletResponse response, HttpSession session) {

		Json result = new Json();
		DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
		String ymd = dateFormat.format(new Date());
		String path = session.getServletContext().getRealPath("/");
		path = path.substring(0, path.lastIndexOf("\\"));
		path = path.substring(0, path.lastIndexOf("\\"));
		System.out.println(path);
		path = path + "/xrjob-img" + "/images/user/" + ymd + "/";

		File floder = new File(path);
		if (!floder.exists()) {
			logger.info("创建目录：" + path);
			floder.mkdirs();
		}
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;

		String fileName = "";
		Iterator<String> it = multipartRequest.getFileNames();

		while (it.hasNext()) {
			String oldName = it.next();
			CommonsMultipartFile ogiFile = (CommonsMultipartFile) multipartRequest.getFile(oldName);

			if (ogiFile.isEmpty())
				continue;

			if (ogiFile.getSize() > 5242880) {
				logger.error("文件大小超过5M");
				result.setMsg("请上传小于5M的图片");
				return result;
			}

			String originalFilename = ogiFile.getOriginalFilename();

			String fileSuffix = originalFilename.substring(originalFilename.lastIndexOf("."));
			if (!fileSuffix.equalsIgnoreCase(".jpg") && !fileSuffix.equalsIgnoreCase(".png")
					&& !fileSuffix.equalsIgnoreCase(".gif") && !fileSuffix.equalsIgnoreCase(".jpeg")) {
				logger.error(String.format("文件后缀不是图片, %s", fileSuffix));
				result.setMsg("请选择以.png、.jpg、.gif或.jpeg结尾的图片");
				return result;
			}

			// 生成新的文件
			fileName = UUID.randomUUID() + fileSuffix;

			UploadUtil uploadutil = new UploadUtil();
			try {
				uploadutil.uploadImage1(multipartRequest, ogiFile, ogiFile.getContentType(),
						ogiFile.getOriginalFilename(), path + fileName);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			
			 * File imageFile = new File(path, fileName); try {
			 * FileCopyUtils.copy(ogiFile.getBytes(), imageFile); } catch
			 * (IOException e) { logger.error("上传图片发生错误", e); }
			 

			fileName = "images/user/" + ymd + "/" + fileName;
			logger.info("新生成图片文件名：" + fileName);
			break;
		}

		Integer personId = (Integer) session.getAttribute("personId");
		UserResume userResumeByUser = resumeService.selectResumeByUser(personId);
		Integer resumeId = userResumeByUser.getResumeId();
		userResumeByUser.setResumeId(resumeId);
		userResumeByUser.setResumePic(fileName);
		resumeService.updateByPrimaryKeySelective(userResumeByUser);

		result.setRes(true);
		result.setMsg(fileName);
		System.out.println("fileName:" + fileName);
		return result;
	}*/

	/**
	 * 用户登录
	 * 
	 * @param userMobile
	 * @param password
	 * @param session
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("login.do")
	@ResponseBody
	public Json login(String userMobile, String password, HttpSession session, HttpServletRequest request,
			Model model) {
		Json res = new Json();
		if (StringUtils.isNullOrEmpty(userMobile) || StringUtils.isNullOrEmpty(password)) {
			logger.error("用户登录，参数为空，userMobile:" + userMobile);
			res.setMsg("用户名或密码为空");
			return res;
		}
		User user = personService.selectUserByName(userMobile);
		if (null == user) {
			logger.error("用户登录，用户不存在，userMobile:" + userMobile);
			res.setMsg("用户不存在");
			return res;
		}
		if (user.getPassword() == null || (!user.getPassword().equals(MD5Util.getMD5Str(password)))) {
			logger.error("用户登录，密码错误，userMobile:" + userMobile);
			res.setMsg("用户名或密码错误");
			return res;
		}
		// 冻结用户
		if (user.getUserStatus().equals(2)) {
			logger.error("用户登录，用户被冻结，userMobile:" + userMobile);
			res.setMsg("用户已被冻结，请联系管理员");
			return res;
		}
		String openId = (String) session.getAttribute("openId");
		user.setOpenId(openId);
		user.setLastLoginTime(new Date());
		personService.changeUser(user);
		session.setAttribute("personStatus", user.getUserStatus());
		session.setAttribute("personId", user.getUserId());
		session.setAttribute("userMobile", userMobile);
		res.setStatus(user.getUserStatus());
		res.setRes(true);
		model.addAttribute("res", res);
		return res;
	}

	/**
	 * 用户注册
	 * 
	 * @param checkCode
	 * @param loginName
	 * @param password
	 * @param surePassword
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping("register.do")
	@ResponseBody
	public Json register(String loginName, String password, String surePassword, Model model,
			HttpSession session) {
		Json res = new Json();
		if (StringUtils.isNullOrEmpty(loginName) || StringUtils.isNullOrEmpty(password)
				|| StringUtils.isNullOrEmpty(surePassword)) {
			logger.error("用户注册，参数为空，loginName:" + loginName);
			res.setMsg("用户名或密码为空");
			return res;
		}

		if (!checkRegForm(loginName, password, surePassword, res, session)) {
			logger.error("用户注册，表单错误，" + loginName);
			return res;
		}
		if (null != personService.selectUserByName(loginName)) {
			logger.error("用户注册，用户已存在，loginName:" + loginName);
			res.setMsg("该用户名已经注册，请重新输入");
			return res;
		}
		Integer personId = personService.register(loginName, password);
		if (null == personId) {
			logger.error("用户注册，注册失败，loginName:" + loginName);
			res.setMsg("注册失败");
			return res;
		}
		session.setAttribute("personId", personId);
		session.setAttribute("userMobile", loginName);

		UserResume userResume = new UserResume();
		userResume.setUserId(personId);
		userResume.setCreateTime(new Date());
		resumeService.insertSelective(userResume);

		res.setRes(true);
		return res;
	}

	/**
	 * 用户注销
	 * 
	 * @param session
	 * @return
	 */

	@RequestMapping("quitUser")
	public String quitUser(HttpSession session) {
		User user = personService.selectUserById((Integer) session.getAttribute("personId"));
		user.setOpenId("1");
		personService.changeUser(user);
		session.invalidate();
		return "index";
	}

	/**
	 * 用户修改密码
	 * 
	 * @param loginName
	 * @param password
	 * @param surePassword
	 * @param checkCode
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping("changePwd")
	@ResponseBody
	public Json changePwd(String loginName, String password, String surePassword,
			HttpServletRequest request, HttpSession session) {
		Json res = new Json();

		if (StringUtils.isNullOrEmpty(loginName) || StringUtils.isNullOrEmpty(password)
				|| StringUtils.isNullOrEmpty(surePassword)) {
			logger.error("用户注册，参数为空，loginName:" + loginName);
			res.setMsg("用户名或密码为空");
			return res;
		}

		if (!checkRegForm(loginName, password, surePassword, res, session)) {
			logger.error("用户注册，表单错误，" + loginName);
			return res;
		}

		User user = personService.selectUserByName(loginName);

		if (user == null) {
			res.setMsg("该用户不存在");
			return res;
		} else {
			user.setPassword(MD5Util.getMD5Str(password));
			personService.changeUser(user);
		}

		res.setMsg("操作成功");
		res.setRes(true);
		return res;
	}

	

	private boolean checkRegForm(String loginName, String password, String surePassword, Json res,
			HttpSession session) {
		// Pattern p = Pattern.compile("[^0-9]");
		// Matcher m = p.matcher(loginName);
		// if(m.find()){
		// res.setMsg("请输入正确的用户名");
		// return false;
		// }

		if (password.length() < 6) {
			res.setMsg("请至少输入6位密码");
			return false;
		}

		if (!password.equals(surePassword)) {
			res.setMsg("两次密码不一致");
			return false;
		}

		// Object code = session.getAttribute("checkCode");
		// if(code == null || !code.toString().equalsIgnoreCase(checkCode)){
		// res.setMsg("请输入正确的验证码");
		// return false;
		// }
		return true;
	}

	private static String createRandomVcode() {
		// 验证码
		String vcode = "";
		for (int i = 0; i < 6; i++) {
			vcode = vcode + (int) (Math.random() * 9);
		}
		return vcode;
	}

	private static String MD5(String sourceStr) {
		String result = "";
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(sourceStr.getBytes());
			byte b[] = md.digest();
			int i;
			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
			result = buf.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String encrypt(String content, String password) {
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
			int blockSize = cipher.getBlockSize();
			byte[] dataBytes = content.getBytes();
			int plaintextLength = dataBytes.length;
			if (plaintextLength % blockSize != 0) {
				plaintextLength = plaintextLength + (blockSize - (plaintextLength % blockSize));
			}
			byte[] plaintext = new byte[plaintextLength];
			System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);
			SecretKeySpec keyspec = new SecretKeySpec(password.getBytes(), "AES");
			IvParameterSpec ivspec = new IvParameterSpec(password.getBytes());
			cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
			byte[] encrypted = cipher.doFinal(plaintext);
			return parseByte2HexStr(encrypted);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String parseByte2HexStr(byte buf[]) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < buf.length; i++) {
			String hex = Integer.toHexString(buf[i] & 0xFF);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}
			sb.append(hex.toUpperCase());
		}
		return sb.toString();
	}
}
