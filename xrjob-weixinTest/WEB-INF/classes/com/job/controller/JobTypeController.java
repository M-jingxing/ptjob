package com.job.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.job.model.JobTypeSecond;
import com.job.model.JobTypeThird;
import com.job.model.JobTypeTop;
import com.job.service.JobTypeService;
/**
 * 工作行业管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("type")
public class JobTypeController {
	
	@Autowired
	private JobTypeService typeService;
	
	@RequestMapping("queryTopTypeList.do")
	@ResponseBody
	public List<JobTypeTop> queryTopTypeList(){
		return  typeService.queryTopTypeList();
	}

	/**
	 * 修改一级类型
	 * @param topId
	 * @return
	 */
	@RequestMapping("changeTopType.do")
	@ResponseBody
	public List<JobTypeSecond> changeTopType(Integer topId){
		if(null == topId) return null;
		return  typeService.querySecondTypeByTop(topId);
		
	}
	
	/**
	 * 修改二级类型
	 * @param topId
	 * @return
	 */
	@RequestMapping("changeSecondType.do")
	@ResponseBody
	public List<JobTypeThird> changeSecondType(Integer secondId){
		if(null == secondId) return null;
		
		return  typeService.queryThirdTypeBySecond(secondId);
		
	}
}
