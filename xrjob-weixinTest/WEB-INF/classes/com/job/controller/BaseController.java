package com.job.controller;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.job.model.CompanyUser;
import com.job.model.User;
import com.job.service.PersonService;
import com.job.service.UserService;
import com.job.util.WeixinUtil;

@Controller
@RequestMapping("/baseController")
public class BaseController {
    
	@Autowired
	private PersonService personService;
	@Autowired
	private UserService userService;
	
	/**
	 * 微信个人用户入口
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/getOpenId.do",method=RequestMethod.GET)
	public String getOpenId(HttpServletRequest request,Model model) {
		 
	    String code = request.getParameter("code");
        System.out.println("code:"+code);
        String openId=  WeixinUtil.setOpenId(code);
        HttpSession session=request.getSession();
        session.setAttribute("openId", openId);
	    User user=personService.selectUserByOpenId(openId);
	    if(user==null){
	    session.setAttribute("personId", null);
		session.setAttribute("userMobile", null);
	    }else {
	    	session.setAttribute("personId", user.getUserId());
			session.setAttribute("userMobile", user.getUserMobile());
		}
	    return "index";
	}
	/**
	 * 微信企业入口
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/companyUserGetOpenId.do",method=RequestMethod.GET)
	public String companyUserGetOpenId(HttpServletRequest request,Model model) {
	    System.out.println("sss-------------------------");
	    String code = request.getParameter("code");
        System.out.println("code:"+code);
        String openId=  WeixinUtil.setOpenId(code);
        HttpSession session=request.getSession();
        session.setAttribute("openId", openId);
	    CompanyUser companyUser=userService.selectByOpenId(openId);
	    if(companyUser==null){
	    session.setAttribute("userId", null);
	    session.setAttribute("userStatus", null);
	    session.setAttribute("companyId",null);
	       return "login";
	    }else {
	    	session.setAttribute("userId", companyUser.getUserId());
	    	session.setAttribute("userStatus", companyUser.getUserStatus());
		    session.setAttribute("companyId",companyUser.getCompanyId());
	    	 return "backstagePages/menu";
		}
	   
	}
	/**
	 * chy add 
	 * app企业入口
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/companyUserFromApp.do",method=RequestMethod.GET)
	public String companyUserFromApp(HttpServletRequest request,Model model) {
		System.out.println("退出企业登录之后进入");
        HttpSession session=request.getSession();
	    CompanyUser companyUser=null;
	    if(companyUser==null){
	    session.setAttribute("userId", null);
	    session.setAttribute("userStatus", null);
	    session.setAttribute("companyId",null);
	    session.setAttribute("apploginflag", 11);
	    System.out.println("进入企业登录界面");
	       return "login";
	    }else {
	    	session.setAttribute("userId", companyUser.getUserId());
	    	session.setAttribute("userStatus", companyUser.getUserStatus());
		    session.setAttribute("companyId",companyUser.getCompanyId());
	    	 return "backstagePages/menu";
		}  
	}
	
	
	
	public Integer getCompanyId(HttpSession session){
		Integer companyId = (Integer) session.getAttribute("companyId");
		return companyId;
	}
	
	public Integer getUserId(HttpSession session){
		Integer userId = (Integer) session.getAttribute("userId");
		return userId;
	}
	
	
}
