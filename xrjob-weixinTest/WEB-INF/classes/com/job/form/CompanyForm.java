package com.job.form;

import java.util.Date;

public class CompanyForm {
	    private Integer companyId;
	    private String companyName;
	    private String companyDescribe;
	    private String companyAddress;
	    private Double companyLongitude;
	    private Double companyLatitude;
	    private Integer companyScale;
	    private Integer companyNature;
	    private Date createTime;
	    private Date modifyTime;
	    private String companyContractPerson;
	    private String companyContractMobile;
	    private String companyLicense;
	    
	    private Integer messageAlert;
	    private String messageEmail;
	    private String messageMobile; 
	    private String messageWechat;
	    
	    
	    
	    
		public Integer getMessageAlert() {
			return messageAlert;
		}
		public void setMessageAlert(Integer messageAlert) {
			this.messageAlert = messageAlert;
		}
		public String getMessageEmail() {
			return messageEmail;
		}
		public void setMessageEmail(String messageEmail) {
			this.messageEmail = messageEmail;
		}
		public String getMessageMobile() {
			return messageMobile;
		}
		public void setMessageMobile(String messageMobile) {
			this.messageMobile = messageMobile;
		}
		public String getMessageWechat() {
			return messageWechat;
		}
		public void setMessageWechat(String messageWechat) {
			this.messageWechat = messageWechat;
		}
		public Integer getCompanyId() {
			return companyId;
		}
		public void setCompanyId(Integer companyId) {
			this.companyId = companyId;
		}
		public String getCompanyName() {
			return companyName;
		}
		public void setCompanyName(String companyName) {
			this.companyName = companyName;
		}
		public String getCompanyDescribe() {
			return companyDescribe;
		}
		public void setCompanyDescribe(String companyDescribe) {
			this.companyDescribe = companyDescribe;
		}
		public String getCompanyAddress() {
			return companyAddress;
		}
		public void setCompanyAddress(String companyAddress) {
			this.companyAddress = companyAddress;
		}
		public Double getCompanyLongitude() {
			return companyLongitude;
		}
		public void setCompanyLongitude(Double companyLongitude) {
			this.companyLongitude = companyLongitude;
		}
		public Double getCompanyLatitude() {
			return companyLatitude;
		}
		public void setCompanyLatitude(Double companyLatitude) {
			this.companyLatitude = companyLatitude;
		}
		public Integer getCompanyScale() {
			return companyScale;
		}
		public void setCompanyScale(Integer companyScale) {
			this.companyScale = companyScale;
		}
		public Integer getCompanyNature() {
			return companyNature;
		}
		public void setCompanyNature(Integer companyNature) {
			this.companyNature = companyNature;
		}
		public Date getCreateTime() {
			return createTime;
		}
		public void setCreateTime(Date createTime) {
			this.createTime = createTime;
		}
		public Date getModifyTime() {
			return modifyTime;
		}
		public void setModifyTime(Date modifyTime) {
			this.modifyTime = modifyTime;
		}
		public String getCompanyContractPerson() {
			return companyContractPerson;
		}
		public void setCompanyContractPerson(String companyContractPerson) {
			this.companyContractPerson = companyContractPerson;
		}
		public String getCompanyContractMobile() {
			return companyContractMobile;
		}
		public void setCompanyContractMobile(String companyContractMobile) {
			this.companyContractMobile = companyContractMobile;
		}
		public String getCompanyLicense() {
			return companyLicense;
		}
		public void setCompanyLicense(String companyLicense) {
			this.companyLicense = companyLicense;
		}
	    
}
