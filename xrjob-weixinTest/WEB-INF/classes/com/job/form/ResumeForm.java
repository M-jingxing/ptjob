package com.job.form;

import com.job.model.Page;


public class ResumeForm extends Page {

	
	private Integer topId;
	
	
	private Integer secondId;
	
	private Integer thirdId;
	
	
	private Integer cityId;
	
	
	private Integer highDegree;
	
	
	private Integer workYear;
	
	
	private Integer salaryId;
	
	private Integer companyId;

	private Integer sex;
	

	public Integer getTopId() {
		return topId;
	}

	public void setTopId(Integer topId) {
		this.topId = topId;
	}

	public Integer getSecondId() {
		return secondId;
	}

	public void setSecondId(Integer secondId) {
		this.secondId = secondId;
	}

	public Integer getThirdId() {
		return thirdId;
	}

	public void setThirdId(Integer thirdId) {
		this.thirdId = thirdId;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getHighDegree() {
		return highDegree;
	}

	public void setHighDegree(Integer highDegree) {
		this.highDegree = highDegree;
	}

	public Integer getWorkYear() {
		return workYear;
	}

	public void setWorkYear(Integer workYear) {
		this.workYear = workYear;
	}

	public Integer getSalaryId() {
		return salaryId;
	}

	public void setSalaryId(Integer salaryId) {
		this.salaryId = salaryId;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

}
