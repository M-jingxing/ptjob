package com.job.form;

public class DistanceForm {
   private Double lng;
   private Double lat;
  
   

public Double getLng() {
	return lng;
}
public void setLng(Double lng) {
	this.lng = lng;
}
public Double getLat() {
	return lat;
}
public void setLat(Double lat) {
	this.lat = lat;
}
public DistanceForm() {
	super();
	// TODO Auto-generated constructor stub
}
public DistanceForm(Double lng, Double lat) {
	super();
	this.lng = lng;
	this.lat = lat;
}

}
