package com.job.dao;

import java.util.List;

import com.job.model.UserResumeWork;

public interface UserResumeWorkMapper {
    int deleteByPrimaryKey(Integer workId);

    int insert(UserResumeWork record);

    int insertSelective(UserResumeWork record);

    UserResumeWork selectByPrimaryKey(Integer workId);

    int updateByPrimaryKeySelective(UserResumeWork record);

    int updateByPrimaryKey(UserResumeWork record);
    
    List<UserResumeWork> getWorkListByResumeId(Integer resumeId);
}