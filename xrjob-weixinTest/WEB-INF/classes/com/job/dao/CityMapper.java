package com.job.dao;

import java.util.List;

import com.job.model.AreaModel;
import com.job.model.CityModel;

public interface CityMapper {

	List<CityModel> queryCityList();
	
	List<CityModel> queryAllCity();
	
	List<AreaModel> queryAreaByCity(Integer cityId);
}
