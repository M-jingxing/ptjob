package com.job.dao;

import java.util.List;

import com.job.model.CompanyImformation;

public interface DistanceJobMapper {
     
    List<CompanyImformation> getDistanceCompany();
}
