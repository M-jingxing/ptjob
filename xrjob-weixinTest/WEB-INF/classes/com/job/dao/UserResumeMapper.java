package com.job.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.job.form.ResumeForm;
import com.job.model.UserResume;

public interface UserResumeMapper {
    int deleteByPrimaryKey(Integer resumeId);

    int insert(UserResume record);

    int insertSelective(UserResume record);

    UserResume selectByPrimaryKey(Integer resumeId);

    int updateByPrimaryKeySelective(UserResume record);

    int updateByPrimaryKey(UserResume record);
    
    UserResume selectResumeById(Integer resumeId);
    
    UserResume selectResumeByUser(Integer userId);
    
    List<UserResume> queryResumeList(@Param(value = "cond") ResumeForm form);
    
    int queryResumeCount(@Param(value = "cond") ResumeForm form);
}