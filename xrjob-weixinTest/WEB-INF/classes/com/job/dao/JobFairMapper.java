package com.job.dao;

import java.util.List;

import com.job.model.JobFair;

public interface JobFairMapper {
   List<JobFair> getJobFair();
}
