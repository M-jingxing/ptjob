package com.job.dao;

import com.job.model.UserIntentJob;

public interface UserIntentJobMapper {

	int deleteByPrimaryKey(Integer intentId);

    int insert(UserIntentJob record);

    int insertSelective(UserIntentJob record);

    UserIntentJob selectByPrimaryKey(Integer intentId);

    int updateByPrimaryKeySelective(UserIntentJob record);

    int updateByPrimaryKey(UserIntentJob record);
    
    UserIntentJob selectByResumeId(Integer resumeId);
}