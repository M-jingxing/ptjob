package com.job.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.job.form.JobForm;
import com.job.form.JobSearchForm;
import com.job.form.ResumeForm;
import com.job.model.CompanyJob;
import com.job.model.Page;
import com.job.model.UserResume;

public interface CompanyJobMapper {
    int deleteByPrimaryKey(Integer jobId);

    int insert(CompanyJob record);

    int insertSelective(CompanyJob record);

    CompanyJob selectByPrimaryKey(Integer jobId);
    
    CompanyJob selectByPrimaryKeyAndCompany(@Param(value = "jobId") Integer jobId, @Param(value = "companyId") Integer companyId);

    int updateByPrimaryKeySelective(CompanyJob record);

    int updateByPrimaryKey(CompanyJob record);
    
    List<CompanyJob> queryJobByCompany(Integer companyId);
    
    List<CompanyJob> queryJobList(@Param(value = "cond") JobForm form);
    
    int queryJobCount(@Param(value = "cond") JobForm form);
    
    List<CompanyJob> jobListByKey(String keyword);
    
    List<CompanyJob> jobListByKeySalary(Integer keySalary);
    
    List<CompanyJob> jobListAll(@Param(value = "cond") Page page);
    
    List<CompanyJob> jobListByJobStick(Integer jobStick);
    List<CompanyJob> jobListByJobHot(Integer jobHot);
    List<CompanyJob> jobListByJobType(Integer jobType);
    
    List<CompanyJob> jobListBycomIdJobName(@Param(value="companyId") Integer companyId,@Param(value="jobName") String jobName);
 
    List<CompanyJob> queryJobSearchList(@Param(value = "cond") JobSearchForm form);
}