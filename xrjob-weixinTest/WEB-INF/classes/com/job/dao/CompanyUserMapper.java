package com.job.dao;

import java.util.Date;

import org.apache.ibatis.annotations.Param;

import com.job.model.CompanyUser;

public interface CompanyUserMapper {
    int deleteByPrimaryKey(Integer userId);

    int insert(CompanyUser record);

    int insertSelective(CompanyUser record);

    CompanyUser selectByPrimaryKey(Integer userId);
    
    CompanyUser selectByName(@Param(value = "loginName") String loginName, @Param(value = "now") Date now);

    int updateByPrimaryKeySelective(CompanyUser record);
    
    CompanyUser selectByOpenId(String openId); 
    
    CompanyUser  selectByCompanyId(Integer companyId);
}