package com.job.dao;

import java.util.Date;

import org.apache.ibatis.annotations.Param;

import com.job.model.CompanyUser;
import com.job.model.User;

public interface UserMapper {
	User selectByName(@Param(value = "userMobile") String userMobile);
	User selectByUserId(@Param(value = "userId") Integer userId);
	int updateByPrimaryKey(User record);
	 int updateByPrimaryKeySelective(User record);
	int insert(User record);
	User selectByOpenId(@Param(value = "openId") String openId);
}
