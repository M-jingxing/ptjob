package com.job.dao;

import java.util.List;

import com.job.model.JobTypeThird;

public interface JobTypeThirdMapper {
    int deleteByPrimaryKey(Integer typeId);

    int insert(JobTypeThird record);

    int insertSelective(JobTypeThird record);

    JobTypeThird selectByPrimaryKey(Integer typeId);

    int updateByPrimaryKeySelective(JobTypeThird record);

    int updateByPrimaryKey(JobTypeThird record);
    
    List<JobTypeThird> queryTypeListByParent(Integer typeId);
    
    List<JobTypeThird> getTypeList();
}