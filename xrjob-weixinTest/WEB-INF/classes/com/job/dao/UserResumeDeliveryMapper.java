package com.job.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.job.form.DeliveryForm;
import com.job.model.UserResumeDelivery;

public interface UserResumeDeliveryMapper {
    int deleteByPrimaryKey(Integer deliveryId);
    int deleteByJobId(Integer jobId);
    int insert(UserResumeDelivery record);

    int insertSelective(UserResumeDelivery record);

    UserResumeDelivery selectByPrimaryKey(Integer deliveryId);

    int updateByPrimaryKeySelective(UserResumeDelivery record);

    int updateByPrimaryKey(UserResumeDelivery record);
    
    List<UserResumeDelivery> queryDeliveryList(@Param(value = "cond") DeliveryForm form);
	
    List<UserResumeDelivery> queryDeliveryListByJobID(Integer jobID);
    
    List<UserResumeDelivery> queryDeliveryListByStatus(Integer status);
    
    List<UserResumeDelivery> queryDeliveryListByUserId(Integer userId);
    
	int queryDeliveryCount(@Param(value = "cond") DeliveryForm form);
	
	UserResumeDelivery selectByJobIdAndUserId(Integer jobId,Integer userId);
	
	List<UserResumeDelivery> queryDeliveryListByUserIdAndjobId(@Param(value = "jobId")Integer jobId,@Param(value = "userId")Integer userId);
	
}