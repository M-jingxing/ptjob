package com.job.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.job.model.UserSearchRecord;

public interface UserSearchRecordMapper {
   int insert(UserSearchRecord userSearchRecord);
   List<UserSearchRecord> queryByUserId(int userId);
   int deleteById(int recordId);
   List<UserSearchRecord> queryByUserIdAndIntentJob(@Param(value = "userId") int userId,@Param(value = "intentJob") int intentJob);
   int updateByPrimaryKey(UserSearchRecord userSearchRecord);
   List<UserSearchRecord> queryByUserIdAndRecordType(@Param(value = "userId") int userId,@Param(value = "recordType") int recordType );
}
