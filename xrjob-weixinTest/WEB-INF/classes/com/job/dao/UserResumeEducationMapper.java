package com.job.dao;

import java.util.List;

import com.job.model.UserResumeEducation;

public interface UserResumeEducationMapper {
    int deleteByPrimaryKey(Integer educationId);

    int insert(UserResumeEducation record);

    int insertSelective(UserResumeEducation record);

    UserResumeEducation selectByPrimaryKey(Integer educationId);

    int updateByPrimaryKeySelective(UserResumeEducation record);

    int updateByPrimaryKey(UserResumeEducation record);
    
    List<UserResumeEducation> getEduListByResumeId(Integer resumeId);
}