package com.job.dao;

import java.util.List;

import com.job.model.JobTypeTop;

public interface JobTypeTopMapper {
    int deleteByPrimaryKey(Integer typeId);

    int insert(JobTypeTop record);

    int insertSelective(JobTypeTop record);

    JobTypeTop selectByPrimaryKey(Integer typeId);

    int updateByPrimaryKeySelective(JobTypeTop record);

    int updateByPrimaryKey(JobTypeTop record);
    
    List<JobTypeTop> queryAllList();
}