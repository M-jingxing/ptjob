package com.job.dao;

import java.util.List;

import com.job.model.JobTypeSecond;

public interface JobTypeSecondMapper {
    int deleteByPrimaryKey(Integer typeId);

    int insert(JobTypeSecond record);

    int insertSelective(JobTypeSecond record);

    JobTypeSecond selectByPrimaryKey(Integer typeId);

    int updateByPrimaryKeySelective(JobTypeSecond record);

    int updateByPrimaryKey(JobTypeSecond record);
    
    List<JobTypeSecond> queryTypeListByParent(Integer typeId);
}