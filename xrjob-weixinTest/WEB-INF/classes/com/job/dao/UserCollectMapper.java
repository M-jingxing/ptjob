package com.job.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.job.model.User;
import com.job.model.UserCollect;

public interface UserCollectMapper {
	 int insert(UserCollect userCollect);
	 int deleteByPrimaryKey(Integer collectId);
	 List<UserCollect> selectByUserId(Integer userId);
	 UserCollect selectByJobIdAndUserId(@Param(value = "jobId") Integer jobId,@Param(value = "userId") Integer userId);
}
