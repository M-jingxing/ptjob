package com.job.dao;

import java.util.List;

import com.job.model.CompanyImformation;

public interface CompanyImformationMapper {
    int deleteByPrimaryKey(Integer companyId);

    int insert(CompanyImformation record);
    /**
     * 插入不完整企业记录
     * @param record
     * @return
     */
    int insertSelective(CompanyImformation record);
    /**
     * 根据企业名字查询该条企业记录
     * @param companyName
     * @return
     */
    CompanyImformation selectByCompanyName(String companyName);
    
    CompanyImformation selectByPrimaryKey(Integer companyId);

    int updateByPrimaryKeySelective(CompanyImformation record);

    int updateByPrimaryKey(CompanyImformation record);
    
    /**
     * 根据距离排序
     * @param lng
     * @param lat
     * @return
     */
    List<CompanyImformation> getDistanceCompany(Double lng,Double lat);
    
    List<CompanyImformation> getAllCompany();
    
}