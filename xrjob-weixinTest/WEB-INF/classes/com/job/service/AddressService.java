package com.job.service;

import java.util.List;

import com.job.model.AreaModel;
import com.job.model.CityModel;

/**
 * 行政区service
 * @author Y.HAO
 * @Date 2015年11月3日
 *
 */
public interface AddressService {

	List<CityModel> queryAllCityList();
	
	List<AreaModel> queryAreaByCity(Integer cityId);
}
