package com.job.service;

import java.util.List;

import com.job.model.CompanyImformation;
import com.job.model.JobInCompany;

public interface DistanceService {
	
     List<JobInCompany> getDistanceJob(double lng,double lat);
     
     List<CompanyImformation> DistanceList(double lng,double lat,double range1,double range2);
     
    
}
