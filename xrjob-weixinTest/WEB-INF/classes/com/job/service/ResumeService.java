package com.job.service;

import java.util.List;

import com.job.form.ResumeForm;
import com.job.model.DataGrid;
import com.job.model.UserIntentJob;
import com.job.model.UserResume;
import com.job.model.UserResumeEducation;
import com.job.model.UserResumeWork;


public interface ResumeService {

	UserResume selectResumeById(Integer resumeId);
	UserResume selectResumeByUser(Integer userId);
	List<UserResume> queryResumePage(ResumeForm form);
	int insertSelective(UserResume record);
	int updateByPrimaryKeySelective(UserResume record);
	int insertIntentJob(UserIntentJob userIntentJob);
	int updateIntentJob(UserIntentJob userIntentJob);
	List<UserResumeWork> getWorkListByResumeId(Integer resumeId);
	int insertResumeWork(UserResumeWork userResumeWork);
	int updateResumeWork(UserResumeWork userResumeWork);
	UserResumeWork getWorkDetail(Integer workId);
	int deleteResumeWork(Integer workId);
	
	List<UserResumeEducation> getEduListByResumeId(Integer resumeId);
	int insertResumeEdu(UserResumeEducation userResumeEducation);
	int updateResumeEdu(UserResumeEducation userResumeEducation);
	UserResumeEducation getEduDetail(Integer educationId);
	int deleteResumeEdu(Integer educationId);
	
}
