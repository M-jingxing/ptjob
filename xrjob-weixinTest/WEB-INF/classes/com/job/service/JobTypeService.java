package com.job.service;

import java.util.List;

import com.job.model.JobTypeSecond;
import com.job.model.JobTypeThird;
import com.job.model.JobTypeTop;
import com.job.model.UserIntentJob;


public interface JobTypeService {

	
	List<JobTypeTop> queryTopTypeList();
	
	
	List<JobTypeSecond> querySecondTypeByTop(Integer typeId);
	
	
	List<JobTypeThird> queryThirdTypeBySecond(Integer typeId);
	
	List<JobTypeThird> getThirdTypeList();
	int getTopType(int secondType);
	int getSecondType(int thirdType);
	
	UserIntentJob getIntentJobByResumeId(int resumeId);
}
