package com.job.service;

import com.job.model.CompanyUser;
import com.job.model.Json;

/**
 * �û�Service
 * @author Y.HAO
 * @Date 2015��11��5��
 *
 */
public interface UserService {

	/**
	 * ���ݵ�¼����ѯ�û�
	 * @param loginName
	 * @return
	 */
	CompanyUser selectUserByName(String loginName);
	CompanyUser selectUserById(Integer userId);
	CompanyUser  selectByCompanyId(Integer companyId);
	
	CompanyUser selectByOpenId(String openId); 
	/**
	 * ע��
	 * @param loginName
	 * @param password
	 * @return
	 */
	Integer register(String loginName, String password,String userMobile);
	
	/**
	 * �޸��û�
	 * @return
	 */
	int changeUser(CompanyUser user);
	
	/**
	 * �޸�����
	 * @param newPwd
	 * @param password
	 * @param userId
	 * @param res
	 * @return
	 */
	Json changeUserPwd(String newPwd, String password, Integer userId, Json res);
}
