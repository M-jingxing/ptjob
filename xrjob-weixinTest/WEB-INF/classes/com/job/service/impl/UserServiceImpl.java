package com.job.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.dao.CompanyUserMapper;
import com.job.model.CompanyUser;
import com.job.model.Json;
import com.job.service.UserService;
import com.job.util.MD5Util;


@Service
@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl implements UserService {
	
	@Autowired
	private CompanyUserMapper userMapper;

	@Override
	public CompanyUser selectUserByName(String loginName) {
		return userMapper.selectByName(loginName, new Date());
	}

	@Override
	public Integer register(String loginName, String password,String userMobile) {
		CompanyUser user = new CompanyUser();
		user.setLoginName(loginName);
		user.setPassword(MD5Util.getMD5Str(password));
		user.setUserMobile(userMobile);
		Date now = new Date();
		user.setCreateTime(now);
		user.setModifyTime(now);
		user.setUserStatus(1);		
		userMapper.insertSelective(user);
		return user.getUserId();
	}

	@Override
	public int changeUser(CompanyUser user) {
		user.setModifyTime(new Date());
		userMapper.updateByPrimaryKeySelective(user);
		return userMapper.updateByPrimaryKeySelective(user);
	}
	
	@Override
	public Json changeUserPwd(String newPwd, String password, Integer userId, Json res) {
		CompanyUser user = userMapper.selectByPrimaryKey(userId);
		if(null == user){
			res.setMsg("");
			return res;
		}
		
		if(!user.getPassword().equals(MD5Util.getMD5Str(password))){
			res.setMsg("");
			return res;
		}
		
		user.setPassword(MD5Util.getMD5Str(newPwd));
		user.setModifyTime(new Date());
		userMapper.updateByPrimaryKeySelective(user);
		res.setRes(true);
		return res;
	}

	@Override
	public CompanyUser selectByOpenId(String openId) {
		// TODO Auto-generated method stub
		return userMapper.selectByOpenId(openId);
	}

	@Override
	public CompanyUser selectUserById(Integer userId) {
		// TODO Auto-generated method stub
		return userMapper.selectByPrimaryKey(userId);
	}

	@Override
	public CompanyUser selectByCompanyId(Integer companyId) {
		// TODO Auto-generated method stub
		return userMapper.selectByCompanyId(companyId);
	}
}
