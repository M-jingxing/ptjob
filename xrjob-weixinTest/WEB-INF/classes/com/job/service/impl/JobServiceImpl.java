package com.job.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.dao.CompanyJobMapper;
import com.job.dao.JobComplainMapper;
import com.job.dao.UserCollectMapper;
import com.job.dao.UserSearchRecordMapper;
import com.job.form.JobForm;
import com.job.form.JobSearchForm;
import com.job.model.CompanyJob;
import com.job.model.DataGrid;
import com.job.model.JobComplain;
import com.job.model.Page;
import com.job.model.User;
import com.job.model.UserCollect;
import com.job.model.UserSearchRecord;
import com.job.service.JobService;


@Service
@Transactional(rollbackFor = Exception.class)
public class JobServiceImpl implements JobService {
	
	@Autowired
	private CompanyJobMapper jobMapper;
	
	@Autowired
	private JobComplainMapper jobComplainMapper;

	@Autowired
	private UserCollectMapper userCollectMapper;
	
	@Autowired
	private UserSearchRecordMapper userSearchRecordMapper;
	
	@Override
	public List<CompanyJob> queryJobByCompany(Integer companyId) {
		return jobMapper.queryJobByCompany(companyId);
	}

	@Override
	public DataGrid queryJobPage(JobForm form) {
		DataGrid dg = new DataGrid();
		dg.setRows(jobMapper.queryJobList(form));
		dg.setTotal(jobMapper.queryJobCount(form));
		return dg;
	}

	@Override
	public int addJob(CompanyJob job) {
		Date now = new Date();
		job.setCreateTime(now);
		job.setModifyTime(now);
		return jobMapper.insertSelective(job);
	}

	@Override
	public int updateJob(CompanyJob job) {
		if(null == jobMapper.selectByPrimaryKeyAndCompany(job.getJobId(), job.getCompanyId()))
			return 0;
		
		job.setModifyTime(new Date());
		return jobMapper.updateByPrimaryKeySelective(job);
	}

	@Override
	public int deleteJob(Integer jobId, Integer companyId) {
		if(null == jobMapper.selectByPrimaryKeyAndCompany(jobId, companyId))
			return 0;
		return jobMapper.deleteByPrimaryKey(jobId);
	}

	@Override
	public CompanyJob jobDetail(Integer jobId, Integer companyId) {
		return jobMapper.selectByPrimaryKeyAndCompany(jobId, companyId);
	}

	@Override
	public List<CompanyJob> queryJobByComIdJobName(Integer companyId,
			String jobName) {
		// TODO Auto-generated method stub
		return jobMapper.jobListBycomIdJobName(companyId, jobName);
	}
    
	@Override
	public List<CompanyJob> queryJobAll(Page page) {
		// TODO Auto-generated method stub
		//判断岗位是否被暂停，只向用户显示发布的岗位
		List<CompanyJob> jobList=jobMapper.jobListAll(page);
		List<CompanyJob> postJobs=new ArrayList<>();
		for (int i = 0; i < jobList.size(); i++) {
			if(jobList.get(i).getJobStatus()==null||jobList.get(i).getJobStatus()!=2){
				postJobs.add(jobList.get(i));
			}
		}
		return postJobs;
	}

	@Override
	public List<CompanyJob> queryJobSearch(JobSearchForm form) {
		// TODO Auto-generated method stub
		//判断岗位是否被暂停，只向用户显示发布的岗位
		List<CompanyJob> jobList=jobMapper.queryJobSearchList(form);
		List<CompanyJob> postJobs=new ArrayList<>();
		for (int i = 0; i < jobList.size(); i++) {
			if(jobList.get(i).getJobStatus()==null||jobList.get(i).getJobStatus()!=2){
				postJobs.add(jobList.get(i));
			}
		}
		return postJobs;
	}

	@Override
	public CompanyJob getJobByJobId(Integer jobId) {
		// TODO Auto-generated method stub
		return jobMapper.selectByPrimaryKey(jobId);
	}

	@Override
	public CompanyJob jobDetailByJobId(Integer jobId) {
		// TODO Auto-generated method stub
		return jobMapper.selectByPrimaryKey(jobId);
	}

	@Override
	public int addJobComplain(JobComplain jobComplain) {
		// TODO Auto-generated method stub
		return jobComplainMapper.insertSelective(jobComplain);
	}

	@Override
	public int addUserCollect(UserCollect userCollect) {
		// TODO Auto-generated method stub
		userCollectMapper.insert(userCollect);		
		return userCollect.getCollectId();
	}

	@Override
	public int deleteUserCollect(Integer collectId) {
		// TODO Auto-generated method stub
		return userCollectMapper.deleteByPrimaryKey(collectId);
	}

	@Override
	public List<UserCollect> getCollectListByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return userCollectMapper.selectByUserId(userId);
	}

	@Override
	public UserCollect collectByJobIdAndUserId(Integer jobId, Integer userId) {
		// TODO Auto-generated method stub
		return userCollectMapper.selectByJobIdAndUserId(jobId, userId);
	}

	@Override
	public List<CompanyJob> getJobListByKey(String key) {
		// TODO Auto-generated method stub
		//判断岗位是否被暂停，只向用户显示发布的岗位
		List<CompanyJob> jobList=jobMapper.jobListByKey(key);
		List<CompanyJob> postJobs=new ArrayList<>();
		for (int i = 0; i < jobList.size(); i++) {
			if(jobList.get(i).getJobStatus()==null||jobList.get(i).getJobStatus()!=2){
				postJobs.add(jobList.get(i));
			}
		}
		return postJobs;
		
	}

	@Override
	public List<CompanyJob> jobListByJobStick(Integer jobStick) {
		// TODO Auto-generated method stub
		//判断岗位是否被暂停，只向用户显示发布的岗位
		List<CompanyJob> jobList=jobMapper.jobListByJobStick(jobStick);
		List<CompanyJob> postJobs=new ArrayList<>();
		for (int i = 0; i < jobList.size(); i++) {
			if(jobList.get(i).getJobStatus()==null||jobList.get(i).getJobStatus()!=2){
				postJobs.add(jobList.get(i));
			}
		}
		return postJobs;
		
	}

	@Override
	public List<CompanyJob> jobListByJobType(Integer jobType) {
		// TODO Auto-generated method stub
		//判断岗位是否被暂停，只向用户显示发布的岗位
		List<CompanyJob> jobList=jobMapper.jobListByJobType(jobType);
		List<CompanyJob> postJobs=new ArrayList<>();
		for (int i = 0; i < jobList.size(); i++) {
			if(jobList.get(i).getJobStatus()==null||jobList.get(i).getJobStatus()!=2){
				postJobs.add(jobList.get(i));
			}
		}
		return postJobs;
	}

	@Override
	public int insertSearchRecord(UserSearchRecord userSearchRecord) {
		// TODO Auto-generated method stub
		userSearchRecord.setCreateTime(new Date());
		return userSearchRecordMapper.insert(userSearchRecord);
	}

	@Override
	public int deleteSearchRecordById(int recordId) {
		// TODO Auto-generated method stub
		return userSearchRecordMapper.deleteById(recordId);
	}

	@Override
	public List<UserSearchRecord> querySearchRecordByUserId(int userId) {
		// TODO Auto-generated method stub
		return userSearchRecordMapper.queryByUserId(userId);
	}

	@Override
	public List<CompanyJob> jobListByJobHot(Integer jobHot) {
		// TODO Auto-generated method stub
		//判断岗位是否被暂停，只向用户显示发布的岗位
		List<CompanyJob> jobList=jobMapper.jobListByJobHot(jobHot);
		List<CompanyJob> postJobs=new ArrayList<>();
		for (int i = 0; i < jobList.size(); i++) {
			if(jobList.get(i).getJobStatus()==null||jobList.get(i).getJobStatus()!=2){
				postJobs.add(jobList.get(i));
			}
		}
		return postJobs;
	}

	@Override
	public List<UserSearchRecord> queryByUserIdAndRecordType(int userId, int recordType) {
		// TODO Auto-generated method stub
		return userSearchRecordMapper.queryByUserIdAndRecordType(userId, recordType);
	}

	@Override
	public int updateSearchRecord(UserSearchRecord userSearchRecord) {
		// TODO Auto-generated method stub
		return userSearchRecordMapper.updateByPrimaryKey(userSearchRecord);
	}

	@Override
	public List<UserSearchRecord> queryByUserIdAndIntentJob(int userId, int intentJob) {
		// TODO Auto-generated method stub
		return userSearchRecordMapper.queryByUserIdAndIntentJob(userId, intentJob);
	}


}
