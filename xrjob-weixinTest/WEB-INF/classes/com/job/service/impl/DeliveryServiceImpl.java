package com.job.service.impl;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.job.dao.UserResumeDeliveryMapper;
import com.job.form.DeliveryForm;
import com.job.model.Json;
import com.job.model.UserResumeDelivery;
import com.job.service.DeliveryService;
import com.sun.xml.internal.messaging.saaj.packaging.mime.MessagingException;





@Service
@Transactional(rollbackFor = Exception.class)
public class DeliveryServiceImpl implements DeliveryService {
	
	@Autowired
	private UserResumeDeliveryMapper deliveryMapper;

	@Override
	public List<UserResumeDelivery> queryDeliveryList(DeliveryForm form) {
		return deliveryMapper.queryDeliveryList(form);
	}

	@Override
	public int queryDeliveryCount(DeliveryForm form) {
		return deliveryMapper.queryDeliveryCount(form);
	}

	@Override
	public int browseResume(Integer deliveryId, Integer jobId) {
		UserResumeDelivery record = deliveryMapper.selectByPrimaryKey(deliveryId);
		if(null == record)
			return 0;
		if(!record.getJobId().equals(jobId))
			return 0;
		
		record.setBrowseTime(new Date());
		record.setDeliveryStatus(2);
		return deliveryMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int deleteByPrimaryKey(Integer deliveryId) {
		return deliveryMapper.deleteByPrimaryKey(deliveryId);
	}

	@Override
	public List<UserResumeDelivery> queryDeliveryListByJobId(Integer jobId) {
		// TODO Auto-generated method stub
		return deliveryMapper.queryDeliveryListByJobID(jobId);
	}

	@Override
	public List<UserResumeDelivery> queryDeliveryByStatus(Integer status) {
		// TODO Auto-generated method stub
		return deliveryMapper.queryDeliveryListByStatus(status);
	}

	@Override
	public int insertSelective(UserResumeDelivery userResumeDelivery) {
		// TODO Auto-generated method stub
		return deliveryMapper.insertSelective(userResumeDelivery);
	}

	@Override
	public List<UserResumeDelivery> queryDeliveryByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return deliveryMapper.queryDeliveryListByUserId(userId);
	}
	
	@Override
	public List<UserResumeDelivery> queryListByUserIdAndjobId(
			Integer jobId, Integer userId) {
		// TODO Auto-generated method stub
		return deliveryMapper.queryDeliveryListByUserIdAndjobId(jobId, userId);
	}
	@Override
	public int deleteByJobId(Integer jobId) {
		// TODO Auto-generated method stub
		return deliveryMapper.deleteByJobId(jobId);
	}
	@Override
	public int updateSelective(UserResumeDelivery userResumeDelivery) {
		// TODO Auto-generated method stub
		return deliveryMapper.updateByPrimaryKeySelective(userResumeDelivery);
	}
	
	@Override
	public Json sendPhoneMessage(String jbPhone,String jobName,String userMobile) {
		// TODO Auto-generated method stub
		Json res = new Json();
        //发短信
        String url="http://www.hzfacaiyu.com/sms/openCard";
        DefaultHttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);
        JSONObject response = null;
        try{
        	//交易流水号
        	String tradeNo=new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date())+(int)(Math.random()*(1000-100)+100);
        	String myPassword=MD5("xrjob001");
        	
        	Map<String,String> signMap=new HashMap<String,String>();
        	signMap.put("tradeNo", tradeNo);
        	signMap.put("userName", "slm");
        	signMap.put("userPassword", "xrjob001");
        	String mySign1=JSONObject.toJSONString(signMap);
        	String mySign2=encrypt(mySign1,"tnZtdAOho10t9TQ9");
        	Map<String,String> objMap=new HashMap<String,String>();
        	objMap.put("tradeNo", tradeNo);
        	objMap.put("userName", "slm");
        	objMap.put("userPassword", myPassword);
        	objMap.put("phones", jbPhone);
        	objMap.put("content", "您公司发布的工作岗位有人投递啦，岗位是"+jobName);
        	objMap.put("etnumber", "");
        	objMap.put("sign", mySign2);
        	String sendMsg = JSONObject.toJSONString(objMap);
        	StringEntity s = new StringEntity(sendMsg,HTTP.UTF_8);
        	//s.setContentEncoding("UTF-8");
			s.setContentType("application/json");//发送json数据需要设置contentType
			post.setEntity(s);
			HttpResponse reslt = client.execute(post);
			if(reslt.getStatusLine().getStatusCode() == HttpStatus.SC_OK){//发送成功
				HttpEntity entity = reslt.getEntity();
				String result = EntityUtils.toString(reslt.getEntity());// 返回json格式：
			
			}else{
				
				res.setRes(false);
			}
        }catch(Exception e){
        	e.printStackTrace();
        }
		return res;
	}

	public Json SendMail(String address,String jobName,String userMobile) throws MessagingException, javax.mail.MessagingException{
 
		Json res = new Json();
		// 配置发送邮件的环境属性
        final Properties props = new Properties();
        /*
         * 可用的属性： mail.store.protocol / mail.transport.protocol / mail.host /
         * mail.user / mail.from
         */
        // 表示SMTP发送邮件，需要进行身份验证
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.exmail.qq.com");
        // 发件人的账号
        props.put("mail.user", "remind@xsrcjob.com");
        // 访问SMTP服务时需要提供的密码
        props.put("mail.password", "Xr12345678");

        // 构建授权信息，用于进行SMTP进行身份验证
        Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                // 用户名、密码
                String userName = props.getProperty("mail.user");
                String password = props.getProperty("mail.password");
                return new PasswordAuthentication(userName, password);
            }
        };
        // 使用环境属性和授权信息，创建邮件会话
        Session mailSession = Session.getInstance(props, authenticator);
        // 创建邮件消息
        MimeMessage message = new MimeMessage(mailSession);
        // 设置发件人
        InternetAddress form = new InternetAddress(
                props.getProperty("mail.user"));
        message.setFrom(form);

        // 设置收件人
        InternetAddress to = new InternetAddress(address);
        message.setRecipient(RecipientType.TO, to);

        // 设置邮件标题
        message.setSubject("萧然找工作----岗位投递通知");

        // 设置邮件的内容体
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String getDate = df.format(new Date());
        message.setContent("<h2>您在萧然找工作所发布的岗位有人投递啦</h2><br>"
        		+ "<p><span>所投递的岗位为："+"<h3>"+jobName+"<h3>"+"</span><br><p><span>请登录微信企业后台或PC端企业后台查看</span></p><br><span>"+getDate+"</span></p>", 
        		"text/html;charset=UTF-8");

        // 发送邮件
        Transport.send(message);
       res.setRes(true);
       
      return res;
    }
	
	 private static String  MD5(String sourceStr) {
	    	String result = "";
	    	try {
		    	MessageDigest md = MessageDigest.getInstance("MD5"); 
		    	md.update(sourceStr.getBytes());
		    	byte b[] = md.digest();
		    	int i;
		    	StringBuffer buf = new StringBuffer("");
		    	for (int offset = 0; offset< b.length; offset++) {
			    	i=b[offset];
			    	if(i<0)
			    		i+=256;
			    	if(i<16)
			    		buf.append("0");
			    	buf.append(Integer.toHexString(i));
		    	}
		    	result=buf.toString();
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
	    	return result;
	    }
	    
	    public static String encrypt(String content,String password){
	    	try{
	    		Cipher cipher=Cipher.getInstance("AES/CBC/NoPadding");
	    		int blockSize=cipher.getBlockSize();
	    		byte[] dataBytes=content.getBytes();
	    		int plaintextLength=dataBytes.length;
	    		if(plaintextLength%blockSize!=0){
	    			plaintextLength=plaintextLength+(blockSize-(plaintextLength%blockSize));
	    		}
	    		byte[] plaintext=new byte[plaintextLength];
	    		System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);
	    		SecretKeySpec keyspec=new SecretKeySpec(password.getBytes(),"AES");
	    		IvParameterSpec ivspec=new IvParameterSpec(password.getBytes());
	    		cipher.init(Cipher.ENCRYPT_MODE,keyspec,ivspec);
	    		byte[] encrypted=cipher.doFinal(plaintext);
	    		return parseByte2HexStr(encrypted);
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
	    	return null;
	    }
	    public static String parseByte2HexStr(byte buf[]){
	    	StringBuffer sb=new StringBuffer();
	    	for(int i=0;i<buf.length;i++){
	    		String hex=Integer.toHexString(buf[i] & 0xFF);
	    		if(hex.length()==1){
	    			hex='0'+hex;
	    		}
	    		sb.append(hex.toUpperCase());
	    	}
	    	return sb.toString();
	    }



}
