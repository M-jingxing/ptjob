package com.job.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.dao.UserMapper;
import com.job.model.CompanyUser;
import com.job.model.User;
import com.job.service.PersonService;
import com.job.util.MD5Util;

@Service
@Transactional(rollbackFor = Exception.class)
public class PersonServiceImpl  implements PersonService{
	
	@Autowired
	private UserMapper userMapper;
	
	@Override
	public User selectUserByName(String userMobile){
		User tu=userMapper.selectByName(userMobile);
		return tu;
	}
	
	@Override
	public int changeUser(User user) {

		user.setModifyTime(new Date());
		int rlt=userMapper.updateByPrimaryKeySelective(user);
		return rlt;
	}
	
	@Override
	public Integer register(String loginName, String password) {
		User user = new User();
		user.setUserMobile(loginName);
		user.setPassword(MD5Util.getMD5Str(password));
		Date now = new Date();
		user.setCreateTime(now);
		user.setModifyTime(now);
		user.setUserStatus(1);
		userMapper.insert(user);
		return user.getUserId();
	}

	@Override
	public int updateUser(User user) {
		// TODO Auto-generated method stub
		return userMapper.updateByPrimaryKeySelective(user);
	}

	@Override
	public User selectUserById(Integer userId) {
		// TODO Auto-generated method stub
		return userMapper.selectByUserId(userId);
	}

	@Override
	public User selectUserByOpenId(String openId) {
		// TODO Auto-generated method stub
		return userMapper.selectByOpenId(openId);
	}
}
