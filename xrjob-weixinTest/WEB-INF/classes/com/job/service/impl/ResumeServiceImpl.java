package com.job.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.dao.UserIntentJobMapper;
import com.job.dao.UserResumeEducationMapper;
import com.job.dao.UserResumeMapper;
import com.job.dao.UserResumeWorkMapper;
import com.job.form.ResumeForm;
import com.job.model.DataGrid;
import com.job.model.UserIntentJob;
import com.job.model.UserResume;
import com.job.model.UserResumeEducation;
import com.job.model.UserResumeWork;
import com.job.service.ResumeService;


@Service
@Transactional(rollbackFor = Exception.class)
public class ResumeServiceImpl implements ResumeService {
	
	@Autowired
	private UserResumeMapper resumeMapper;

	@Autowired 
	private UserIntentJobMapper userIntentJobMapper;
	
	@Autowired
	private UserResumeWorkMapper userResumeWorkMapper;
	
	@Autowired
	private UserResumeEducationMapper userResumeEducationMapper;
	
	@Override
	public UserResume selectResumeById(Integer resumeId) {
		return resumeMapper.selectResumeById(resumeId);
	}
	
	@Override
	public UserResume selectResumeByUser(Integer userId) {
		return resumeMapper.selectResumeByUser(userId);
	}
	
	@Override
	public List<UserResume> queryResumePage(ResumeForm form) {
		return resumeMapper.queryResumeList(form);
	}
	
	@Override
	public int insertSelective(UserResume record){
		return resumeMapper.insertSelective(record);
	}
	
	@Override
	public int updateByPrimaryKeySelective(UserResume record){
		return resumeMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int insertIntentJob(UserIntentJob userIntentJob) {
		// TODO Auto-generated method stub
		return userIntentJobMapper.insert(userIntentJob);
	}

	@Override
	public List<UserResumeWork> getWorkListByResumeId(Integer resumeId) {
		// TODO Auto-generated method stub
		return userResumeWorkMapper.getWorkListByResumeId(resumeId);
	}

	@Override
	public int insertResumeWork(UserResumeWork userResumeWork) {
		// TODO Auto-generated method stub
		userResumeWorkMapper.insertSelective(userResumeWork);
		return userResumeWork.getWorkId();
	}

	@Override
	public int updateResumeWork(UserResumeWork userResumeWork) {
		// TODO Auto-generated method stub
		return userResumeWorkMapper.updateByPrimaryKeySelective(userResumeWork);
	}

	@Override
	public UserResumeWork getWorkDetail(Integer workId) {
		// TODO Auto-generated method stub
		return userResumeWorkMapper.selectByPrimaryKey(workId);
	}

	@Override
	public int deleteResumeWork(Integer workId) {
		// TODO Auto-generated method stub
		return userResumeWorkMapper.deleteByPrimaryKey(workId);
	}

	@Override
	public List<UserResumeEducation> getEduListByResumeId(Integer resumeId) {
		// TODO Auto-generated method stub
		return userResumeEducationMapper.getEduListByResumeId(resumeId);
	}

	@Override
	public int insertResumeEdu(UserResumeEducation userResumeEducation) {
		// TODO Auto-generated method stub
		return userResumeEducationMapper.insertSelective(userResumeEducation);
	}

	@Override
	public int updateResumeEdu(UserResumeEducation userResumeEducation) {
		// TODO Auto-generated method stub
		return userResumeEducationMapper.updateByPrimaryKeySelective(userResumeEducation);
	}

	@Override
	public UserResumeEducation getEduDetail(Integer educationId) {
		// TODO Auto-generated method stub
		return userResumeEducationMapper.selectByPrimaryKey(educationId);
	}

	@Override
	public int deleteResumeEdu(Integer educationId) {
		// TODO Auto-generated method stub
		return userResumeEducationMapper.deleteByPrimaryKey(educationId);
	}

	@Override
	public int updateIntentJob(UserIntentJob userIntentJob) {
		// TODO Auto-generated method stub
		return userIntentJobMapper.updateByPrimaryKeySelective(userIntentJob);
	}
}
