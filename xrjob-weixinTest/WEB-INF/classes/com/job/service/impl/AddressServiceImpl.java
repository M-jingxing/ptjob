package com.job.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.dao.CityMapper;
import com.job.model.AreaModel;
import com.job.model.CityModel;
import com.job.service.AddressService;


@Service
@Transactional(rollbackFor = Exception.class)
public class AddressServiceImpl implements AddressService {
	
	@Autowired
	private CityMapper cityMapper;
	
	@Override
	public List<CityModel> queryAllCityList() {
		return cityMapper.queryAllCity();
	}

	@Override
	public List<AreaModel> queryAreaByCity(Integer cityId) {
		return cityMapper.queryAreaByCity(cityId);
	}

}
