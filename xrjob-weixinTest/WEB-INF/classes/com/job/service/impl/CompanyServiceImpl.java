package com.job.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.dao.CompanyImformationMapper;
import com.job.dao.CompanyJobMapper;
import com.job.dao.CompanyUserMapper;
import com.job.model.CompanyImformation;
import com.job.model.CompanyJob;
import com.job.model.CompanyUser;
import com.job.service.CompanyService;


@Service
@Transactional(rollbackFor = Exception.class)
public class CompanyServiceImpl implements CompanyService {
	
	@Autowired
	private CompanyImformationMapper companyMapper;
	
	@Autowired
	private CompanyUserMapper userMapper;

	
	
	@Override
	public CompanyImformation queryCompanyById(Integer companyId) {
		return companyMapper.selectByPrimaryKey(companyId);
	}

	@Override
	public int saveCompany(CompanyImformation company) {
		return companyMapper.updateByPrimaryKeySelective(company);
	}

	@Override
	public Integer addCompany(CompanyImformation company, Integer userId) {
		Date now = new Date();
		company.setCreateTime(now);
		company.setModifyTime(now);
	    companyMapper.insertSelective(company);
		
		Integer companyId = company.getCompanyId();
	    System.out.println("companyId:"+companyId);
		if(null != companyId){
			CompanyUser user = userMapper.selectByPrimaryKey(userId);
			user.setCompanyId(companyId);
			user.setModifyTime(now);
			userMapper.updateByPrimaryKeySelective(user);
		}
		
		return companyId;
	}

	@Override
	public CompanyImformation queryCompanyByName(String companyName) {
		// TODO Auto-generated method stub
		return companyMapper.selectByCompanyName(companyName);
	}

	
}
