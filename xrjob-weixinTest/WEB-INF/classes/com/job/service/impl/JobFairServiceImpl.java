package com.job.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.dao.CompanyImformationMapper;
import com.job.dao.CompanyJobMapper;
import com.job.dao.JobFairMapper;
import com.job.model.CompanyImformation;
import com.job.model.CompanyJob;
import com.job.model.JobFair;
import com.job.model.JobInCompany;
import com.job.service.JobFairService;

@Service
@Transactional(rollbackFor = Exception.class)
public class JobFairServiceImpl implements JobFairService {
	@Autowired
	private JobFairMapper jobFairMapper;

	@Autowired
	private CompanyJobMapper companyJobMapper;

	@Autowired
	private CompanyImformationMapper companyImformationMapper;

	private static double EARTH_RADIUS = 6378.137;

	private static double rad(double d) {
		return d * Math.PI / 180.0;
	}

	@Override
	public List<JobFair> getJobFair() { 
		// TODO Auto-generated method stub
		List<JobFair> jobFairList = new ArrayList<JobFair>();
		jobFairList = jobFairMapper.getJobFair();
		return jobFairList;
	}

	
	

	
	/*@Override
	public List<JobInCompany> getJobListByKeyDistance(Double lng, Double lat,Double range1,Double range2) {
		// TODO Auto-generated method stub
		Double lat1 = lat;
		Double lng1 = lng;
		Double lat2;
		Double lng2;
		List<CompanyImformation> companyList = new ArrayList<CompanyImformation>();
		companyList = companyImformationMapper.getDistanceCompany(lng, lat);
		
		List<JobInCompany> jobComList = new ArrayList<JobInCompany>();
		String jobSalary="";
		for (int i = 0; i < companyList.size(); i++) {

			CompanyImformation companyImformation = companyList.get(i);
            
			lat2 = companyImformation.getCompanyLatitude();
			lng2 = companyImformation.getCompanyLongitude();
			if(lat2!=null&&lng2!=null){
			double radLat1 = rad(lat1);
			double radLat2 = rad(lat2);
			double difference = radLat1 - radLat2;
			double mdifference = rad(lng1) - rad(lng2);
			double distance = 2 * Math.asin(Math.sqrt(Math.pow(
					Math.sin(difference / 2), 2)
					+ Math.cos(radLat1)
					* Math.cos(radLat2)
					* Math.pow(Math.sin(mdifference / 2), 2)));
			distance = distance * EARTH_RADIUS;
			distance = Math.round(distance * 10000) / 10000;
			
			String distanceStr = distance + "";
			distanceStr = distanceStr.substring(0, distanceStr.indexOf("."));
          
			
			
			if(distance>range1&&distance<=range2){
			
			ArrayList<CompanyJob> jobList = (ArrayList<CompanyJob>) companyJobMapper
					.queryJobByCompany(companyImformation.getCompanyId());
			
			if (jobList.size() != 0) {
			  for (int j = 0; j < jobList.size(); j++) {
				  
						int k = jobList.get(j).getJobSalaryId();

						if (k == 0) {
							jobSalary = "面议";
						} else if (k == 1) {
							jobSalary = "1500以下";
						} else if (k == 2) {
							jobSalary = "1500-2500";
						} else if (k == 3) {
							jobSalary = "2500-3500";
						} else if (k == 4) {
							jobSalary = "3500-5000";
						} else if (k == 5) {
							jobSalary = "5000-7000";
						} else if (k == 6) {
							jobSalary = "7000-10000";
						} else if (k == 7) {
							jobSalary = "10000-15000";
						} else if (k == 8) {
							jobSalary = "15000以上";
						} 
			       
				JobInCompany jobInCompany = new JobInCompany(jobList.get(j)
						.getJobId(), jobList.get(j).getCompanyId(), jobList
						.get(j).getJobName(),
						companyImformation.getCompanyName(), jobSalary,jobList.get(j).getJobAddress());
				jobComList.add(jobInCompany);
			  }
			}
			}
			}
		}

		return jobComList;
	}
*/


}
