package com.job.service.impl;

import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.model.UserResume;
import com.job.service.SendWeixinService;
import com.job.util.WeixinUtil;
import com.job.weixinModel.TemplateData;
import com.job.weixinModel.Token;
import com.job.weixinModel.WxTemplate;

@Service
@Transactional(rollbackFor = Exception.class)
public class SendWeixinServiceImpl implements SendWeixinService{


	  Logger log = LoggerFactory.getLogger(getClass());

	    /**
	     * 发送模板消息
	     * appId 公众账号的唯一标识
	     * appSecret 公众账号的密钥
	     * openId 用户标识
	     */
	    public void send_template_message(String appId, String appSecret, String openId,String jobName,UserResume userResume) {
	        Token token = WeixinUtil.getAccessToken(appId, appSecret);
	        String access_token = token.getAccessToken();
	        String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token="+access_token;
	        WxTemplate temp = new WxTemplate();
	        temp.setUrl("http://xiaoranjob.com/xrjob-weixingzh/resume/viewResume.do?resumeId="+userResume.getResumeId());
	        temp.setTouser(openId);
	        temp.setTopcolor("#000000");
	        temp.setTemplate_id("5n_T-3h60i6JQs0xT6qzzv9MXerLuOjVCetq4BtBcbE");
	        Map<String,TemplateData> m = new HashMap<String,TemplateData>();
	        TemplateData first = new TemplateData();
	        first.setColor("#000000");  
	        first.setValue("您所发布的岗位【"+jobName+"】有人投递啦");  
	        m.put("first", first);  
	        TemplateData name = new TemplateData();  
	        name.setColor("#000000");  
	        name.setValue(userResume.getResumeUserName());  
	        m.put("keyword1", name);
	        TemplateData workYear = new TemplateData();  
	        workYear.setColor("#000000");  
	        workYear.setValue(workYear(userResume.getWorkYear()));  
	        m.put("keyword2", workYear); 
	        TemplateData degree = new TemplateData();  
	        degree.setColor("#000000");  
	        degree.setValue(highDegree(userResume.getHighDegree()));  
	        m.put("keyword3", degree); 
	        TemplateData remark = new TemplateData();  
	        remark.setColor("#000000");  
	        remark.setValue("点击查看简历");  
	        m.put("remark", remark);
	        temp.setData(m);
	        String jsonString = JSONObject.fromObject(temp).toString();
	        JSONObject jsonObject = WeixinUtil.httpRequest(url, "POST", jsonString);
	        System.out.println(jsonObject);
	        int result = 0;
	        if (null != jsonObject) {  
	             if (0 != jsonObject.getInt("errcode")) {  
	                 result = jsonObject.getInt("errcode");
	                 log.error("错误 errcode:{} errmsg:{}", jsonObject.getInt("errcode"), jsonObject.getString("errmsg"));  
	             }  
	         }
	        log.info("模板消息发送结果："+result);
	    }
	    
	public static String workYear(Integer intWork) {
		String workYear="";
		if(intWork==1){
			workYear="1年以下";
		}else if (intWork==2) {
			workYear="1-2年";
		}else if (intWork==3) {
			workYear="3-5年";
		}else if (intWork==4) {
			workYear="5-10年";
		}else if (intWork==5) {
			workYear="10年以上";
		}
		
		return workYear;
	}
	 
	public static String  highDegree(Integer intdegree) {
		String highDegree="";
		if(intdegree==1){
			highDegree="高中以下";
		}else if (intdegree==2) {
			highDegree="高中";
		}else if (intdegree==3) {
			highDegree="中专/技校";
		}else if (intdegree==4) {
			highDegree="大专";
		}else if (intdegree==5) {
			highDegree="本科";
		}else if (intdegree==6) {
			highDegree="硕士以上";
		}

		return highDegree;
	}
	    
}
