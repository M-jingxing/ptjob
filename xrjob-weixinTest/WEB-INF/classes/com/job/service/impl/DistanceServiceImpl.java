package com.job.service.impl;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.dao.CompanyImformationMapper;
import com.job.dao.CompanyJobMapper;
import com.job.model.CompanyImformation;
import com.job.model.CompanyJob;
import com.job.model.JobInCompany;
import com.job.service.DistanceService;

@Service
@Transactional(rollbackFor = Exception.class)
public class DistanceServiceImpl implements DistanceService{
   
	@Autowired
	private CompanyImformationMapper companyMapper; 
	@Autowired
	private CompanyJobMapper jobMapper;
	
	private static double EARTH_RADIUS = 6378.137;

	private static double rad(double d) {
		return d * Math.PI / 180.0;
	}
	
	@Override
	public List<JobInCompany> getDistanceJob(double lng, double lat) {
		// TODO Auto-generated method stub
		
		Double lat1 = lat;
		Double lng1 = lng;
		Double lat2;
		Double lng2;
		Double result = null;
      List<CompanyImformation> companyList=new ArrayList<CompanyImformation>();
      companyList=  companyMapper.getDistanceCompany(lng, lat);
      List<JobInCompany> jobComList=new ArrayList<JobInCompany>();
     
    
      for(int i=0;i<companyList.size();i++){
    	 
    	 CompanyImformation companyImformation=companyList.get(i);
    	 ArrayList<CompanyJob> companyJobList= (ArrayList<CompanyJob>) jobMapper.queryJobByCompany(companyImformation.getCompanyId());
    	    lat2 = companyImformation.getCompanyLatitude();
			lng2 = companyImformation.getCompanyLongitude();
			if(lat2!=null&&lng2!=null){
				  double jl_jd = 102834.74258026089786013677476285;  
				  double jl_wd = 111712.69150641055729984301412873;  
				  double b = Math.abs((lng1 - lng2) * jl_jd);  
				  double a = Math.abs((lat1 - lat2) * jl_wd);  
				  double distance= Math.sqrt((a * a + b * b))/1000.0;  
				 DecimalFormat df = new DecimalFormat("0.0");
				 result = Double.parseDouble(df.format(distance));
			}
    	 
    	 CompanyJob companyJob=new CompanyJob();
    	 if(result!=null&&result<=5.0){
    	if (companyJobList.size()!=0) {
    		for (int j = 0; j < companyJobList.size(); j++) {
    			
    			companyJob=companyJobList.get(j);
           	    JobInCompany jobInCompany=new JobInCompany(companyJob.getJobName(), companyImformation.getCompanyName(), companyJob.getJobId(), companyImformation.getCompanyId());
           	    jobInCompany.setJobSalaryId(companyJob.getJobSalaryId());
           	    jobInCompany.setJobAddress(companyJob.getJobAddress());
           	    jobInCompany.setDistance(result);
           	    jobComList.add(jobInCompany);
			}
    	
    	 }
    	 }
      }		
		return jobComList;
	}


	@Override
	public List<CompanyImformation> DistanceList(double lng, double lat,
			double range1, double range2) {
		// TODO Auto-generated method stub
		
			Double lat1 = lat;
			Double lng1 = lng;
		
			
			Double lat2;
			Double lng2;
		   /*System.out.println("range1:"+range1+"---"+"range2:"+range2);*/
			List<CompanyImformation> companyList = new ArrayList<CompanyImformation>();
			companyList = companyMapper.getAllCompany();
			List<CompanyImformation> companyList2 = new ArrayList<CompanyImformation>();
			
			for (int i = 0; i < companyList.size(); i++) {

				CompanyImformation companyImformation = companyList.get(i);

				lat2 = companyImformation.getCompanyLatitude();
				lng2 = companyImformation.getCompanyLongitude();
				if(lat2!=null&&lng2!=null){
					
					  double jl_jd = 102834.74258026089786013677476285;  
					  double jl_wd = 111712.69150641055729984301412873;  
					  double b = Math.abs((lng1 - lng2) * jl_jd);  
					  double a = Math.abs((lat1 - lat2) * jl_wd);  
					  double distance= Math.sqrt((a * a + b * b));  
         
				/*	  BigDecimal bg = new BigDecimal(distance);
					  double dis = bg.setScale(4, BigDecimal.ROUND_DOWN).doubleValue();*/
                
				if(distance>=range1&&distance<range2){
					companyList2.add(companyList.get(i));
				}		
				}	
		}
			return companyList2;
	
}

	
}