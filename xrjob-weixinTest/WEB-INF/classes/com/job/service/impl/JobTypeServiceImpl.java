package com.job.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.dao.JobTypeSecondMapper;
import com.job.dao.JobTypeThirdMapper;
import com.job.dao.JobTypeTopMapper;
import com.job.dao.UserIntentJobMapper;
import com.job.model.JobTypeSecond;
import com.job.model.JobTypeThird;
import com.job.model.JobTypeTop;
import com.job.model.UserIntentJob;
import com.job.service.JobTypeService;


@Service
@Transactional(rollbackFor = Exception.class)
public class JobTypeServiceImpl implements JobTypeService {
	
	@Autowired
	private JobTypeTopMapper topMapper;
	
	@Autowired
	private JobTypeSecondMapper secondMapper;
	
	@Autowired
	private JobTypeThirdMapper thirdMapper;

	@Autowired 
	private UserIntentJobMapper userIntentJobMapper;
	@Override
	public List<JobTypeTop> queryTopTypeList() {
		return topMapper.queryAllList();
	}

	@Override
	public List<JobTypeSecond> querySecondTypeByTop(Integer typeId) {
		return secondMapper.queryTypeListByParent(typeId);
	}

	@Override
	public List<JobTypeThird> queryThirdTypeBySecond(Integer typeId) {
		return thirdMapper.queryTypeListByParent(typeId);
	}
	
	@Override
	public int getTopType(int secondType){
		JobTypeSecond secondt=secondMapper.selectByPrimaryKey(secondType);
		int topType=secondt.getParentTypeId();
		return topType;
	}
	
	@Override
	public int getSecondType(int thirdType){
		JobTypeThird thirdt=thirdMapper.selectByPrimaryKey(thirdType);
		int secondType=thirdt.getParentTypeId();
		return secondType;
	}

	@Override
	public List<JobTypeThird> getThirdTypeList() {
		// TODO Auto-generated method stub
		return thirdMapper.getTypeList();
	}

	@Override
	public UserIntentJob getIntentJobByResumeId(int resumeId) {
		// TODO Auto-generated method stub
		return userIntentJobMapper.selectByResumeId(resumeId);
	}

}
