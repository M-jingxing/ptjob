package com.job.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;  
  
import javax.servlet.http.HttpServletRequest;

import com.job.weixinModel.Article;
import com.job.weixinModel.NewsMessage;
import com.job.weixinModel.TextMessage;  
import com.job.util.MessageUtil;
 
  
/** 
 * 核心服务类 
 
 */  
public class CoreService {  
	
	


    /** 
     * 处理微信发来的请求 
     *  
     * @param request 
     * @return 
     */  
    public static String processRequest(HttpServletRequest request) {  
        String respMessage = null;  
        try {  
            // 默认返回的文本消息内容  
            String respContent = "请求处理异常，请稍候尝试！";  
  
            // xml请求解析  
            Map<String, String> requestMap = MessageUtil.parseXml(request);  
  
            // 发送方帐号（open_id）  
            String fromUserName = requestMap.get("FromUserName");  
            // 公众帐号  
            String toUserName = requestMap.get("ToUserName");  
            // 消息类型  
            String msgType = requestMap.get("MsgType");  
            //消息内容
            String msgContent=requestMap.get("Content");
            // 回复文本消息  
            TextMessage textMessage = new TextMessage();  
            textMessage.setToUserName(fromUserName);  
            textMessage.setFromUserName(toUserName);  
            textMessage.setCreateTime(new Date().getTime());  
            textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);  
   
            String str="使用优惠券";
           
            // 文本消息  
            if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_TEXT)) {  
            	if(msgContent.equals(str)){
                respContent = "使用优惠券前请确认是否已注册\n个人用户请回复：姓名+兑换码 \n企业用户请回复：公司名+兑换码\n我们将在收到消息24之内与您联系"; 
                textMessage.setContent(respContent);  
                respMessage = MessageUtil.messageToXml(textMessage);  
            	}
            }  
            // 图片消息  
            else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_IMAGE)) {  
                respContent = "您发送的是图片消息！";  
            }  
            // 地理位置消息  
            else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_LOCATION)) {  
                respContent = "您发送的是地理位置消息！";  
            }  
            // 链接消息  
            else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_LINK)) {  
                respContent = "您发送的是链接消息！";  
            }  
            // 音频消息  
            else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_VOICE)) {  
                respContent = "您发送的是音频消息！";  
            }  
            // 事件推送  
            else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_EVENT)) {  
                // 事件类型  
                String eventType = requestMap.get("Event");  
                // 订阅  
                NewsMessage newsMessage=new NewsMessage();
                newsMessage.setToUserName(fromUserName);
                newsMessage.setFromUserName(toUserName);
                newsMessage.setCreateTime(new Date().getTime());
                newsMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_NEWS);
                List<Article> articleList = new ArrayList<Article>(); 
                if (eventType.equals(MessageUtil.EVENT_TYPE_SUBSCRIBE)) {  
                	
/*                    respContent = "欢迎进入萧山人才市场指定合作平台「萧然找工作」。\n轻松就业，尽在萧然找工作！！！\n目前平台正处于测试阶段，欢迎大家提供宝贵的意见，如发现bug请直接将问题回复在公众号或发送到邮箱fankui@xsrcjob.com。";  
*/            
                Article article=new Article();
                article.setTitle("欢迎关注The Youth");
                 Article article2=new Article();
                articleList.add(article);
                articleList.add(article2);
                newsMessage.setArticleCount(articleList.size());
                newsMessage.setArticles(articleList);
              
                
                respMessage = MessageUtil.newsMessageToXml(newsMessage);  
                }  
                // 取消订阅  
                else if (eventType.equals(MessageUtil.EVENT_TYPE_UNSUBSCRIBE)) {  
                    // TODO 取消订阅后用户再收不到公众号发送的消息，因此不需要回复消息  
                }  
                // 自定义菜单点击事件  
                else if (eventType.equals(MessageUtil.EVENT_TYPE_CLICK)) {  
                    // 事件KEY值，与创建自定义菜单时指定的KEY值对应  
                    String eventKey = requestMap.get("EventKey");  
                }  
            }  
  
           
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
  
        return respMessage;  
    }  
}  
