package com.job.service;

import com.job.model.UserResume;

public interface SendWeixinService {
 
 void	send_template_message(String appId, String appSecret, String openId,String jobName,UserResume userResume);
}
