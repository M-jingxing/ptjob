package com.job.service;

import com.job.model.User;

public interface PersonService {
	User selectUserByName(String userMobile);
	User selectUserById(Integer userId);
	User selectUserByOpenId(String openId);
	int changeUser(User user);
	Integer register(String loginName, String password);
	int updateUser(User user);
}
