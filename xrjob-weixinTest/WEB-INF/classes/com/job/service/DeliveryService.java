package com.job.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.job.form.DeliveryForm;
import com.job.model.Json;
import com.job.model.UserResumeDelivery;
import com.sun.xml.internal.messaging.saaj.packaging.mime.MessagingException;


public interface DeliveryService {

	List<UserResumeDelivery> queryDeliveryListByJobId(Integer jobId);
	
	List<UserResumeDelivery> queryDeliveryList(DeliveryForm form);
	
	int queryDeliveryCount(DeliveryForm form);
	
	int browseResume(Integer deliveryId, Integer jobId);

	int deleteByPrimaryKey(Integer deliveryId);
	
	int deleteByJobId(Integer jobId);
	
	List<UserResumeDelivery> queryDeliveryByStatus(Integer status);
	
	int insertSelective(UserResumeDelivery userResumeDelivery);
	
	int updateSelective(UserResumeDelivery userResumeDelivery);
	
	Json sendPhoneMessage(String jbPhone,String jobName,String userMobile);
	
	Json SendMail(String address,String jobName,String userMobile) throws MessagingException, javax.mail.MessagingException;
	
	List<UserResumeDelivery> queryDeliveryByUserId(Integer userId);
	
	List<UserResumeDelivery> queryListByUserIdAndjobId(Integer jobId,Integer userId);
	
}
