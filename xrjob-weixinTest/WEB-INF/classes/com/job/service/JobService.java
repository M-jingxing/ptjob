package com.job.service;

import java.util.List;

import com.job.form.JobForm;
import com.job.form.JobSearchForm;
import com.job.model.CompanyJob;
import com.job.model.DataGrid;
import com.job.model.JobComplain;
import com.job.model.Page;
import com.job.model.User;
import com.job.model.UserCollect;
import com.job.model.UserSearchRecord;


public interface JobService {

	List<CompanyJob> queryJobByCompany(Integer companyId);
	
	List<CompanyJob> queryJobAll(Page page);
	List<CompanyJob> jobListByJobStick(Integer jobStick);
	List<CompanyJob> jobListByJobHot(Integer jobHot);
	List<CompanyJob> jobListByJobType(Integer jobType);
	List<CompanyJob> queryJobByComIdJobName(Integer companyId ,String jobName);
	
	DataGrid queryJobPage(JobForm form);
	
   
	
	int addJob(CompanyJob job);
	
	
	int updateJob(CompanyJob job);
	
	
	int deleteJob(Integer jobId, Integer companyId);
	
	CompanyJob jobDetail(Integer jobId, Integer companyId);
	
	CompanyJob jobDetailByJobId(Integer jobId);
	
	List<CompanyJob> queryJobSearch(JobSearchForm form);
	
	List<CompanyJob> getJobListByKey(String key);
	
	CompanyJob getJobByJobId(Integer jobId);
	
	int addJobComplain(JobComplain jobComplain);
	
	int addUserCollect(UserCollect userCollect);
	int deleteUserCollect(Integer collectId);
	List<UserCollect> getCollectListByUserId(Integer userId);
	UserCollect collectByJobIdAndUserId(Integer jobId,Integer userId);
	
	int insertSearchRecord(UserSearchRecord userSearchRecord);
	int deleteSearchRecordById(int recordId);
	List<UserSearchRecord> querySearchRecordByUserId(int userId);
	List<UserSearchRecord> queryByUserIdAndRecordType(int userId,int recordType);
	List<UserSearchRecord> queryByUserIdAndIntentJob(int userId,int intentJob);
	int updateSearchRecord(UserSearchRecord userSearchRecord);
	
}
