package com.job.service;

import java.util.List;

import com.job.model.CompanyImformation;
import com.job.model.CompanyJob;

public interface CompanyService {

	
	CompanyImformation queryCompanyById(Integer companyId);
	
	CompanyImformation queryCompanyByName(String companyName);

	int saveCompany(CompanyImformation company);

	
	
	Integer addCompany(CompanyImformation company, Integer userId);
}
