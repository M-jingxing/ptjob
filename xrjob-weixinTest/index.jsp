<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>The Youth大学生兼职平台</title>
	<base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta charset="utf-8" />
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<!--CSS-->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style type="text/css">
	body {
		font-size:14px;
		color:#666;
		background-color:#f5f5f5;
		font-weight:400;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	
	.search{
		background-color:#ff9037;
		padding:8px 0px 10px 10px;
		width:auto;
		height:46px;
	}
	input[type=search]{
		border:none;
		outline:none;
		color:#999;
		appearance: none;
		-moz-appearance:none;
		-webkit-appearance: none;
		padding-left:5px;
		text-align:left;
		width:78%;
		border-radius:6px;
		padding-top:5px;
		padding-bottom:5px;
		float:left;
	}
	.search button{
		padding-top:5px;
		border:none;
		outline:none;
		background-color:#ff9137;
		width:10%;
	}
	.search button img{
		width:20px;
		height:20px;
	}
	:-moz-placeholder{
		color:#999; /*mozilla ff 4-18*/
	}
	::-moz-placeholder{
		color:#999; /*mozilla ff 19+*/
	}
	:-ms-input-placeholder{
		color:#999; /*ie 10+*/
	}
	::-webkit-input-placeholder{
		color:#999; /*webkit browsers*/
	}
	.login{
		float:left;
		width:20%;
		text-align:center;
		padding-top:3px;
		font-size:18px;
	}
	.search a{
		color:#fff;
		line-height:20px;
	}
	.carousel-indicators{
		bottom: -10px;
	}
	.item img{
		width:100%;
	}
	.row{
		margin:0;
	}
	/*6个大的导航*/
	.point{
		padding-top:10px;
		background-color:#fff;
	}
	.point img{
		width:40px;
		height:40px;
	}
	.point .row .col-sm-4{
		text-align:center;
	}
	.point .row .col-sm-4 a{
		color:#222;
	}
	.point .row ul{
		margin-bottom:6px;
	}
	.point .row ul li p{
		padding-top:3px;
		text-align:center;
	}
	/*热门推荐*/
	.setImg{
		background:#f5f5f5 url(img/hotRecommend.png) no-repeat 0 0;
		background-size:55px 55px;
		margin-top:10px;
		margin-bottom:10px;
		padding-top:5px;
		border:none;
	}
	.hotRecommend {
		background: #fff url(img/hotRecommend.png) no-repeat 0 -5px;
		background-size: 55px 55px;
		height: 110px;
		overflow: hidden;
		border: none;
	}
	.hotRecommend ul{
		padding:5px 5px 5px 35px;
	}
	.hotRecommend ul li{
		padding:7px;
		color:#666;
		position:relative;
		white-space:nowrap;
		overflow:hidden;
		margin-right:10px;
	}
	.hotRecommend ul li a{
		color:#666;
	}
	.hotRecommend ul li span{
		color:#d3461e;
		font-size:15px;
	}
	/*最新发布*/
	.newest{
		background-color:#fff;
		padding-top:14px;
		padding-bottom:14px;
	}
	.newest p{
		font-size:15px;
		color:#222;
		font-weight:700;
		margin-left:10px;
	}
	.newest button{
		background-color:#fe562c;
		font-size:12px;
		color:#fff;
		border:none;
		outline:none;
		border-radius:6px;
		padding:0 4px;
		margin-left:5px;
	}
	.newest ul{
		padding-left:20px;
		padding-right:20px;
	}
	.newest ul li{
		padding-bottom:7px;
		padding-top:7px;
		border-bottom:1px solid #eee;
		white-space:nowrap;
		overflow:hidden;
		white-space:nowrap;
		text-overflow:ellipsis;
	}
	.newest ul li span{
		color:#d65f00;

	}
	.newest ul li a{
		color:#666;
	}
	
   /*登录变成已登录之后start*/
	.logined{
	float:left;
		width:20%;
		text-align:center;
		font-size:18px;
		padding-top:0;
		padding-bottom:0;
	}
	.logined ul{
		padding-left:0;
		margin-bottom:0;
		height:34px;
	}
	.logined ul li:last-child{
		height:12px;
	}
	.logined .dropdown-menu{
		padding-top:0;
		padding-bottom:0;
		margin-right:3px;
		min-width:20px;
		height:28px;
		color:#fff;
		background-color:#fff;
		border:none;
	}
	.glyphicon-chevron-down{
		font-size:12px;
		top:-2px;
	}
	.dropdown-menu li{
		padding-top:0;
		border:none;
	}
	.dropdown-menu li a{
		color:#ff9037;
	}
	/*登录变成已登录之后end*/
	
	/*刷新出更多*/
	.moreList{
		position:fixed;
		right:5px;
		bottom:10px;
		background-color:rgba(255,255,255,0.5);
		width:35px;
		height:35px;
		border-radius:35px;
		-webkit-border-radius:35px;
		-moz-border-radius:35px;
		color:rgba(0,0,0,0.3);
		font-size:12px;
		padding:0;
		border:1px solid #aaa;
		-user-select:none;
	}
	.moreList:focus{
		outline:none;
	}
	.moreList:active:focus{
		border:1px solid #d65f00;
	}
	
</style>
</head>
<body>
	<!--搜索框-->
	<section class="search">
	  <a href="distance/toJobSearchPage.do">
		<input type="search" name="key" value="" id="key" placeholder="请输入职位名/公司名">
		<!-- <button type="submit" onclick="keySearch()"><img src="img/search.png"></button> -->
         </a>
		<div class="login pull-right"" id="noLogin"><a href="xrjobPages/login.jsp">登录</a></div> 
		
		<div class="logined dropdown pull-right"" id="logined">
			<a id="dLabel" role="button" data-toggle="dropdown"  data-target="#">
			<ul>
				<li>已登录</li>
				<li><span class="glyphicon glyphicon-chevron-down"></span></li>
			</ul>
			</a>
			<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
				<li><a href="person/quitUser.do">退出</a></li>
			</ul>
		</div>
	
	</section>
	<!--图片轮播-->
	<section id="changeImg">
		<div id="myCarousel" class="carousel slide">
		   <!-- 轮播（Carousel）指标 -->
		   <ol class="carousel-indicators">
			  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			  <li data-target="#myCarousel" data-slide-to="1"></li>
			  <li data-target="#myCarousel" data-slide-to="2"></li>
		   </ol> 
		   <!-- 轮播（Carousel）项目 -->
		   <div class="carousel-inner">
			  <div class="item active">
				 <img src="img/banner01.png" alt="First slide">
			  </div>
			  <div class="item">
				 <img src="img/banner02.png" alt="Second slide">
			  </div>
			  <div class="item">
				 <img src="img/banner03.png" alt="Third slide">
			  </div>
		   </div>
		   <!-- 轮播（Carousel）导航--> 
		   <a href="#myCarousel" data-slide="prev"></a>
		   <a href="#myCarousel" data-slide="next"></a>
		</div> 
	</section>
	<!--6个大的导航-->
	<section class="point">
		<div class="row">
			<ul class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<li><a href="person/toSaveResume.do"><img src="img/myResume.png">
				<p>我的简历</p></a></li>
			</ul>
			<ul class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<li><a href="job/toJobCollect.do"><img src="img/myStar.png">
				<p>我的收藏</p></a></li>
			</ul>
			<ul class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<li><a href="delivery/userDelivery.do"><img src="img/deliveryRecord.png">
				<p>投递记录</p></a></li>
			</ul>
		</div>
		<div class="row">
			<ul class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<li><a href="distance/toJobSearch.do"><img src="img/jobSearch.png"><p>职位搜索</p></a></li>
			</ul>
			<ul class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<li><a href="job/practiceJob.do"><img src="img/internship.png"><p>找应届生</p></a></li>
			</ul>
			<ul class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<li><a href="distance/jobFair.do"><img src="img/jobfair.png"><p>招聘会</p></a></li>
			</ul>
		</div>
	</section>
	<!--热门推荐-->
	<div class="setImg">
		<div class="hotRecommend">
			<ul class="dowebok">
				 <c:forEach varStatus="status" items="${hotJobList }" var="hotJobList" begin="0" end="10">
			<li><a href="job/jobDetail.do?jobId=${hotJobList.jobId }"><span>${hotJobList.jobName }</span>&nbsp;&nbsp;<c:choose><c:when test="${fn:contains(hotJobList.companyName,'xrjobOfficial')}">${fn:substring(hotJobList.companyName,0,fn:length(hotJobList.companyName)-13)}</c:when><c:otherwise >${hotJobList.companyName }</c:otherwise></c:choose></a></li>
			</c:forEach>
			</ul>
		</div>
	</div>
	<!--最近发布-->
	<section class="newest">
		<p>
			最近发布<button>new</button>
		</p>
		<ul id="content">
		   <c:forEach varStatus="status" items="${newJobList }" var="newJobList" >
			<li><a href="job/jobDetail.do?jobId=${newJobList.jobId }"><span>${newJobList.jobName }</span>&nbsp;&nbsp;<c:choose><c:when test="${fn:contains(newJobList.companyName,'xrjobOfficial')}">${fn:substring(newJobList.companyName,0,fn:length(newJobList.companyName)-13)}</c:when><c:otherwise >${newJobList.companyName }</c:otherwise></c:choose></a></li>
			</c:forEach>
		</ul>
	</section>
<!-- 	 <div  style="height:960px">  
   <div id="follow">this is a scroll test;<br/>    页面下拉自动加载内容</div>  
</div> -->
	<input type="hidden" id="userMobile" value="${sessionScope.userMobile}"/>
	<input type="hidden" id="page" value="">
	<div id="msg"></div>
	<!-- 刷新出更多 -->
	<button class="moreList" onclick="getList()">更多</button>
<!--javascript-->
<script src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="js/jgestures.min.js"></script>
<script src="js/scroll.js"></script>
<script src="js/jquery.bottomScrollLoading.js" ></script>
<script type="text/javascript">
//  $(document).ready(function() {
	
//     $(window).scroll(function() {
//      // $(document).scrollTop()获取垂直滚动的距离
//      // $(document).scrollLeft() 这是获取水平滚动条的距离

//         if ($(document).scrollTop() > $(document).height() - $(window).height()) {   
        	
//         	setInterval( getList(),2000);
//            $("#msg").val("加载中"); 
//         }

//       });

// }); 
// function getList(){
	
//    var html="";
// 	$.post("job/getNewList.do",{page:$("#page").val()},function(data){
// 		$("#page").val(data.numPage);
  	 
//      	   $("#content").append(function(){
//      		  $.each(data.list,function(k,v){
//                      var companyName=v.companyName;
//      			 if(companyName.indexOf("xrjobOfficial") >=0){
//      				companyName=companyName.substring(0,companyName.length-13);
//      			  }   
//      			 var text="<li><a href='job/jobDetail.do?jobId="+v.jobId+"'><span>"+v.jobName+"</span>&nbsp;&nbsp;"+companyName+"</a></li>";
//      			  html=html+text;
//      		  });   
//      		return html;
//      	   });      	
//  },"json");	
// }

//add by chy
var  newjobpage=1;
function getList(){
	newjobpage+=1;
	$.ajax({  
        url : "job/getNewList.do",  
        type : 'POST',  
        data : {page:newjobpage},  
        dataType:'json',
        async : true,   
        success : function(data) {  
        	 var html="";
        	 $("#content").append(function(){
        		  $.each(data.list,function(k,v){
                        var companyName=v.companyName;
        			 if(companyName.indexOf("xrjobOfficial") >=0){
        				companyName=companyName.substring(0,companyName.length-13);
        			  }   
        			 var text="<li><a href='job/jobDetail.do?jobId="+v.jobId+"'><span>"+v.jobName+"</span>&nbsp;&nbsp;"+companyName+"</a></li>";
        			  html=html+text;
        		  });   
        		return html;
        	   });      	
        },  
        error : function(responseStr) {  
        	
        }  
    });
}
</script> 
<script type="text/javascript">
    $('.carousel').carousel();
    $(document).ready(function(){ 
    	
    	 
    	
           var userMobile=$("#userMobile").val();
           
           if(userMobile==null||userMobile==""){    
                $("#logined").remove();
           }else{
                $("#noLogin").remove();
           }
           
          if (${isRead==null}) {
            	location="job/indexJobList.do";
			}
              
           /*h5手机滑动图片*/
		//手势右滑 回到上一个画面
		$('#myCarousel').bind('swiperight swiperightup swiperightdown',function(){
			$("#myCarousel").carousel('prev');
		});
		//手势左滑 进入下一个画面
		$('#myCarousel').bind('swipeleft swipeleftup swipeleftdown',function(){
			$("#myCarousel").carousel('next');
		});

		/*热门推荐板块的滚动*/
		$(".hotRecommend").myScroll({
			speed:50,	//数值越大，速度越慢
			rowHeight:35 //li的高度
		});
  
        });
        
        
        
</script>


</body>	
	<script type="text/javascript">
/* 	$("a").click(function(){
		
		var str = window.location.href;
		str = str.substring(0,str.lastIndexOf("?"));
		var top = $(document).scrollTop();
		alert(top);
		$.cookie(str, top, { path: '/' });
		return $.cookie(str);
	})
	function putCookie(jobId){
		var str = window.location.href;
		str = str.substring(0,str.lastIndexOf("?"));
		var top = $(document).scrollTop();
		alert(top);
		$.cookie(str, top, { path: '/' });
		location=""+jobId;
		
	}
	 */
	
	
	/* 
	$(document).ready(function(){ 
		var str = window.location.href;
		str = str.substring(0,str.lastIndexOf("?"));

		
		 if ($.cookie(str)) {
		
		$("html,body").animate({ scrollTop: $.cookie(str) });
		 }

	 })
	 $(window).scroll(function () {
		var str = window.location.href;
		str = str.substring(0,str.lastIndexOf("?"));
		var top = $(document).scrollTop();
		$.cookie(str, top, { path: '/' });
		return $.cookie(str);
		})    */
	
String.prototype.queryString= function(name) {
    var reg=new RegExp("[\?\&]" + name+ "=([^\&]+)","i"),r = this.match(reg);
    return r!==null?unescape(r[1]):null;
};
    window.onload=function(){
        var last=location.href.queryString("_v");
       
        if(location.href.indexOf("?")==-1){
            location.href=location.href+"?_v="+(new Date().getTime());
        }else{
            var now=new Date().getTime();
            if(!last){
                location.href=location.href+"&_v="+(new Date().getTime());
            }else if(parseInt(last)<(now-1000)){
                location.href=location.href.replace("_v="+last,"_v="+(new Date().getTime()));
            }
        }
    };
</script> 
</html>