<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<title>举报</title>
	<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
	<meta charset="utf-8" />
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	body{
		font-weight:400;
		font-size:14px;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	.addJobExp{
		padding-top:14px;
	}
	.content{
		margin-left:30px;
		padding-top:20px;
		padding-bottom:8px;
		margin-right:30px;
		border-bottom:1px solid #e8dfe8;
		color:#222;
	}
	.reportDetail{
		margin-left:30px;
		padding-top:20px;
		padding-bottom:8px;
		margin-right:30px;
		color:#222;
	}
	.content:last-child{
		padding-top:36px;
	}
	:-moz-placeholder{
		color:#888; /*mozilla ff 4-18*/
	}
	::-moz-placeholder{
		color:#888; /*mozilla ff 19+*/
	}
	:-ms-input-placeholder{
		color:#888; /*ie 10+*/
	}
	::-webkit-input-placeholder{
		color:#888; /*webkit browsers*/
	}
	.content select{
		background:#fff url(img/myResume/arrow.png) no-repeat right;
		width:60%;
		text-align:left;
	}
	label{
		font-weight:400;
		width:80px;
	}
	input[type=text]{
		width:75%;
	}
	input[type=text],select{
		border:none;
		outline:none;
		background-color:#fff;
		color:#888;
		appearance: none;
		-moz-appearance:none;
		-webkit-appearance: none;
		padding-left:1px;
		text-align:left;
	}
	select::-ms-expand{
		display:none;	/*清除ie的默认选择框样式，隐藏下拉箭头（*/
	}
	.store{
		text-align:center;
		padding-top:45px;
	}
	.store button{
		width:80px;
		height:30px;
		background-color:#ff9037;
		border:1px solid #fb8c00;
		outline:none;
		border-radius:6px;
		color:#fff;
	}
	textarea{
		width:100%;
		border:none;
		background-color:#f5f5f5;
		margin-top:11px;
		outline:none;
		border-radius: 12px;
		padding:10px;
	}
</style>
</head>
<body>
<form action="" id="form">
<div class="addJobExp">
    <input type="hidden" name="companyId" value="<%=request.getParameter("companyId") %>">
    <input type="hidden" name="jobId" value="<%=request.getParameter("jobId") %>">
	<div class="content">
		举报情况：
		<select name="objMsg" id="objMsg">
			<option value="">请选择</option>
			<option value="1">虚假信息</option>
			<option value="2">岗位已招满</option>
			<option value="3">其他</option>
		</select>
	</div>
	<div class="content">
		举报类型：
		<select name="objType" id="objType">
			<option value="">请选择</option>
			<option value="1">企业</option>
			<option value="2">职位</option>
		</select>
	</div>

	<div class="reportDetail">
		<div>
			<textarea name="complainContent" id="complainContent" cols="30" rows="10" maxlength="200" placeholder="请详细说明您的举报原因，字数在200字以内"></textarea>
		</div>
	</div>
	<div  class="store">
		<button type="button" onclick="report()">提交</button>
	</div>
</div>
</form>
<div class="setInfo"></div>
<script src="js/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
   function report(){
   
      if(checkForm()){
        $.post("job/jobReport.do",$("#form").serialize(),function(data){
          if(data.res){
                alert(data.msg);
                self.location=document.referrer;
                
          }else{
              alert(data.msg);
          }
          },"JSON");
          }
     }
   
   function checkForm(){
       if($("#objMsg").val()==null||$("#objMsg").val()==""){
         alert("请完善举报信息");
         return false;
       }
       
       if($("#objType").val()==null||$("#objType").val()==""){
        alert("请完善举报信息");
         return false;
       }
       if($("#complainContent").val()==null||$("#complainContent").val()==""){
        alert("请完善举报信息");
         return false;
       }
       
       return true;
   }


</script>
</body>
</html>