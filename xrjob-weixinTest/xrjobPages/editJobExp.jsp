<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
	<title>工作经历</title>
	<base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta charset="utf-8" >
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	body{
		font-weight:400;
		font-size:14px;
		background-color:#f5f5f5;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	.head p{
		color:#999;
		font-size:12px;
		padding-top:10px;
		padding-bottom:3px;
	}
	.jobExpContent{
		
		margin-top:14px;
	}
	.jobExpContent ul li{
		padding-top:4px;
		padding-bottom:4px;
	}
	.post{
		color:#000;
		font-size:16px;
	}
	.jobExpContent ul{
		padding-right:15px;
		padding-left:25px;
		padding-top:16px;
		background-color:#fff;
	}
	.jobExpContent ul li:first-child{
		color:#222;
	}
	.jobExpContent ul li:nth-child(3){
		color:#666;
	}
	.infoContent ul .edit,.jobExpContent ul .edit{
		font-size:12px;
		color:#ff9037;
	}
	.eduInfoContent ul{
		padding-left:0;
		padding-top:13px;
		margin-bottom:8px;
	}
	.eduInfoContent ul li{
		vertical-align:middle;
		padding-bottom:10px;
	}
	.glyphicon{
		top:3px;
	}
	.eduInfoContent ul li:first-child a{
		color:#ff9037;
		font-weight:600;
		vertical-align:center;
	}
	.eduInfoContent ul li:last-child{
		font-size:12px;
		color:#999;
	}
	.btnAdd{
		padding-left:30px;
		padding-right:30px;
		text-align:center;
	}
	.btnAdd button{
		background-color:#ff9037;
		border:1px solid #fb8c00;
		border-radius:6px;
		color:#fff;
		padding-top:8px;
		padding-bottom:8px;
		width:100%;
		text-align:center;
		outline:none;
	}
	textarea{
		width:100%;
		border:none;
		background-color:#fff;
		margin-top:11px;
		outline:none;
		resize:none;
		padding:0;
	}
    /*导航栏*/
	nav{
		color:#fff;
		background-color:#ff9037;
		padding:6px;
	}
	nav a:link,nav a:visited,nav a:hover,nav a:active{
		color:#fff;
		font-size:16px;
	}
</style>
</head>
<body>
<!--导航栏-->
<nav>
	<a href="person/toSaveResume.do"><span class="glyphicon glyphicon-chevron-left"></span>
		返回</a>
	<a href="index.jsp"><span class="glyphicon glyphicon-home pull-right"></span></a>
</nav>
<div class="editJobExp">
	<div class="jobExpContent">
	   <c:forEach varStatus="status" items="${userResumeWork }" var="userResumeWork">
		<ul>
			<li>${userResumeWork.startDate }至${userResumeWork.endDate }<a href="person/toEditResumeWork.do?workId=${userResumeWork.workId }"><span class="edit pull-right"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<span>编辑</span></span></a>
			<li class="post">${userResumeWork.companyName }/${userResumeWork.userJob }</li>
			<li>工作描述:<textarea rows="5" cols="" disabled="disabled">${userResumeWork.workDescribe }</textarea></li>
		</ul>
		</c:forEach>
	</div>
	<div class="btnAdd">
		<a href="xrjobPages/addJobExp.jsp"><button>添加工作经历</button></a>
	</div>
</div>
<script src="js/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
</body>	
</html>