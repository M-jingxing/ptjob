<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>搜索</title>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta charset="utf-8">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<meta name="format-detection" content="telephone=no">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"
	media="screen">
<style>
body {
	font-weight: 400;
	font-size: 14px;
	color: #666;
	background-color: #f5f5f5;
}

ul {
	padding-left: 0;
}

li {
	list-style-type: none;
}

a:link, a:visited, a:hover, a:active {
	text-decoration: none;
	color: #666;
}
/*搜索+导航栏*/
.search {
	background-color: #ff9037;
	padding: 8px 0 10px 10px;
	width: auto;
	height: 46px;
}

input[type=search] {
	border: none;
	outline: none;
	color: #999;
	padding-left: 5px;
	text-align: left;
	width: 75%;
	padding-top: 5px;
	padding-bottom: 5px;
	float: left;
	background-size: 24px 24px;
	border-radius: 6px 0 0 6px;
}

input[type=submit] {
	border: none;
	outline: none;
	color: #999;
	padding-left: 25px;
	text-align: left;
	width: 5%;
	padding-top: 5px;
	padding-bottom: 5px;
	float: left;
	background: #fff url(img/search.jpg) no-repeat 0 3px;
	background-size: 24px 24px;
	border-radius: 0 6px 6px 0;
}

.glyphicon-home {
	color: #fff;
	line-height: 30px;
	padding-right: 15px;
	font-size: 18px;
}
form{
	margin-bottom:0;
}
/*附近热门*/
.carousel-inner {
	padding: 0 0 30px 0;
	background-color: #fff;
}

.item .line:first-child {
	padding-top: 10px;
}

.item .line p {
	border-right: 1px solid #666;
}

.item .line p:last-child {
	border-right: 0;
}

.item p {
	width: 20%;
	text-align: center;
	float: left;
	margin-top: 7px;
	margin-bottom: 7px;
}

.item .nearby a {
	color: #ff9137;
}

.carousel-indicators {
	bottom: -10px;
}

.carousel-indicators li {
	width: 12px;
	height: 12px;
	background-color: #999;
	border: 0;
}

.carousel-indicators .active {
	background-color: #ff9137;
	margin: 1px;
}
/*近期搜索记录*/
.glyphicon-trash {
	color: #999;
	font-size: 12px;
	line-height: 18px;
	padding-right: 5px;
}

.glyphicon-chevron-right {
	color: #999;
	font-size: 12px;
	line-height: 14px;
}

.searchRecord {
	margin-top: 10px;
	background-color: #fff;
}

.searchRecord #nearbySearch {
	font-size: 15px;
	color: #222;
	padding: 10px;
	font-weight: 700;
	border-bottom: 1px solid #eee;
}

.searchRecord ul li {
	padding: 15px;
	border-bottom: 1px solid #eee;
}

.searchRecord ul li a {
	width: 100%;
}
/*hot*/
.hot {
	margin-top: 10px;
	background-color: #fff;
}

.hot ul li {
	padding: 8px;
	font-size: 13px;
}

.hot p {
	float: left;
	font-size: 12px;
	background-color: #fe552c;
	border-radius: 5px;
	padding-left: 4px;
	padding-right: 4px;
	margin-top: 4px;
	margin-right: 3px;
	color: #fff;
	line-height: 12px;
}

.hot .job {
	color: #d3461e;
	font-size: 15px;
}
</style>
</head>
<body>
	<!--导航栏-->
	<nav class="search">
	<form method="post" action="distance/jobSearch.do">
		<input type="search" name="key" value="" id="key" value=""
			placeholder="请输入职位名/公司名"> <input type="submit" name=""
			value="">
	</form>
	<a href="index.jsp"><span
		class="glyphicon glyphicon-home pull-right"></span></a> </nav>
	<!--div轮播-->
	<section id="changeImg">
	<div id="myCarousel" class="carousel slide">
		<!-- 轮播（Carousel）指标 -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
		</ol>
		<!-- 轮播（Carousel）项目 -->
		<div class="carousel-inner">
			<div class="item active">
				<div class="line">
					<p class="nearby">
						<a href="xrjobPages/nearHotJobs.jsp">附近热门</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=业务员">业务员</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=收银">收银</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=操作工">操作工</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=技术员">技术员</a>
					</p>
				</div>
				<div class="line">
					<p>
						<a href="distance/jobSearchHot.do?key=普工">普工</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=采购">采购</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=设计师">设计师</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=顾问">顾问</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=销售">销售</a>
					</p>
				</div>
				<div class="line">
					<p>
						<a href="distance/jobSearchHot.do?key=专员">专员</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=电工">电工</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=机修工">机修工</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=外贸">外贸</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=前台">前台</a>
					</p>
				</div>
				<div class="line">
					<p>
						<a href="distance/jobSearchHot.do?key=文员">文员</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=资料员">资料员</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=收银">收银</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=客服">客服</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=工程">工程</a>
					</p>
				</div>
			</div>
			<div class="item">
				<div class="line">
					<p class="nearby">
						<a href="distance/jobSearchHot.do">附近热门</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=会计">会计</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=驾驶员">驾驶员</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=仓库">仓库</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=推广">推广</a>
					</p>
				</div>
				<div class="line">
					<p>
						<a href="distance/jobSearchHot.do?key=司机">司机</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=美工">美工</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=生产">生产</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=实习生">实习生</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=导购">导购</a>
					</p>
				</div>
				<div class="line">
					<p>
						<a href="distance/jobSearchHot.do?key=房产">房产</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=保安">保安</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=财务">财务</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=推广">推广</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=服务员">服务员</a>
					</p>
				</div>
				<div class="line">
					<p>
						<a href="distance/jobSearchHot.do?key=老师">老师</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=人事">人事</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=助理">助理</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=法务">法务</a>
					</p>
					<p>
						<a href="distance/jobSearchHot.do?key=生产">生产</a>
					</p>
				</div>
			</div>
		</div>
		<!-- 轮播（Carousel）导航-->
		<a id="carleft" href="#myCarousel" data-slide="prev"></a> 
		<a id="carright" href="#myCarousel"
			data-slide="next"></a>
	</div>
	</section>
	<!--近期搜索记录-->
	<div class="searchRecord">
		<div id="nearbySearch">近期搜索记录</div>
		<ul>
			   <c:forEach items="${keyList }" var="keyList" varStatus="status" begin="0" end="2">
					<li><a href="distance/jobSearch.do?key=${keyList.searchRecord }&isRecord=1">${keyList.searchRecord }</a>
					<a href="javascript:void(0);" onclick="delKey('${keyList.recordId}')"> <span class="glyphicon glyphicon-trash pull-right"></span> </a></li>
			   </c:forEach>  
			
		</ul>
	</div>

	<!--hot-->
	<div class="hot">
		<ul id="content">
			<c:forEach varStatus="status" items="${newJobList }" var="newJobList">
				<li><p>hot</p>
					<a href="job/jobDetail.do?jobId=${newJobList.jobId }"><span
						class="job">${newJobList.jobName }</span>&nbsp;&nbsp;<span><c:choose><c:when test="${fn:contains(newJobList.companyName,'xrjobOfficial')}">${fn:substring(newJobList.companyName,0,fn:length(newJobList.companyName)-13)}</c:when><c:otherwise >${newJobList.companyName }</c:otherwise></c:choose></span></a></li>
			</c:forEach>
		</ul>
	</div>
   <input type="hidden" id="page" value="">
	<script src="js/jquery-2.2.3.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/toucher.js"></script>
	
	<script type="text/javascript">
String.prototype.queryString= function(name) {
    var reg=new RegExp("[\?\&]" + name+ "=([^\&]+)","i"),r = this.match(reg);
    return r!==null?unescape(r[1]):null;
};
    window.onload=function(){
        var last=location.href.queryString("_v");
       
        if(location.href.indexOf("?")==-1){
            location.href=location.href+"?_v="+(new Date().getTime());
        }else{
            var now=new Date().getTime();
            if(!last){
                location.href=location.href+"&_v="+(new Date().getTime());
            }else if(parseInt(last)<(now-1000)){
                location.href=location.href.replace("_v="+last,"_v="+(new Date().getTime()));
            }
        }
    };
</script>
	<!-- <script type="text/javascript">
	$(document).ready(function() {
		
	    $(window).scroll(function() {
	     // $(document).scrollTop()获取垂直滚动的距离
	     // $(document).scrollLeft() 这是获取水平滚动条的距离

	   
	        if ($(document).scrollTop() > $(document).height() - $(window).height()) {      	
	        	setInterval( getList(),2000);
	       
	        }
	     
	    });
	});
	function getList(){
		   var html="";
			$.post("job/getNewList.do",{page:$("#page").val()},function(data){
				$("#page").val(data.numPage);
		  	 
		     	   $("#content").append(function(){
		     		  $.each(data.list,function(k,v){
		     			 var companyName=v.companyName;
		     			 if(companyName.indexOf("xrjobOfficial") >=0){
		     				companyName=companyName.substring(0,companyName.length-13);
		     			  }   
		     			 var text="<li><p>hot</p><a href='job/jobDetail.do?jobId="+v.jobId+"'><span class='job'>"+v.jobName+"</span>&nbsp;&nbsp;"+companyName+"</a></li>";
		     			  html=html+text;
		     		  });   
		     		return html;
		     	   });
		     	   
		                	
		 },"json");
			
		}
	
	</script> -->
	<script type="text/javascript">

	/*删除历史记录*/
	function delKey(id){
		 
		  if(confirm("您确定要删除这条记录吗？")){
		  $.post("distance/delKey.do", {
				recordId: id
			}, function(data) {
				if (data.res) {
		         location.reload();
				} else {
				
				}
			}, "JSON");
  
		  }   
		
	}
	

	
	/*div自动轮播*/
	$('.carousel').carousel();
	$(document).ready(function(){
		/*h5手机滑动div*/
		var myTouch = util.toucher(document.getElementById('myCarousel'));

		myTouch.on('swipeLeft',function(e){
			$('#carright').click();
		}).on('swipeRight',function(e){
			$('#carleft').click();
		});

	});
</script>
</body>
</html>