<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
	<title>教育经历</title>
	<base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	body{
		font-weight:400;
		font-size:14px;
		background-color:#f5f5f5;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	.head p{
		color:#999;
		font-size:12px;
		padding-top:10px;
		padding-bottom:3px;
	}
	.eduExpContent{
		margin-top:14px;
	}
	.eduExpContent ul li{
		padding-top:4px;
		padding-bottom:4px;
	}
	.eduExpContent ul{
		padding-right:15px;
		padding-left:25px;
		padding-top:16px;
		background-color:#fff;
	}
	.eduExpContent ul li:first-child{
		color:#222;
	}
	.eduExpContent ul li:nth-child(3){
		color:#666;
	}
	.infoContent ul .edit,.eduExpContent ul .edit{
		font-size:12px;
		color:#ff9037;
	}
	.eduInfoContent ul{
		padding-left:0;
		padding-top:13px;
		margin-bottom:8px;
	}
	.eduInfoContent ul li{
		vertical-align:middle;
		padding-bottom:10px;
	}
	.glyphicon{
		top:3px;
	}
	.eduInfoContent ul li:first-child a{
		color:#ff9037;
		font-weight:600;
		vertical-align:center;
	}
	.eduInfoContent ul li:last-child{
		font-size:12px;
		color:#999;
	}
	.btnAdd{
		padding-left:30px;
		padding-right:30px;
		text-align:center;
	}
	.btnAdd button{
		background-color:#ff9037;
		border:1px solid #fb8c00;
		border-radius:6px;
		color:#fff;
		padding-top:8px;
		padding-bottom:8px;
		width:100%;
		text-align:center;
		outline:none;
	}
	.edu{
		color:#999;
	}
     /*导航栏*/
	nav{
		color:#fff;
		background-color:#ff9037;
		padding:6px;
	}
	nav a:link,nav a:visited,nav a:hover,nav a:active{
		color:#fff;
		font-size:16px;
	}

</style>
</head>
<body>
<!--导航栏-->
<nav>
	<a href="person/toSaveResume.do"><span class="glyphicon glyphicon-chevron-left"></span>
		返回</a>
	<a href="index.jsp"><span class="glyphicon glyphicon-home pull-right"></span></a>
</nav>
<div class="editEduExp">
	<div class="eduExpContent">
	<c:forEach varStatus="status" items="${userResumeEdu }" var="userResumeEdu">
		<ul>
			<li>${userResumeEdu.schoolName }<a href="person/toEditResumeEdu.do?eduId=${userResumeEdu.educationId }"><span class="edit pull-right"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<span>编辑</span></span></a>
			<li class="edu"><c:if test="${userResumeEdu.educationGrade==1 }">高中以下</c:if><c:if 
			test="${userResumeEdu.educationGrade==2 }">高中</c:if><c:if 
			test="${userResumeEdu.educationGrade==3 }">中专/技校</c:if><c:if
			test="${userResumeEdu.educationGrade==4 }">大专</c:if><c:if
			test="${userResumeEdu.educationGrade==5 }">本科</c:if><c:if 
			test="${userResumeEdu.educationGrade==6 }">硕士以上</c:if>
			&nbsp;&nbsp;&nbsp;${userResumeEdu.professionalName }</li>
		</ul>
		</c:forEach>
	</div>
	<div class="btnAdd">
		<a href="xrjobPages/addEduExp.jsp"><button>添加教育经历</button></a>
	</div>
</div>
<script src="js/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
</body>	
</html>