<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<title>期望岗位</title>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta charset="utf-8">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<meta name="format-detection" content="telephone=no">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"
	media="screen">
<style>
body {
	font-weight: 400;
	font-size: 14px;
}

li {
	list-style-type: none;
}

a:link, a:visited, a:hover, a:active {
	text-decoration: none;
}

.content {
	margin-left: 30px;
	padding-top: 40px;
	padding-bottom: 8px;
	margin-right: 30px;
	border-bottom: 1px solid #e8dfe8;
}

:-moz-placeholder {
	color: #888; /*mozilla ff 4-18*/
}

::-moz-placeholder {
	color: #888; /*mozilla ff 19+*/
}

:-ms-input-placeholder {
	color: #888; /*ie 10+*/
}

::-webkit-input-placeholder {
	color: #888; /*webkit browsers*/
}

.content select {
	background: #fff url(img/myResume/arrow.png) no-repeat right;
	width: 60%;
}

label {
	font-weight: 400;
	width: 80px;
}

input[type=text], select {
	border: none;
	outline: none;
	background-color: #fff;
	color: #888;
	appearance: none;
	-moz-appearance: none;
	-webkit-appearance: none;
}

select::-ms-expand {
	display: none; /*清除ie的默认选择框样式，隐藏下拉箭头（*/
}

.store {
	text-align: center;
	padding-top: 45px;
}

.store button {
	width: 80px;
	height: 30px;
	background-color: #ff9037;
	border: 1px solid #fb8c00;
	outline: none;
	border-radius: 6px;
	color: #fff;
}
</style>
</head>
<body>
	<form action="" id="form">
		<input type="hidden" name="resumeId" value="${userResume.resumeId }">
		<div class="editJob">
			<div class="content">
				<label>行&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;业：</label> <select
					name="topId" id="topId" onchange="changeTop()">
					<option value="">请选择</option>
                  <c:forEach items="${tops}" var="top">
	    					<option value="${top.typeId}" <c:if test="${topId == top.typeId}">selected="selected"</c:if>>
	    						${top.typeName}
	    					</option>
	    			 </c:forEach>
				</select>
			</div>
			<div class="content">
				<label>职&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;能：</label> <select
					name="secondId" id="secondId" onchange="changeSecond()">
					<option value="">请选择</option>
					<c:forEach items="${seconds}" var="second">
	    					<option value="${second.typeId}" <c:if test="${secondId == second.typeId}">selected="selected"</c:if>>
	    						${second.typeName}
	    					</option>
	    				</c:forEach>
				</select>
			</div>
			<div class="content">
				<label>职&nbsp;&nbsp;位&nbsp;名：</label> <select name="intentJob"
					id="jobIndustry">
					
					<option value="" >请选择</option>
					  <c:forEach items="${thirds}" var="third">
	    					<option value="${third.typeId}" <c:if test="${thirdId==third.typeId}">selected="selected"</c:if>>
	    						${third.typeName}
	    					</option>
	    				</c:forEach>
				</select>
			</div>
			<div class="content">
				<label>工作类型：</label> <select name="jobType" id="jobType">
					<option value="">请选择</option>
					<option value="1"
						<c:if test="${userResume.jobType == 1}">selected="selected"</c:if>>全职</option>
					<option value="2"
						<c:if test="${userResume.jobType == 2}">selected="selected"</c:if>>兼职</option>
					<option value="3"
						<c:if test="${userResume.jobType == 3}">selected="selected"</c:if>>应届生</option>
				</select>
			</div>
			<div class="content">
				<label>期望薪资：</label> <select name="salaryId" id="salaryId">
					<option value="">请选择</option>
					<option value="0"
						<c:if test="${userResume.salaryId == 0}">selected="selected"</c:if>>薪资面议</option>
					<option value="1"
						<c:if test="${userResume.salaryId == 1}">selected="selected"</c:if>>1500元以下/月</option>
					<option value="2"
						<c:if test="${userResume.salaryId == 2}">selected="selected"</c:if>>1500-2500元/月</option>
					<option value="3"
						<c:if test="${userResume.salaryId == 3}">selected="selected"</c:if>>2500-3500元/月</option>
					<option value="4"
						<c:if test="${userResume.salaryId == 4}">selected="selected"</c:if>>3500-5000元/月</option>
					<option value="5"
						<c:if test="${userResume.salaryId == 5}">selected="selected"</c:if>>5000-7000元/月</option>
					<option value="6"
						<c:if test="${userResume.salaryId == 6}">selected="selected"</c:if>>7000-10000元/月</option>
					<option value="7"
						<c:if test="${userResume.salaryId == 7}">selected="selected"</c:if>>10000-15000元/月</option>
					<option value="8"
						<c:if test="${userResume.salaryId == 8}">selected="selected"</c:if>>15000元以上/月</option>
				</select>
			</div>
			<div class="store">
				<button type="button" onclick="savaIntentJob()">保存</button>
			</div>
		</div>
	</form>
	<div class="setInfo"></div>
	<script src="js/jquery-2.2.3.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript">
	   function checkForm(){
	
	       if($("#topId").val()==null||$("#topId").val()==""){
	        alert("请完善资料");
	        return false;
	        }
	        if($("#secondId").val()==null||$("#secondId").val()==""){
	        alert("请完善资料");
	        return false;
	        }
	        if($("#jobIndustry").val()==null||$("#jobIndustry").val()==""){
	        alert("请完善资料");
	        return false;
	        }
	        if($("#jobType").val()==null||$("#jobType").val()==""){
	        alert("请完善资料");
	        return false;
	        }
	        if($("#salaryId").val()==null||$("#salaryId").val()==""){
	        alert("请完善资料");
	        return false;
	        }
	        
	        
	      return true 
	  }
	  
	
		function savaIntentJob() {
          if(checkForm()){
			$.post("person/saveIntentJob.do", $("#form").serialize(), function(
					data) {
				if (data.res) {
					alert(data.msg);
					location = "person/toSaveResume.do";
				} else {
					alert(data.msg);
				}

			}, "json");
           }
		}

		function changeTop() {
			var topId = $("#topId").val();
			var second = $("#secondId");
			var third = $("#jobIndustry");
		    if(topId==""){
		    	   second.html("<option value=\"\">全部</option>");
		   		   third.html("<option value=\"\">全部</option>");
				}else{
					second.html("");
					third.html("");
				}
			
			if (topId == "") {
				return;
			}

			$.post("type/changeTopType.do", {
				topId : topId
			}, function(data) {
				if (data == null || data == "") {
					return;
				}

				second.append("<option value=\"" + data[0].typeId + "\"selected=selected>" + data[0].typeName + "</option>");
				second.val(data[0].typeId);
				for(var o in data){
					if(o!=0){
					second.append("<option value=\"" + data[o].typeId + "\">" + data[o].typeName + "</option>");
					}
				}
				second.trigger("onchange");
			}, 'json');
		}

		function changeSecond() {
			var secondId = $("#secondId").val();
			var third = $("#jobIndustry");
			if(secondId==""){
				third.html("<option value=\"\">全部</option>");
			}else{
				third.html("");
			}
			if (secondId == "") {
				return;
			}

			$.post("type/changeSecondType.do", {
				secondId : secondId
			}, function(data) {
				if (data == null || data == "") {
					return;
				}

				third.append("<option value=\"" + data[0].typeId + "\"selected=selected>" + data[0].typeName + "</option>");
				for(var o in data){
					if(o!=0){
					third.append("<option value=\"" + data[o].typeId + "\">" + data[o].typeName + "</option>");
					}
				}
			}, 'json');
		}
	</script>
</body>
</html>