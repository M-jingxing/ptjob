<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
	<title>个人注册</title>
	<base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta charset="utf-8" />
	<meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	body{
		background-color:#f5f5f5;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	:-moz-placeholder{
		color:#999; /*mozilla ff 4-18*/
		font-size:15px;
	}
	::-moz-placeholder{
		color:#999; /*mozilla ff 19+*/
		font-size:15px;
	}
	:-ms-input-placeholder{
		color:#999; /*ie 10+*/
		font-size:15px;
	}
	::-webkit-input-placeholder{
		color:#999; /*webkit browsers*/
		font-size:15px;
	}
	input[type=text],select,input[type=password]{
		border:none;
		outline:none;
		background-color:#f5f5f5;
		color:#999;
		appearance: none;
		-moz-appearance:none;
		-webkit-appearance: none;
		padding-left:1px;
		text-align:left;
		font-size:15px;
		height:26px;
	}
	.userName input[type=text],.psd input[type=text],.confirmPsd input[type=text]{
		width:100%;
	}
	.userName input[type=password],.psd input[type=password],.confirmPsd input[type=password]{
		width:100%;
	}
	select::-ms-expand{
		display:none;
	}
	.logo{
		text-align:center;
		padding-top:46px;
		padding-bottom:54px;
	}
	.logo img{
		width:80px;
		height:80px;
	}
	.loginContent{
		margin-left:50px;
		margin-right:50px;
	}
	.userName,.psd,.confirmPsd{
		border-bottom:1px solid #999;
		padding-left:28px;
		margin-bottom:18px;
	}
	.userName{
		background:url(img/user.png) no-repeat;
		padding-bottom:5px;
	}
	.psd{
		background:url(img/password.png) no-repeat;
		padding-bottom:5px;
	}
	.confirmPsd{
		background:url(img/password.png) no-repeat;
		padding-bottom:5px;
	}
	#checkCode{
		width:45%;
		margin-right:8%;
		border-bottom:1px solid #999;
		text-align:center;
		padding:0;
	}
	.btn1{
		background-color:#f5f5f5;
		border:none;
		border-bottom:1px solid #ff9037;
		color:#ff9037;
		outline:none;
		width:45%;
		padding-left:0;
		padding-right:0;
		padding-bottom:2px;
	}
	.btn2{
		width:100%;
		padding-top:10px;
		padding-bottom:10px;
		border:none;
		background-color:#ff9137;
		color:#fff;
		border-radius: 6px;
		outline:none;
		margin-bottom:16px;
		margin-top:45px;
	}
	.error{
		font-size:12px;
		color:#ff9037;
	}
</style>
</head>
<body>
<div class="logo">
	<img src="img/logo.png">
</div>
<div class="loginContent">
	<form id="frmInc" autocomplete="off">
		<div class="userName">
			<input type="text" name="loginName" id="loginName"  placeholder="请输入手机号码">
		</div>
		<div class="psd">
			<input type="password" name="password" id="password" placeholder="请输入密码">
		</div>
		<div class="confirmPsd">
			<input type="password" name="surePassword" id="surePassword" placeholder="请确认密码">
		</div>
		<input type="hidden" id="msg1" name="msg1" value="${res.msg}">
		<div>
			<button class="btn2" type="button" onclick="register()">注册</button>
		</div>
	</form>
</div>
<script src="js/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script type="text/javascript">

function register(){
   var loginName=$("#loginName").val();
   var password=$("#password").val();
   var surePassword=$("#surePassword").val();
  $.post("person/register.do",{
					loginName:loginName,
					password:password,
					surePassword:surePassword,
				},function(data) {
					if(data.res){//注册成功
						location="person/toSaveResume.do";
					}
					else{
                        alert(data.msg); 
					}
				}, 'JSON');				
}

	/*表单验证
	* 用户名不能为空
	* 密码不能为空，在6位数以上
	* 确认密码不能为空，和密码equal
	* 验证码不能为空
	* */
	$(function(){
		$("#frmInc").validate({
			rules:{
				loginName:{
					required:true
				},
				password:{
					required:true,
					minlength:6
				},
				surePassword:{
					required:true,
					minlength:6,
					equalTo:"#password"
				},
				checkCode:{
					required:true
				}
			},
			messages:{
				loginName:{
					required:"请输入用户名"
				},
				password:{
					required:"请输入密码",
					minlength:"输入长度在6位以上"
				},
				surePassword:{
					required:"请输入确认密码",
					minlength:"输入长度在6位以上",
					equalTo:"请与密码保持一致"
				},
				checkCode:{
					required:"请输入验证码"
				}
			},
			/*修改错误信息的提示位置*/
			errorPlacement: function(error, element) {
				if ( element.is(":radio") )
					error.appendTo ( element.parent() );
				else if ( element.is(":checkbox") )
					error.appendTo ( element.parent() );
				else if ( element.is("input[name=checkCode]") )
					error.appendTo ( element.parent() );
				else
					error.insertAfter(element);
				$('<br/>').appendTo(element.parent());
				error.appendTo(element.parent());
			}
		});
	});

</script>

</body>	
</html>