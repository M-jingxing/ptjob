<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
	<title>个人说明</title>
	<base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	body{
		font-weight:400;
		font-size:14px;
		background-color:#f5f5f5;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	.editNote{
		padding-top:14px;
	}
	:-moz-placeholder{
		color:#888; /*mozilla ff 4-18*/
		padding:5px;
	}
	::-moz-placeholder{
		color:#888; /*mozilla ff 19+*/
		padding:5px;
	}
	:-ms-input-placeholder{
		color:#888; /*ie 10+*/
		padding:5px;
	}
	::-webkit-input-placeholder{
		color:#888; /*webkit browsers*/
		padding:5px;
	}
	.content select{
		background:#fff url(img/myResume/arrow.png) no-repeat right;
		width:60%;
		text-align:left;
	}
	label{
		font-weight:400;
		width:80px;
	}
	input[type=text],select{
		border:none;
		outline:none;
		background-color:#fff;
		color:#888;
		appearance: none;
		-moz-appearance:none;
		-webkit-appearance: none;
		padding-left:1px;
		text-align:left;
	}
	select::-ms-expand{
		display:none;	/*清除ie的默认选择框样式，隐藏下拉箭头（*/
	}
	.store{
		text-align:center;
		padding-top:45px;
	}
	.store button{
		width:80px;
		height:30px;
		background-color:#ff9037;
		border:1px solid #fb8c00;
		outline:none;
		border-radius:6px;
		color:#fff;
	}
	textarea{
		width:100%;
		border:none;
		background-color:#fff;
		margin-top:11px;
		outline:none;
		resize:none;
	}
</style>
</head>
<body>
<div class="editNote">
	<div>
	   <input type="hidden" name="resumeId" id="resumeId" value="${resumeId }">
	   <input type="hidden" id="userDescribeH" value="${userDescribe }">
		<textarea name="userDescribe" id="userDescribe" cols="30" rows="10" maxlength="200" placeholder="请输入您的个人说明，不超过200个字......"></textarea>
	</div>
	<div  class="store">
		<button type="button" onclick="savaResume()">保存</button>
	</div>
</div>
<div class="setInfo"></div>
<script src="js/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
       var userDescribeH=$("#userDescribeH").val();
       $("#userDescribe").val(userDescribeH);
     });



 function savaResume() {
      var userDescribe=$("#userDescribe").val();
      var resumeId=$("#resumeId").val();
			$.post("person/saveResume.do", {resumeId:resumeId,
			                                userDescribe:userDescribe}, function(
					data) {
				if (data.res) {
					alert(data.msg);
					location = "person/toSaveResume.do";
				} else {
					alert(data.msg);
				}

			}, "json");

		}
</script>
</body>	
</html>