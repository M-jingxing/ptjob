<%@page import="com.job.model.UserResume"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<title>我的简历</title>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta charset="utf-8" />
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<meta name="format-detection" content="telephone=no">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"
	media="screen">
<style>
body {
	font-weight: 400;
	font-size: 14px;
}

li {
	list-style-type: none;
}

a:link, a:visited, a:hover, a:active {
	text-decoration: none;
}

.head {
	padding-top: 10px;
	text-align: center;
}

.headImg {
	width: 60px;
	height: 60px;
	text-align: center;
	border-radius: 50%;
}

.head p {
	color: #999;
	font-size: 12px;
	padding-top: 10px;
	padding-bottom: 3px;
}

.head button {
	border: none;
	background-color: #fff;
	outline: none;
	border: 1px solid #fff;
	background: url(img/photo.png) no-repeat 50% 0%;
}

.setInfo {
	background-color: #f5f5f5;
	padding: 12px 14px 12px 9px;
	vertical-align: middle;
}

.info {
	font-size: 16px;
	color: #222;
	font-weight: 600;
}

.request {
	font-size: 12px;
	color: #ff9037;
	border: 1px solid #ff9037;
	border-radius: 6px;
	padding: 2px 8px;
	line-height: 20px;
}

.infoContent, .jobExpContent {
	color: #666;
}

.infoContent ul {
	padding-left: 20px;
	padding-right: 15px;
}

.infoContent ul li, .jobExpContent ul li {
	padding-top: 4px;
	padding-bottom: 4px;
}

.post {
	color: #000;
	font-size: 16px;
}

.jobExpContent .line {
	border-left: 1px solid #ff9037;
	margin-left: 20px;
	margin-top: 10px;
	position: relative;
}

.jobExpContent ul {
	padding-right: 15px;
	padding-left: 25px;
}

.jobExpContent ul li:first-child {
	color: #222;
}

.jobExpContent ul li:nth-child(3) {
	color: #666;
	word-wrap: break-word;
}

.jobExpContent .circleLine {
	position: relative;
	top: 0px;
	left: -10px;
	width: 20px;
	height: 20px;
	border: 4px solid #f5d395;
	border-radius: 50%;
	float: left;
	background-color: #ff9035;
}

.infoContent ul .edit, .jobExpContent ul .edit {
	font-size: 12px;
	color: #ff9037;
}

.eduInfoContent {
	text-align: center;
	border-top: 1px solid #ff9037;
	border-bottom: 1px solid #ff9037;
	background-color: #fcf0e1;
}

.eduInfoContent ul {
	padding-left: 0;
	padding-top: 13px;
	margin-bottom: 8px;
}

.eduInfoContent ul li {
	vertical-align: middle;
	padding-bottom: 10px;
}

.eduInfoContent ul .glyphicon-plus-sign {
	font-size: 20px;
}

.glyphicon {
	top: 3px;
}

.eduInfoContent ul li:first-child a {
	color: #ff9037;
	font-weight: 600;
	vertical-align: center;
}

.eduInfoContent ul li:last-child {
	font-size: 12px;
	color: #999;
}

.glyphicon-plus-sign:before {
	width: 18px;
	height: 18px;
}

.pNote {
	padding: 10px 15px 10px 15px;
}

.pNote a>span {
	color: #ff9137;
	font-size: 12px;
}
/*导航栏*/
nav {
	color: #fff;
	background-color: #ff9037;
	padding: 6px;
}

nav a:link, nav a:visited, nav a:hover, nav a:active {
	color: #fff;
	font-size: 16px;
}

textarea {
	width: 100%;
	border: none;
	background-color: #fff;
	margin-top: 11px;
	outline: none;
	border-radius: 12px;
	resize: none;
}

/*绝对对齐*/
.infoContent1{
	padding:15px;
}
.infoContent1 table{
	width:100%;
}
.infoContent1 table tr{
	height:25px;
}
.infoContent1 table td{
	text-align:left;
}
.infoContent1 table td:first-child{
	width:80px;
}
.infoContent1 table .edit1{
	text-align:right;
}

.infoContent1 .ul1{
	float:left;
	padding:0;
	margin:0;
}
.ul1 li{
	float:left;
	width:15px;
	height:20px;
}
.infoContent1 .edit, .jobExpContent1 .edit{
	font-size: 12px;
    color: #ff9037;
}




</style>



</head>
<body>
	<!--导航栏-->
	<nav>
		<a href="index.jsp"><span class="glyphicon glyphicon-chevron-left"></span>
			返回</a> <a href="index.jsp"><span
			class="glyphicon glyphicon-home pull-right"></span></a>
	</nav>
	<form action="fileForm" style="display: none;"
		enctype="multipart/form-data" id="fileForm">
		<input type="file" id="file" name="imgFile" onchange="uploadImg()">
	</form>
	<%-- 	<!--头像-->
	<div class="head">
		
			<button id="uploadBtn" >
			<img class="headImg" id="licenseImg"  src="../xrjob-img/${userResume.resumePic }" alt="">
		    	<p>点击更换</p>
			</button>
		
	</div> --%>
	<!--基本信息-->
	<div>
		<div class="setInfo">
			<span class="info">基本信息</span>&nbsp;&nbsp;<span class="request">必填</span>
		</div>
	</div>
	<div class="infoContent1">
	
		<table>
			<tr>
				<td><ul class='ul1'>
						<li>姓</li>
						<li></li>
						<li></li>
						<li>名：</li></ul>
				</td>
				<td><span>${userResume.resumeUserName }</span></td>
				<td class="edit1"><a
				href="person/toChangeInfo.do"><span class="edit"><span
						class="glyphicon glyphicon-pencil"></span>&nbsp;<span>编辑</span></span></a></td>
			</tr>
			<tr>
				<td><ul class='ul1'>
					<li>性</li>
					<li></li>
					<li></li>
					<li>别：</li>
				</ul></td>
				<td colspan="2"><span><c:if
						test="${userResume.sex==1 }">男</c:if><c:if
						test="${userResume.sex==0 }">女</c:if></span></td>
			</tr>
			<tr>
				<td><ul class='ul1'>
					<li>最</li>
					<li>高</li>
					<li>学</li>
					<li>历：</li>
				</ul></td>
				<td colspan="2"><span><c:if
						test="${userResume.highDegree == 1}">高中以下</c:if><c:if 
						test="${userResume.highDegree == 2}">高中</c:if><c:if 
					test="${userResume.highDegree == 3}">中专/技校</c:if><c:if 
					test="${userResume.highDegree == 4}">大专</c:if><c:if 
					test="${userResume.highDegree == 5}">本科</c:if><c:if 
					test="${userResume.highDegree == 6}">硕士以上</c:if></span></td>
			</tr>
			<tr>
				<td><ul class='ul1'>
					<li>工</li>
					<li>作</li>
					<li>年</li>
					<li>限：</li>
				</ul></td>
				<td><span><c:if test="${userResume.workYear == 1}">1年以下</c:if><c:if 
							test="${userResume.workYear == 2}">1-2年</c:if><c:if 
							test="${userResume.workYear == 3}">3-5年</c:if><c:if 
							test="${userResume.workYear == 4}">5-10年</c:if><c:if 
							test="${userResume.workYear == 5}">10年以上</c:if></span></td>
			</tr>
			<tr>
				<td><ul class='ul1'>
					<li>出</li>
					<li>生</li>
					<li>年</li>
					<li>月：</li>
				</ul></td>
				<td><span><fmt:formatDate
						value="${userResume.birthDate}" pattern="yyyy-MM-dd" /></span></td>
			</tr>
			<tr>
				<td><ul class='ul1'>
					<li>到</li>
					<li>岗</li>
					<li>时</li>
					<li>间：</li>
				</ul></td>
				<td><span><c:if
						test="${userResume.toJobDate == 1}">随时到岗</c:if><c:if 
					test="${userResume.toJobDate == 2}">一周以内</c:if><c:if 
					test="${userResume.toJobDate == 3}">一个月以内</c:if><c:if 
					test="${userResume.toJobDate == 4}">1-3个月</c:if><c:if 
					test="${userResume.toJobDate == 5}">三个月后</c:if><c:if 
					test="${userResume.toJobDate == 6}">待定</c:if></span></td>
			</tr>
			<tr>
				<td><ul class='ul1'>
					<li>求</li>
					<li>职</li>
					<li>状</li>
					<li>态：</li>
				</ul></td>
				<td><span><c:if
						test="${userResume.jobStatus == 1}">离职在找工作</c:if><c:if
						test="${userResume.jobStatus == 2}">在职在找工作</c:if><c:if
						test="${userResume.jobStatus == 3}">暂时不想找工作</c:if></span></td>
			</tr>
			<tr>
				<td><ul class='ul1'>
					<li>籍</li>
					<li></li>
					<li></li>
					<li>贯：</li>
				</ul></td>
				<td><span>${userResume.originPlace }</span></td>
			</tr>
			<tr>
				<td><ul class='ul1'>
					<li>联</li>
					<li>系</li>
					<li>电</li>
					<li>话：</li>
				</ul></td>
				<td><span>${userResume.userMobile }</span></td>
			</tr>
			<tr>
				<td><ul class='ul1'>
					<li>邮</li>
					<li>箱</li>
					<li>地</li>
					<li>址：</li>
				</ul></td>
				<td><span>${userResume.userEmail }</span></td>
			</tr>
		</table>
	</div>
	
	
		<!--
			
			
			<span>${userResume.resumeUserName }</span><a
				href="person/toChangeInfo.do"><span class="edit pull-right"><span
						class="glyphicon glyphicon-pencil"></span>&nbsp;<span>编辑</span></span></a></li>
			<li>性&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;别：&nbsp;<span> <c:if
						test="${userResume.sex==1 }">男</c:if> <c:if
						test="${userResume.sex==0 }">女</c:if></span></li>
			<li>最高学历：&nbsp;<span> <c:if
						test="${userResume.highDegree == 1}">高中以下</c:if>
					<c:if test="${userResume.highDegree == 2}">高中</c:if>
					<c:if test="${userResume.highDegree == 3}">中专/技校</c:if>
					<c:if test="${userResume.highDegree == 4}">大专</c:if>
					<c:if test="${userResume.highDegree == 5}">本科</c:if>
					<c:if test="${userResume.highDegree == 6}">硕士以上</c:if></span></span></li>
			<li>工作年限：&nbsp;<span> <c:if
						test="${userResume.workYear == 1}">1年以下</c:if>
					<c:if test="${userResume.workYear == 2}">1-2年</c:if>
					<c:if test="${userResume.workYear == 3}">3-5年</c:if>
					<c:if test="${userResume.workYear == 4}">5-10年</c:if>
					<c:if test="${userResume.workYear == 5}">10年以上</c:if></span></li>
			<li>出生年月：&nbsp;<span> <fmt:formatDate
						value="${userResume.birthDate}" pattern="yyyy-MM-dd" /></span></li>
			<li>到岗时间：&nbsp;<span> <c:if
						test="${userResume.toJobDate == 1}">随时到岗</c:if>
					<c:if test="${userResume.toJobDate == 2}">一周以内</c:if>
					<c:if test="${userResume.toJobDate == 3}">一个月以内</c:if>
					<c:if test="${userResume.toJobDate == 4}">1-3个月</c:if>
					<c:if test="${userResume.toJobDate == 5}">三个月后</c:if>
					<c:if test="${userResume.toJobDate == 6}">待定</c:if></span></li>
			<li>求职状态：&nbsp;<span> <c:if
						test="${userResume.jobStatus == 1}">离职在找工作</c:if> <c:if
						test="${userResume.jobStatus == 2}">在职在找工作</c:if> <c:if
						test="${userResume.jobStatus == 3}">暂时不想找工作</c:if></span></li>
			<li>籍&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;贯：&nbsp;<span>${userResume.originPlace }</span></li>
			<li>联系电话：&nbsp;<span>${userResume.userMobile }</span></li>
			<li>邮箱地址：&nbsp;<span>${userResume.userEmail }</span></li>
		</ul>
	</div>-->
	<!--期望岗位-->
	<div>
		<div class="setInfo">
			<span class="info">期望岗位</span>&nbsp;&nbsp;<span class="request">必填</span>
		</div>
	</div>
	<c:if test="${userResume.typeName==null}">
		<div class="eduInfoContent">
			<ul>
				<li><a href="person/toChangeIntentJob.do"><span
						class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;添加期望岗位</a></li>
			</ul>
		</div>
	</c:if>
	<c:if test="${userResume.typeName!=null}">
		<div class="infoContent">

			<ul>
				<li class="post">${userResume.typeName}<a
					href="person/toChangeIntentJob.do"><span
						class="edit pull-right"><span
							class="glyphicon glyphicon-pencil"></span>&nbsp;<span>编辑</span></span></a>
				<li><c:if test="${userResume.jobType == 1}">全职</c:if> <c:if
						test="${userResume.jobType == 2}">兼职</c:if> <c:if
						test="${userResume.jobType == 3}">应届生</c:if>/ <c:if
						test="${userResume.salaryId == 1}">1500元及以下/月</c:if> <c:if
						test="${userResume.salaryId == 2}">1500-2500元/月</c:if> <c:if
						test="${userResume.salaryId == 3}">2500-3500元/月</c:if> <c:if
						test="${userResume.salaryId == 4}">3500-5000元/月</c:if> <c:if
						test="${userResume.salaryId == 5}">5000-7000元/月</c:if> <c:if
						test="${userResume.salaryId == 6}">7000-10000元/月</c:if> <c:if
						test="${userResume.salaryId == 7}">10000-15000元/月</c:if> <c:if
						test="${userResume.salaryId == 8}">15000元以上/月</c:if> <c:if
						test="${userResume.salaryId == 0}">薪资面议</c:if></li>
			</ul>
		</div>
	</c:if>
	<!--工作经历-->
	<div>
		<div class="setInfo">
			<span class="info">工作经历</span>
		</div>
	</div>

	<c:if test="${userResumeWork.size()==0}">
		<div class="eduInfoContent">
			<ul>
				<li><a href="xrjobPages/addJobExp.jsp"><span
						class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;添加工作经历</a></li>
				<li>请从最近的工作经历开始添加</li>
			</ul>
		</div>
	</c:if>
	<div class="jobExpContent">
		<div class="line">

			<c:if test="${userResumeWork.size()!=0 }">
				<c:forEach items="${userResumeWork}" var="item">
					<div class="circleLine"></div>
					<ul>
						<li>${item.startDate}至${item.endDate}<a
							href="person/toAddResumeWork.do"><span
								class="edit pull-right"><span
									class="glyphicon glyphicon-pencil"></span>&nbsp;<span>编辑</span></span></a>
						<li class="post">${item.companyName}/${item.userJob}</li>
						<li>工作描述：${item.workDescribe}</li>
					</ul>
				</c:forEach>
			</c:if>

		</div>
	</div>
	<!--教育经历-->
	<div>
		<div class="setInfo">
			<span class="info">教育经历</span>
		</div>
	</div>
	<c:if test="${userResumeEdu.size()==0 }">
		<div class="eduInfoContent">
			<ul>
				<li><a href="xrjobPages/addEduExp.jsp"><span
						class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;添加教育经历</a></li>
				<li>请从最高学历说起</li>
			</ul>
		</div>
	</c:if>
	<div class="jobExpContent">
		<div class="line">
			<c:if test="${userResumeEdu.size()!=0 }">
				<c:forEach items="${userResumeEdu}" var="item">
					<div class="circleLine"></div>
					<ul>
						<li>${item.startDate}至${item.endDate}<a
							href="person/toAddResumeEdu.do"><span class="edit pull-right"><span
									class="glyphicon glyphicon-pencil"></span>&nbsp;<span>编辑</span></span></a>
						<li class="post">${item.schoolName}/${item.professionalName}</li>
						<li>学历：<c:if test="${item.educationGrade == 1}">高中以下</c:if><c:if
						        test="${item.educationGrade == 2}">高中</c:if><c:if
								test="${item.educationGrade == 3}">中专/技校</c:if><c:if
								test="${item.educationGrade == 4}">大专</c:if><c:if
								test="${item.educationGrade == 5}">本科</c:if><c:if
								test="${item.educationGrade == 6}">硕士以上</c:if></li>
					</ul>
				</c:forEach>
			</c:if>
		</div>
	</div>
	<!--个人说明-->
	<div>
		<div class="setInfo">
			<span class="info">个人说明</span>
		</div>
	</div>
	<c:if test="${userResume.userDescribe==null }">
		<div class="eduInfoContent">
			<ul>
				<li><a href="person/toEditUserDescribe.do"><span
						class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;添加个人说明</a></li>
				<li>你离高薪只差一步</li>
			</ul>
		</div>
	</c:if>
	<c:if test="${userResume.userDescribe!=null }">
		<div class="pNote">
			<a href="person/toEditUserDescribe.do"><span
				class="edit pull-right"><span
					class="glyphicon glyphicon-pencil"></span>&nbsp;<span>编辑</span></span></a>
			<%-- <p><%
								  UserResume userResume=(UserResume)request.getAttribute("userResume");
								  String msg = userResume.getUserDescribe();
                                  String str = msg.replaceAll("\r\n","<br>");
                                  out.print(str); %></p> --%>
			<textarea rows="10" cols="" readonly="readonly">${userResume.userDescribe }</textarea>
		</div>
	</c:if>
	<!--给页面底端留的空白-->
	<div class="setInfo"></div>
	<script src="js/jquery-2.2.3.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>
<!-- <script type="text/javascript">


  $(document).ready(function(){
    $("#uploadBtn").on("click", function(){
			$("#file").click();
		});
	});	
	
	function uploadImg(){
	  
		var formData = new FormData($("#fileForm")[0]);
		if($("#file").val() == "") return false;
       
		$.ajax({  
            url : "person/uploadPic.do",  
            type : 'POST',  
            data : formData,  
            dataType:'json',
            processData : false,  
            contentType : false,  
            success : function(data) {  
                if(data.res){
                	$("#licenseImg").attr("src","../xrjob-img/"+data.msg);

                }
                else{
                	alert("上传失败");
                }
            },  
            error : function(responseStr) {  
            	parent.$.messager.alert('提示', "网络异常", 'error');
            }  
        });
	}	
  
</script> -->
</html>