<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<base href="<%=basePath%>">
	<meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<title>企业介绍</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	*{
		margin:0;
		padding:0;
	}
	body{
		background-color:#f5f5f5;
		font-size:14px;
		font-weight:400;
		color:#222;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	/*导航栏*/
	nav{
		width:100%;
		height:44px;
		background-color:#ff9137;
		color:#fff;
		line-height:44px;
		font-size:18px;
		text-align:center;
		padding-left:10px;
		padding-right:10px;
	}
	nav a{
		float:left;
	}
	nav img{
		width:20px;
		height:20px;
		vertical-align:middle;
	}
	nav a.home{
		float:right;
		color:#fff;
		font-size:18px;
	}
	/*主体内容*/
	h2{
		text-align:left;
		font-size:20px;
		padding:15px 25px 15px 25px;
		margin-top:15px;
		margin-bottom:10px;
	}
	.content{
		padding:0 25px 15px 25px;
		float:left;
		width:100%;
	}
	ul.hehe{
		float:left;
		width:70px;
		height:20px;
		margin-bottom:0;
	}
	ul.hehe li{
		float:left;
		width:15px;
		height:20px;
	}
	ul.hehe li.contact{
		width:30px;
		text-align:center;
	}
	.location{
		float:left;
		margin-bottom:0;
	}
	textarea{
		border:0;
		outline:none;
		resize:none;
		vertical-align:top;
		background-color:#f5f5f5;
		width:100%;
		padding:10px 0 0 0;

	}

</style>
</head>
<body>
	<!--导航栏-->
	<nav>
		<a href="javascript:history.go(-1);"><img src="img/back.png"></a>
		企业介绍<a class="home" href="index.jsp"><span class="glyphicon glyphicon-home"></span></a>
	</nav>
	<!--主体内容-->
	<h2>${company.companyName }</h2>
	<div>
		<div class="content">
			<ul class="hehe">
				<li>企</li>
				<li>业</li>
				<li>性</li>
				<li>质：</li>
			</ul>
			<span><c:if
			 test="${company.companyNature == 1}">国有企业</c:if><c:if 
			 test="${company.companyNature == 2}">外资企业</c:if><c:if 
			 test="${company.companyNature == 3}">合资企业</c:if><c:if 
			 test="${company.companyNature == 4}">私营企业</c:if><c:if 
			 test="${company.companyNature == 5}">民营企业</c:if><c:if 
			 test="${company.companyNature == 6}">股份制企业</c:if><c:if 
			 test="${company.companyNature == 7}">集体企业</c:if><c:if 
			 test="${company.companyNature == 8}">社会团体</c:if><c:if 
			 test="${company.companyNature == 9}">分支机构</c:if></span>
		</div>
		<div class="content">
			<ul class="hehe">
				<li>企</li>
				<li>业</li>
				<li>规</li>
				<li>模：</li>
			</ul>
			<span><c:if
			 test="${company.companyScale == 1}">15人以下</c:if><c:if
			 test="${company.companyScale == 2}">15-50人</c:if><c:if
			 test="${company.companyScale == 3}">50-200人</c:if><c:if
			 test="${company.companyScale == 4}">200-500人</c:if><c:if
			 test="${company.companyScale == 5}">500-2000人</c:if><c:if
			 test="${company.companyScale == 6}">2000人以上</c:if></span>
		</div>
		<div class="content">
			<ul class="hehe">
				<li>联</li>
				<li class="contact">系</li>
				<li>人：</li>
			</ul>
			<span>${company.companyContractPerson }</span>
		</div>
		<div class="content">
			<ul class="hehe">
				<li>联</li>
				<li>系</li>
				<li>电</li>
				<li>话：</li>
			</ul>
			<span>${company.companyContractMobile }</span>
		</div>
		<div class="content">
			<ul class="hehe">
				<li>企</li>
				<li>业</li>
				<li>地</li>
				<li>址：</li>
			</ul>
			<p class="location">${company.companyAddress }</p>
		</div>
		<div class="content">
			<ul class="hehe description">
				<li>企</li>
				<li>业</li>
				<li>描</li>
				<li>述：</li>
			</ul>
			<textarea readonly="readonly" rows="15">${company.companyDescribe }</textarea>
		</div>
	</div>





<script src="js/jquery-2.2.3.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script>
	$(function(){
		/*p和textarea的内容随页面宽度改变而改变*/
		var w=$('.content').width()-90;
		$('.location').width(w);
	})


</script>
</body>	
</html>