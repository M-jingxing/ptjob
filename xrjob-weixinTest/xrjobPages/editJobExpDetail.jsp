<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
	<title>编辑工作经历</title>
	<base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta charset="utf-8" />
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="http://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.4.0/animate.min.css">
	<link rel="stylesheet" href="src/jquery.gDialog.css">
	<link href="dev/css/mobiscroll.core-2.5.2.css" rel="stylesheet" type="text/css" />
	<link href="dev/css/mobiscroll.animation-2.5.2.css" rel="stylesheet" type="text/css" />

	<!-- S 可根据自己喜好引入样式风格文件 -->
	<link href="dev/css/mobiscroll.android-ics-2.5.2.css" rel="stylesheet" type="text/css" />
	<!-- E 可根据自己喜好引入样式风格文件 -->
<style>
	body{
		font-weight:400;
		font-size:14px;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	.addJobExp{
		padding-top:14px;
	}
	.content{
		margin-left:30px;
		padding-top:20px;
		padding-bottom:8px;
		margin-right:30px;
		border-bottom:1px solid #e8dfe8;
		color:#888;
	}
	.content:last-child{
		padding-top:36px;
	}
	:-moz-placeholder{
		color:#888; /*mozilla ff 4-18*/
	}
	::-moz-placeholder{
		color:#888; /*mozilla ff 19+*/
	}
	:-ms-input-placeholder{
		color:#888; /*ie 10+*/
	}
	::-webkit-input-placeholder{
		color:#888; /*webkit browsers*/
	}
	.content select{
		background:#fff url(img/myResume/arrow.png) no-repeat right;
		width:50%;
		text-align:left;
	}
	label{
		font-weight:400;
		width:80px;
	}
	input[type=text],select{
		border:none;
		outline:none;
		background-color:#fff;
		color:#888;
		appearance: none;
		-moz-appearance:none;
		-webkit-appearance: none;
		padding-left:3px;
		text-align:left;
	}
	select{
		padding-left:0;
	}
	select::-ms-expand{
		display:none;	/*清除ie的默认选择框样式，隐藏下拉箭头（*/
	}
	.glyphicon-edit{
		color: #fff;
		background-color:#ff9037;
		padding:6px;
		border-radius:50%;
		margin-right:6px;
	}
	.store{
		text-align:center;
		padding-top:45px;
	}
	.store .btn1{
		width:80px;
		height:30px;
		background-color:#fa0217;
		border:1px solid #fb8c00;
		outline:none;
		border-radius:6px;
		color:#fff;
		margin-left:33px;
		margin-right:33px;
	}
	.store .btn2{
		width:80px;
		height:30px;
		background-color:#ff9037;
		border:1px solid #fb8c00;
		outline:none;
		border-radius:6px;
		color:#fff;
		margin-left:33px;
		margin-right:33px;
	}
	textarea{
		width:100%;
		border:none;
		background-color:#f5f5f5;
		margin-top:11px;
		outline:none;
		border-radius: 12px;
		resize:none;
	}
	/*日期框*/
	.android-ics .dwv{
		border-bottom:1px solid #ff9037;
		color:#ff9037;
	}
	.android-ics .dw{
		color:#ff9037;
	}
	.android-ics .dw .dwwol{
		border-top:1px solid #ff9037;
		border-bottom:1px solid #ff9037;
	}
	.android-ics.light .dwb{
		color:#ff9037;
	}
	
</style>
</head>
<body>
<form action="" id="form">
      <input type="hidden" name="workId" value="${workDetail.workId }"> 
<div class="addJobExp">
	<div class="content">
		<span class="glyphicon glyphicon-edit"></span>
		<input type="text" id="companyName" placeholder="公司名称" name="companyName" value="${workDetail.companyName }">
	</div>
	<div class="content">
		<span class="glyphicon glyphicon-edit"></span>
		<input type="text" id="userJob" placeholder="您的职位  如：销售经理" name="userJob" value="${workDetail.userJob }">
	</div>
	<div class="content">
	<input type="hidden" id="startDateH" name="startDate" value="${workDetail.startDate }">
		<span class="glyphicon glyphicon-edit"></span>
		<input type="text" id="startDate" placeholder="${workDetail.startDate }"  value="${workDetail.startDate }">
	</div>
	<div class="content">
	<input type="hidden" id="endDateH"  name="endDate" value="${workDetail.endDate }">
		<span class="glyphicon glyphicon-edit"></span>
		<input type="text" id="endDate" placeholder="${workDetail.endDate }"  value="${workDetail.endDate }">
	</div>
	<div class="content">
		<span class="glyphicon glyphicon-edit"></span>
		工作描述
		<div>
		     <input type="hidden"  id="workDescribeH" value="${workDetail.workDescribe }">
			<textarea name="workDescribe" id="workDescribe" cols="30" rows="10" maxlength="90" placeholder="不超过90个字......" ></textarea>
		</div>
	</div>
	<div  class="store">
		<button class="btn1" type="button" onclick="deleteWork('${workDetail.workId}')">删除</button>
		<button class="btn2" type="button" onclick="updateWork()">保存</button>
	</div>
</div>
</form>
<script src="js/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="src/jquery.js"></script>
<script type="text/javascript" src="src/jquery.gDialog.js"></script>
	<!--datePicker-->
<script src="dev/js/mobiscroll.core-2.5.2.js" type="text/javascript"></script>
<script src="dev/js/mobiscroll.core-2.5.2-zh.js" type="text/javascript"></script>
<script src="dev/js/mobiscroll.datetime-2.5.1.js" type="text/javascript"></script>
<script src="dev/js/mobiscroll.datetime-2.5.1-zh.js" type="text/javascript"></script>
<!-- S 可根据自己喜好引入样式风格文件 -->
<script src="dev/js/mobiscroll.android-ics-2.5.2.js" type="text/javascript"></script>
<script>
     $(document).ready(function(){
       var workDescribeH=$("#workDescribeH").val();
       $("#workDescribe").val(workDescribeH);
     });
    
    $(function(){
	    
		
		var currYear = (new Date()).getFullYear();
		var opt={};
		opt.date = {preset : 'date'};
		//opt.datetime = { preset : 'datetime', minDate: new Date(2012,3,10,9,22), maxDate: new Date(2014,7,30,15,44), stepMinute: 5  };
		opt.datetime = {preset : 'datetime'};
		opt.time = {preset : 'time'};
		opt.default = {
			theme: 'android-ics light', //皮肤样式
			display: 'modal', //显示方式
			mode: 'scroller', //日期选择模式
			lang:'zh',
			startYear:currYear - 100, //开始年份
			endYear:currYear	//结束年份
		};

		$("#startDate").scroller('destroy').scroller($.extend(opt['date'], opt['default']));
		$("#endDate").scroller('destroy').scroller($.extend(opt['date'], opt['default']));
	});

     function deleteWork(workId){
       if(confirm("您确定要删除该条记录吗，删除后记录将无法恢复")){
     	$.post("person/deleteResumeWork.do", {workId:workId}, function(
					data) {
				if (data.res) {
					alert(data.msg);
					location = "person/toAddResumeWork.do";
				} else {
					alert(data.msg);
				}

			}, "json");
			}

     }
     
   
   function updateWork(){
        if($("#startDate").val()!=""){
         $("#startDateH").val($("#startDate").val()); 
         }
         if($("#endDate").val()!=""){
         $("#endDateH").val($("#endDate").val()); 
           }
          
       if(checkForm()){
     	$.post("person/editResumeWork.do", $("#form").serialize(), function(
					data) {
				if (data.res) {
					alert(data.msg);
					location = "person/toAddResumeWork.do";
				} else {
					alert(data.msg);
				}

			}, "json");
         }
     }

    function checkForm(){
       var startDate=$("#startDate").val();
       var endDate=$("#endDate").val();
       var startDate1=startDate.split("-");
       var endDate1=endDate.split("-");
       var startDate2=new Date(startDate1[0],startDate1[1],startDate1[2]);
       var endDate2=new Date(endDate1[0],endDate1[1],endDate1[2]);
       if(Date.parse(startDate2)>Date.parse(endDate2)){
         alert("开始时间不能大于结束时间");
         return false;
       }
       
       if($("#companyName").val()==null||$("#companyName").val()==""){
         alert("公司名称不能为空");
            return false;
       }
       if($("#userJob").val()==null||$("#userJob").val()==""){
         alert("工作岗位不能为空");
            return false;
       }
       if(startDate==null||startDate==""){
           alert("开始时间不能为空");
              return false;
         }
     
        if(endDate==null||endDate==""){
         alert("结束时间不能为空");
            return false;
       }
       
       return true;
    } 
     

	$('.btn1').click(function(){
		$.gDialog.confirm("删除此工作经历将无法恢复，确认删除吗?", {
			title: "删除",
			animateIn : "bounceInDown",
			animateOut: "bounceOutUp"
		});
	});
</script>
</body>	
</html>