<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<title>基本信息</title>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta charset="utf-8" >
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<meta name="format-detection" content="telephone=no">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"
	media="screen">
	<link href="dev/css/mobiscroll.core-2.5.2.css" rel="stylesheet" type="text/css" />
	<link href="dev/css/mobiscroll.animation-2.5.2.css" rel="stylesheet" type="text/css" />

	<!-- S 可根据自己喜好引入样式风格文件 -->
	<link href="dev/css/mobiscroll.android-ics-2.5.2.css" rel="stylesheet" type="text/css" />
	<!-- E 可根据自己喜好引入样式风格文件 -->
<style>
	body{
		font-weight:400;
		font-size:14px;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	.editInfo{
		padding-top:14px;
	}
	.content{
		margin-left:30px;
		padding-top:10px;
		padding-bottom:8px;
		margin-right:30px;
		border-bottom:1px solid #e8dfe8;
	}
	:-moz-placeholder{
		color:#888; /*mozilla ff 4-18*/
	}
	::-moz-placeholder{
		color:#888; /*mozilla ff 19+*/
	}
	:-ms-input-placeholder{
		color:#888; /*ie 10+*/
	}
	::-webkit-input-placeholder{
		color:#888; /*webkit browsers*/
	}
	.content select{
		background:#fff url(img/myResume/arrow.png) no-repeat right;
		width:60%;
		text-align:left;
	}
	label{
		font-weight:400;
		padding-right:5px;
		margin-bottom:0;
		vertical-align:middle;
	}
	input[type=text],select{
		border:none;
		outline:none;
		background-color:#fff;
		color:#888;
		appearance: none;
		-moz-appearance:none;
		-webkit-appearance: none;
		text-align:left;
		display:inline-block;
		padding-left:10px;
	}
	input{
		
	}
	select::-ms-expand{
		display:none;	/*清除ie的默认选择框样式，隐藏下拉箭头（*/
	}
	.store{
		text-align:center;
		padding-top:45px;
	}
	.store button{
		width:80px;
		height:30px;
		background-color:#ff9037;
		border:1px solid #fb8c00;
		outline:none;
		border-radius:6px;
		color:#fff;
	}
	.error{
		color:#ff9037;
		font-size:12px;
		padding-left:80px;
	}
	em{
		font-style:normal;
		font-size:14px;
	}
	.select-datetime dl.datetime-day dt span{
		font-size:12px;
	}
	.select-datetime dl.datetime-day dt{
		font-size:12px;
		width:40px;
		font-weight:400;
	}
	 .select-datetime dl.datetime-day dd{
		width:41px;
	}
	.android-ics .dwv{
		border-bottom:1px solid #ff9037;
		color:#ff9037;
	}
	.android-ics .dw{
		color:#ff9037;
	}
	.android-ics .dw .dwwol{
		border-top:1px solid #ff9037;
		border-bottom:1px solid #ff9037;
	}
	.android-ics.light .dwb{
		color:#ff9037;
	}
	input[type="text"]:disabled{
		color:#999;
	}
	
	ul.hehe{
		margin-left:0;
		padding-left:0;
		width:60px;
		height:20px;
		margin-bottom:0;
	}
	ul.hehe li{
		width:15px;
		height:20px;
		float:left;
	}
</style>
</head>
<body>
	<form method="post" id="form">
		<input type="hidden" name="resumeId" value="${userResume.resumeId }">
		<div class="editInfo">
			<div class="content">
				<label for="name"><ul class="hehe">
						<li>姓</li>
						<li></li>
						<li></li>
						<li>名：</li>
					</ul></label>
				<input type="text" id="name" placeholder="请输入姓名" name="resumeUserName"
					value="${userResume.resumeUserName }"  disabled="disabled">
			  <input type="hidden" id="nameH" value="${userResume.resumeUserName }" name="resumeUserName" >
			</div>
			<div class="content">
				<label>
				<ul class="hehe">
						<li>性</li>
						<li></li>
						<li></li>
						<li>别：</li>
					</ul></label> <select
					name="sex" id="sex">
					<option value="">请选择</option>
					<option value="1"
						<c:if test="${userResume.sex==1 }">selected="selected"</c:if>>男</option>
					<option value="0"
						<c:if test="${userResume.sex==0 }">selected="selected"</c:if>>女</option>
				</select>
			</div>
			<div class="content">
				<label>
				<ul class="hehe">
						<li>最</li>
						<li>高</li>
						<li>学</li>
						<li>历：</li>
					</ul></label> <select name="highDegree" id="highDegree">
					<option value="">请选择</option>
					<option value="1"
						<c:if test="${userResume.highDegree==1 }">selected="selected"</c:if>>高中以下</option>
					<option value="2"
						<c:if test="${userResume.highDegree==2 }">selected="selected"</c:if>>高中</option>
					<option value="3"
						<c:if test="${userResume.highDegree==3 }">selected="selected"</c:if>>中专/技校</option>
					<option value="4"
						<c:if test="${userResume.highDegree==4 }">selected="selected"</c:if>>大专</option>
					<option value="5"
						<c:if test="${userResume.highDegree==5 }">selected="selected"</c:if>>本科</option>
					<option value="6"
						<c:if test="${userResume.highDegree==6 }">selected="selected"</c:if>>硕士以上</option>
				</select>
			</div>
			<div class="content">
				<label><ul class="hehe">
						<li>工</li>
						<li>作</li>
						<li>年</li>
						<li>限：</li>
					</ul></label> <select name="workYear" id="workYear">
					<option value="">请选择</option>
					<option value="1"
						<c:if test="${userResume.workYear==1 }">selected="selected"</c:if>>1年以下</option>
					<option value="2"
						<c:if test="${userResume.workYear==2 }">selected="selected"</c:if>>1-2年</option>
					<option value="3"
						<c:if test="${userResume.workYear==3 }">selected="selected"</c:if>>3-5年</option>
					<option value="4"
						<c:if test="${userResume.workYear==4 }">selected="selected"</c:if>>5-10年</option>
					<option value="5"
						<c:if test="${userResume.workYear==5 }">selected="selected"</c:if>>10年以上</option>
				</select>
			</div>
			<div class="content">
			
				<input type="hidden" id="birthDateH" name="birthDate" value="<fmt:formatDate
						value="${userResume.birthDate}" pattern="yyyy-MM-dd" />">
						
				<label for=""><ul class="hehe">
						<li>出</li>
						<li>生</li>
						<li>年</li>
						<li>月：</li>
					</ul></label> <input type="text" id="birthDate"
					value=""
					placeholder="<fmt:formatDate
						value="${userResume.birthDate}" pattern="yyyy-MM-dd" />" >
			</div>
			<div class="content">
				<label><ul class="hehe">
						<li>到</li>
						<li>岗</li>
						<li>时</li>
						<li>间：</li>
					</ul></label> <select name="toJobDate" id="toJobDate">
					<option value="">请选择</option>
					<option value="1"
						<c:if test="${userResume.toJobDate==1 }">selected="selected"</c:if>>随时到岗</option>
					<option value="2"
						<c:if test="${userResume.toJobDate==2 }">selected="selected"</c:if>>1周以内</option>
					<option value="3"
						<c:if test="${userResume.toJobDate==3 }">selected="selected"</c:if>>1个月以内</option>
					<option value="4"
						<c:if test="${userResume.toJobDate==4 }">selected="selected"</c:if>>1-3个月</option>
					<option value="5"
						<c:if test="${userResume.toJobDate==5 }">selected="selected"</c:if>>3个月后</option>
					<option value="6"
						<c:if test="${userResume.toJobDate==6 }">selected="selected"</c:if>>待定</option>
				</select>
			</div>
			<div class="content">
				<label><ul class="hehe">
						<li>求</li>
						<li>职</li>
						<li>状</li>
						<li>态：</li>
					</ul></label> <select name="jobStatus" id="jobStatus">
					<option value="">请选择</option>
					<option value="1"
						<c:if test="${userResume.jobStatus==1 }">selected="selected"</c:if>>离职找工作</option>
					<option value="2"
						<c:if test="${userResume.jobStatus==2 }">selected="selected"</c:if>>在职找工作</option>
					<option value="3"
						<c:if test="${userResume.jobStatus==3 }">selected="selected"</c:if>>暂时不想找工作</option>
				</select>
			</div>
			<div class="content">
				<label for=""><ul class="hehe">
						<li>籍</li>
						<li></li>
						<li></li>
						<li>贯：</li>
					</ul></label>
			
				<input type="text" id="originPlace" placeholder="如浙江杭州" name="originPlace"
					value="${userResume.originPlace }">
			</div>
			<div class="content">
				<label for=""><ul class="hehe">
						<li>联</li>
						<li>系</li>
						<li>电</li>
						<li>话：</li>
					</ul></label> <input type="text" id="userMobile"
					value="${userResume.userMobile }"
					 placeholder="请输入手机号"
					name="userMobile">
			</div>
			<div class="content">
				<label for=""><ul class="hehe">
						<li>邮</li>
						<li>箱</li>
						<li>地</li>
						<li>址：</li>
					</ul></label> <input type="text" id="userEmail"
					value="${userResume.userEmail }" placeholder="请输入邮箱地址"
					name="userEmail">
			</div>
			<div class="store">
				<button type="button" onclick="savaResume()">保存</button>
			</div>
		</div>
	</form>
	<div class="setInfo"></div>
	<script src="js/jquery-2.2.3.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/jquery.validate.js"></script>
	<!--datePicker-->
<script src="dev/js/mobiscroll.core-2.5.2.js" type="text/javascript"></script>
<script src="dev/js/mobiscroll.core-2.5.2-zh.js" type="text/javascript"></script>
<script src="dev/js/mobiscroll.datetime-2.5.1.js" type="text/javascript"></script>
<script src="dev/js/mobiscroll.datetime-2.5.1-zh.js" type="text/javascript"></script>
<!-- S 可根据自己喜好引入样式风格文件 -->
<script src="dev/js/mobiscroll.android-ics-2.5.2.js" type="text/javascript"></script>

	<script type="text/javascript">
	$(function(){
		if($("#nameH").val()==null||$("#nameH").val()==""){
			$("#name").removeAttr("disabled");
		}else{
			$("#nameH").attr("name","");
		}
		
	});
	
	
		function checkFrom() {
		  
	
		    if ($("#name").val() == null || $("#name").val() == "") {
				alert("名字不能为空");
				return false;
			}
		
		    if ($("#sex").val() == null || $("#sex").val() == "") {
				alert("性别不能为空");
				return false;
			}
	    
		    if ($("#highDegree").val() == null || $("#highDegree").val() == "") {
				alert("学历不能为空");
				return false;
			}
		    if ($("#workYear").val() == null || $("#workYear").val() == "") {
				alert("工作年限不能为空");
			    return false;
			}
		     if ($("#birthDateH").val() == null || $("#birthDateH").val() == "") {
				alert("出生年月不能为空");
			     return false;
			}
			 if ($("#toJobDate").val() == null || $("#toJobDate").val() == "") {
				alert("到岗时间不能为空");
				return false;
			}
			 if ($("#jobStatus").val() == null || $("#jobStatus").val() == "") {
				alert("求职状态不能为空");
				return false;
			}
			 if ($("#originPlace").val() == null || $("#originPlace").val() == "") {
				alert("籍贯不能为空");
				return false;
			}
			
			if ($("#userMobile").val() == null || $("#userMobile").val() == "") {
				alert("联系电话不能为空");
				return false;
			}
			if (!(/^1[3458][0-9]\d{4,8}$|^((\+86)|(86))?(13)\d{9}$|(^(\d{3,4}-)?\d{7,8})$|^400[016789]\d{6}$/.test($("#userMobile").val()))) {
                 alert("请输入正确的联系方式")
                return false;
			}
		/* 	
			if ($("#userEmail").val() == null || $("#userEmail").val() == "") {
				alert("邮箱地址不能为空");
				return false;
			}
			if(!(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test($("#userEmail").val()))){
			   alert("请输入正确的邮箱地址");
			   return false;
			} */

			return true;
		}

		function savaResume() {
		if($("#birthDate").val()!=""){
            $("#birthDateH").val($("#birthDate").val()); 
            } 
           if(checkFrom()){
			$.post("person/saveResume.do", $("#form").serialize(), function(
					data) {
				if (data.res) {
					alert(data.msg);
					location = "person/toSaveResume.do";
				} else {
					alert(data.msg);
				}

			}, "json");
          }
		}
		
	$(function(){
	    
	 
		$("#form").validate({
			rules:{
				userMobile:{
					tel:true
				},
				userEmail:{
					email:true
				}
			},
			messages:{
				userMobile:{
					tel:"请输入正确的联系方式"
				},
				userEmail:{
					email:"请输入正确的邮箱地址"
				}
			},
			/*修改错误信息的提示位置*/
			errorPlacement: function(error, element) {
				if ( element.is(":radio") )
					error.appendTo ( element.parent() );
				else if ( element.is(":checkbox") )
					error.appendTo ( element.parent() );
				else if ( element.is("input[type=text]") ){
					error.appendTo ( element.parent() );
				}
				else{
					error.insertAfter(element);
				}
				/*在这里没有必要要这句话，br会增加.error和input之间的空隙，但是这儿没有必要
				$('<br/>').appendTo(element.parent());
				 */
				error.appendTo(element.parent());
			}
		});
		
		
		var currYear = (new Date()).getFullYear();
		var opt={};
		opt.date = {preset : 'date'};
		//opt.datetime = { preset : 'datetime', minDate: new Date(2012,3,10,9,22), maxDate: new Date(2014,7,30,15,44), stepMinute: 5  };
		opt.datetime = {preset : 'datetime'};
		opt.time = {preset : 'time'};
		opt.default = {
			theme: 'android-ics light', //皮肤样式
			display: 'modal', //显示方式
			mode: 'scroller', //日期选择模式
			lang:'zh',
			startYear:currYear - 100, //开始年份
			endYear:currYear	//结束年份
		};

		$("#birthDate").scroller('destroy').scroller($.extend(opt['date'], opt['default']));
	});
		
		

	
		
	</script>
</body>
</html>