<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<meta charset="utf-8" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta name="format-detection" content="telephone=no">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>投递记录</title>
<!--CSS-->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<style>
	*{
		margin:0;
		padding:0;
	}
	body{
		font-size:14px;
		background-color:#f5f5f5;
		overflow-y:scroll;
		color:#666;
	}
	ul{
		margin-bottom:0;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration: none;
	}
	/*最上面的黄框*/
	nav{
		width:auto;
		height:30px;
		font-size:12px;
		color:#fff;
		background-color:#ff9137;
		text-align:center;
		line-height:30px;
	}

	/*投递记录*/
	.jobInfoList{
		background-color:#fff;
		padding:15px;
	}
	.list-block .deliveryDetail{	/*设置.deliveryDetail这个div就是为了让第一行没有线，接下来的几行都有上边框*/
		border-top:1px solid #f5f5f5;
	}
	.list-block  .deliveryDetail:first-child{
		border-top:0;
	}
	.list-block .item-inner{
		padding-top:8px;
		padding-bottom:8px;
	}
	.list-block .item-title{
		font-size:16px;
		font-weight:700;
		line-height:30px;
		color:#666;
	}
	.list-block .salary{
		font-size:16px;
		line-height:25px;
		color:#ff9137;
	}
	.list-block .cName{
		font-size:13px;
		color:#666;

	}

</style>
</head>
<body>
<!--导航栏-->
<nav>
	投递记录为您保管60日内的记录，超出会自动删除
</nav>
<!--投递记录-->
<div class="jobInfoList">
	<div class="list-block media-list">
		<!--循环start-->
		<c:forEach varStatus="status" items="${deliveryList }" var="userDelivery">
		<div class="deliveryDetail">
			<a href="job/jobDetail.do?jobId=${userDelivery.jobId }">
				<ul>
					<li>
						<div class="item-inner">
							<div class="item-title-row">
								<div class="item-title">${userDelivery.jobName }
								<span class="salary pull-right"><c:if test="${userDelivery.jobSalaryId == 1}">1500元及以下/月</c:if>
																		<c:if test="${userDelivery.jobSalaryId == 2}">1500-2500元/月</c:if>
																		<c:if test="${userDelivery.jobSalaryId == 3}">2500-3500元/月</c:if>
																		<c:if test="${userDelivery.jobSalaryId == 4}">3500-5000元/月</c:if>
																		<c:if test="${userDelivery.jobSalaryId == 5}">5000-7000元/月</c:if>
																		<c:if test="${userDelivery.jobSalaryId == 6}">7000-10000元/月</c:if>
																		<c:if test="${userDelivery.jobSalaryId == 7}">10000-15000元/月</c:if>
																		<c:if test="${userDelivery.jobSalaryId == 8}">15000元以上/月</c:if>
																		<c:if test="${userDelivery.jobSalaryId == 0}">薪资面议</c:if></span></div>
							</div>
							<div class="item-title-row">
								<div class="item-text cName"><c:choose><c:when test="${fn:contains(userDelivery.companyName,'xrjobOfficial')}">${fn:substring(userDelivery.companyName,0,fn:length(userDelivery.companyName)-13)}</c:when><c:otherwise >${userDelivery.companyName }</c:otherwise></c:choose><span class="pull-right"><fmt:formatDate
						value="${userDelivery.createTime}" pattern="yyyy-MM-dd" /></span></div>
							</div>
						</div>
					</li>
				</ul>
			</a>
		</div>
		<!--循环end-->
   </c:forEach>
	</div>
</div>


<!--javascript-->
<script src="js/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
 

</script>
</body>	
</html>