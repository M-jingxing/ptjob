<%@page import="com.job.model.CompanyJob"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page language="java" import="com.job.util.WeixinShareUtil" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta charset="utf-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<title>岗位详情</title>
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<meta name="format-detection" content="telephone=no">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"
	media="screen">
	<link href="css/nativeShare.css" rel="stylesheet">
<style>
p{
		margin:0;
		padding:0;
	}
	body{
		font-weight:400;
		font-size:14px;
		background-color:#fff;
		color:#666;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	.info{
		padding:18px 20px 0 20px;
		background-color:#fff;
		border-bottom:10px solid #f5f5f5;
	}
	ul{
		padding-left:0;
		padding-top:5px;
		padding-bottom:2px;
		margin-bottom:0;
	}
	.post{
		font-size:18px;
		margin-bottom:5px;
	}
	.post span:first-child{
		color:#222;
	}
	.post span:last-child{
		color:#fe552c;
	}
	.info ul li span a{
		color:#ff9137;
	}
	.info ul li a{
		color:#666;
	}
	.edu li{
		padding-bottom:10px;
		line-height:25px;
	}
	.edu li span{
		width:50%;
		float:left;
		line-height:25px;
	}
	.edu li img{
		width:20px;
		height:20px;
	}
	/*岗位要求*/
	.postRequire,.postDescription{
		background-color:#fff;
		padding:18px 20px 0 20px;
	}
	.postRequire p:first-child,.postDescription p:first-child{
		font-size:15px;
		color:#ff9137;
		margin-bottom:0;
	}
	.postRequire p:last-child,.postDescription p:last-child{
		line-height:24px;
		padding-bottom:10px;
	}
	.postDescription{
		margin-bottom:80px;
	}
/*底栏*/
.footer{
		width:100%;
		height:60px;
		position:fixed;
		left:0;
		bottom:0;
		padding:15px 0 10px 10px;
		background-color:#f5f5f5;
		z-index:50;
	}
	button{
		margin:0 auto;
		outline:none;
	}
	#btn1{
		border:none;
		background-color:#ff9137;
		width:90%;
		height:30px;
		color:#fff;
		letter-spacing:8px;
	}
	#nativeShare{
		width:30px;
		height:30px;
		border:none;
		padding:0;
		background:url(img/result.png)no-repeat;
		background-position:-60px 0;
		overflow:hidden;
		/*position:relative;
		left:20px;
		bottom:-10px;*/
	}
	#btn3{
		width:30px;
		height:30px;
		border:none;
		padding:0;

		overflow:hidden;
		/*position:relative;
		left:20px;
		bottom:-10px;*/
	}
	.gray{
		background:url(img/result.png) no-repeat;
		background-position:0 0;

	}
	.red{
		background:url(img/result.png) no-repeat;
		background-position:-30px 0;
	}
	.apply{
		width:70%;
		height:30px;
		float:left;
	}
	.share{
		width:15%;
		height:30px;
		float:left;
		text-align:center;
		padding-left:10px;
	}
	.star{
		width:15%;
		height:30px;
		float:left;
		text-align:center;
		padding-right:10px;
	}
/*提示信息*/
.box {
	position: fixed;
	top: 250px;
	left: 20%;
	right: 20%;
	background-color: #000;
	color: #fff;
	padding: 10px;
	text-align: center;
	-moz-border-radius: 10px;
	-webkit-border-radius: 10px;
	-khtml-border-radius: 10px;
	-moz-box-shadow: 0 1px 5px #999;
	-webkit-box-shadow: 0 1px 5px #999;
	opacity: 0.8;
	z-index: 101;
	display: none;
	letter-spacing: 4px;
}

textarea {
	width: 100%;
	border: none;
	background-color: #fff;
	margin-top: 11px;
	outline: none;
	border-radius: 12px;
	resize: none;
}
	nav{
		color:#fff;
		background-color:#ff9037;
		padding:6px;
	}
	nav a:link,nav a:visited,nav a:hover,nav a:active{
		color:#fff;
		font-size:16px;
	}
	/*模态框*/
	.modal-dialog{
		float:right;
	}
	.modal-dialog .arrow{
		float:right;
		margin-right:10px;
		margin-top:5px;
		width:50px;
		height:50px;
	}
	.mainContent{
		float:right;
		margin-top:55px;
		margin-right:-30px;
		color:#fff;
		/*border:1px solid #f00;*/
		width:140px;
		font-size:13px;
		line-height:30px;
		z-index:999;
	}
	.modal-dialog div p{
		text-align:center;
	}
	.friend{
		width:20px;
		height:20px;
		margin-left:5px;
		margin-right:5px;
		vertical-align: middle;
	}
	.info ul li a.cLink{
		color:#337ab7;
	}
	
</style>
</head>
<%--    <% 
   
        Map<String,Object>  ret = new HashMap<String,Object> ();
        ret=WeixinShareUtil.getWxConfig(request,(String)request.getAttribute("url"));
        request.setAttribute("appId", ret.get("appId"));
        request.setAttribute("timestamp", ret.get("timestamp"));
        request.setAttribute("nonceStr", ret.get("nonceStr"));
        request.setAttribute("signature", ret.get("signature"));
        %>  --%> 
<body>
	
	<nav>
		 <a href="javascript:history.go(-1);"><span
			class="glyphicon glyphicon-chevron-left"></span> 返回</a>
			 <a
			href="index.jsp"><span
			class="glyphicon glyphicon-home pull-right"></span></a>
	</nav> 
	<div class="info">
		<p class="post">
			<span>${jobDetail.jobName }</span><span class="pull-right"><c:if
					test="${jobDetail.jobSalaryId == 1}">1500元及以下/月</c:if> <c:if
					test="${jobDetail.jobSalaryId == 2}">1500-2500元/月</c:if> <c:if
					test="${jobDetail.jobSalaryId == 3}">2500-3500元/月</c:if> <c:if
					test="${jobDetail.jobSalaryId == 4}">3500-5000元/月</c:if> <c:if
					test="${jobDetail.jobSalaryId == 5}">5000-7000元/月</c:if> <c:if
					test="${jobDetail.jobSalaryId == 6}">7000-10000元/月</c:if> <c:if
					test="${jobDetail.jobSalaryId == 7}">10000-15000元/月</c:if> <c:if
					test="${jobDetail.jobSalaryId == 8}">15000元以上/月</c:if> <c:if
					test="${jobDetail.jobSalaryId == 0}">薪资面议</c:if></span>
		</p>
		<ul>
			<li><a class="cLink" href="job/companyInfo.do?companyId=${jobDetail.companyId }"><c:choose><c:when test="${fn:contains(jobDetail.companyName,'xrjobOfficial')}">${fn:substring(jobDetail.companyName,0,fn:length(jobDetail.companyName)-13)}<img src="../xrjob-img/${jobDetail.companyLicense }"></c:when><c:otherwise >${jobDetail.companyName }</c:otherwise></c:choose></a></li>
			<li>联系人：${jobDetail.companyContractPerson } <span
				class="pull-right"><a onclick="report()">举报</a></span>
			</li>
			<li>联系电话：<a href="tel:${jobDetail.companyContractMobile }">${jobDetail.companyContractMobile }</a></li>
		</ul>
		<ul class="edu">
			<li><span> <img src="img/qualifications.png">&nbsp;&nbsp;<c:if
						test="${jobDetail.jobDegree == 0}">不限</c:if><c:if
						test="${jobDetail.jobDegree == 1}">高中以下</c:if><c:if
						test="${jobDetail.jobDegree == 2}">高中</c:if><c:if
						test="${jobDetail.jobDegree == 3}">中专/技校</c:if><c:if
						test="${jobDetail.jobDegree == 4}">大专</c:if><c:if
						test="${jobDetail.jobDegree == 5}">本科</c:if><c:if
						test="${jobDetail.jobDegree == 6}">硕士以上</c:if>
			</span> <span> <img src="img/experience.png">&nbsp;&nbsp;<c:if
						test="${jobDetail.jobWorkYear == 0}">工作经验不限</c:if><c:if
						test="${jobDetail.jobWorkYear == 1}">1年以下工作经验</c:if><c:if
						test="${jobDetail.jobWorkYear == 2}">1-2年工作经验</c:if><c:if
						test="${jobDetail.jobWorkYear == 3}">3-5年工作经验</c:if><c:if
						test="${jobDetail.jobWorkYear == 4}">5-10年工作经验</c:if><c:if
						test="${jobDetail.jobWorkYear == 5}">10年以上工作经验</c:if>
			</span></li>
			<li><span> <img src="img/jobAll.png">&nbsp;&nbsp;<c:if
						test="${jobDetail.jobType == 1}">全职</c:if><c:if
						test="${jobDetail.jobType == 2}">兼职</c:if><c:if
						test="${jobDetail.jobType == 3}">应届生</c:if>
			</span> <span> <img src="img/people.png">&nbsp;&nbsp;${jobDetail.jobNumber }人
			</span></li>
			<li><img src="img/address.png">&nbsp;&nbsp;${jobDetail.jobAddress }
			</li>
		</ul>		
		<a  onclick="otherJob('${jobDetail.companyId }','${jobDetail.jobId}')">查看该公司的其他岗位</a>
		    <input type="hidden" id="jobCount" value="${jobCount }">
	</div>
	<div class="postRequire">
		<p>岗位要求</p>
		<p>
		  <%
								 CompanyJob companyJob=(CompanyJob)request.getAttribute("jobDetail");
								  String msg = companyJob.getJobRequire();
                                  String str = msg.replaceAll("\r\n","<br>");
                                  out.print(str); 
		   %>
			<%-- <textarea rows="6" cols="" readonly="readonly">${jobDetail.jobRequire }</textarea> --%>
		</p>
	</div>
	<div class="postDescription">
		<p>岗位描述</p>
		<p>
		  <%                    CompanyJob companyJob1=(CompanyJob)request.getAttribute("jobDetail");
								  String msg1 = companyJob1.getJobDescribe();
                                  String str1 = msg1.replaceAll("\r\n","<br>");
                                  out.print(str1) ;
                                  %>
			<%-- <textarea rows="6" cols="" readonly="readonly">${jobDetail.jobDescribe }</textarea> --%>
		</p>
	</div>
	<input type="hidden" id="collectId" value="">
	<!--固定底栏-->
	<div>
		<div class="footer">
			<div class="apply">
				<input type="button" id="btn1" onclick="delivery('${jobDetail.jobId}')" value="立即投递">
			</div>
			<div class="share">
				<button id="nativeShare" onclick="sendMessage()" data-toggle="modal" data-target="#myModal"></button>
			</div>
			<div class="star">
				<button id="btn3" class="gray"></button>
			</div>
		</div>
	</div>
	<!--提示消息-->
	<div class="box" id="box">
		<div class="message">
			<span id="mess"></span>
		</div>
	</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
	 aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<img src="img/arrow.png" class="arrow">
		<div class="mainContent">
			1.点击右上角<img class="friend" src="img/List.png"><br>
			2.点击<img class="friend" src="img/friendShare.png"><img class="friend" src="img/friendCircle.png">按钮<br>
			<p>即可分享内容</p>
		</div>
	</div>
</div>

    
	<script src="js/jquery-2.2.3.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
 <!-- 微信分享start -->
  <%--  <script type="text/javascript">

//微信信息的以及调用的配置
$(function(){
wx.config({
   debug: true, 
   appId: '<%=ret.get("appId") %>', 
   timestamp: '<%=ret.get("timestamp") %>' , 
   nonceStr: '<%=ret.get("nonceStr") %>', 
   signature: '<%=ret.get("signature") %>',
   jsApiList: [ 
             'checkJsApi',
       	     'onMenuShareTimeline',
    	     'onMenuShareAppMessage',
    	     'onMenuShareQQ',
    	     'onMenuShareWeibo',
    	     ] 
});
wx.ready(function(){
/* 	document.querySelector('#nativeShare').onclick = function () {
	    wx.checkJsApi({
	      jsApiList: [
	        'getNetworkType',
	        'previewImage'
	      ],
	      success: function (res) {
	        alert(JSON.stringify(res));
	      }
	    });
	}
	 */
	/*  document.querySelector('#nativeShare').onclick = function () {
		    wx.onMenuShareTimeline({
		      title: '互联网之子',
		      link: 'http://movie.douban.com/subject/25785114/',
		      imgUrl: 'http://img3.douban.com/view/movie_poster_cover/spst/public/p2166127561.jpg',
		      trigger: function (res) {
		        alert('用户点击分享到朋友圈');
		      },
		      success: function (res) {
		        alert('已分享');
		      },
		      cancel: function (res) {
		        alert('已取消');
		      },
		      fail: function (res) {
		        alert(JSON.stringify(res));
		      }
		    });
		    alert('已注册获取“分享到朋友圈”状态事件');
		  }; */


});
wx.error(function(res){
	alert("wx.config 失败");
});
});
function sendMessage(){

	WeixinJSBridge.on('menu:share:timeline', function(argv){

	WeixinJSBridge.invoke('shareTimeline',{

	"appid":"", //appid 设置空就好了。
	"img_url": imgUrl, //分享时所带的图片路径
	"img_width": "120", //图片宽度
	"img_height": "120", //图片高度
	"link":url, //分享附带链接地址
	"desc":"我是一个介绍", //分享内容介绍
	"title":"标题，再简单不过了。"
	}, function(res){/*** 回调函数，最好设置为空 ***/});

	});
	} 



  </script> 
 <!-- 微信分享end  --> --%>
	
	 
	<script type="text/javascript">
	
	function otherJob(companyId,jobId){
		var count=$("#jobCount").val();
		if(count>1){
			location="job/companyOtherJob.do?companyId="+companyId+"&jobId="+jobId;
		}else{
			alert("该公司尚未发布其他岗位");
		}
	}
	
	
	$(function() {
	
	    if(${userCollectId}==0){
	       $("#btn3").attr("class", "gray");
	    }else{
	       $("#btn3").attr("class", "red");
	       $("#collectId").val(${userCollectId});
	    }
	   
	   if(${isDelivery}==1){
	      $("#btn1").val("已投递");
	      $("#btn1").attr("disabled","disabled");
	      $("#btn1").css("background-color","#aaa");
	      $("#btn1").css("letter-spacing","12px");
	   }
	   
	   
	   $('#btn2').click(function(){
			$('.modal-backdrop').stop(true,true);
			$('#myModal').modal({
				keyboard: false,
				show:false
			})
		});
	   
	});
	
	
	
		function delivery(jobId) {
			if (${sessionScope.personId==null}) {
				alert("如需投递，请先登录");
				location = "xrjobPages/login.jsp";
			} else {
				if(confirm("您确定要投递该岗位吗")){
				$.post("delivery/deliveryJob.do", {
					jobId : jobId
				}, function(data) {
					if (data.res) {
						alert(data.msg);
                        location.reload();
					} else {
						alert(data.msg);
					}
				}, "JSON");
			 }
			}
		}

		function report(){
			if (${sessionScope.personId==null}) {
				alert("如需举报，请先登录");
				location = "xrjobPages/login.jsp";
			} else {
				location="xrjobPages/report.jsp?companyId=${jobDetail.companyId }&&jobId=${jobDetail.jobId }";
			}
		}
		$(function() {
			$("#btn3").click(function() {

				var jobId = ${jobDetail.jobId};

				if ($("#btn3").attr("class") == "gray") {
					if (${sessionScope.personId==null}) {
						alert("请先登录");
						location = "xrjobPages/login.jsp";
					}else{
						$.post("job/jobCollect.do", {
							jobId : jobId
						}, function(data) {
							if (data.res) {
								$("#btn3").attr("class", "red");
								$('#box').fadeIn(1000);
								$("#mess").html("收藏成功");
								setTimeout(function() {
									$('#box').fadeOut(1000);
								}, 2000);
								
								$("#collectId").val(data.msg);

							} else {
								alert(data.msg);
							}
						}, "JSON");
					}
						
			
				} else {
				   var collectId=$("#collectId").val();
				
				   	$.post("job/deleteJobCollect.do", {
						collectId : collectId
					}, function(data) {
						if (data.res) {
							$("#btn3").attr("class", "gray");
					        $('#box').fadeIn(1000);
					        $("#mess").html("取消收藏成功");
					        setTimeout(function() {
					        	$('#box').fadeOut(1000);
					       }, 2000);
							
						} else {
							alert(data.msg);
						}
					}, "JSON");
		
				}
			})
		});
	</script>
</body>
</html>