<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 


<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
     <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">  
	<meta charset="utf-8" />
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone-no">
	<title>该公司其他岗位</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	body{
		padding:0;
		margin:0;
		font-size:14px;
		color:#666;
		background-color:#f5f5f5;
	}
	ul{
		margin-bottom:0;
		padding-left:0;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		color:#666;
		text-decoration:none;
	}

	/*岗位列表*/
	.jobContent{
		float:left;
		background-color:#fff;
		width:100%;
		height:80px;
		padding:15px 10px;
	}
	.jobList{
		float:left;
		white-space:nowrap;
		overflow:hidden;
		text-overflow:ellipsis;
	}

	.job{
		font-weight:700;
		font-size:15px;
		white-space:nowrap;
		overflow:hidden;
		text-overflow:ellipsis;
	}
	.time{
		font-size:13px;
		color:#777;
		padding-top:10px;
		overflow:visible;
	}
	.rightArrow{
		float:right;
		line-height:50px;
		padding-left:10px;
		width:147px;
		text-align:left;
	}
	.salary{
		color:#ff9137;
		font-size:15px;

	}
	.arrow-right img{
		width:9px;
		height:20px;
		margin-bottom:3px;
		margin-top:15px;
		float:right;
	}
	hr{
		border:1px solid #f5f5f5;
		margin:0;
	}

</style>
</head>
<body>
	<!--循环start-->
	<c:if test="${jobs.size()!=0 }">
	<c:forEach items="${jobs }" var="jobs" varStatus="status">
	<a href="job/jobDetail.do?jobId=${jobs.jobId}">
		<div class="jobContent">
			<ul class="jobList">
				<li class="job">
					${jobs.jobName}
				</li>
				<li class="time">
					<span><fmt:formatDate
						value="${jobs.createTime}" pattern="yyyy-MM-dd" /></span>&nbsp;&nbsp;
					<span>人数：${jobs.jobNumber }人</span>
				</li>
			</ul>
			<div class="rightArrow">
				<span class="salary"><c:if
					test="${jobs.jobSalaryId == 1}">1500元及以下 /月</c:if><c:if
					test="${jobs.jobSalaryId == 2}">1500-2500元/月</c:if><c:if
					test="${jobs.jobSalaryId == 3}">2500-3500元/月</c:if><c:if
					test="${jobs.jobSalaryId == 4}">3500-5000元/月</c:if><c:if
					test="${jobs.jobSalaryId == 5}">5000-7000元/月</c:if><c:if
					test="${jobs.jobSalaryId == 6}">7000-10000元/月</c:if><c:if
					test="${jobs.jobSalaryId == 7}">10000-15000元/月</c:if><c:if
					test="${jobs.jobSalaryId == 8}">15000元以上/月</c:if><c:if
					test="${jobs.jobSalaryId == 0}">薪资面议</c:if></span>
				<span class="arrow-right">
					<img src="img/arrow-right.png">
				</span>
			</div>
		</div>
	</a>
	<hr>
	</c:forEach>
	</c:if>
	<!--循环end-->
  <c:if test="${jobs.size()==0 }">
   该公司尚未发布其他岗位
  </c:if>

<!--javascript-->
<script src="js/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script>
	$(function(){
		var x=$(".rightArrow").outerWidth(true);
		var y=$(".jobContent").width();
		var z=y-x;
		$(".jobList").css("width",z);

	});
</script>
</body>	
</html>