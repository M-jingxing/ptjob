<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 


<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
	<meta charset="utf-8" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta name="format-detection" content="telephone=no">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>我的收藏</title>
<!--CSS-->
<style>
	*{
		margin:0;
		padding:0;
	}
	body{
		font-size:14px;
		background-color:#f5f5f5;
		overflow-y:scroll;
		color:#666;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration: none;
	}
/*最上面的黄框*/
/* 	nav{
		width:auto;
		height:30px;
		font-size:12px;
		color:#fff;
		background-color:#ff9137;
		text-align:center;
		line-height:30px;
	}  */

	/*收藏记录*/
	.jobInfoList{
		background-color:#fff;
		padding:0;
	}
	.list-block .line-normal-left-wrapper{	/*设置上边框的，那条小灰线*/
		border-top:1px solid #f5f5f5;
	}
	.list-block  .line-normal-left-wrapper:first-child{
		border-top:0;
	}
	.list-block .item-title{
		font-size:16px;
		font-weight:700;
		line-height:30px;
		color:#666;
		padding-left:15px;
	}
	.list-block .salary{
		font-size:16px;
		line-height:25px;
		color:#ff9137;
		font-weight:700;
		padding-left:15px;
	}
	.list-block .cName{
		font-size:13px;
		color:#666;
		padding-right:15px;
		padding-left:15px;
		overflow:hidden;
		text-overflow:ellipsis;
	}
	.list-block .cLocation{
		font-size:13px;
		color:#666;
		padding-right:15px;
		padding-left:15px;
		overflow:hidden;
		text-overflow:ellipsis;
	}
	ul{
		margin-bottom:0;
	}
		/*滑动删除*/
	.line-wrapper {
		width: 100%;
		height: 100px;
		overflow: hidden;
		border-bottom: 1px solid #f5f5f5;
	}
	.line-scroll-wrapper {
		white-space: nowrap;
		height: 100px;
	}
	.line-btn-delete {
		float:right;
		width: 15%;
		height: 100px;
	}
	.line-btn-delete button {	/*删除按钮的设置*/
		width: 100%;
		height: 100%;
		background: #fff;
		border:none;
		font-size: 16px;
		color: #ff9137;
		outline:none;
		text-align:right;
		padding-right:15px;
	}
	.line-btn-delete button:visited.line-btn-delete button:hover.line-btn-delete button:active{
		color:#ff9137;
	}
	.line-normal-wrapper {
		float: left;
		padding-top:3px;
		width:85%;
		white-space:nowrap;
		overflow:hidden;
		text-overflow:ellipsis;
	}
	.starRecord{
		text-align:center;
		font-size:12px;
		padding-top:10px;
	}
/*导航栏*/
nav {
	color: #fff;
	background-color: #ff9037;
	padding: 6px;
	text-align:center;
}
nav a{
		color:#fff;
		font-size:16px;
	}

nav a:link, nav a:visited, nav a:hover, nav a:active {
	color: #fff;
	font-size: 16px;
}
</style>
</head>
<body>
<!--导航栏-->
<nav>
	我的收藏为您保管60日内的收藏信息
</nav>
<!--投递记录-->
<div class="jobInfoList">
	<div class="list-block media-list">
		<!--循环start-->
		<c:forEach varStatus="status" items="${collectList }" var="collectList">
		<div class="line-wrapper">
			<div class="line-scroll-wrapper">
				<div class="line-normal-wrapper">
					<div class="line-normal-left-wrapper">
						<a href="job/jobDetail.do?jobId=${collectList.jobId }">
							<ul>
								<li>
									<div class="item-inner">
										<div class="item-title-row">
											<div class="item-title">${collectList.jobName }</div>

										</div>
										<div class="item-title-row">
											<div class="item-text salary">薪资：<c:if
					test="${collectList.jobSalaryId == 1}">1500元及以下/月</c:if> <c:if
					test="${collectList.jobSalaryId == 2}">1500-2500元/月</c:if><c:if
					test="${collectList.jobSalaryId == 3}">2500-3500元/月</c:if><c:if
					test="${collectList.jobSalaryId == 4}">3500-5000元/月</c:if><c:if
					test="${collectList.jobSalaryId == 5}">5000-7000元/月</c:if><c:if
					test="${collectList.jobSalaryId == 6}">7000-10000元/月</c:if><c:if
					test="${collectList.jobSalaryId == 7}">10000-15000元/月</c:if><c:if
					test="${collectList.jobSalaryId == 8}">15000元以上/月</c:if><c:if
					test="${collectList.jobSalaryId == 0}">薪资面议</c:if></div>
										</div>
										<div class="item-title-row">
											<div class="item-text cName">公司：<c:choose><c:when test="${fn:contains(collectList.companyName,'xrjobOfficial')}">${fn:substring(collectList.companyName,0,fn:length(collectList.companyName)-13)}</c:when><c:otherwise >${collectList.companyName }</c:otherwise></c:choose></div>
										</div>
										<div class="item-title-row">
											<div class="item-text cLocation">地址：${collectList.jobAddress }</div>
										</div>
									</div>
								</li>
							</ul>
						</a>
					</div>
				</div>
				 <div class="line-btn-delete"><button class="btn" type="button" onclick="deleteCollect('${collectList.collectId}')">删除</button></div>
			</div>
		</div>
		</c:forEach>
		
		<!--循环end-->


	</div>
</div>


<!--javascript-->
<script src="js/jquery-2.2.3.min.js"></script>
 <script type="text/javascript">
     function deleteCollect(collectId){
    	 if(confirm("您确定要删除该条记录吗？")){
          $.post("job/deleteJobCollect.do", {
					collectId : collectId
				}, function(data) {
					if (data.res) {
						$(".btn").parent().parent().parent().remove();
		            	$(".btn").parent().fadeOut(300, function(){
				           $(".btn").parent().parent().remove();
				          
			        });
			         location.reload();
					} else {
					
					}
				}, "JSON");
    	 }
     }


	/* $(document).ready(function(e) {
		// 设定每一行的宽度=屏幕宽度+按钮宽度
		$(".line-scroll-wrapper").width($(".line-wrapper").width() + $(".line-btn-delete").width());
		// 设定常规信息区域宽度=屏幕宽度
		$(".line-normal-wrapper").width($(".line-wrapper").width());
		//设定文字部分宽度（为了实现文字过长时在末尾显示...）
		$(".item-title-row").width($(".line-normal-wrapper").width() );

		// 获取所有行，对每一行设置监听
		var lines = $(".line-normal-wrapper");
		var len = lines.length;
		var lastX, lastXForMobile;

		// 用于记录被按下的对象
		var pressedObj;  // 当前左滑的对象
		var lastLeftObj; // 上一个左滑的对象

		// 用于记录按下的点
		var start;

		// 网页在移动端运行时的监听
		for (var i = 0; i < len; ++i) {
			lines[i].addEventListener('touchstart', function(e){
				lastXForMobile = e.changedTouches[0].pageX;
				pressedObj = this; // 记录被按下的对象

				// 记录开始按下时的点
				var touches = event.touches[0];
				start = {
					x: touches.pageX, // 横坐标
					y: touches.pageY  // 纵坐标
				};
			});

			lines[i].addEventListener('touchmove',function(e){
				// 计算划动过程中x和y的变化量
				var touches = event.touches[0];
				delta = {
					x: touches.pageX - start.x,
					y: touches.pageY - start.y
				};

				// 横向位移大于纵向位移，阻止纵向滚动
				if (Math.abs(delta.x) > Math.abs(delta.y)) {
					event.preventDefault();
				}
			});

			lines[i].addEventListener('touchend', function(e){
				var diffX = e.changedTouches[0].pageX - lastXForMobile;
				if (diffX < -150) {
					$(pressedObj).animate({marginLeft:"-100px"}, 400); // 左滑 这个100px其实就是button的宽度
					lastLeftObj && lastLeftObj != pressedObj &&
					$(lastLeftObj).animate({marginLeft:"0"}, 400); // 已经左滑状态的按钮右滑
					lastLeftObj = pressedObj; // 记录上一个左滑的对象
				} else if (diffX > 150) {
					$(pressedObj).animate({marginLeft:"0"}, 400); // 右滑
					lastLeftObj = null; // 清空上一个左滑的对象
				}
			});
		}

		// 网页在PC浏览器中运行时的监听
		for (var i = 0; i < len; ++i) {
			$(lines[i]).bind('mousedown', function(e){
				lastX = e.clientX;
				pressedObj = this; // 记录被按下的对象
			});

			$(lines[i]).bind('mouseup', function(e){
				var diffX = e.clientX - lastX;
				if (diffX < -150) {
					$(pressedObj).animate({marginLeft:"-100px"}, 400); // 左滑
					lastLeftObj && lastLeftObj != pressedObj &&
					$(lastLeftObj).animate({marginLeft:"0"}, 400); // 已经左滑状态的按钮右滑
					lastLeftObj = pressedObj; // 记录上一个左滑的对象
				} else if (diffX > 150) {
					$(pressedObj).animate({marginLeft:"0"}, 400); // 右滑
					lastLeftObj = null; // 清空上一个左滑的对象
				}
			});
		} */

		 /*删除按钮删除当前div*/
		/* $(".btn").click(function () {
			$(this).parent().parent().parent().remove();
			$(this).parent().fadeOut(300, function(){
				$(this).parent().parent().remove();
			});
		}) */
	/* }); */

</script> 

	<script type="text/javascript">
String.prototype.queryString= function(name) {
    var reg=new RegExp("[\?\&]" + name+ "=([^\&]+)","i"),r = this.match(reg);
    return r!==null?unescape(r[1]):null;
};
    window.onload=function(){
        var last=location.href.queryString("_v");
       
        if(location.href.indexOf("?")==-1){
            location.href=location.href+"?_v="+(new Date().getTime());
        }else{
            var now=new Date().getTime();
            if(!last){
                location.href=location.href+"&_v="+(new Date().getTime());
            }else if(parseInt(last)<(now-1000)){
                location.href=location.href.replace("_v="+last,"_v="+(new Date().getTime()));
            }
        }
    };
</script>
</body>	
</html>