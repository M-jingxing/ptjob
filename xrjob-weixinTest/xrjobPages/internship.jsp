<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>

<base href="<%=basePath%>">
	<meta charset="utf-8" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta name="format-detection" content="telephone=no">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>找应届生</title>
<!--CSS-->
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	*{
		margin:0;
		padding:0;
	}
	body{
		font-size:14px;
		background-color:#f5f5f5;
		overflow-y:scroll;
		color:#666;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration: none;
	}
	/*最上面的黄框*/
	nav{
		width:auto;
		height:30px;
		font-size:12px;
		color:#fff;
		background-color:#ff9137;
		text-align:center;
		line-height:30px;
	}

	/*实习*/
	.list-block{
		background-color:#fff;
		padding:10px;
		margin-top:1px;
	}
	.list-block .line-normal-left-wrapper{	/*设置上边框的，那条小灰线*/
		border-top:1px solid #f5f5f5;
	}
	.list-block  .line-normal-left-wrapper:first-child{
		border-top:0;
	}
	.list-block .item-title{
		font-size:16px;
		font-weight:700;
		line-height:30px;
		color:#666;
	}
	.list-block .item-after{
		font-size:12px;
		font-weight:400;
		color:#999;
	}
	.list-block a .salary{
		font-size:16px;
		line-height:25px;
		color:#ff9137;
		font-weight:700;
	}
	.list-block .cName{
		font-size:13px;
		color:#666;
		padding-right:15px;
		overflow:hidden;
		text-overflow:ellipsis;
	}
	.list-block .cLocation{
		font-size:13px;
		color:#666;
		padding-right:15px;
		overflow:hidden;
		text-overflow:ellipsis;
	}
	ul{
		margin-bottom:0;
	}
</style>
</head>
<body>
<!--内容-->
	<div class="content">
		<!--职位信息-->
		<c:forEach items="${jobs}" varStatus="status" var="jobs">
		<div class="list-block media-list">
			<ul>
				<li>
					<a href="job/jobDetail.do?jobId=${jobs.jobId}">
						<div class="item-inner">
							<div class="item-title-row">
								<div class="item-title">${jobs.jobName}
									<span class="item-after pull-right"><fmt:formatDate
														value="${jobs.createTime }"
														pattern="yyyy-MM-dd HH:mm:ss" /></span>
								</div>
							</div>
							<div class="item-title-row">
								<div class="item-subtitle salary">薪资：<c:if
					test="${jobs.jobSalaryId == 1}">1500元及以下/月</c:if> <c:if
					test="${jobs.jobSalaryId == 2}">1500-2500元/月</c:if> <c:if
					test="${jobs.jobSalaryId == 3}">2500-3500元/月</c:if> <c:if
					test="${jobs.jobSalaryId == 4}">3500-5000元/月</c:if> <c:if
					test="${jobs.jobSalaryId == 5}">5000-7000元/月</c:if> <c:if
					test="${jobs.jobSalaryId == 6}">7000-10000元/月</c:if> <c:if
					test="${jobs.jobSalaryId == 7}">10000-15000元/月</c:if> <c:if
					test="${jobs.jobSalaryId == 8}">15000元以上/月</c:if> <c:if
					test="${jobs.jobSalaryId == 0}">薪资面议</c:if></div>
								
							</div>
							<div class="item-title-row">
								<div class="item-text cName">公司：<c:choose><c:when test="${fn:contains(jobs.companyName,'xrjobOfficial')}">${fn:substring(jobs.companyName,0,fn:length(jobs.companyName)-13)}</c:when><c:otherwise >${jobs.companyName }</c:otherwise></c:choose></div>
							</div>
							<div class="item-title-row">
								<div class="item-text cLocation">地址：${jobs.jobAddress}</div>
							</div>						
						</div>
					</a>
				</li>
			</ul>
		</div>
		</c:forEach>

<!--javascript-->
<script src="js/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
</body>	
</html>