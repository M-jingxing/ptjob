<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
	<title>忘记密码</title>
	<base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta charset="utf-8" />
	<meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	body{
		background-color:#f5f5f5;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	:-moz-placeholder{
		color:#999; /*mozilla ff 4-18*/
		font-size:15px;
	}
	::-moz-placeholder{
		color:#999; /*mozilla ff 19+*/
		font-size:15px;
	}
	:-ms-input-placeholder{
		color:#999; /*ie 10+*/
		font-size:15px;
	}
	::-webkit-input-placeholder{
		color:#999; /*webkit browsers*/
		font-size:15px;
	}
	input[type=text],select,input[type=password]{
		border:none;
		outline:none;
		background-color:#f5f5f5;
		color:#999;
		appearance: none;
		-moz-appearance:none;
		-webkit-appearance: none;
		padding-left:1px;
		text-align:left;
		font-size:15px;
		height:26px;
	}
	.userName input[type=text],.psd input[type=text],.confirmPsd input[type=text]{
		width:100%;
	}
	.userName input[type=password],.psd input[type=password],.confirmPsd input[type=password]{
		width:100%;
	}
	select::-ms-expand{
		display:none;
	}
	.loginContent{
		margin-left:50px;
		margin-right:50px;
		margin-top:50px;
	}
	.userName,.psd,.confirmPsd{
		border-bottom:1px solid #999;
		padding-left:28px;
		margin-bottom:18px;
	}
	.userName{
		background:url(img/user.png) no-repeat;
		padding-bottom:5px;
	}
	.psd{
		background:url(img/password.png) no-repeat;
		padding-bottom:5px;
	}
	.confirmPsd{
		background:url(img/password.png) no-repeat;
		padding-bottom:5px;
	}
	#checkCode{
		width:45%;
		margin-right:8%;
		border-bottom:1px solid #999;
		text-align:center;
		padding:0;
	}
	.btn1{
		background-color:#f5f5f5;
		border:none;
		border-bottom:1px solid #ff9037;
		color:#ff9037;
		outline:none;
		width:45%;
		padding-left:0;
		padding-right:0;
		padding-bottom:2px;
	}
	.btn2{
		width:100%;
		padding-top:10px;
		padding-bottom:10px;
		border:none;
		background-color:#ff9137;
		color:#fff;
		border-radius: 6px;
		outline:none;
		margin-bottom:16px;
		margin-top:70px;
	}
	.error{
		font-size:12px;
		color:#ff9037;
		padding-left:5px;
	}
</style>
</head>
<body>
<div class="loginContent">
	<form id="frmInc" autocomplete="off">
		<div class="userName">
			<input type="text" name="loginName" id="loginName" placeholder="请输入手机号">
		</div>
		<div class="psd">
			<input type="password" name="password" id="password" placeholder="请输入新密码">
		</div>
		<div class="confirmPsd">
			<input type="password" name="surePassword" id="surePassword" placeholder="请确认新密码">
		</div>
		<div class="vCode">
			<input type="text" name="checkCode" id="checkCode"  placeholder="请输入验证码">
			<input type="button" class="btn1" id="btnSendCode" name="btnSendCode" onclick="sendMessage()" value="免费获取验证码"> 
		</div>
		<div>
			<button class="btn2" type="button" onclick="changePwd()">确认</button>
		</div>
		<input type="hidden" id="isChangePwd" name="isChangePwd" value="1">
	</form>
</div>
<script src="js/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script>
    function changePwd(){
    var loginName=$("#loginName").val();
   var password=$("#password").val();
   var surePassword=$("#surePassword").val();
   var checkCode=$("#checkCode").val();
        
        	$.post("person/changePwd.do", {
					loginName:loginName,
					password:password,
					surePassword:surePassword,
					checkCode:checkCode
				}, function(
					data) {
				if (data.res) {
					alert(data.msg);
					location = "xrjobPages/login.jsp";
				} else {
					alert(data.msg);
				}

			}, "json");
      
    }


	/*表单验证
	* 手机号不能为空，正确手机号
	* 密码不能为空，在6位数以上
	* 确认密码不能为空，和密码equal
	* 验证码不能为空
	* */
	$(function(){
		$("#frmInc").validate({
			rules:{
				loginName:{
					required:true,
					mobile:true
				},
				password:{
					required:true,
					minlength:6
				},
				surePassword:{
					required:true,
					minlength:6,
					equalTo:"#password"
				},
				checkCode:{
					required:true
				}
			},
			messages:{
				loginName:{
					required:"请输入手机号",
					mobile:"请输入正确的手机号"
				},
				password:{
					required:"请输入密码",
					minlength:"输入长度在6位以上"
				},
				surePassword:{
					required:"请输入确认密码",
					minlength:"输入长度在6位以上",
					equalTo:"请与密码保持一致"
				},
				checkCode:{
					required:"请输入验证码"
				}
			},
			/*修改错误信息的提示位置*/
			errorPlacement: function(error, element) {
				if ( element.is(":radio") )
					error.appendTo ( element.parent() );
				else if ( element.is(":checkbox") )
					error.appendTo ( element.parent() );
				else if ( element.is("input[name=vCode]") )
					error.appendTo ( element.parent() );
				else
					error.insertAfter(element);
				$('<br/>').appendTo(element.parent());
				error.appendTo(element.parent());
			}
		});
	});

  var InterValObj; //timer变量，控制时间  
var count = 120; //间隔函数，1秒执行  
var curCount;//当前剩余秒数  
var code = ""; //验证码  
var codeLength = 6;//验证码长度  

function sendMessage(){
	curCount = count;
	 var jbPhone = $("#loginName").val();
	 var isChangePwd=$("#isChangePwd").val();
	 	 if (jbPhone != "") { 
		 // 产生验证码  
         //for ( var i = 0; i < codeLength; i++) {  
         //    code += parseInt(Math.random() * 9).toString();  
         //} 
      	// 设置button效果，开始计时  
        $("#btnSendCode").attr("disabled","true"); 
        $("#btnSendCode").val("请在" + curCount + "秒内输入验证码");  
        InterValObj = window.setInterval(SetRemainTime, 1000); // 启动计时器，1秒执行一次   // 向后台发送处理数据  
        $.post("person/UserActionsms.do",{
        	jbPhone:jbPhone,
        	code:code,
        	isChangePwd:isChangePwd
		},function(data) {
			if(data.res){//发送成功
				alert(data.msg); 
			}
			else{
                alert(data.msg); 
			}
		}, 'JSON');
	 }else{
		 alert("手机号码不能为空");
	 }
}

function SetRemainTime() {  
    if (curCount == 0) {                  
        window.clearInterval(InterValObj);// 停止计时器  
        $("#btnSendCode").removeAttr("disabled");// 启用按钮  
        $("#btnSendCode").val("重新发送验证码");  
        code = ""; // 清除验证码。如果不清除，过时间后，输入收到的验证码依然有效  
    }else {  
        curCount--;  
        $("#btnSendCode").val("请在" + curCount + "秒内输入验证码");  
    }
}
function changeCode(){
   $("#code").attr("src","Code/checkCode.do?"+Math.random())
}

</script>

</body>	
</html>