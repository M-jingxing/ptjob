<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
	<meta charset="utf-8" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta name="format-detection" content="telephone=no">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>附近热门</title>
<!--CSS-->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	*{
		margin:0;
		padding:0;
	}
	body{
		font-size:14px;
		background-color:#f5f5f5;
		overflow-y:scroll;
		color:#666;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration: none;
	}
	/*导航栏*/
	.search{
		background-color:#ff9037;
		padding:8px 0 10px 10px;
		width:auto;
		height:46px;
	}
	input[type=search]{
		border:none;
		outline:none;
		color:#999;
		padding-left:5px;
		text-align:left;
		width:75%;
		padding-top:5px;
		padding-bottom:5px;
		float:left;
		background-size:24px 24px;
		border-radius:6px 0 0 6px;
	}
	input[type=button]{
		border:none;
		outline:none;
		color:#999;
		padding-left:25px;
		text-align:left;
		width:5%;
		padding-top:5px;
		padding-bottom:5px;
		float:left;
		background:#fff url(img/search.jpg) no-repeat 0 3px;
		background-size:24px 24px;
		border-radius:0 6px 6px 0;
	}
	
	/*附近热门*/
	.jobInfoList{
		background-color:#fff;
		padding:0;
	}
	.list-block  .notUse1{	/*设置上边框的，那条小灰线*/
		border-top:2px solid #f5f5f5;
	}
	.list-block  .notUse1:first-child{
		border-top:0;
	}
	.list-block .item-title{
		font-size:16px;
		font-weight:700;
		line-height:30px;
		color:#666;
	}
	.list-block .item-inner{
		padding:8px 10px 10px 10px;
	}
	.list-block .salary{
		font-size:16px;
		line-height:25px;
		color:#ff9137;
		font-weight:700;
	}
	.list-block .cName{
		font-size:13px;
		color:#666;
		overflow:hidden;
		text-overflow:ellipsis;
	}
	.list-block .cLocation{
		font-size:13px;
		color:#666;
		overflow:hidden;
		text-overflow:ellipsis;
	}
	ul{
		margin-bottom:0;
	}
nav{
		color:#fff;
		background-color:#ff9037;
		padding:6px;
	}
	nav a:link,nav a:visited,nav a:hover,nav a:active{
		color:#fff;
		font-size:16px;
	}

</style>
</head>
<body>
<!--导航栏-->
<nav>
	<a href="distance/toJobSearchPage.do"><span class="glyphicon glyphicon-chevron-left"></span>
		返回</a>
	<a href="index.jsp"><span class="glyphicon glyphicon-home pull-right"></span></a>
</nav>
<div class="jobInfoList">
	<div class="list-block media-list">
		<!--循环start-->
		 <c:if test="${jobs.size()!=0}">
		  <c:forEach var="jobs" items="${jobs}" varStatus="status" >
		<div class="notUse1">
			<a href="job/jobDetail.do?jobId=${jobs.jobId}">
				<ul>
					<li>
						<div class="item-inner">
							<div class="item-title-row">
								<div class="item-title">${jobs.jobName}</div>

							</div>
							<div class="item-title-row">
								<div class="item-text salary">薪资：<c:if
					test="${jobs.jobSalaryId == 1}">1500元及以下/月</c:if> <c:if
					test="${jobs.jobSalaryId == 2}">1500-2500元/月</c:if> <c:if
					test="${jobs.jobSalaryId == 3}">2500-3500元/月</c:if> <c:if
					test="${jobs.jobSalaryId == 4}">3500-5000元/月</c:if> <c:if
					test="${jobs.jobSalaryId == 5}">5000-7000元/月</c:if> <c:if
					test="${jobs.jobSalaryId == 6}">7000-10000元/月</c:if> <c:if
					test="${jobs.jobSalaryId == 7}">10000-15000元/月</c:if> <c:if
					test="${jobs.jobSalaryId == 8}">15000元以上/月</c:if> <c:if
					test="${jobs.jobSalaryId == 0}">薪资面议</c:if></div>
							</div>
							<div class="item-title-row">
								<div class="item-text cName">距离：${jobs.distance}km</div>
							</div>
							<div class="item-title-row">
								<div class="item-text cName">公司：<c:choose><c:when test="${fn:contains(jobs.companyName,'xrjobOfficial')}">${fn:substring(jobs.companyName,0,fn:length(jobs.companyName)-13)}</c:when><c:otherwise >${jobs.companyName }</c:otherwise></c:choose></div>
							</div>
							<div class="item-title-row">
								<div class="item-text cLocation">地址：${jobs.jobAddress}</div>
							</div>
						</div>
					</li>
				</ul>
			</a>
		</div>
	</c:forEach>
	</c:if>
		<!--循环end-->

	

	</div>
</div>
  <form action="distance/distanceJob.do" method="post" id="form1" name="form1">
	  <input type="hidden" name="lng" id="lng"> 
      <input type="hidden" name="lat" id="lat"> 
      </form>
<!--信息提示-->
<div class="box" id="box">
	<div class="message">
		<span id="mess"></span>
	</div>
</div>


<!--javascript-->
<script src="js/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
/*  function keySearch(){
      var key = $("#key").val();
      location="distance/jobSearch.do?key="+key;
    } */
 
    
 $(document).ready(function()
  {
    if(<%=request.getAttribute("isRead") %>==null){
      if (navigator.geolocation)
    {
    navigator.geolocation.getCurrentPosition(showPosition);
    }
   }  
  });
 function showPosition(position)
  {
    
    document.getElementById("lat").value=position.coords.latitude;
    document.getElementById("lng").value=position.coords.longitude;
    var form = window.document.getElementById("form1");
    form.submit();
 
  }


</script>
</body>	
</html>