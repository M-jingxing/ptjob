<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>招聘会</title>
	<!--CSS-->
	<!--<link rel="stylesheet" media="screen" href="bootstrap/css/bootstrap.min.css">-->
<style>
	body{
		margin:0;
		padding:10px;
		font-size:14px;
		color:#666;
	}
	h2{
		text-align:center;
	}
	table{
		border-collapse:collapse;
		table-layout:fixed;
		width:100%;
	}
	th,td{
		border:1px solid #ff9137;
		text-align:center;
		vertical-align:middle;
		line-height:20px;
	}
	th:first-child,td:first-child{
		width:40%;
	}
	th:last-child,td:last-child{
		width:60%;
	}

</style>
</head>
<body>
	<h2>招聘会</h2>
	<table cellpadding="8px" cellspacing="0">
		<tr>
			<th>时间</th>
			<th>招聘会</th>
		</tr>
		<c:if test="${jobFair!=null }">
		 <c:forEach var="jobFair" items="${jobFair}" varStatus="status">
		<tr>
			<td>${jobFair.fairTime}<br>
				</td>
			<td>${jobFair.fairName}</td>
		</tr>
		</c:forEach>
		</c:if>
		<tr>
			<td colspan="2">
				联系电话：82624787<br>
				地址：杭州市萧山区蜀山街道沈家里路199号
			</td>
		</tr>
	</table>


<!--javascript-->
<script src="js/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
</body>	
</html>