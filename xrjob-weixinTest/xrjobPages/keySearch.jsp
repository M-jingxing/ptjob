<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<base href="<%=basePath%>">
	<meta charset="utf-8" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta name="format-detection" content="telephone=no">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>搜索职位</title>
<!--CSS-->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<style>
	*{
	margin:0;
	padding:0;
	}
	body{
	font-size:14px;
	background-color:#f5f5f5;
	color:#666;
	overflow-y:scroll;
	}
	li{
	list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
	text-decoration: none;
	}
	/*导航栏*/
	nav{
	color:#fff;
	background-color:#ff9037;
	padding:6px;
	}
	nav a:link,nav a:visited,nav a:hover,nav a:active{
	color:#fff;
	font-size:16px;
	}
	/*搜索的那些框框*/
	.form-horizontal .form-group{
	margin:0;
	background-color:#fff;
	padding-top:15px;
	padding-bottom:20px;
	}
	.form-control{	/*这里就是改变select的样式*/
	border:none;
	border-bottom:1px solid #ff9137;
	color:#888;
	appearance: none;
	-moz-appearance:none;
	-webkit-appearance: none;
	padding-left:3px;
	text-align:left;
	background:#fff url(img/myResume/arrow.png) no-repeat right;
	box-shadow:none;
	border-radius:0;
	outline:none;
	}
	select::-ms-expand{
	display:none;
	}
	.job{
	padding-left:15px;
	padding-right:8px;
	}
	.distance{
	padding-left:10px;
	padding-right:8px;
	}
	.salaryAll{
	padding-left:10px;
	padding-right:15px;
	}
	.form-group .btn{
	background-color:#ff9137;
	margin-top:15px;
	color:#fff;
	border:none;
	outline:none;
	width:100px;
	height:30px;
	}

	/*职位信息*/
	.jobInfoList{
	margin-top:12px;
	background-color:#fff;
	padding:15px;
	}
	.list-block ul{
	border-top:1px solid #f5f5f5;
	}
	.list-block ul:first-child{
	border-top:0;
	}
	#content .item-inner{
		border-bottom:1px solid rgb(238,238,238);
		padding-bottom:10px;
	}

	.list-block .item-title{
	font-size:16px;
	font-weight:600;
	line-height:30px;
	color:#666;
	}
	.list-block .salary{
	font-size:16px;
	line-height:25px;
	color:#ff9137;
	}
	.list-block .cName{
	font-size:13px;
	color:#666;
	}
	.list-block .cLocation{
	font-size:13px;
	color:#666;
	}
	.linethrough{
		text-align:center;
		margin-bottom:0;
		padding-top:5px;
		padding-bottom:5px;
		color:#f00;
	}
	hr{
		margin:0;
	}
	</style>
</head>
<body>
<!--导航栏-->
<nav>
	<a href="javascript:history.go(-1);"><span class="glyphicon glyphicon-chevron-left"></span>
		返回</a>
	<a href="index.jsp"><span class="glyphicon glyphicon-home pull-right"></span></a>
</nav>

	<!--职位信息-->
	<div class="jobInfoList">
	<div class="list-block media-list">
	 <c:choose>
			<c:when test="${jobs.size()!=0}"> 
		 <c:forEach var="jobs" items="${jobs}" varStatus="status">
		<a href="job/jobDetail.do?jobId=${jobs.jobId}">
		<ul>
			<li>
				<div class="item-inner">
					<div class="item-title-row">
						<div class="item-title">${jobs.jobName}</div>
						
					</div>
					<div class="item-title-row">
						<div class="item-text salary">薪资：<c:if
					test="${jobs.jobSalaryId == 1}">1500元及以下/月</c:if> <c:if
					test="${jobs.jobSalaryId == 2}">1500-2500元/月</c:if> <c:if
					test="${jobs.jobSalaryId == 3}">2500-3500元/月</c:if> <c:if
					test="${jobs.jobSalaryId == 4}">3500-5000元/月</c:if> <c:if
					test="${jobs.jobSalaryId == 5}">5000-7000元/月</c:if> <c:if
					test="${jobs.jobSalaryId == 6}">7000-10000元/月</c:if> <c:if
					test="${jobs.jobSalaryId == 7}">10000-15000元/月</c:if> <c:if
					test="${jobs.jobSalaryId == 8}">15000元以上/月</c:if> <c:if
					test="${jobs.jobSalaryId == 0}">薪资面议</c:if></div>
					</div>
					<div class="item-title-row">
						<div class="item-text cName">公司名称：<c:choose><c:when test="${fn:contains(jobs.companyName,'xrjobOfficial')}">${fn:substring(jobs.companyName,0,fn:length(jobs.companyName)-13)}</c:when><c:otherwise >${jobs.companyName }</c:otherwise></c:choose></div>
					</div>
					<div class="item-title-row">
						<div class="item-text cLocation">地址：${jobs.jobAddress}</div>
					</div>						
				</div>
				
			</li>
		</ul>
		</a>
		<hr>
		</c:forEach>
		</c:when>
		<c:otherwise>
			  <b>没有查询到记录，请更改搜索条件</b>
			</c:otherwise>
		</c:choose> 
		 <p class="linethrough">以下为最新发布的岗位</p>
		
		<ul id="content">
		<c:forEach varStatus="status" items="${newJobList }" var="newJobList">
		  <a href="job/jobDetail.do?jobId=${newJobList.jobId}">
			<li>
				<div class="item-inner">
					<div class="item-title-row">
						<div class="item-title">${newJobList.jobName}</div>
						
					</div>
					<div class="item-title-row">
						<div class="item-text salary">薪资：<c:if
					test="${newJobList.jobSalaryId == 1}">1500元及以下/月</c:if> <c:if
					test="${newJobList.jobSalaryId == 2}">1500-2500元/月</c:if> <c:if
					test="${newJobList.jobSalaryId == 3}">2500-3500元/月</c:if> <c:if
					test="${newJobList.jobSalaryId == 4}">3500-5000元/月</c:if> <c:if
					test="${newJobList.jobSalaryId == 5}">5000-7000元/月</c:if> <c:if
					test="${newJobList.jobSalaryId == 6}">7000-10000元/月</c:if> <c:if
					test="${newJobList.jobSalaryId == 7}">10000-15000元/月</c:if> <c:if
					test="${newJobList.jobSalaryId == 8}">15000元以上/月</c:if> <c:if
					test="${newJobList.jobSalaryId == 0}">薪资面议</c:if></div>
					</div>
					<div class="item-title-row">
						<div class="item-text cName">公司名称：<c:choose><c:when test="${fn:contains(newJobList.companyName,'xrjobOfficial')}">${fn:substring(newJobList.companyName,0,fn:length(newJobList.companyName)-13)}</c:when><c:otherwise >${newJobList.companyName }</c:otherwise></c:choose></div>
					</div>
					<div class="item-title-row">
						<div class="item-text cLocation">地址：${newJobList.jobAddress}</div>
					</div>						
				</div>
				
			</li>
			</a>
			</c:forEach>
		</ul>
		
		<hr>
		
	</div>
	
</div>
<input type="hidden" id="page" value="">

<!--javascript-->
<script src="js/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script type='text/javascript' src='SUIMobile/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
<script type='text/javascript' src='SUIMobile/msui/sm/0.6.2/js/sm.min.js' charset='utf-8'></script>

<script type="text/javascript">
 $(document).ready(function() {
	
    $(window).scroll(function() {
     // $(document).scrollTop()获取垂直滚动的距离
     // $(document).scrollLeft() 这是获取水平滚动条的距离

  
        if ($(document).scrollTop() > $(document).height() - $(window).height()) {      	
        	setInterval( getList(),2000);

        }
     
    });
});

 function getList(){
     var html="";
     
	$.post("job/getNewList.do",{page:$("#page").val()},function(data){
		$("#page").val(data.numPage);
		 $("#content").append(function(){
			 $.each(data.list,function(k,v){
				 var companyName=v.companyName;
     			 if(companyName.indexOf("xrjobOfficial") >=0){
     				companyName=companyName.substring(0,companyName.length-13);
     			  }   
		  		   if(v.jobSalaryId==1){
		     	   var  text=("<a href='job/jobDetail.do?jobId="+v.jobId+"'><li><div class='item-inner'><div class='item-title-row'><div class='item-title'>"+v.jobName+"</div></div></div><div class='item-title-row'><div class='item-text salary'>薪资：1500元及以下/月</div></div><div class='item-title-row'><div class='item-text cName'>公司名称："+companyName+"</div></div><div class='item-title-row'><div class='item-text cLocation'>地址："+v.jobAddress+"</div></div></div></li></a>");
		  		   }
		  		 if(v.jobSalaryId==2){
		  			var text=("<a href='job/jobDetail.do?jobId="+v.jobId+"'><li><div class='item-inner'><div class='item-title-row'><div class='item-title'>"+v.jobName+"</div></div></div><div class='item-title-row'><div class='item-text salary'>薪资：1500-2500元/月</div></div><div class='item-title-row'><div class='item-text cName'>公司名称："+companyName+"</div></div><div class='item-title-row'><div class='item-text cLocation'>地址："+v.jobAddress+"</div></div></div></li></a>");
		    		   }
		  		 if(v.jobSalaryId==3){
		  			var text=("<a href='job/jobDetail.do?jobId="+v.jobId+"'><li><div class='item-inner'><div class='item-title-row'><div class='item-title'>"+v.jobName+"</div></div></div><div class='item-title-row'><div class='item-text salary'>薪资：2500-3500元/月</div></div><div class='item-title-row'><div class='item-text cName'>公司名称："+companyName+"</div></div><div class='item-title-row'><div class='item-text cLocation'>地址："+v.jobAddress+"</div></div></div></li></a>");
		    		   }
		  		 if(v.jobSalaryId==4){
		  			var text=("<a href='job/jobDetail.do?jobId="+v.jobId+"'><li><div class='item-inner'><div class='item-title-row'><div class='item-title'>"+v.jobName+"</div></div></div><div class='item-title-row'><div class='item-text salary'>薪资：3500-5000元/月</div></div><div class='item-title-row'><div class='item-text cName'>公司名称："+companyName+"</div></div><div class='item-title-row'><div class='item-text cLocation'>地址："+v.jobAddress+"</div></div></div></li></a>");
		    		   }
		  		 if(v.jobSalaryId==5){
		  			var text=("<a href='job/jobDetail.do?jobId="+v.jobId+"'><li><div class='item-inner'><div class='item-title-row'><div class='item-title'>"+v.jobName+"</div></div></div><div class='item-title-row'><div class='item-text salary'>薪资：5000-7000元/月</div></div><div class='item-title-row'><div class='item-text cName'>公司名称："+companyName+"</div></div><div class='item-title-row'><div class='item-text cLocation'>地址："+v.jobAddress+"</div></div></div></li></a>");
		    		   }
		  		 if(v.jobSalaryId==6){
		  			var text=("<a href='job/jobDetail.do?jobId="+v.jobId+"'><li><div class='item-inner'><div class='item-title-row'><div class='item-title'>"+v.jobName+"</div></div></div><div class='item-title-row'><div class='item-text salary'>薪资：7000-10000元/月</div></div><div class='item-title-row'><div class='item-text cName'>公司名称："+companyName+"</div></div><div class='item-title-row'><div class='item-text cLocation'>地址："+v.jobAddress+"</div></div></div></li></a>");
		    		   }
		  		 if(v.jobSalaryId==7){
		  		 var text=("<a href='job/jobDetail.do?jobId="+v.jobId+"'><li><div class='item-inner'><div class='item-title-row'><div class='item-title'>"+v.jobName+"</div></div></div><div class='item-title-row'><div class='item-text salary'>薪资：10000-15000元/月</div></div><div class='item-title-row'><div class='item-text cName'>公司名称："+companyName+"</div></div><div class='item-title-row'><div class='item-text cLocation'>地址："+v.jobAddress+"</div></div></div></li></a>");
		    		   }
		  		 if(v.jobSalaryId==8){
		  		var text=("<a href='job/jobDetail.do?jobId="+v.jobId+"'><li><div class='item-inner'><div class='item-title-row'><div class='item-title'>"+v.jobName+"</div></div></div><div class='item-title-row'><div class='item-text salary'>薪资：15000元以上/月</div></div><div class='item-title-row'><div class='item-text cName'>公司名称："+companyName+"</div></div><div class='item-title-row'><div class='item-text cLocation'>地址："+v.jobAddress+"</div></div></div></li></a>");
		    		   }
		  		 if(v.jobSalaryId==0){
		  			var text=("<a href='job/jobDetail.do?jobId="+v.jobId+"'><li><div class='item-inner'><div class='item-title-row'><div class='item-title'>"+v.jobName+"</div></div></div><div class='item-title-row'><div class='item-text salary'>薪资：薪资面议</div></div><div class='item-title-row'><div class='item-text cName'>公司名称："+companyName+"</div></div><div class='item-title-row'><div class='item-text cLocation'>地址："+v.jobAddress+"</div></div></div></li></a>");
		    		   }
		  		 html=html+text;
		  		   });            	
			 
			 return html;
		 });
		
  	  
 },"json");
	
}
 
</script>
</body>	
</html>