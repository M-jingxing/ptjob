<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
<title>个人登录</title>
<base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta charset="utf-8" >
	<meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport">
	<meta name="format-detection" content="telephone=no">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<style>
	body{
		background-color:#f5f5f5;
	}
	li{
		list-style-type:none;
	}
	a:link,a:visited,a:hover,a:active{
		text-decoration:none;
	}
	:-moz-placeholder{
		color:#999; /*mozilla ff 4-18*/
		font-size:15px;
	}
	::-moz-placeholder{
		color:#999; /*mozilla ff 19+*/
		font-size:15px;
	}
	:-ms-input-placeholder{
		color:#999; /*ie 10+*/
		font-size:15px;
	}
	::-webkit-input-placeholder{
		color:#999; /*webkit browsers*/
		font-size:15px;
	}
	input[type=text],select,input[type=password]{
		border:none;
		outline:none;
		background-color:#f5f5f5;
		color:#999;
		appearance: none;
		-moz-appearance:none;
		-webkit-appearance: none;
		padding-left:1px;
		text-align:left;
		font-size:15px;
		height:26px;
		width:100%;
	}
	select::-ms-expand{
		display:none;
	}
	.logo{
		text-align:center;
		padding-top:62px;
		padding-bottom:48px;
	}
	.logo img{
		width:80px;
		height:80px;
	}
	.loginContent{
		margin-left:50px;
		margin-right:50px;
	}
	.userName,.psd{
		border-bottom:1px solid #999;
		padding-left:28px;
		margin-bottom:18px;
	}
	.userName{
		background:url(img/user.png) no-repeat;
		padding-bottom:5px;
	}
	.psd{
		background:url(img/password.png) no-repeat;
		padding-bottom:5px;
	}
	.forgetPsd{
		text-align:right;
		margin-bottom:30px;
	}
	.forgetPsd a{
		border-bottom:1px solid #999;
		color:#999;
		font-size:12px;
		padding-bottom:2px;
	}
	button{
		width:100%;
		padding-top:10px;
		padding-bottom:10px;
		border:none;
	}
	.btn1{
		background-color:#ff9137;
		color:#fff;
		border-radius: 6px;
		outline:none;
		margin-bottom:16px;
	}
	.btn2{
		background-color:#fff;
		color:#ff9137;
		border:1px solid #ff9137;
		border-radius: 6px;
		outline:none;
	}
	.error{
		font-size:12px;
		color:#ff9037;
	}
</style>
</head>
<body>
<div class="logo">
	<img src="img/logo.png">
</div>
<div class="loginContent">
	<form id="frmLogin" autocomplete="off">
		<div class="userName">
			<input type="text" name="loginName" id="loginN" placeholder="请输入手机号码">
		</div>
		<div class="psd">
			<input type="password" name="password" id="pwd" placeholder="请输入密码">
		</div>
		<!-- <div class="forgetPsd">
			<a href="xrjobPages/forgetPsd.jsp">忘记密码？</a>
		</div> -->
		<div>
			<button class="btn1" type="button" onclick="login()">登录</button>
		</div>
	</form>
	<div>
		<a href="xrjobPages/reg.jsp"><button class="btn2">注册</button></a>
	</div>
</div>
<script src="js/jquery-2.2.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script>
    function login() {
		var loginName = $("#loginN").val();
		var password = $("#pwd").val();
		$.post("person/login.do", {
			userMobile : loginName,
			password : password
		}, function(data) {
			if (data.res) {//登录成功
				/* self.location=document.referrer; */
				location="index.jsp";
			} else {
					alert(data.msg);
			}
		}, 'JSON');
	}


	//	密码验证：在6位数以上
	$(function(){
		$("#frmLogin").validate({
			rules:{
				loginName:{
					required:true
				},
				password:{
					required:true,
					minlength:6
				}
			},
			messages:{
				loginName:{
					required:"请输入用户名"
				},
				password:{
					required:"请输入密码",
					minlength:"输入长度在6位以上"
				}
			}
		});
	});
</script>
</body>	
</html>